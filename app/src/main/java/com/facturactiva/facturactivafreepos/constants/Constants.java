package com.facturactiva.facturactivafreepos.constants;

public class Constants {

    public static final int idEmisor = 1;
    public static final String tipoDocEmisor = "6";
    public static final String numDocEmisor = "20123654789";
    public static final int idCuenta = 1;
    public static final String serieBoleta = "B001";
    public static final String serieFactura = "F001";
    public static final String serieNotaCre = "B015";
    public static final String serieNotaDeb = "F024";
    public static final int idPuntoEmision = 2;
    public static final String nombreEmisor = "Chifa You Ta SAC";
    public static final String direccionOrigen = "Calle Los Claveles 388 Miraflores.";
    public static final String codUbigeoOrigen = "150122";
    public static final String correoNotifEmisor = "chifayouta@gmail.com";
    public static final String tipoMoneda = "PEN";

    public static final String numDocEmisorApiPruebas = "20135647895";
    public static final float tasaIgv = (float)0.18;







}
