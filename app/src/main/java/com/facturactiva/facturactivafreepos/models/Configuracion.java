package com.facturactiva.facturactivafreepos.models;

public class Configuracion {

    int idEmisor;
    String codConfiguracion;
    String valor;
    String codServicio;
    int activo;
    String usuarioEdicion;
    String fechaEdicion;


    public Configuracion() {
    }

    public Configuracion(int idEmisor, String codConfiguracion, String valor, String codServicio, int activo, String usuarioEdicion, String fechaEdicion) {
        this.idEmisor = idEmisor;
        this.codConfiguracion = codConfiguracion;
        this.valor = valor;
        this.codServicio = codServicio;
        this.activo = activo;
        this.usuarioEdicion = usuarioEdicion;
        this.fechaEdicion = fechaEdicion;
    }

    public int getIdEmisor() {
        return idEmisor;
    }

    public void setIdEmisor(int idEmisor) {
        this.idEmisor = idEmisor;
    }

    public String getCodConfiguracion() {
        return codConfiguracion;
    }

    public void setCodConfiguracion(String codConfiguracion) {
        this.codConfiguracion = codConfiguracion;
    }

    public String getValor() {
        return valor;
    }

    public void setValor(String valor) {
        this.valor = valor;
    }

    public String getCodServicio() {
        return codServicio;
    }

    public void setCodServicio(String codServicio) {
        this.codServicio = codServicio;
    }

    public int getActivo() {
        return activo;
    }

    public void setActivo(int activo) {
        this.activo = activo;
    }

    public String getUsuarioEdicion() {
        return usuarioEdicion;
    }

    public void setUsuarioEdicion(String usuarioEdicion) {
        this.usuarioEdicion = usuarioEdicion;
    }

    public String getFechaEdicion() {
        return fechaEdicion;
    }

    public void setFechaEdicion(String fechaEdicion) {
        this.fechaEdicion = fechaEdicion;
    }
}
