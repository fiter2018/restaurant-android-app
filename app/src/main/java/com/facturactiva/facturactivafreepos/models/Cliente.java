package com.facturactiva.facturactivafreepos.models;

public class Cliente extends Persona{

    int idCliente;
    String emailContacto;
    int emailConfirmado;
    int activo;
    String usuarioCreacion;
    String fechaCreacion;
    String usuarioEdicion;
    String fechaEdicion;
    int idEmisorCreacion;
    int condicionPago;

    public Cliente() {
    }

    public Cliente(int idPersona, String nombre, String nombres, String apellidoPaterno, String apellidoMaterno, String direccionPrincipal, String direccionSecundaria, String codUbigeoPrincipal, String codUbigeoSecundario, String codPais, String telefono, String fax, String tipoPersona, String tipoDocPrincipal, String numDocPrincipal, String tipoDocSecundario, String numDocSecundario, int activo, String usuarioCreacion, String fechaCreacion, String usuarioEdicion, String fechaEdicion, int idCliente, String emailContacto, int emailConfirmado, int activo1, String usuarioCreacion1, String fechaCreacion1, String usuarioEdicion1, String fechaEdicion1, int idEmisorCreacion, int condicionPago) {
        super(idPersona, nombre, nombres, apellidoPaterno, apellidoMaterno, direccionPrincipal, direccionSecundaria, codUbigeoPrincipal, codUbigeoSecundario, codPais, telefono, fax, tipoPersona, tipoDocPrincipal, numDocPrincipal, tipoDocSecundario, numDocSecundario, activo, usuarioCreacion, fechaCreacion, usuarioEdicion, fechaEdicion);
        this.idCliente = idCliente;
        this.emailContacto = emailContacto;
        this.emailConfirmado = emailConfirmado;
        this.activo = activo1;
        this.usuarioCreacion = usuarioCreacion1;
        this.fechaCreacion = fechaCreacion1;
        this.usuarioEdicion = usuarioEdicion1;
        this.fechaEdicion = fechaEdicion1;
        this.idEmisorCreacion = idEmisorCreacion;
        this.condicionPago = condicionPago;
    }

    public int getIdCliente() {
        return idCliente;
    }

    public void setIdCliente(int idCliente) {
        this.idCliente = idCliente;
    }

    public String getEmailContacto() {
        return emailContacto;
    }

    public void setEmailContacto(String emailContacto) {
        this.emailContacto = emailContacto;
    }

    public int getEmailConfirmado() {
        return emailConfirmado;
    }

    public void setEmailConfirmado(int emailConfirmado) {
        this.emailConfirmado = emailConfirmado;
    }

    public int getActivo() {
        return activo;
    }

    public void setActivo(int activo) {
        this.activo = activo;
    }

    public String getUsuarioCreacion() {
        return usuarioCreacion;
    }

    public void setUsuarioCreacion(String usuarioCreacion) {
        this.usuarioCreacion = usuarioCreacion;
    }

    public String getFechaCreacion() {
        return fechaCreacion;
    }

    public void setFechaCreacion(String fechaCreacion) {
        this.fechaCreacion = fechaCreacion;
    }

    public String getUsuarioEdicion() {
        return usuarioEdicion;
    }

    public void setUsuarioEdicion(String usuarioEdicion) {
        this.usuarioEdicion = usuarioEdicion;
    }

    public String getFechaEdicion() {
        return fechaEdicion;
    }

    public void setFechaEdicion(String fechaEdicion) {
        this.fechaEdicion = fechaEdicion;
    }

    public int getIdEmisorCreacion() {
        return idEmisorCreacion;
    }

    public void setIdEmisorCreacion(int idEmisorCreacion) {
        this.idEmisorCreacion = idEmisorCreacion;
    }

    public int getCondicionPago() {
        return condicionPago;
    }

    public void setCondicionPago(int condicionPago) {
        this.condicionPago = condicionPago;
    }
}
