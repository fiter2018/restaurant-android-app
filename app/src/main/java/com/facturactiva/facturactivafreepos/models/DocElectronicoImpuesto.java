package com.facturactiva.facturactivafreepos.models;

public class DocElectronicoImpuesto {

    int idEmisor;
    String serie;
    int correlativo;
    String tipoDocumento;
    String tipoDocEmisor;
    String numDocEmisor;
    String codImpuesto;
    float montoImpuesto;
    float tasaImpuesto;


    public DocElectronicoImpuesto() {
    }

    public DocElectronicoImpuesto(int idEmisor, String serie, int correlativo, String tipoDocumento, String tipoDocEmisor, String numDocEmisor, String codImpuesto, float montoImpuesto, float tasaImpuesto) {
        this.idEmisor = idEmisor;
        this.serie = serie;
        this.correlativo = correlativo;
        this.tipoDocumento = tipoDocumento;
        this.tipoDocEmisor = tipoDocEmisor;
        this.numDocEmisor = numDocEmisor;
        this.codImpuesto = codImpuesto;
        this.montoImpuesto = montoImpuesto;
        this.tasaImpuesto = tasaImpuesto;
    }

    public int getIdEmisor() {
        return idEmisor;
    }

    public void setIdEmisor(int idEmisor) {
        this.idEmisor = idEmisor;
    }

    public String getSerie() {
        return serie;
    }

    public void setSerie(String serie) {
        this.serie = serie;
    }

    public int getCorrelativo() {
        return correlativo;
    }

    public void setCorrelativo(int correlativo) {
        this.correlativo = correlativo;
    }

    public String getTipoDocumento() {
        return tipoDocumento;
    }

    public void setTipoDocumento(String tipoDocumento) {
        this.tipoDocumento = tipoDocumento;
    }

    public String getTipoDocEmisor() {
        return tipoDocEmisor;
    }

    public void setTipoDocEmisor(String tipoDocEmisor) {
        this.tipoDocEmisor = tipoDocEmisor;
    }

    public String getNumDocEmisor() {
        return numDocEmisor;
    }

    public void setNumDocEmisor(String numDocEmisor) {
        this.numDocEmisor = numDocEmisor;
    }

    public String getCodImpuesto() {
        return codImpuesto;
    }

    public void setCodImpuesto(String codImpuesto) {
        this.codImpuesto = codImpuesto;
    }

    public float getMontoImpuesto() {
        return montoImpuesto;
    }

    public void setMontoImpuesto(float montoImpuesto) {
        this.montoImpuesto = montoImpuesto;
    }

    public float getTasaImpuesto() {
        return tasaImpuesto;
    }

    public void setTasaImpuesto(float tasaImpuesto) {
        this.tasaImpuesto = tasaImpuesto;
    }
}
