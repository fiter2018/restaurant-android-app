package com.facturactiva.facturactivafreepos.models;

public class Detail {

    float cantidadItem;
    String unidadMedidaItem;
    String codItem;
    String nombreItem;
    float precioItem;
    float precioItemSinIgv;
    float montoItem;
    float descuentoMonto;
    String codAfectacionIgv;
    float tasaIgv;
    float montoIgv;
    String codSistemaCalculoIsc;
    float montoIsc;
    float tasaIsc;
    float precioItemReferencia;
    String idOperacion;

    public Detail() {
    }

    public Detail(float cantidadItem, String unidadMedidaItem, String codItem, String nombreItem, float precioItem, float precioItemSinIgv, float montoItem, float descuentoMonto, String codAfectacionIgv, float tasaIgv, float montoIgv, String codSistemaCalculoIsc, float montoIsc, float tasaIsc, float precioItemReferencia, String idOperacion) {
        this.cantidadItem = cantidadItem;
        this.unidadMedidaItem = unidadMedidaItem;
        this.codItem = codItem;
        this.nombreItem = nombreItem;
        this.precioItem = precioItem;
        this.precioItemSinIgv = precioItemSinIgv;
        this.montoItem = montoItem;
        this.descuentoMonto = descuentoMonto;
        this.codAfectacionIgv = codAfectacionIgv;
        this.tasaIgv = tasaIgv;
        this.montoIgv = montoIgv;
        this.codSistemaCalculoIsc = codSistemaCalculoIsc;
        this.montoIsc = montoIsc;
        this.tasaIsc = tasaIsc;
        this.precioItemReferencia = precioItemReferencia;
        this.idOperacion = idOperacion;
    }

    public float getCantidadItem() {
        return cantidadItem;
    }

    public void setCantidadItem(float cantidadItem) {
        this.cantidadItem = cantidadItem;
    }

    public String getUnidadMedidaItem() {
        return unidadMedidaItem;
    }

    public void setUnidadMedidaItem(String unidadMedidaItem) {
        this.unidadMedidaItem = unidadMedidaItem;
    }

    public String getCodItem() {
        return codItem;
    }

    public void setCodItem(String codItem) {
        this.codItem = codItem;
    }

    public String getNombreItem() {
        return nombreItem;
    }

    public void setNombreItem(String nombreItem) {
        this.nombreItem = nombreItem;
    }

    public float getPrecioItem() {
        return precioItem;
    }

    public void setPrecioItem(float precioItem) {
        this.precioItem = precioItem;
    }

    public float getPrecioItemSinIgv() {
        return precioItemSinIgv;
    }

    public void setPrecioItemSinIgv(float precioItemSinIgv) {
        this.precioItemSinIgv = precioItemSinIgv;
    }

    public float getMontoItem() {
        return montoItem;
    }

    public void setMontoItem(float montoItem) {
        this.montoItem = montoItem;
    }

    public float getDescuentoMonto() {
        return descuentoMonto;
    }

    public void setDescuentoMonto(float descuentoMonto) {
        this.descuentoMonto = descuentoMonto;
    }

    public String getCodAfectacionIgv() {
        return codAfectacionIgv;
    }

    public void setCodAfectacionIgv(String codAfectacionIgv) {
        this.codAfectacionIgv = codAfectacionIgv;
    }

    public float getTasaIgv() {
        return tasaIgv;
    }

    public void setTasaIgv(float tasaIgv) {
        this.tasaIgv = tasaIgv;
    }

    public float getMontoIgv() {
        return montoIgv;
    }

    public void setMontoIgv(float montoIgv) {
        this.montoIgv = montoIgv;
    }

    public String getCodSistemaCalculoIsc() {
        return codSistemaCalculoIsc;
    }

    public void setCodSistemaCalculoIsc(String codSistemaCalculoIsc) {
        this.codSistemaCalculoIsc = codSistemaCalculoIsc;
    }

    public float getMontoIsc() {
        return montoIsc;
    }

    public void setMontoIsc(float montoIsc) {
        this.montoIsc = montoIsc;
    }

    public float getTasaIsc() {
        return tasaIsc;
    }

    public void setTasaIsc(float tasaIsc) {
        this.tasaIsc = tasaIsc;
    }

    public float getPrecioItemReferencia() {
        return precioItemReferencia;
    }

    public void setPrecioItemReferencia(float precioItemReferencia) {
        this.precioItemReferencia = precioItemReferencia;
    }

    public String getIdOperacion() {
        return idOperacion;
    }

    public void setIdOperacion(String idOperacion) {
        this.idOperacion = idOperacion;
    }




}
