package com.facturactiva.facturactivafreepos.models;

public class GeneralCuenta {

    int idCuenta;
    String nombreContacto;
    String emailContacto;
    String tipoCuenta;
    int activo;
    String usuarioCreacion;
    String fechaCreacion;
    String usuarioEdicion;
    String fechaEdicion;

    public GeneralCuenta() {

    }

    public GeneralCuenta(int idCuenta, String nombreContacto, String emailContacto, String tipoCuenta, int activo, String usuarioCreacion, String fechaCreacion, String usuarioEdicion, String fechaEdicion) {
        this.idCuenta = idCuenta;
        this.nombreContacto = nombreContacto;
        this.emailContacto = emailContacto;
        this.tipoCuenta = tipoCuenta;
        this.activo = activo;
        this.usuarioCreacion = usuarioCreacion;
        this.fechaCreacion = fechaCreacion;
        this.usuarioEdicion = usuarioEdicion;
        this.fechaEdicion = fechaEdicion;
    }


    public int getIdCuenta() {
        return idCuenta;
    }

    public void setIdCuenta(int idCuenta) {
        this.idCuenta = idCuenta;
    }

    public String getNombreContacto() {
        return nombreContacto;
    }

    public void setNombreContacto(String nombreContacto) {
        this.nombreContacto = nombreContacto;
    }

    public String getEmailContacto() {
        return emailContacto;
    }

    public void setEmailContacto(String emailContacto) {
        this.emailContacto = emailContacto;
    }

    public String getTipoCuenta() {
        return tipoCuenta;
    }

    public void setTipoCuenta(String tipoCuenta) {
        this.tipoCuenta = tipoCuenta;
    }

    public int getActivo() {
        return activo;
    }

    public void setActivo(int activo) {
        this.activo = activo;
    }

    public String getUsuarioCreacion() {
        return usuarioCreacion;
    }

    public void setUsuarioCreacion(String usuarioCreacion) {
        this.usuarioCreacion = usuarioCreacion;
    }

    public String getFechaCreacion() {
        return fechaCreacion;
    }

    public void setFechaCreacion(String fechaCreacion) {
        this.fechaCreacion = fechaCreacion;
    }

    public String getUsuarioEdicion() {
        return usuarioEdicion;
    }

    public void setUsuarioEdicion(String usuarioEdicion) {
        this.usuarioEdicion = usuarioEdicion;
    }

    public String getFechaEdicion() {
        return fechaEdicion;
    }

    public void setFechaEdicion(String fechaEdicion) {
        this.fechaEdicion = fechaEdicion;
    }
}
