package com.facturactiva.facturactivafreepos.models;

public class SerieCorrelativo {

    int idEmisor;
    String tipoDocEmisor;
    String numDocEmisor;
    int idPuntoEmision;
    String tipoDocumento;
    String serie;
    int correlativo;


    public SerieCorrelativo() {
    }

    public SerieCorrelativo(int idEmisor, String tipoDocEmisor, String numDocEmisor, int idPuntoEmision, String tipoDocumento, String serie, int correlativo) {
        this.idEmisor = idEmisor;
        this.tipoDocEmisor = tipoDocEmisor;
        this.numDocEmisor = numDocEmisor;
        this.idPuntoEmision = idPuntoEmision;
        this.tipoDocumento = tipoDocumento;
        this.serie = serie;
        this.correlativo = correlativo;
    }

    public int getIdEmisor() {
        return idEmisor;
    }

    public void setIdEmisor(int idEmisor) {
        this.idEmisor = idEmisor;
    }

    public String getTipoDocEmisor() {
        return tipoDocEmisor;
    }

    public void setTipoDocEmisor(String tipoDocEmisor) {
        this.tipoDocEmisor = tipoDocEmisor;
    }

    public String getNumDocEmisor() {
        return numDocEmisor;
    }

    public void setNumDocEmisor(String numDocEmisor) {
        this.numDocEmisor = numDocEmisor;
    }

    public int getIdPuntoEmision() {
        return idPuntoEmision;
    }

    public void setIdPuntoEmision(int idPuntoEmision) {
        this.idPuntoEmision = idPuntoEmision;
    }

    public String getTipoDocumento() {
        return tipoDocumento;
    }

    public void setTipoDocumento(String tipoDocumento) {
        this.tipoDocumento = tipoDocumento;
    }

    public String getSerie() {
        return serie;
    }

    public void setSerie(String serie) {
        this.serie = serie;
    }

    public int getCorrelativo() {
        return correlativo;
    }

    public void setCorrelativo(int correlativo) {
        this.correlativo = correlativo;
    }
}
