package com.facturactiva.facturactivafreepos.models;

public class Emisor {

    int idEmisor;
    String tipoDocEmisor;
    String numDocEmisor;
    String nombreEmisor;
    String correoNotif;
    int activo;
    String usuarioCreacion;
    String fechaCreacion;
    String usuarioEdicion;
    String fechaEdicion;

    public Emisor() {

    }

    public Emisor(int idEmisor, String tipoDocEmisor, String numDocEmisor, String nombreEmisor, String correoNotif, int activo, String usuarioCreacion, String fechaCreacion, String usuarioEdicion, String fechaEdicion) {
        this.idEmisor = idEmisor;
        this.tipoDocEmisor = tipoDocEmisor;
        this.numDocEmisor = numDocEmisor;
        this.nombreEmisor = nombreEmisor;
        this.correoNotif = correoNotif;
        this.activo = activo;
        this.usuarioCreacion = usuarioCreacion;
        this.fechaCreacion = fechaCreacion;
        this.usuarioEdicion = usuarioEdicion;
        this.fechaEdicion = fechaEdicion;
    }


    public int getIdEmisor() {
        return idEmisor;
    }

    public void setIdEmisor(int idEmisor) {
        this.idEmisor = idEmisor;
    }

    public String getTipoDocEmisor() {
        return tipoDocEmisor;
    }

    public void setTipoDocEmisor(String tipoDocEmisor) {
        this.tipoDocEmisor = tipoDocEmisor;
    }

    public String getNumDocEmisor() {
        return numDocEmisor;
    }

    public void setNumDocEmisor(String numDocEmisor) {
        this.numDocEmisor = numDocEmisor;
    }

    public String getNombreEmisor() {
        return nombreEmisor;
    }

    public void setNombreEmisor(String nombreEmisor) {
        this.nombreEmisor = nombreEmisor;
    }

    public String getCorreoNotif() {
        return correoNotif;
    }

    public void setCorreoNotif(String correoNotif) {
        this.correoNotif = correoNotif;
    }

    public int getActivo() {
        return activo;
    }

    public void setActivo(int activo) {
        this.activo = activo;
    }

    public String getUsuarioCreacion() {
        return usuarioCreacion;
    }

    public void setUsuarioCreacion(String usuarioCreacion) {
        this.usuarioCreacion = usuarioCreacion;
    }

    public String getFechaCreacion() {
        return fechaCreacion;
    }

    public void setFechaCreacion(String fechaCreacion) {
        this.fechaCreacion = fechaCreacion;
    }

    public String getUsuarioEdicion() {
        return usuarioEdicion;
    }

    public void setUsuarioEdicion(String usuarioEdicion) {
        this.usuarioEdicion = usuarioEdicion;
    }

    public String getFechaEdicion() {
        return fechaEdicion;
    }

    public void setFechaEdicion(String fechaEdicion) {
        this.fechaEdicion = fechaEdicion;
    }





}
