package com.facturactiva.facturactivafreepos.models;

import java.util.ArrayList;

public class Comprobante {

    String tipoDocumento;
    String fechaEmision;
    String idTransaccion;
    String correoReceptor;
    Document documento;
    ArrayList<Tax> impuesto;
    ArrayList<Detail> detalle;
    Discount descuento;
    AdditionalData datosAdicionales;


    public Comprobante() {
    }

    public Comprobante(String tipoDocumento, String fechaEmision, String idTransaccion, String correoReceptor, Document documento, ArrayList<Tax> impuesto, ArrayList<Detail> detalle, Discount descuento, AdditionalData datosAdicionales) {
        this.tipoDocumento = tipoDocumento;
        this.fechaEmision = fechaEmision;
        this.idTransaccion = idTransaccion;
        this.correoReceptor = correoReceptor;
        this.documento = documento;
        this.impuesto = impuesto;
        this.detalle = detalle;
        this.descuento = descuento;
        this.datosAdicionales = datosAdicionales;
    }

    public String getTipoDocumento() {
        return tipoDocumento;
    }

    public void setTipoDocumento(String tipoDocumento) {
        this.tipoDocumento = tipoDocumento;
    }

    public String getFechaEmision() {
        return fechaEmision;
    }

    public void setFechaEmision(String fechaEmision) {
        this.fechaEmision = fechaEmision;
    }

    public String getIdTransaccion() {
        return idTransaccion;
    }

    public void setIdTransaccion(String idTransaccion) {
        this.idTransaccion = idTransaccion;
    }

    public String getCorreoReceptor() {
        return correoReceptor;
    }

    public void setCorreoReceptor(String correoReceptor) {
        this.correoReceptor = correoReceptor;
    }

    public Document getDocumento() {
        return documento;
    }

    public void setDocumento(Document documento) {
        this.documento = documento;
    }

    public ArrayList<Tax> getImpuesto() {
        return impuesto;
    }

    public void setImpuesto(ArrayList<Tax> impuesto) {
        this.impuesto = impuesto;
    }

    public ArrayList<Detail> getDetalle() {
        return detalle;
    }

    public void setDetalle(ArrayList<Detail> detalle) {
        this.detalle = detalle;
    }

    public Discount getDescuento() {
        return descuento;
    }

    public void setDescuento(Discount descuento) {
        this.descuento = descuento;
    }

    public AdditionalData getDatosAdicionales() {
        return datosAdicionales;
    }

    public void setDatosAdicionales(AdditionalData datosAdicionales) {
        this.datosAdicionales = datosAdicionales;
    }
}
