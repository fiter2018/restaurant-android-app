package com.facturactiva.facturactivafreepos.models;

public class Persona {

    int idPersona;
    String nombre;
    String nombres;
    String apellidoPaterno;
    String apellidoMaterno;
    String direccionPrincipal;
    String direccionSecundaria;
    String codUbigeoPrincipal;
    String codUbigeoSecundario;
    String codPais;
    String telefono;
    String fax;
    String tipoPersona;
    String tipoDocPrincipal;
    String numDocPrincipal;
    String tipoDocSecundario;
    String numDocSecundario;
    int activo;
    String usuarioCreacion;
    String fechaCreacion;
    String usuarioEdicion;
    String fechaEdicion;


    public Persona() {
    }

    public Persona(int idPersona, String nombre, String nombres, String apellidoPaterno, String apellidoMaterno, String direccionPrincipal, String direccionSecundaria, String codUbigeoPrincipal, String codUbigeoSecundario, String codPais, String telefono, String fax, String tipoPersona, String tipoDocPrincipal, String numDocPrincipal, String tipoDocSecundario, String numDocSecundario, int activo, String usuarioCreacion, String fechaCreacion, String usuarioEdicion, String fechaEdicion) {
        this.idPersona = idPersona;
        this.nombre = nombre;
        this.nombres = nombres;
        this.apellidoPaterno = apellidoPaterno;
        this.apellidoMaterno = apellidoMaterno;
        this.direccionPrincipal = direccionPrincipal;
        this.direccionSecundaria = direccionSecundaria;
        this.codUbigeoPrincipal = codUbigeoPrincipal;
        this.codUbigeoSecundario = codUbigeoSecundario;
        this.codPais = codPais;
        this.telefono = telefono;
        this.fax = fax;
        this.tipoPersona = tipoPersona;
        this.tipoDocPrincipal = tipoDocPrincipal;
        this.numDocPrincipal = numDocPrincipal;
        this.tipoDocSecundario = tipoDocSecundario;
        this.numDocSecundario = numDocSecundario;
        this.activo = activo;
        this.usuarioCreacion = usuarioCreacion;
        this.fechaCreacion = fechaCreacion;
        this.usuarioEdicion = usuarioEdicion;
        this.fechaEdicion = fechaEdicion;
    }

    public int getIdPersona() {
        return idPersona;
    }

    public void setIdPersona(int idPersona) {
        this.idPersona = idPersona;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getNombres() {
        return nombres;
    }

    public void setNombres(String nombres) {
        this.nombres = nombres;
    }

    public String getApellidoPaterno() {
        return apellidoPaterno;
    }

    public void setApellidoPaterno(String apellidoPaterno) {
        this.apellidoPaterno = apellidoPaterno;
    }

    public String getApellidoMaterno() {
        return apellidoMaterno;
    }

    public void setApellidoMaterno(String apellidoMaterno) {
        this.apellidoMaterno = apellidoMaterno;
    }

    public String getDireccionPrincipal() {
        return direccionPrincipal;
    }

    public void setDireccionPrincipal(String direccionPrincipal) {
        this.direccionPrincipal = direccionPrincipal;
    }

    public String getDireccionSecundaria() {
        return direccionSecundaria;
    }

    public void setDireccionSecundaria(String direccionSecundaria) {
        this.direccionSecundaria = direccionSecundaria;
    }

    public String getCodUbigeoPrincipal() {
        return codUbigeoPrincipal;
    }

    public void setCodUbigeoPrincipal(String codUbigeoPrincipal) {
        this.codUbigeoPrincipal = codUbigeoPrincipal;
    }

    public String getCodUbigeoSecundario() {
        return codUbigeoSecundario;
    }

    public void setCodUbigeoSecundario(String codUbigeoSecundario) {
        this.codUbigeoSecundario = codUbigeoSecundario;
    }

    public String getCodPais() {
        return codPais;
    }

    public void setCodPais(String codPais) {
        this.codPais = codPais;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public String getFax() {
        return fax;
    }

    public void setFax(String fax) {
        this.fax = fax;
    }

    public String getTipoPersona() {
        return tipoPersona;
    }

    public void setTipoPersona(String tipoPersona) {
        this.tipoPersona = tipoPersona;
    }

    public String getTipoDocPrincipal() {
        return tipoDocPrincipal;
    }

    public void setTipoDocPrincipal(String tipoDocPrincipal) {
        this.tipoDocPrincipal = tipoDocPrincipal;
    }

    public String getNumDocPrincipal() {
        return numDocPrincipal;
    }

    public void setNumDocPrincipal(String numDocPrincipal) {
        this.numDocPrincipal = numDocPrincipal;
    }

    public String getTipoDocSecundario() {
        return tipoDocSecundario;
    }

    public void setTipoDocSecundario(String tipoDocSecundario) {
        this.tipoDocSecundario = tipoDocSecundario;
    }

    public String getNumDocSecundario() {
        return numDocSecundario;
    }

    public void setNumDocSecundario(String numDocSecundario) {
        this.numDocSecundario = numDocSecundario;
    }

    public int getActivo() {
        return activo;
    }

    public void setActivo(int activo) {
        this.activo = activo;
    }

    public String getUsuarioCreacion() {
        return usuarioCreacion;
    }

    public void setUsuarioCreacion(String usuarioCreacion) {
        this.usuarioCreacion = usuarioCreacion;
    }

    public String getFechaCreacion() {
        return fechaCreacion;
    }

    public void setFechaCreacion(String fechaCreacion) {
        this.fechaCreacion = fechaCreacion;
    }

    public String getUsuarioEdicion() {
        return usuarioEdicion;
    }

    public void setUsuarioEdicion(String usuarioEdicion) {
        this.usuarioEdicion = usuarioEdicion;
    }

    public String getFechaEdicion() {
        return fechaEdicion;
    }

    public void setFechaEdicion(String fechaEdicion) {
        this.fechaEdicion = fechaEdicion;
    }
}
