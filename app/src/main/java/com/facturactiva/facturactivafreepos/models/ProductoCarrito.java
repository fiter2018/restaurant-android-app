package com.facturactiva.facturactivafreepos.models;


public class ProductoCarrito {

    int idEmisor;
    String codProducto;
    String codCategoria;
    int nroPlatoCarta;
    String descripcion;
    String tipoMoneda;
    float valorUnitario;
    int activo;
    String imagePath;
    int cantidad;

    public ProductoCarrito() {
    }

    public ProductoCarrito(int idEmisor, String codProducto, String codCategoria, int nroPlatoCarta, String descripcion, String tipoMoneda, float valorUnitario, int activo, String imagePath, int cantidad) {
        this.idEmisor = idEmisor;
        this.codProducto = codProducto;
        this.codCategoria = codCategoria;
        this.nroPlatoCarta = nroPlatoCarta;
        this.descripcion = descripcion;
        this.tipoMoneda = tipoMoneda;
        this.valorUnitario = valorUnitario;
        this.activo = activo;
        this.imagePath = imagePath;
        this.cantidad = cantidad;
    }


    public int getIdEmisor() {
        return idEmisor;
    }

    public void setIdEmisor(int idEmisor) {
        this.idEmisor = idEmisor;
    }

    public String getCodProducto() {
        return codProducto;
    }

    public void setCodProducto(String codProducto) {
        this.codProducto = codProducto;
    }

    public String getCodCategoria() {
        return codCategoria;
    }

    public void setCodCategoria(String codCategoria) {
        this.codCategoria = codCategoria;
    }

    public int getNroPlatoCarta() {
        return nroPlatoCarta;
    }

    public void setNroPlatoCarta(int nroPlatoCarta) {
        this.nroPlatoCarta = nroPlatoCarta;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getTipoMoneda() {
        return tipoMoneda;
    }

    public void setTipoMoneda(String tipoMoneda) {
        this.tipoMoneda = tipoMoneda;
    }

    public float getValorUnitario() {
        return valorUnitario;
    }

    public void setValorUnitario(float valorUnitario) {
        this.valorUnitario = valorUnitario;
    }

    public int getActivo() {
        return activo;
    }

    public void setActivo(int activo) {
        this.activo = activo;
    }

    public String getImagePath() {
        return imagePath;
    }

    public void setImagePath(String imagePath) {
        this.imagePath = imagePath;
    }

    public int getCantidad() {
        return cantidad;
    }

    public void setCantidad(int cantidad) {
        this.cantidad = cantidad;
    }
}
