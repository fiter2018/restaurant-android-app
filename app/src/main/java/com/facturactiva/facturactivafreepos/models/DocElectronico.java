package com.facturactiva.facturactivafreepos.models;

public class DocElectronico {

    int idEmisor;
    String serie;
    int correlativo;
    String tipoDocumento;
    String tipoDocEmisor;
    String numDocEmisor;
    int idPuntoEmision;
    int idUsuario;
    String idElectronico;
    String rucEmisorPrincipal;
    String nombreEmisor;
    String nombreComercialEmisor;
    String direccionOrigen;
    String direccionUbigeo;
    String correoNotificacionEmisor;
    String tipoDocReceptor;
    String numDocReceptor;
    String nombreReceptor;
    String nombreComercialReceptor;
    String direccionDestino;
    String correoNotificacionReceptor;
    String tipoDocReceptorAsociado;
    String numDocReceptorAsociado;
    String nombreReceptorAsociado;
    String tipoMoneda;
    String tipoMonedaDestino;
    float tipoCambioDestino;
    String tipoOperacion;
    String sustento;
    String tipoMotivoNotaModificatoria;
    float mntNeto;
    float mntExe;
    float mntExo;
    float mntDescuentoGlobal;
    float mntTotalIgv;
    float mntTotalIsc;
    float mntTotal;
    float mntTotalDescuentos;
    float mntTotalGrat;
    float mntTotalOtros;
    float mntTotalOtrosCargos;
    float mntTotalAnticipos;
    String fechaEmision;
    String fechaVencimiento;
    String glosaDocumento;
    int contieneAdjunto;
    String tipoFormatoRepresentacionImpresa;
    String tipoModalidadEmision;
    String codContrato;
    String codAgencia;
    String codUbicacion;
    String codError;
    String idEnvio;
    String codEnvio;
    int idCuenta;
    String idTransaccion;
    String estadoDocumento;
    String estadoEmision;
    int reenvioHabilitado;
    String tipoJustificacionReenvio;
    String usuarioCreacion;
    String fechaCreacion;

    public DocElectronico() {

    }


    public DocElectronico(int idEmisor, String serie, int correlativo, String tipoDocumento, String tipoDocEmisor, String numDocEmisor, int idPuntoEmision, int idUsuario, String idElectronico, String rucEmisorPrincipal, String nombreEmisor, String nombreComercialEmisor, String direccionOrigen, String direccionUbigeo, String correoNotificacionEmisor, String tipoDocReceptor, String numDocReceptor, String nombreReceptor, String nombreComercialReceptor, String direccionDestino, String correoNotificacionReceptor, String tipoDocReceptorAsociado, String numDocReceptorAsociado, String nombreReceptorAsociado, String tipoMoneda, String tipoMonedaDestino, float tipoCambioDestino, String tipoOperacion, String sustento, String tipoMotivoNotaModificatoria, float mntNeto, float mntExe, float mntExo, float mntDescuentoGlobal, float mntTotalIgv, float mntTotalIsc, float mntTotal, float mntTotalDescuentos, float mntTotalGrat, float mntTotalOtros, float mntTotalOtrosCargos, float mntTotalAnticipos, String fechaEmision, String fechaVencimiento, String glosaDocumento, int contieneAdjunto, String tipoFormatoRepresentacionImpresa, String tipoModalidadEmision, String codContrato, String codAgencia, String codUbicacion, String codError, String idEnvio, String codEnvio, int idCuenta, String idTransaccion, String estadoDocumento, String estadoEmision, int reenvioHabilitado, String tipoJustificacionReenvio, String usuarioCreacion, String fechaCreacion) {
        this.idEmisor = idEmisor;
        this.serie = serie;
        this.correlativo = correlativo;
        this.tipoDocumento = tipoDocumento;
        this.tipoDocEmisor = tipoDocEmisor;
        this.numDocEmisor = numDocEmisor;
        this.idPuntoEmision = idPuntoEmision;
        this.idUsuario = idUsuario;
        this.idElectronico = idElectronico;
        this.rucEmisorPrincipal = rucEmisorPrincipal;
        this.nombreEmisor = nombreEmisor;
        this.nombreComercialEmisor = nombreComercialEmisor;
        this.direccionOrigen = direccionOrigen;
        this.direccionUbigeo = direccionUbigeo;
        this.correoNotificacionEmisor = correoNotificacionEmisor;
        this.tipoDocReceptor = tipoDocReceptor;
        this.numDocReceptor = numDocReceptor;
        this.nombreReceptor = nombreReceptor;
        this.nombreComercialReceptor = nombreComercialReceptor;
        this.direccionDestino = direccionDestino;
        this.correoNotificacionReceptor = correoNotificacionReceptor;
        this.tipoDocReceptorAsociado = tipoDocReceptorAsociado;
        this.numDocReceptorAsociado = numDocReceptorAsociado;
        this.nombreReceptorAsociado = nombreReceptorAsociado;
        this.tipoMoneda = tipoMoneda;
        this.tipoMonedaDestino = tipoMonedaDestino;
        this.tipoCambioDestino = tipoCambioDestino;
        this.tipoOperacion = tipoOperacion;
        this.sustento = sustento;
        this.tipoMotivoNotaModificatoria = tipoMotivoNotaModificatoria;
        this.mntNeto = mntNeto;
        this.mntExe = mntExe;
        this.mntExo = mntExo;
        this.mntDescuentoGlobal = mntDescuentoGlobal;
        this.mntTotalIgv = mntTotalIgv;
        this.mntTotalIsc = mntTotalIsc;
        this.mntTotal = mntTotal;
        this.mntTotalDescuentos = mntTotalDescuentos;
        this.mntTotalGrat = mntTotalGrat;
        this.mntTotalOtros = mntTotalOtros;
        this.mntTotalOtrosCargos = mntTotalOtrosCargos;
        this.mntTotalAnticipos = mntTotalAnticipos;
        this.fechaEmision = fechaEmision;
        this.fechaVencimiento = fechaVencimiento;
        this.glosaDocumento = glosaDocumento;
        this.contieneAdjunto = contieneAdjunto;
        this.tipoFormatoRepresentacionImpresa = tipoFormatoRepresentacionImpresa;
        this.tipoModalidadEmision = tipoModalidadEmision;
        this.codContrato = codContrato;
        this.codAgencia = codAgencia;
        this.codUbicacion = codUbicacion;
        this.codError = codError;
        this.idEnvio = idEnvio;
        this.codEnvio = codEnvio;
        this.idCuenta = idCuenta;
        this.idTransaccion = idTransaccion;
        this.estadoDocumento = estadoDocumento;
        this.estadoEmision = estadoEmision;
        this.reenvioHabilitado = reenvioHabilitado;
        this.tipoJustificacionReenvio = tipoJustificacionReenvio;
        this.usuarioCreacion = usuarioCreacion;
        this.fechaCreacion = fechaCreacion;
    }

    public int getIdEmisor() {
        return idEmisor;
    }

    public void setIdEmisor(int idEmisor) {
        this.idEmisor = idEmisor;
    }

    public String getSerie() {
        return serie;
    }

    public void setSerie(String serie) {
        this.serie = serie;
    }

    public int getCorrelativo() {
        return correlativo;
    }

    public void setCorrelativo(int correlativo) {
        this.correlativo = correlativo;
    }

    public String getTipoDocumento() {
        return tipoDocumento;
    }

    public void setTipoDocumento(String tipoDocumento) {
        this.tipoDocumento = tipoDocumento;
    }

    public String getTipoDocEmisor() {
        return tipoDocEmisor;
    }

    public void setTipoDocEmisor(String tipoDocEmisor) {
        this.tipoDocEmisor = tipoDocEmisor;
    }

    public String getNumDocEmisor() {
        return numDocEmisor;
    }

    public void setNumDocEmisor(String numDocEmisor) {
        this.numDocEmisor = numDocEmisor;
    }

    public int getIdPuntoEmision() {
        return idPuntoEmision;
    }

    public void setIdPuntoEmision(int idPuntoEmision) {
        this.idPuntoEmision = idPuntoEmision;
    }

    public int getIdUsuario() {
        return idUsuario;
    }

    public void setIdUsuario(int idUsuario) {
        this.idUsuario = idUsuario;
    }

    public String getIdElectronico() {
        return idElectronico;
    }

    public void setIdElectronico(String idElectronico) {
        this.idElectronico = idElectronico;
    }

    public String getRucEmisorPrincipal() {
        return rucEmisorPrincipal;
    }

    public void setRucEmisorPrincipal(String rucEmisorPrincipal) {
        this.rucEmisorPrincipal = rucEmisorPrincipal;
    }

    public String getNombreEmisor() {
        return nombreEmisor;
    }

    public void setNombreEmisor(String nombreEmisor) {
        this.nombreEmisor = nombreEmisor;
    }

    public String getNombreComercialEmisor() {
        return nombreComercialEmisor;
    }

    public void setNombreComercialEmisor(String nombreComercialEmisor) {
        this.nombreComercialEmisor = nombreComercialEmisor;
    }

    public String getDireccionOrigen() {
        return direccionOrigen;
    }

    public void setDireccionOrigen(String direccionOrigen) {
        this.direccionOrigen = direccionOrigen;
    }

    public String getDireccionUbigeo() {
        return direccionUbigeo;
    }

    public void setDireccionUbigeo(String direccionUbigeo) {
        this.direccionUbigeo = direccionUbigeo;
    }

    public String getCorreoNotificacionEmisor() {
        return correoNotificacionEmisor;
    }

    public void setCorreoNotificacionEmisor(String correoNotificacionEmisor) {
        this.correoNotificacionEmisor = correoNotificacionEmisor;
    }

    public String getTipoDocReceptor() {
        return tipoDocReceptor;
    }

    public void setTipoDocReceptor(String tipoDocReceptor) {
        this.tipoDocReceptor = tipoDocReceptor;
    }

    public String getNumDocReceptor() {
        return numDocReceptor;
    }

    public void setNumDocReceptor(String numDocReceptor) {
        this.numDocReceptor = numDocReceptor;
    }

    public String getNombreReceptor() {
        return nombreReceptor;
    }

    public void setNombreReceptor(String nombreReceptor) {
        this.nombreReceptor = nombreReceptor;
    }

    public String getNombreComercialReceptor() {
        return nombreComercialReceptor;
    }

    public void setNombreComercialReceptor(String nombreComercialReceptor) {
        this.nombreComercialReceptor = nombreComercialReceptor;
    }

    public String getDireccionDestino() {
        return direccionDestino;
    }

    public void setDireccionDestino(String direccionDestino) {
        this.direccionDestino = direccionDestino;
    }

    public String getCorreoNotificacionReceptor() {
        return correoNotificacionReceptor;
    }

    public void setCorreoNotificacionReceptor(String correoNotificacionReceptor) {
        this.correoNotificacionReceptor = correoNotificacionReceptor;
    }

    public String getTipoDocReceptorAsociado() {
        return tipoDocReceptorAsociado;
    }

    public void setTipoDocReceptorAsociado(String tipoDocReceptorAsociado) {
        this.tipoDocReceptorAsociado = tipoDocReceptorAsociado;
    }

    public String getNumDocReceptorAsociado() {
        return numDocReceptorAsociado;
    }

    public void setNumDocReceptorAsociado(String numDocReceptorAsociado) {
        this.numDocReceptorAsociado = numDocReceptorAsociado;
    }

    public String getNombreReceptorAsociado() {
        return nombreReceptorAsociado;
    }

    public void setNombreReceptorAsociado(String nombreReceptorAsociado) {
        this.nombreReceptorAsociado = nombreReceptorAsociado;
    }

    public String getTipoMoneda() {
        return tipoMoneda;
    }

    public void setTipoMoneda(String tipoMoneda) {
        this.tipoMoneda = tipoMoneda;
    }

    public String getTipoMonedaDestino() {
        return tipoMonedaDestino;
    }

    public void setTipoMonedaDestino(String tipoMonedaDestino) {
        this.tipoMonedaDestino = tipoMonedaDestino;
    }

    public float getTipoCambioDestino() {
        return tipoCambioDestino;
    }

    public void setTipoCambioDestino(float tipoCambioDestino) {
        this.tipoCambioDestino = tipoCambioDestino;
    }

    public String getTipoOperacion() {
        return tipoOperacion;
    }

    public void setTipoOperacion(String tipoOperacion) {
        this.tipoOperacion = tipoOperacion;
    }

    public String getSustento() {
        return sustento;
    }

    public void setSustento(String sustento) {
        this.sustento = sustento;
    }

    public String getTipoMotivoNotaModificatoria() {
        return tipoMotivoNotaModificatoria;
    }

    public void setTipoMotivoNotaModificatoria(String tipoMotivoNotaModificatoria) {
        this.tipoMotivoNotaModificatoria = tipoMotivoNotaModificatoria;
    }

    public float getMntNeto() {
        return mntNeto;
    }

    public void setMntNeto(float mntNeto) {
        this.mntNeto = mntNeto;
    }

    public float getMntExe() {
        return mntExe;
    }

    public void setMntExe(float mntExe) {
        this.mntExe = mntExe;
    }

    public float getMntExo() {
        return mntExo;
    }

    public void setMntExo(float mntExo) {
        this.mntExo = mntExo;
    }

    public float getMntDescuentoGlobal() {
        return mntDescuentoGlobal;
    }

    public void setMntDescuentoGlobal(float mntDescuentoGlobal) {
        this.mntDescuentoGlobal = mntDescuentoGlobal;
    }

    public float getMntTotalIgv() {
        return mntTotalIgv;
    }

    public void setMntTotalIgv(float mntTotalIgv) {
        this.mntTotalIgv = mntTotalIgv;
    }

    public float getMntTotalIsc() {
        return mntTotalIsc;
    }

    public void setMntTotalIsc(float mntTotalIsc) {
        this.mntTotalIsc = mntTotalIsc;
    }

    public float getMntTotal() {
        return mntTotal;
    }

    public void setMntTotal(float mntTotal) {
        this.mntTotal = mntTotal;
    }

    public float getMntTotalDescuentos() {
        return mntTotalDescuentos;
    }

    public void setMntTotalDescuentos(float mntTotalDescuentos) {
        this.mntTotalDescuentos = mntTotalDescuentos;
    }

    public float getMntTotalGrat() {
        return mntTotalGrat;
    }

    public void setMntTotalGrat(float mntTotalGrat) {
        this.mntTotalGrat = mntTotalGrat;
    }

    public float getMntTotalOtros() {
        return mntTotalOtros;
    }

    public void setMntTotalOtros(float mntTotalOtros) {
        this.mntTotalOtros = mntTotalOtros;
    }

    public float getMntTotalOtrosCargos() {
        return mntTotalOtrosCargos;
    }

    public void setMntTotalOtrosCargos(float mntTotalOtrosCargos) {
        this.mntTotalOtrosCargos = mntTotalOtrosCargos;
    }

    public float getMntTotalAnticipos() {
        return mntTotalAnticipos;
    }

    public void setMntTotalAnticipos(float mntTotalAnticipos) {
        this.mntTotalAnticipos = mntTotalAnticipos;
    }

    public String getFechaEmision() {
        return fechaEmision;
    }

    public void setFechaEmision(String fechaEmision) {
        this.fechaEmision = fechaEmision;
    }

    public String getFechaVencimiento() {
        return fechaVencimiento;
    }

    public void setFechaVencimiento(String fechaVencimiento) {
        this.fechaVencimiento = fechaVencimiento;
    }

    public String getGlosaDocumento() {
        return glosaDocumento;
    }

    public void setGlosaDocumento(String glosaDocumento) {
        this.glosaDocumento = glosaDocumento;
    }

    public int getContieneAdjunto() {
        return contieneAdjunto;
    }

    public void setContieneAdjunto(int contieneAdjunto) {
        this.contieneAdjunto = contieneAdjunto;
    }

    public String getTipoFormatoRepresentacionImpresa() {
        return tipoFormatoRepresentacionImpresa;
    }

    public void setTipoFormatoRepresentacionImpresa(String tipoFormatoRepresentacionImpresa) {
        this.tipoFormatoRepresentacionImpresa = tipoFormatoRepresentacionImpresa;
    }

    public String getTipoModalidadEmision() {
        return tipoModalidadEmision;
    }

    public void setTipoModalidadEmision(String tipoModalidadEmision) {
        this.tipoModalidadEmision = tipoModalidadEmision;
    }

    public String getCodContrato() {
        return codContrato;
    }

    public void setCodContrato(String codContrato) {
        this.codContrato = codContrato;
    }

    public String getCodAgencia() {
        return codAgencia;
    }

    public void setCodAgencia(String codAgencia) {
        this.codAgencia = codAgencia;
    }

    public String getCodUbicacion() {
        return codUbicacion;
    }

    public void setCodUbicacion(String codUbicacion) {
        this.codUbicacion = codUbicacion;
    }

    public String getCodError() {
        return codError;
    }

    public void setCodError(String codError) {
        this.codError = codError;
    }

    public String getIdEnvio() {
        return idEnvio;
    }

    public void setIdEnvio(String idEnvio) {
        this.idEnvio = idEnvio;
    }

    public String getCodEnvio() {
        return codEnvio;
    }

    public void setCodEnvio(String codEnvio) {
        this.codEnvio = codEnvio;
    }

    public int getIdCuenta() {
        return idCuenta;
    }

    public void setIdCuenta(int idCuenta) {
        this.idCuenta = idCuenta;
    }

    public String getIdTransaccion() {
        return idTransaccion;
    }

    public void setIdTransaccion(String idTransaccion) {
        this.idTransaccion = idTransaccion;
    }

    public String getEstadoDocumento() {
        return estadoDocumento;
    }

    public void setEstadoDocumento(String estadoDocumento) {
        this.estadoDocumento = estadoDocumento;
    }

    public String getEstadoEmision() {
        return estadoEmision;
    }

    public void setEstadoEmision(String estadoEmision) {
        this.estadoEmision = estadoEmision;
    }

    public int getReenvioHabilitado() {
        return reenvioHabilitado;
    }

    public void setReenvioHabilitado(int reenvioHabilitado) {
        this.reenvioHabilitado = reenvioHabilitado;
    }

    public String getTipoJustificacionReenvio() {
        return tipoJustificacionReenvio;
    }

    public void setTipoJustificacionReenvio(String tipoJustificacionReenvio) {
        this.tipoJustificacionReenvio = tipoJustificacionReenvio;
    }

    public String getUsuarioCreacion() {
        return usuarioCreacion;
    }

    public void setUsuarioCreacion(String usuarioCreacion) {
        this.usuarioCreacion = usuarioCreacion;
    }

    public String getFechaCreacion() {
        return fechaCreacion;
    }

    public void setFechaCreacion(String fechaCreacion) {
        this.fechaCreacion = fechaCreacion;
    }
}
