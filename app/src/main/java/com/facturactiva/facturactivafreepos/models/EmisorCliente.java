package com.facturactiva.facturactivafreepos.models;

public class EmisorCliente {

    int idEmisor;
    int idCliente;
    String correoNotificacion;
    String usuarioCreacion;
    String fechaCreacion;
    String usuarioEdicion;
    String fechaEdicion;


    public EmisorCliente() {
    }

    public EmisorCliente(int idEmisor, int idCliente, String correoNotificacion, String usuarioCreacion, String fechaCreacion, String usuarioEdicion, String fechaEdicion) {
        this.idEmisor = idEmisor;
        this.idCliente = idCliente;
        this.correoNotificacion = correoNotificacion;
        this.usuarioCreacion = usuarioCreacion;
        this.fechaCreacion = fechaCreacion;
        this.usuarioEdicion = usuarioEdicion;
        this.fechaEdicion = fechaEdicion;
    }

    public int getIdEmisor() {
        return idEmisor;
    }

    public void setIdEmisor(int idEmisor) {
        this.idEmisor = idEmisor;
    }

    public int getIdCliente() {
        return idCliente;
    }

    public void setIdCliente(int idCliente) {
        this.idCliente = idCliente;
    }

    public String getCorreoNotificacion() {
        return correoNotificacion;
    }

    public void setCorreoNotificacion(String correoNotificacion) {
        this.correoNotificacion = correoNotificacion;
    }

    public String getUsuarioCreacion() {
        return usuarioCreacion;
    }

    public void setUsuarioCreacion(String usuarioCreacion) {
        this.usuarioCreacion = usuarioCreacion;
    }

    public String getFechaCreacion() {
        return fechaCreacion;
    }

    public void setFechaCreacion(String fechaCreacion) {
        this.fechaCreacion = fechaCreacion;
    }

    public String getUsuarioEdicion() {
        return usuarioEdicion;
    }

    public void setUsuarioEdicion(String usuarioEdicion) {
        this.usuarioEdicion = usuarioEdicion;
    }

    public String getFechaEdicion() {
        return fechaEdicion;
    }

    public void setFechaEdicion(String fechaEdicion) {
        this.fechaEdicion = fechaEdicion;
    }
}
