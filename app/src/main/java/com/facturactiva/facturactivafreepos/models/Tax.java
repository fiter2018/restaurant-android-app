package com.facturactiva.facturactivafreepos.models;

/**
 * Created by KEVIN on 23/03/2018.
 */

public class Tax {

    String codImpuesto;
    float montoImpuesto;
    float tasaImpuesto;

    public Tax() {

    }

    public Tax(String codImpuesto, float montoImpuesto, float tasaImpuesto) {
        this.codImpuesto = codImpuesto;
        this.montoImpuesto = montoImpuesto;
        this.tasaImpuesto = tasaImpuesto;
    }


    public String getCodImpuesto() {
        return codImpuesto;
    }

    public void setCodImpuesto(String codImpuesto) {
        this.codImpuesto = codImpuesto;
    }

    public float getMontoImpuesto() {
        return montoImpuesto;
    }

    public void setMontoImpuesto(float montoImpuesto) {
        this.montoImpuesto = montoImpuesto;
    }

    public float getTasaImpuesto() {
        return tasaImpuesto;
    }

    public void setTasaImpuesto(float tasaImpuesto) {
        this.tasaImpuesto = tasaImpuesto;
    }


}
