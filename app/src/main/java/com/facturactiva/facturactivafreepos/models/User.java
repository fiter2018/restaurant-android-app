package com.facturactiva.facturactivafreepos.models;

public class User {

    int idUsuario;
    String nombre;
    String descripcion;
    String email;
    String password;
    int emailConfirmado;
    int renovacionPassword;
    int idPersona;
    int idCuenta;
    int idEmisor;
    int idTipoUsuario;
    int activo;
    String usuarioCreacion;
    String fechaCreacion;
    String usuarioEdicion;
    String fechaEdicion;


    public User() {
    }


    public User(int idUsuario, String nombre, String descripcion, String email, String password, int emailConfirmado, int renovacionPassword, int idPersona, int idCuenta, int idEmisor, int idTipoUsuario, int activo, String usuarioCreacion, String fechaCreacion, String usuarioEdicion, String fechaEdicion) {
        this.idUsuario = idUsuario;
        this.nombre = nombre;
        this.descripcion = descripcion;
        this.email = email;
        this.password = password;
        this.emailConfirmado = emailConfirmado;
        this.renovacionPassword = renovacionPassword;
        this.idPersona = idPersona;
        this.idCuenta = idCuenta;
        this.idEmisor = idEmisor;
        this.idTipoUsuario = idTipoUsuario;
        this.activo = activo;
        this.usuarioCreacion = usuarioCreacion;
        this.fechaCreacion = fechaCreacion;
        this.usuarioEdicion = usuarioEdicion;
        this.fechaEdicion = fechaEdicion;
    }


    public int getIdUsuario() {
        return idUsuario;
    }

    public void setIdUsuario(int idUsuario) {
        this.idUsuario = idUsuario;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public int getEmailConfirmado() {
        return emailConfirmado;
    }

    public void setEmailConfirmado(int emailConfirmado) {
        this.emailConfirmado = emailConfirmado;
    }

    public int getRenovacionPassword() {
        return renovacionPassword;
    }

    public void setRenovacionPassword(int renovacionPassword) {
        this.renovacionPassword = renovacionPassword;
    }

    public int getIdPersona() {
        return idPersona;
    }

    public void setIdPersona(int idPersona) {
        this.idPersona = idPersona;
    }

    public int getIdCuenta() {
        return idCuenta;
    }

    public void setIdCuenta(int idCuenta) {
        this.idCuenta = idCuenta;
    }

    public int getIdEmisor() {
        return idEmisor;
    }

    public void setIdEmisor(int idEmisor) {
        this.idEmisor = idEmisor;
    }

    public int getIdTipoUsuario() {
        return idTipoUsuario;
    }

    public void setIdTipoUsuario(int idTipoUsuario) {
        this.idTipoUsuario = idTipoUsuario;
    }

    public int getActivo() {
        return activo;
    }

    public void setActivo(int activo) {
        this.activo = activo;
    }

    public String getUsuarioCreacion() {
        return usuarioCreacion;
    }

    public void setUsuarioCreacion(String usuarioCreacion) {
        this.usuarioCreacion = usuarioCreacion;
    }

    public String getFechaCreacion() {
        return fechaCreacion;
    }

    public void setFechaCreacion(String fechaCreacion) {
        this.fechaCreacion = fechaCreacion;
    }

    public String getUsuarioEdicion() {
        return usuarioEdicion;
    }

    public void setUsuarioEdicion(String usuarioEdicion) {
        this.usuarioEdicion = usuarioEdicion;
    }

    public String getFechaEdicion() {
        return fechaEdicion;
    }

    public void setFechaEdicion(String fechaEdicion) {
        this.fechaEdicion = fechaEdicion;
    }
}
