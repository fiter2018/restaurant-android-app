package com.facturactiva.facturactivafreepos.models;

public class Discount {

    float mntDescuentoGlobal;
    float mntTotalDescuentos;


    public Discount() {
    }

    public Discount(float mntDescuentoGlobal, float mntTotalDescuentos) {
        this.mntDescuentoGlobal = mntDescuentoGlobal;
        this.mntTotalDescuentos = mntTotalDescuentos;
    }


    public float getMntDescuentoGlobal() {
        return mntDescuentoGlobal;
    }

    public void setMntDescuentoGlobal(float mntDescuentoGlobal) {
        this.mntDescuentoGlobal = mntDescuentoGlobal;
    }

    public float getMntTotalDescuentos() {
        return mntTotalDescuentos;
    }

    public void setMntTotalDescuentos(float mntTotalDescuentos) {
        this.mntTotalDescuentos = mntTotalDescuentos;
    }
}
