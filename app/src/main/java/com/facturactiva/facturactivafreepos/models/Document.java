package com.facturactiva.facturactivafreepos.models;

public class Document {

    String serie;
    int correlativo;
    String nombreEmisor;
    String tipoDocEmisor;
    String numDocEmisor;
    String direccionOrigen;
    String direccionUbigeo;
    String nombreComercialEmisor;
    String tipoDocReceptor;
    String numDocReceptor;
    String nombreReceptor;
    String nombreComercialReceptor;
    String tipoDocReceptorAsociado;
    String numDocReceptorAsociado;
    String nombreReceptorAsociado;
    String direccionDestino;
    String tipoMoneda;
    String sustento;
    String tipoMotivoNotaModificatoria;
    float mntNeto;
    float mntTotalIgv;
    float mntTotal;
    String fechaVencimiento;
    String glosaDocumento;
    String codContrato;
    String codCentroCosto;
    float tipoCambioDestino;
    String tipoFormatoRepresentacionImpresa;

    public Document() {

    }

    public Document(String serie, int correlativo, String nombreEmisor, String tipoDocEmisor, String numDocEmisor, String direccionOrigen, String direccionUbigeo, String nombreComercialEmisor, String tipoDocReceptor, String numDocReceptor, String nombreReceptor, String nombreComercialReceptor, String tipoDocReceptorAsociado, String numDocReceptorAsociado, String nombreReceptorAsociado, String direccionDestino, String tipoMoneda, String sustento, String tipoMotivoNotaModificatoria, float mntNeto, float mntTotalIgv, float mntTotal, String fechaVencimiento, String glosaDocumento, String codContrato, String codCentroCosto, float tipoCambioDestino, String tipoFormatoRepresentacionImpresa) {
        this.serie = serie;
        this.correlativo = correlativo;
        this.nombreEmisor = nombreEmisor;
        this.tipoDocEmisor = tipoDocEmisor;
        this.numDocEmisor = numDocEmisor;
        this.direccionOrigen = direccionOrigen;
        this.direccionUbigeo = direccionUbigeo;
        this.nombreComercialEmisor = nombreComercialEmisor;
        this.tipoDocReceptor = tipoDocReceptor;
        this.numDocReceptor = numDocReceptor;
        this.nombreReceptor = nombreReceptor;
        this.nombreComercialReceptor = nombreComercialReceptor;
        this.tipoDocReceptorAsociado = tipoDocReceptorAsociado;
        this.numDocReceptorAsociado = numDocReceptorAsociado;
        this.nombreReceptorAsociado = nombreReceptorAsociado;
        this.direccionDestino = direccionDestino;
        this.tipoMoneda = tipoMoneda;
        this.sustento = sustento;
        this.tipoMotivoNotaModificatoria = tipoMotivoNotaModificatoria;
        this.mntNeto = mntNeto;
        this.mntTotalIgv = mntTotalIgv;
        this.mntTotal = mntTotal;
        this.fechaVencimiento = fechaVencimiento;
        this.glosaDocumento = glosaDocumento;
        this.codContrato = codContrato;
        this.codCentroCosto = codCentroCosto;
        this.tipoCambioDestino = tipoCambioDestino;
        this.tipoFormatoRepresentacionImpresa = tipoFormatoRepresentacionImpresa;
    }


    public String getSerie() {
        return serie;
    }

    public void setSerie(String serie) {
        this.serie = serie;
    }

    public int getCorrelativo() {
        return correlativo;
    }

    public void setCorrelativo(int correlativo) {
        this.correlativo = correlativo;
    }

    public String getNombreEmisor() {
        return nombreEmisor;
    }

    public void setNombreEmisor(String nombreEmisor) {
        this.nombreEmisor = nombreEmisor;
    }

    public String getTipoDocEmisor() {
        return tipoDocEmisor;
    }

    public void setTipoDocEmisor(String tipoDocEmisor) {
        this.tipoDocEmisor = tipoDocEmisor;
    }

    public String getNumDocEmisor() {
        return numDocEmisor;
    }

    public void setNumDocEmisor(String numDocEmisor) {
        this.numDocEmisor = numDocEmisor;
    }

    public String getDireccionOrigen() {
        return direccionOrigen;
    }

    public void setDireccionOrigen(String direccionOrigen) {
        this.direccionOrigen = direccionOrigen;
    }

    public String getDireccionUbigeo() {
        return direccionUbigeo;
    }

    public void setDireccionUbigeo(String direccionUbigeo) {
        this.direccionUbigeo = direccionUbigeo;
    }

    public String getNombreComercialEmisor() {
        return nombreComercialEmisor;
    }

    public void setNombreComercialEmisor(String nombreComercialEmisor) {
        this.nombreComercialEmisor = nombreComercialEmisor;
    }

    public String getTipoDocReceptor() {
        return tipoDocReceptor;
    }

    public void setTipoDocReceptor(String tipoDocReceptor) {
        this.tipoDocReceptor = tipoDocReceptor;
    }

    public String getNumDocReceptor() {
        return numDocReceptor;
    }

    public void setNumDocReceptor(String numDocReceptor) {
        this.numDocReceptor = numDocReceptor;
    }

    public String getNombreReceptor() {
        return nombreReceptor;
    }

    public void setNombreReceptor(String nombreReceptor) {
        this.nombreReceptor = nombreReceptor;
    }

    public String getNombreComercialReceptor() {
        return nombreComercialReceptor;
    }

    public void setNombreComercialReceptor(String nombreComercialReceptor) {
        this.nombreComercialReceptor = nombreComercialReceptor;
    }

    public String getTipoDocReceptorAsociado() {
        return tipoDocReceptorAsociado;
    }

    public void setTipoDocReceptorAsociado(String tipoDocReceptorAsociado) {
        this.tipoDocReceptorAsociado = tipoDocReceptorAsociado;
    }

    public String getNumDocReceptorAsociado() {
        return numDocReceptorAsociado;
    }

    public void setNumDocReceptorAsociado(String numDocReceptorAsociado) {
        this.numDocReceptorAsociado = numDocReceptorAsociado;
    }

    public String getNombreReceptorAsociado() {
        return nombreReceptorAsociado;
    }

    public void setNombreReceptorAsociado(String nombreReceptorAsociado) {
        this.nombreReceptorAsociado = nombreReceptorAsociado;
    }

    public String getDireccionDestino() {
        return direccionDestino;
    }

    public void setDireccionDestino(String direccionDestino) {
        this.direccionDestino = direccionDestino;
    }

    public String getTipoMoneda() {
        return tipoMoneda;
    }

    public void setTipoMoneda(String tipoMoneda) {
        this.tipoMoneda = tipoMoneda;
    }

    public String getSustento() {
        return sustento;
    }

    public void setSustento(String sustento) {
        this.sustento = sustento;
    }

    public String getTipoMotivoNotaModificatoria() {
        return tipoMotivoNotaModificatoria;
    }

    public void setTipoMotivoNotaModificatoria(String tipoMotivoNotaModificatoria) {
        this.tipoMotivoNotaModificatoria = tipoMotivoNotaModificatoria;
    }

    public float getMntNeto() {
        return mntNeto;
    }

    public void setMntNeto(float mntNeto) {
        this.mntNeto = mntNeto;
    }

    public float getMntTotalIgv() {
        return mntTotalIgv;
    }

    public void setMntTotalIgv(float mntTotalIgv) {
        this.mntTotalIgv = mntTotalIgv;
    }

    public float getMntTotal() {
        return mntTotal;
    }

    public void setMntTotal(float mntTotal) {
        this.mntTotal = mntTotal;
    }

    public String getFechaVencimiento() {
        return fechaVencimiento;
    }

    public void setFechaVencimiento(String fechaVencimiento) {
        this.fechaVencimiento = fechaVencimiento;
    }

    public String getGlosaDocumento() {
        return glosaDocumento;
    }

    public void setGlosaDocumento(String glosaDocumento) {
        this.glosaDocumento = glosaDocumento;
    }

    public String getCodContrato() {
        return codContrato;
    }

    public void setCodContrato(String codContrato) {
        this.codContrato = codContrato;
    }

    public String getCodCentroCosto() {
        return codCentroCosto;
    }

    public void setCodCentroCosto(String codCentroCosto) {
        this.codCentroCosto = codCentroCosto;
    }

    public float getTipoCambioDestino() {
        return tipoCambioDestino;
    }

    public void setTipoCambioDestino(float tipoCambioDestino) {
        this.tipoCambioDestino = tipoCambioDestino;
    }

    public String getTipoFormatoRepresentacionImpresa() {
        return tipoFormatoRepresentacionImpresa;
    }

    public void setTipoFormatoRepresentacionImpresa(String tipoFormatoRepresentacionImpresa) {
        this.tipoFormatoRepresentacionImpresa = tipoFormatoRepresentacionImpresa;
    }

}
