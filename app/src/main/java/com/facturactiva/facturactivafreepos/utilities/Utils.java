package com.facturactiva.facturactivafreepos.utilities;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Utils {

    public static String getActualSystemDatetime(){
        //Obtenemos datetime actual del sistema
        Date date = new Date();
        //DateFormat hourdateFormat0 = new SimpleDateFormat("HH:mm:ss dd/MM/yyyy");
        DateFormat hourdateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
        //DateFormat hourdateFormat2 = DateFormat.getDateTimeInstance();
        String fechaCreac = hourdateFormat.format(date);

        return fechaCreac;
    }






}
