package com.facturactiva.facturactivafreepos.utilities;

import android.widget.EditText;

import com.facturactiva.facturactivafreepos.constants.Constants;
import com.facturactiva.facturactivafreepos.models.Cliente;

public class Utilidades {

    //TABLA EMISOR----------------------------------------------------------------------------------
    public static final String TABLA_EMISOR = "`facturacion.emisor`";
    public static final String CAMPO_ID_EMISOR = "idEmisor";
    public static final String CAMPO_TIPO_DOC_EMISOR = "tipoDocEmisor";
    public static final String CAMPO_NUM_DOC_EMISOR = "numDocEmisor";
    public static final String CAMPO_NOMBRE_EMISOR = "nombreEmisor";
    public static final String CAMPO_CORREO_NOTIF = "correoNotif";
    public static final String CAMPO_ACTIVO = "activo";
    public static final String CAMPO_USUARIO_CREAC = "usuarioCreacion";
    public static final String CAMPO_FECHA_CREAC = "fechaCreacion";
    public static final String CAMPO_USUARIO_EDIC = "usuarioEdicion";
    public static final String CAMPO_FECHA_EDIC = "fechaEdicion";
    public static final String CAMPO_FLAG_EMISOR = "flagEmisor";

    public static final String CREAR_TABLA_EMISOR =
            "CREATE TABLE " + TABLA_EMISOR + "(" +
                    CAMPO_ID_EMISOR + " INTEGER PRIMARY KEY, " +
                    CAMPO_TIPO_DOC_EMISOR + " TEXT, " +
                    CAMPO_NUM_DOC_EMISOR + " TEXT, " +
                    CAMPO_NOMBRE_EMISOR+" TEXT NOT NULL, "+
                    CAMPO_CORREO_NOTIF + " TEXT NOT NULL, " +
                    CAMPO_ACTIVO + " INTEGER NOT NULL, " +
                    CAMPO_USUARIO_CREAC + " TEXT, " +
                    CAMPO_FECHA_CREAC + " TEXT, " +
                    CAMPO_USUARIO_EDIC + " TEXT, " +
                    CAMPO_FECHA_EDIC + " TEXT, " +
                    CAMPO_FLAG_EMISOR + " INTEGER NOT NULL);";

    public static final String INSERT_INTO_EMISOR1 = "INSERT INTO " + TABLA_EMISOR + " VALUES (1, '6', '201542316598', 'Chifa YuTa EIRL', 'duenio@chifayouta.com', 1, 'ksancho', '2018-01-17 18:06:41', '', '', 0)";


    //TABLA CATEGORIA-------------------------------------------------------------------------------
    public static final String TABLA_CATEGORIA = "`facturacion.emisor_categoria_producto`";
    public static final String CAMPO_ID_EMISOR_CATEG = "idEmisor";
    public static final String CAMPO_COD_CATEGORIA = "codCategoria";
    public static final String CAMPO_DESCRIPCION = "descripcion";
    public static final String CAMPO_ABREVIATURA = "abreviatura";
    public static final String CAMPO_ACTIVO_CATEG = "activo";
    public static final String CAMPO_USUARIO_CREAC_CATEG = "usuarioCreacion";
    public static final String CAMPO_FECHA_CREAC_CATEG = "fechaCreacion";
    public static final String CAMPO_USUARIO_EDIC_CATEG = "usuarioEdicion";
    public static final String CAMPO_FECHA_EDIC_CATEG = "fechaEdicion";
    public static final String CAMPO_FLAG_CATEG = "flagCategoria";

    public static final String CREAR_TABLA_CATEGORIA =
            "CREATE TABLE " + TABLA_CATEGORIA + "(" +
                    CAMPO_ID_EMISOR_CATEG + " INTEGER, " +
                    CAMPO_COD_CATEGORIA + " TEXT, " +
                    CAMPO_DESCRIPCION + " TEXT NOT NULL, " +
                    CAMPO_ABREVIATURA + " TEXT, " +
                    CAMPO_ACTIVO_CATEG + " INTEGER NOT NULL, " +
                    CAMPO_USUARIO_CREAC_CATEG + " TEXT, " +
                    CAMPO_FECHA_CREAC_CATEG + " TEXT NOT NULL, " +
                    CAMPO_USUARIO_EDIC_CATEG + " TEXT, " +
                    CAMPO_FECHA_EDIC_CATEG + " TEXT, " +
                    CAMPO_FLAG_CATEG + " INTEGER NOT NULL, " +
                    "PRIMARY KEY(" + CAMPO_ID_EMISOR_CATEG + ", " + CAMPO_COD_CATEGORIA + "), " +
                    "FOREIGN KEY (" + CAMPO_ID_EMISOR_CATEG + ") REFERENCES " + TABLA_EMISOR + " (" + CAMPO_ID_EMISOR + ") ON DELETE CASCADE ON UPDATE CASCADE);";

    public static final String INSERT_INTO_CATEGORIA1 = "INSERT INTO " + TABLA_CATEGORIA + " VALUES (1, 'Arroz', 'Arroz', 'Arroz', 1, 'ksancho', '2018-01-17 18:06:41', '', '', 0)";
    public static final String INSERT_INTO_CATEGORIA2 = "INSERT INTO " + TABLA_CATEGORIA + " VALUES (1, 'Tallarin', 'Tallarin', 'Tallarin', 1, 'ksancho', '2018-01-17 18:06:41', '', '', 0)";
    public static final String INSERT_INTO_CATEGORIA3 = "INSERT INTO " + TABLA_CATEGORIA + " VALUES (1, 'Sopa', 'Sopa', 'Sopa', 1, 'ksancho', '2018-01-17 18:06:41', '', '', 0)";

    //TABLA PRODUCTO -------------------------------------------------------------------------------
    public static final String TABLA_PRODUCTO = "`facturacion.emisor_producto`";
    public static final String CAMPO_ID_EMISOR_PROD = "idEmisor";
    public static final String CAMPO_COD_PROD = "codProducto";
    public static final String CAMPO_COD_CATEG_PROD = "codCategoria";
    public static final String CAMPO_NRO_PLATO_CARTA_PROD = "numPlatoCarta";
    public static final String CAMPO_DESCRIPCION_PROD = "descripcion";
    public static final String CAMPO_TIPO_MONEDA_PROD = "tipoMoneda";
    public static final String CAMPO_VALOR_UNIT_PROD = "valorUnitario";
    public static final String CAMPO_ACTIVO_PROD = "activo";
    public static final String CAMPO_IMAGEN_PROD = "imagenProdRuta";
    public static final String CAMPO_USUARIO_CREAC_PROD = "usuarioCreacion";
    public static final String CAMPO_FECHA_CREAC_PROD = "fechaCreacion";
    public static final String CAMPO_USUARIO_EDIC_PROD = "usuarioEdicion";
    public static final String CAMPO_FECHA_EDIC_PROD = "fechaEdicion";
    public static final String CAMPO_FLAG_PROD = "flagProducto";

    public static final String CREAR_TABLA_PRODUCTO =
            "CREATE TABLE " + TABLA_PRODUCTO + "(" +
                    CAMPO_ID_EMISOR_PROD + " INTEGER, " +
                    CAMPO_COD_PROD + " TEXT, " +
                    CAMPO_COD_CATEG_PROD + " TEXT NOT NULL, " +
                    CAMPO_NRO_PLATO_CARTA_PROD + " INTEGER NOT NULL, " +
                    CAMPO_DESCRIPCION_PROD + " TEXT, " +
                    CAMPO_TIPO_MONEDA_PROD + " TEXT, " +
                    CAMPO_VALOR_UNIT_PROD + " REAL, " +
                    CAMPO_ACTIVO_PROD + " INTEGER NOT NULL, " +
                    CAMPO_IMAGEN_PROD + " TEXT NOT NULL, " +
                    CAMPO_USUARIO_CREAC_PROD + " TEXT, " +
                    CAMPO_FECHA_CREAC_PROD + " TEXT NOT NULL, " +
                    CAMPO_USUARIO_EDIC_PROD + " TEXT, " +
                    CAMPO_FECHA_EDIC_PROD + " TEXT, " +
                    CAMPO_FLAG_PROD + " INTEGER NOT NULL, " +
                    "PRIMARY KEY(" + CAMPO_ID_EMISOR_PROD + ", " + CAMPO_COD_PROD + "), " +
                    "FOREIGN KEY (" + CAMPO_ID_EMISOR_PROD + ") REFERENCES " + TABLA_EMISOR + " (" + CAMPO_ID_EMISOR + ") ON DELETE CASCADE ON UPDATE CASCADE, " +
                    "FOREIGN KEY (" + CAMPO_COD_CATEG_PROD + ") REFERENCES " + TABLA_CATEGORIA + " (" + CAMPO_COD_CATEGORIA + ") ON DELETE CASCADE ON UPDATE CASCADE);";

    public static final String INSERT_INTO_PRODUCTO1 = "INSERT INTO " + TABLA_PRODUCTO + " VALUES (1, 'arroz1', 'Arroz', 1, 'Arroz Chaufa de Pollo', 'PEN', 20.00, 1, '','ksancho', '2018-01-17 18:06:41', '', '', 0)";
    public static final String INSERT_INTO_PRODUCTO2 = "INSERT INTO " + TABLA_PRODUCTO + " VALUES (1, 'arroz2', 'Arroz', 2, 'Arroz Chaufa Especial', 'PEN', 25.00, 1, '', 'ksancho', '2018-01-17 18:06:41', '', '', 0)";
    public static final String INSERT_INTO_PRODUCTO3 = "INSERT INTO " + TABLA_PRODUCTO + " VALUES (1, 'arroz3', 'Arroz', 3, 'Arroz con Ensalada China', 'PEN', 18.00, 1, '', 'ksancho', '2018-01-17 18:06:41', '', '', 0)";
    public static final String INSERT_INTO_PRODUCTO4 = "INSERT INTO " + TABLA_PRODUCTO + " VALUES (1, 'sopa4', 'Sopa', 4, 'Sopa Wantan', 'PEN', 18.00, 1, '','ksancho', '2018-01-17 18:06:41', '', '', 0)";
    public static final String INSERT_INTO_PRODUCTO5 = "INSERT INTO " + TABLA_PRODUCTO + " VALUES (1, 'sopa5', 'Sopa', 5, 'Sopa de La Casa', 'PEN', 16.00, 1, '', 'ksancho', '2018-01-17 18:06:41', '', '', 0)";
    public static final String INSERT_INTO_PRODUCTO6 = "INSERT INTO " + TABLA_PRODUCTO + " VALUES (1, 'sopa6', 'Sopa', 6, 'Sopa Especial', 'PEN', 20.00, 1, '', 'ksancho', '2018-01-17 18:06:41', '', '', 0)";
    public static final String INSERT_INTO_PRODUCTO7 = "INSERT INTO " + TABLA_PRODUCTO + " VALUES (1, 'tallarin7', 'Tallarin', 7, 'Tallarín Chino', 'PEN', 22.00, 1, '','ksancho', '2018-01-17 18:06:41', '', '', 0)";
    public static final String INSERT_INTO_PRODUCTO8 = "INSERT INTO " + TABLA_PRODUCTO + " VALUES (1, 'tallarin8', 'Tallarin', 8, 'Tallarín Saltado', 'PEN', 23.00, 1, '', 'ksancho', '2018-01-17 18:06:41', '', '', 0)";
    public static final String INSERT_INTO_PRODUCTO9 = "INSERT INTO " + TABLA_PRODUCTO + " VALUES (1, 'tallarin9', 'Tallarin', 9, 'Tallarín con Wantan', 'PEN', 19.00, 1, '', 'ksancho', '2018-01-17 18:06:41', '', '', 0)";
    public static final String INSERT_INTO_PRODUCTO10 = "INSERT INTO " + TABLA_PRODUCTO + " VALUES (1, 'vegetariano10', 'Vegetariano', 10, 'Enrebozado de Carne', 'PEN', 18.00, 1, '','ksancho', '2018-01-17 18:06:41', '', '', 0)";
    public static final String INSERT_INTO_PRODUCTO11 = "INSERT INTO " + TABLA_PRODUCTO + " VALUES (1, 'vegetariano11', 'Vegetariano', 11, 'Enrebozado de Pollo Mixto', 'PEN', 16.00, 1, '', 'ksancho', '2018-01-17 18:06:41', '', '', 0)";
    public static final String INSERT_INTO_PRODUCTO12 = "INSERT INTO " + TABLA_PRODUCTO + " VALUES (1, 'vegetariano12', 'Vegetariano', 12, 'Saltado de Verduras', 'PEN', 20.00, 1, '', 'ksancho', '2018-01-17 18:06:41', '', '', 0)";


    //TABLA PERSONA --------------------------------------------------------------------------------
    //La persona con idPersona = -100 será la persona carrito, será una copia de la persona que debe ser creada/debe existir en la lista normal de personas
    public static final String TABLA_PERSONA = "`general.persona`";
    public static final String CAMPO_ID_PERS = "idPersona";
    public static final String CAMPO_NOMBRE_PERS = "nombre";
    public static final String CAMPO_NOMBRES_PERS = "nombres";
    public static final String CAMPO_AP_PATERNO_PERS = "apellidoPaterno";
    public static final String CAMPO_AP_MATERNO_PERS = "apellidoMaterno";
    public static final String CAMPO_DIREC_PRINC_PERS = "direccionPrincipal";
    public static final String CAMPO_DIREC_SECUND_PERS = "direccionSecundaria";
    public static final String CAMPO_COD_UBIG_PRINC_PERS = "codUbigeoPrincipal";
    public static final String CAMPO_COD_UBIG_SECUND_PERS = "codUbigeoSecundario";
    public static final String CAMPO_COD_PAIS_PERS = "codPais";
    public static final String CAMPO_TELEFONO_PERS = "telefono";
    public static final String CAMPO_FAX_PERS = "fax";
    public static final String CAMPO_TIPO_PERSONA_PERS = "tipoPersona";
    public static final String CAMPO_TIPO_DOC_PRINC_PERS = "tipoDocPrincipal";
    public static final String CAMPO_NUM_DOC_PRINC_PERS = "numDocPrincipal";
    public static final String CAMPO_TIPO_DOC_SECUND_PERS = "tipoDocSecundario";
    public static final String CAMPO_NUM_DOC_SECUND_PERS = "numDocSecundario";
    public static final String CAMPO_ACTIVO_PERS = "activo";
    public static final String CAMPO_USUARIO_CREAC_PERS = "usuarioCreacion";
    public static final String CAMPO_FECHA_CREAC_PERS = "fechaCreacion";
    public static final String CAMPO_USUARIO_EDIC_PERS = "usuarioEdicion";
    public static final String CAMPO_FECHA_EDIC_PERS = "fechaEdicion";
    public static final String CAMPO_FLAG_PERS = "flagPersona";


    public static final String CREAR_TABLA_PERSONA =
            "CREATE TABLE " + TABLA_PERSONA + "(" +
                    CAMPO_ID_PERS + " INTEGER PRIMARY KEY, " +
                    CAMPO_NOMBRE_PERS + " TEXT NOT NULL, " +
                    CAMPO_NOMBRES_PERS + " TEXT, " +
                    CAMPO_AP_PATERNO_PERS + " TEXT, " +
                    CAMPO_AP_MATERNO_PERS + " TEXT, " +
                    CAMPO_DIREC_PRINC_PERS + " TEXT, " +
                    CAMPO_DIREC_SECUND_PERS + " TEXT, " +
                    CAMPO_COD_UBIG_PRINC_PERS + " TEXT, " +
                    CAMPO_COD_UBIG_SECUND_PERS + " TEXT, " +
                    CAMPO_COD_PAIS_PERS + " TEXT, " +
                    CAMPO_TELEFONO_PERS + " TEXT, " +
                    CAMPO_FAX_PERS + " TEXT, " +
                    CAMPO_TIPO_PERSONA_PERS + " TEXT NOT NULL, " +
                    CAMPO_TIPO_DOC_PRINC_PERS + " TEXT NOT NULL, " +
                    CAMPO_NUM_DOC_PRINC_PERS + " TEXT NOT NULL, " +
                    CAMPO_TIPO_DOC_SECUND_PERS + " TEXT, " +
                    CAMPO_NUM_DOC_SECUND_PERS + " TEXT, " +
                    CAMPO_ACTIVO_PERS + " INTEGER NOT NULL, " +
                    CAMPO_USUARIO_CREAC_PERS + " TEXT, " +
                    CAMPO_FECHA_CREAC_PERS + " TEXT NOT NULL, " +
                    CAMPO_USUARIO_EDIC_PERS + " TEXT, " +
                    CAMPO_FECHA_EDIC_PERS + " TEXT, " +
                    CAMPO_FLAG_PERS + " INTEGER NOT NULL);";

    public static final String INSERT_INTO_PERSONA1 = "INSERT INTO " + TABLA_PERSONA + " VALUES (1, 'nombreDuenio1', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '02', '1', '45213658', '-', '-', 1, 'ksancho', '2018-01-17 18:06:41', '', '', 0)";
    public static final String INSERT_INTO_PERSONA2 = "INSERT INTO " + TABLA_PERSONA + " VALUES (2, 'nombreDuenio2', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '02', '1', '45213658', '-', '-', 1, 'ksancho', '2018-01-17 18:06:41', '', '', 0)";
    public static final String INSERT_INTO_PERSONA3 = "INSERT INTO " + TABLA_PERSONA + " VALUES (3, 'nombreSupervisor', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '02', '1', '45213658', '-', '-', 1, 'ksancho', '2018-01-17 18:06:41', '', '', 0)";
    public static final String INSERT_INTO_PERSONA4 = "INSERT INTO " + TABLA_PERSONA + " VALUES (4, 'nombreVendedor1', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '02', '1', '45213658', '-', '-', 1, 'ksancho', '2018-01-17 18:06:41', '', '', 0)";
    public static final String INSERT_INTO_PERSONA5 = "INSERT INTO " + TABLA_PERSONA + " VALUES (5, 'nombreVendedor2', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '02', '1', '45213658', '-', '-', 1, 'ksancho', '2018-01-17 18:06:41', '', '', 0)";

    //TABLA CLIENTE --------------------------------------------------------------------------------
    //El cliente con idCliente = -100 será el cliente carrito, será una copia del cliente que debe ser creado/debe existir en la lista normal de clientes
    public static final String TABLA_CLIENTE = "`facturacion.cliente`";
    public static final String CAMPO_ID_CLIENTE_CLI = "idCliente";
    public static final String CAMPO_EMAIL_CONTACTO_CLI = "emailContacto";
    public static final String CAMPO_EMAIL_CONFIRMADO_CLI = "emailConfirmado";
    public static final String CAMPO_ACTIVO_CLI = "activo";
    public static final String CAMPO_USUARIO_CREAC_CLI = "usuarioCreacion";
    public static final String CAMPO_FECHA_CREAC_CLI = "fechaCreacion";
    public static final String CAMPO_USUARIO_EDIC_CLI = "usuarioEdicion";
    public static final String CAMPO_FECHA_EDIC_CLI = "fechaEdicion";
    public static final String CAMPO_ID_EMISOR_CREAC_CLI = "idEmisorCreacion";
    public static final String CAMPO_CONDICION_PAGO_CLI = "condicionPago";
    public static final String CAMPO_FLAG_CLI = "flagCliente";


    public static final String CREAR_TABLA_CLIENTE =
            "CREATE TABLE " + TABLA_CLIENTE + "(" +
                    CAMPO_ID_CLIENTE_CLI + " INTEGER, " +
                    CAMPO_EMAIL_CONTACTO_CLI + " TEXT, " +
                    CAMPO_EMAIL_CONFIRMADO_CLI + " INTEGER NOT NULL, " +
                    CAMPO_ACTIVO_CLI + " INTEGER NOT NULL, " +
                    CAMPO_USUARIO_CREAC_CLI + " TEXT, " +
                    CAMPO_FECHA_CREAC_CLI + " TEXT, " +
                    CAMPO_USUARIO_EDIC_CLI + " TEXT, " +
                    CAMPO_FECHA_EDIC_CLI + " TEXT, " +
                    CAMPO_ID_EMISOR_CREAC_CLI + " INTEGER, " +
                    CAMPO_CONDICION_PAGO_CLI + " INTEGER, " +
                    CAMPO_FLAG_CLI + " INTEGER NOT NULL, " +
                    "PRIMARY KEY(" + CAMPO_ID_CLIENTE_CLI + ", " + CAMPO_ID_EMISOR_CREAC_CLI + "), " +
                    "FOREIGN KEY (" + CAMPO_ID_EMISOR_CREAC_CLI + ") REFERENCES " + TABLA_EMISOR + " (" + CAMPO_ID_EMISOR + ") ON DELETE CASCADE ON UPDATE CASCADE, " +
                    "FOREIGN KEY (" + CAMPO_ID_CLIENTE_CLI + ") REFERENCES " + TABLA_PERSONA + " (" + CAMPO_ID_PERS + ") ON DELETE CASCADE ON UPDATE CASCADE);";


    //TABLA PROVEEDOR-------------------------------------------------------------------------
    public static final String TABLA_PROVEEDOR = "`facturacion.proveedor`";
    public static final String CAMPO_ID_PROVEEDOR = "idProveedor";
    public static final String CAMPO_EMAIL_CONTACTO_PROV = "emailContacto";
    public static final String CAMPO_EMAIL_CONFIRMADO_PROV = "emailConfirmado";
    public static final String CAMPO_ACTIVO_PROV = "activo";
    public static final String CAMPO_USUARIO_CREAC_PROV = "usuarioCreacion";
    public static final String CAMPO_FECHA_CREAC_PROV = "fechaCreacion";
    public static final String CAMPO_USUARIO_EDIC_PROV = "usuarioEdicion";
    public static final String CAMPO_FECHA_EDIC_PROV = "fechaEdicion";
    public static final String CAMPO_ID_EMISOR_CREAC_PROV = "idEmisorCreacion";
    public static final String CAMPO_FLAG_PROV = "flagProveedor";


    public static final String CREAR_TABLA_PROVEEDOR =
            "CREATE TABLE " + TABLA_PROVEEDOR + "(" +
                    CAMPO_ID_PROVEEDOR + " INTEGER, " +
                    CAMPO_EMAIL_CONTACTO_PROV + " TEXT, " +
                    CAMPO_EMAIL_CONFIRMADO_PROV + " INTEGER NOT NULL, " +
                    CAMPO_ACTIVO_PROV + " INTEGER NOT NULL, " +
                    CAMPO_USUARIO_CREAC_PROV + " TEXT, " +
                    CAMPO_FECHA_CREAC_PROV + " TEXT, " +
                    CAMPO_USUARIO_EDIC_PROV + " TEXT, " +
                    CAMPO_FECHA_EDIC_PROV + " TEXT, " +
                    CAMPO_ID_EMISOR_CREAC_PROV + " INTEGER, " +
                    CAMPO_FLAG_PROV + " INTEGER NOT NULL, " +
                    "PRIMARY KEY(" + CAMPO_ID_PROVEEDOR + "), " +
                    "FOREIGN KEY (" + CAMPO_ID_EMISOR_CREAC_PROV + ") REFERENCES " + TABLA_EMISOR + " (" + CAMPO_ID_EMISOR + ") ON DELETE CASCADE ON UPDATE CASCADE, " +
                    "FOREIGN KEY (" + CAMPO_ID_PROVEEDOR + ") REFERENCES " + TABLA_PERSONA + " (" + CAMPO_ID_PERS + ") ON DELETE CASCADE ON UPDATE CASCADE);";


    //TABLAS MÓDULO VENTAS --------------------------------------------------------------------------------------------------------------------
    //TABLA CARRITO
    public static final String TABLA_CARRITO = "`facturacion.carrito`";
    public static final String CAMPO_ID_EMISOR_CARR = "idEmisor";
    public static final String CAMPO_COD_PROD_CARR = "codProducto";
    public static final String CAMPO_COD_CATEG_CARR = "codCategoria";
    public static final String CAMPO_NRO_PLATO_CARTA_CARR = "numPlatoCarta";
    public static final String CAMPO_DESCRIPCION_CARR = "descripcion";
    public static final String CAMPO_TIPO_MONEDA_CARR = "tipoMoneda";
    public static final String CAMPO_VALOR_UNIT_CARR = "valorUnitario";
    public static final String CAMPO_ACTIVO_CARR = "activo";
    public static final String CAMPO_IMAGEN_CARR = "imagenProdRuta";
    public static final String CAMPO_CANTIDAD_CARR = "cantidad";

    public static final String CREAR_TABLA_CARRITO =
            "CREATE TABLE " + TABLA_CARRITO + "(" +
                    CAMPO_ID_EMISOR_CARR + " INTEGER, " +
                    CAMPO_COD_PROD_CARR + " TEXT, " +
                    CAMPO_COD_CATEG_CARR + " TEXT NOT NULL, " +
                    CAMPO_NRO_PLATO_CARTA_CARR + " INTEGER NOT NULL, " +
                    CAMPO_DESCRIPCION_CARR + " TEXT, " +
                    CAMPO_TIPO_MONEDA_CARR + " TEXT, " +
                    CAMPO_VALOR_UNIT_CARR + " REAL, " +
                    CAMPO_ACTIVO_CARR + " INTEGER NOT NULL, " +
                    CAMPO_IMAGEN_CARR + " TEXT NOT NULL, " +
                    CAMPO_CANTIDAD_CARR + " INTEGER NOT NULL, " +
                    "PRIMARY KEY(" + CAMPO_ID_EMISOR_CARR + ", " + CAMPO_COD_PROD_CARR + "), " +
                    "FOREIGN KEY (" + CAMPO_ID_EMISOR_CARR + ") REFERENCES " + TABLA_EMISOR + " (" + CAMPO_ID_EMISOR + ") ON DELETE CASCADE ON UPDATE CASCADE, " +
                    "FOREIGN KEY (" + CAMPO_COD_CATEG_CARR + ") REFERENCES " + TABLA_CATEGORIA + " (" + CAMPO_COD_CATEGORIA + ") ON DELETE CASCADE ON UPDATE CASCADE);";


    // TABLAS DONDE SE ALMACENA LA EMISION
    //TABLA general.cuenta
    public static final String TABLA_GENERAL_CUENTA = "`general.cuenta`";
    public static final String CAMPO_ID_CUENTA = "idCuenta";
    public static final String CAMPO_NOMBRE_CONTAC_CUENTA = "nombreContacto";
    public static final String CAMPO_EMAIL_CONTAC_CUENTA = "emailContacto";
    public static final String CAMPO_TIPO_CUENTA = "tipoCuenta";
    public static final String CAMPO_ACTIVO_CUENTA = "activo";
    public static final String CAMPO_USUARIO_CREAC_CUENTA = "usuarioCreacion";
    public static final String CAMPO_FECHA_CREAC_CUENTA = "fechaCreacion";
    public static final String CAMPO_USUARIO_EDIC_CUENTA = "usuarioEdicion";
    public static final String CAMPO_FECHA_EDIC_CUENTA = "fechaEdicion";
    public static final String CAMPO_FLAG_CUENTA = "flagCuenta";


    public static final String CREAR_TABLA_CUENTA =
            "CREATE TABLE " + TABLA_GENERAL_CUENTA + "(" +
                    CAMPO_ID_CUENTA + " INTEGER PRIMARY KEY, " +
                    CAMPO_NOMBRE_CONTAC_CUENTA + " TEXT NOT NULL, " +
                    CAMPO_EMAIL_CONTAC_CUENTA + " TEXT NOT NULL, " +
                    CAMPO_TIPO_CUENTA + " TEXT NOT NULL, " +
                    CAMPO_ACTIVO_CUENTA + " INTEGER NOT NULL, " +
                    CAMPO_USUARIO_CREAC_CUENTA + " TEXT, " +
                    CAMPO_FECHA_CREAC_CUENTA + " TEXT NOT NULL, " +
                    CAMPO_USUARIO_EDIC_CUENTA + " TEXT, " +
                    CAMPO_FECHA_EDIC_CUENTA + " TEXT, " +
                    CAMPO_FLAG_CUENTA + " INTEGER NOT NULL);";

    public static final String INSERT_INTO_GENERAL_CUENTA1 = "INSERT INTO " + TABLA_GENERAL_CUENTA + " VALUES (1, 'nombreDeDuenio', 'duenio@chifayouta.com', '01', 1, 'ksancho', '2018-01-17 18:06:41', '', '', 0)";


    //Tabla puntos de emision
    public static final String TABLA_PUNTO_EMIS = "`punto_emision`";
    public static final String CAMPO_ID_EMISOR_PUNTO_EMIS = "idEmisor";
    public static final String CAMPO_TIPO_DOC_EMISOR_PUNTO_EMIS = "tipoDocEmisor";
    public static final String CAMPO_NUM_DOC_EMISOR_PUNTO_EMIS = "numDocEmisor";
    public static final String CAMPO_ID_PUNTO_EMIS = "idPuntoEmision";
    public static final String CAMPO_NOMBRE_PUNTO_EMIS = "nombre";
    public static final String CAMPO_ACTIVO_PUNTO_EMIS = "activo";
    public static final String CAMPO_USUARIO_CREAC_PUNTO_EMIS = "usuarioCreacion";
    public static final String CAMPO_FECHA_CREAC_PUNTO_EMIS = "fechaCreacion";
    public static final String CAMPO_USUARIO_EDIC_PUNTO_EMIS = "usuarioEdicion";
    public static final String CAMPO_FECHA_EDIC_PUNTO_EMIS = "fechaEdicion";
    public static final String CAMPO_FLAG_PUNTO_EMIS = "flagPuntoEmision";


    public static final String CREAR_TABLA_PUNTO_EMISION =
            "CREATE TABLE " + TABLA_PUNTO_EMIS + "(" +
                    CAMPO_ID_EMISOR_PUNTO_EMIS + " INTEGER, " +
                    CAMPO_TIPO_DOC_EMISOR_PUNTO_EMIS + " TEXT, " +
                    CAMPO_NUM_DOC_EMISOR_PUNTO_EMIS + " TEXT, " +
                    CAMPO_ID_PUNTO_EMIS + " INTEGER, " +
                    CAMPO_NOMBRE_PUNTO_EMIS + " TEXT NOT NULL, " +
                    CAMPO_ACTIVO_PUNTO_EMIS+" INTEGER NOT NULL, "+
                    CAMPO_USUARIO_CREAC_PUNTO_EMIS+" TEXT, "+
                    CAMPO_FECHA_CREAC_PUNTO_EMIS+" TEXT, "+
                    CAMPO_USUARIO_EDIC_PUNTO_EMIS+" TEXT, "+
                    CAMPO_FECHA_EDIC_PUNTO_EMIS+" TEXT, "+
                    CAMPO_FLAG_PUNTO_EMIS+" INTEGER NOT NULL, "+
                    "PRIMARY KEY("+CAMPO_ID_EMISOR_PUNTO_EMIS+", "+CAMPO_TIPO_DOC_EMISOR_PUNTO_EMIS+", "+CAMPO_NUM_DOC_EMISOR_PUNTO_EMIS+", "+CAMPO_ID_PUNTO_EMIS+"), "+
                    "FOREIGN KEY ("+CAMPO_ID_EMISOR_PUNTO_EMIS+") REFERENCES "+TABLA_EMISOR+" ("+CAMPO_ID_EMISOR+") ON DELETE CASCADE ON UPDATE CASCADE);";

    public static final String INSERT_INTO_PUNTO_EMIS1 = "INSERT INTO " + TABLA_PUNTO_EMIS + " VALUES (1, '6', '201542316598', 1, 'Caja 1', 1, 'ksancho', '2018-01-17 18:06:41', '', '', 0)";
    public static final String INSERT_INTO_PUNTO_EMIS2 = "INSERT INTO " + TABLA_PUNTO_EMIS + " VALUES (1, '6', '201542316598', 2, 'Caja 2', 1, 'ksancho', '2018-01-17 18:06:41', '', '', 0)";


    //Tabla Tipo de Usuarios
    public static final String TABLA_TIPO_USUAR = "`seguridad.tipo_usuario`";
    public static final String CAMPO_ID_TIPO_USUAR = "idTipoUsuario";
    public static final String CAMPO_NOMBRE_TIPO_USUAR = "nombre";
    public static final String CAMPO_DESCRIP_TIPO_USUAR = "descripcion";
    public static final String CAMPO_ACTIVO_TIPO_USUAR = "activo";
    public static final String CAMPO_FLAG_TIPO_USUAR = "flagTipoUsuario";


    public static final String CREAR_TABLA_TIPO_USUAR =
            "CREATE TABLE "+TABLA_TIPO_USUAR+"("+
                    CAMPO_ID_TIPO_USUAR+" INTEGER PRIMARY KEY, "+
                    CAMPO_NOMBRE_TIPO_USUAR+" TEXT NOT NULL, "+
                    CAMPO_DESCRIP_TIPO_USUAR+" TEXT NOT NULL, "+
                    CAMPO_ACTIVO_TIPO_USUAR+" INTEGER NOT NULL, "+
                    CAMPO_FLAG_TIPO_USUAR+" INTEGER NOT NULL);";

    public static final String INSERT_INTO_TIPO_USUAR1 = "INSERT INTO " + TABLA_TIPO_USUAR + " VALUES (1, 'Administrador', 'Persona con permisos de administracion del sistema: configuraciones, administracion, etc', 1, 0)";
    public static final String INSERT_INTO_TIPO_USUAR2 = "INSERT INTO " + TABLA_TIPO_USUAR + " VALUES (2, 'Dueño', 'Persona con todos los permisos: configuraciones, administracion, reportes por filtros, etc', 1, 0)";
    public static final String INSERT_INTO_TIPO_USUAR3 = "INSERT INTO " + TABLA_TIPO_USUAR + " VALUES (3, 'Mozo', 'Persona con permisos para realizar manejar(crud) las órdenes de pedidos', 1, 0)";
    public static final String INSERT_INTO_TIPO_USUAR4 = "INSERT INTO " + TABLA_TIPO_USUAR + " VALUES (4, 'Cajero', 'Persona con permisos para manejar las cobranzas y recibir comentarios de los comensales', 1, 0)";
    public static final String INSERT_INTO_TIPO_USUAR5 = "INSERT INTO " + TABLA_TIPO_USUAR + " VALUES (5, 'Cocinero', 'Persona con permisos de visualizar las órdenes de pedidos', 1, 0)";


    //TABLA Usuarios -------------------------------------------------------------------------
    public static final String TABLA_USUARIOS = "`seguridad.usuario`";
    public static final String CAMPO_ID_USER = "idUsuario";
    public static final String CAMPO_NOMBRE_USER = "nombre";
    public static final String CAMPO_DESC_USER = "descripcion";
    public static final String CAMPO_EMAIL_USER = "email";
    public static final String CAMPO_PASS_USER = "password";
    public static final String CAMPO_EMAIL_CONFIRM_USER = "emailConfirmado";
    public static final String CAMPO_RENOV_PASS_USER = "renovacionPassword";
    public static final String CAMPO_ID_PERS_USER = "idPersona";
    public static final String CAMPO_ID_CUENTA_USER = "idCuenta";
    public static final String CAMPO_ID_EMISOR_USER = "idEmisor";
    public static final String CAMPO_ID_TIPO_USUAR_USER = "idTipoUsuario";
    public static final String CAMPO_ACTIVO_USER = "activo";
    public static final String CAMPO_USUARIO_CREAC_USER = "usuarioCreacion";
    public static final String CAMPO_FECHA_CREAC_USER = "fechaCreacion";
    public static final String CAMPO_USUARIO_EDIC_USER = "usuarioEdicion";
    public static final String CAMPO_FECHA_EDIC_USER = "fechaEdicion";
    public static final String CAMPO_FLAG_USER = "flagUsuarios";

    public static final String CREAR_TABLA_USUARIOS =
            "CREATE TABLE "+TABLA_USUARIOS+"("+
                    CAMPO_ID_USER+" INTEGER PRIMARY KEY, "+
                    CAMPO_NOMBRE_USER+" TEXT NOT NULL, "+
                    CAMPO_DESC_USER+" TEXT, "+
                    CAMPO_EMAIL_USER+" TEXT NOT NULL, "+
                    CAMPO_PASS_USER+" TEXT NOT NULL, "+
                    CAMPO_EMAIL_CONFIRM_USER+" INTEGER NOT NULL, "+
                    CAMPO_RENOV_PASS_USER+" INTEGER NOT NULL, "+
                    CAMPO_ID_PERS_USER+" INTEGER NOT NULL, "+
                    CAMPO_ID_CUENTA_USER+" INTEGER NOT NULL, "+
                    CAMPO_ID_EMISOR_USER+" INTEGER NOT NULL, "+
                    CAMPO_ID_TIPO_USUAR_USER+" INTEGER NOT NULL, "+
                    CAMPO_ACTIVO_USER+" INTEGER NOT NULL, "+
                    CAMPO_USUARIO_CREAC_USER+" TEXT, "+
                    CAMPO_FECHA_CREAC_USER+" TEXT NOT NULL, "+
                    CAMPO_USUARIO_EDIC_USER+" TEXT, "+
                    CAMPO_FECHA_EDIC_USER+" TEXT, "+
                    CAMPO_FLAG_USER+" INTEGER NOT NULL, "+
                    "FOREIGN KEY ("+CAMPO_ID_PERS_USER+") REFERENCES "+TABLA_PERSONA+" ("+CAMPO_ID_PERS+") ON DELETE CASCADE ON UPDATE CASCADE, "+
                    "FOREIGN KEY ("+CAMPO_ID_TIPO_USUAR_USER+") REFERENCES "+TABLA_TIPO_USUAR+" ("+CAMPO_ID_TIPO_USUAR+") ON DELETE CASCADE ON UPDATE CASCADE, "+
                    "FOREIGN KEY ("+CAMPO_ID_EMISOR_USER+") REFERENCES "+TABLA_EMISOR+" ("+CAMPO_ID_EMISOR+") ON DELETE CASCADE ON UPDATE CASCADE, "+
                    "FOREIGN KEY ("+CAMPO_ID_CUENTA_USER+") REFERENCES "+TABLA_GENERAL_CUENTA+" ("+CAMPO_ID_CUENTA+") ON DELETE CASCADE ON UPDATE CASCADE);";

    public static final String INSERT_INTO_USUARIOS0 = "INSERT INTO " + Utilidades.TABLA_USUARIOS + " VALUES (1, 'Andrés Cáceres', '', 'acaceresr@facturactiva.com', '123456', 1, 1, 1, 1, 1, 1, 1, 'ksancho', '2018-01-17 18:06:41', '', '', 0)";
    public static final String INSERT_INTO_USUARIOS1 = "INSERT INTO " + Utilidades.TABLA_USUARIOS + " VALUES (2, 'Kevin Sancho Navarro', '', 'ksancho@facturactiva.com', '*Kevin21051993*', 1, 1, 2, 1, 1, 1, 1, 'ksancho', '2018-01-17 18:06:41', '', '', 0)";
    public static final String INSERT_INTO_USUARIOS2 = "INSERT INTO " + Utilidades.TABLA_USUARIOS + " VALUES (3, 'supervisor1', '', 'supervisor1@gmail.com', '123456', 1, 1, 3, 1, 1, 2, 1, 'ksancho', '2018-01-17 18:06:41', '', '', 0)";
    public static final String INSERT_INTO_USUARIOS3 = "INSERT INTO " + Utilidades.TABLA_USUARIOS + " VALUES (4, 'vendedor1', '', 'vendedor1@gmail.com', '123456', 1, 1, 4, 1, 1, 3, 1, 'ksancho', '2018-01-17 18:06:41', '', '', 0)";
    public static final String INSERT_INTO_USUARIOS4 = "INSERT INTO " + Utilidades.TABLA_USUARIOS + " VALUES (5, 'vendedor2', '', 'vendedor2@gmail.com', '123456', 1, 1, 5, 1, 1, 3, 1, 'ksancho', '2018-01-17 18:06:41', '', '', 0)";


    //TABLA documento_electronico
    public static final String TABLA_DOC_ELECT = "`facturacion.documento_electronico`";
    public static final String CAMPO_ID_EMISOR_DOC = "idEmisor";
    public static final String CAMPO_SERIE_DOC = "serie";
    public static final String CAMPO_CORRELATIVO_DOC = "correlativo";
    public static final String CAMPO_TIPO_DOCUM_DOC = "tipoDocumento";
    public static final String CAMPO_TIPO_DOC_EMISOR_DOC = "tipoDocEmisor";
    public static final String CAMPO_NUM_DOC_EMISOR_DOC = "numDocEmisor";
    public static final String CAMPO_ID_PUNTO_EMIS_DOC = "idPuntoEmision";
    public static final String CAMPO_ID_USUAR_DOC = "idUsuario";
    public static final String CAMPO_ID_ELECT_DOC = "idElectronico";
    public static final String CAMPO_RUC_EMIS_PRINC_DOC = "rucEmisorPrincipal";
    public static final String CAMPO_NOM_EMIS_DOC = "nombreEmisor";
    public static final String CAMPO_NOM_COMERC_EMI_DOC = "nombreComercialEmisor";
    public static final String CAMPO_DIR_ORI_DOC = "direccionOrigen";
    public static final String CAMPO_DIR_UBIG_DOC = "direccionUbigeo";
    public static final String CAMPO_CORREO_NOT_EMI_DOC = "correoNotificacionEmisor";
    public static final String CAMPO_TIPO_DOC_RECEP_DOC = "tipoDocReceptor";
    public static final String CAMPO_NRO_DOC_RECEP_DOC = "numDocReceptor";
    public static final String CAMPO_NOMB_RECEP_DOC = "nombreReceptor";
    public static final String CAMPO_NOMB_COMERC_RECEP_DOC = "nombreComercialReceptor";
    public static final String CAMPO_DIR_DEST_DOC = "direccionDestino";
    public static final String CAMPO_CORREO_NOT_RECEP_DOC = "correoNotificacionReceptor";
    public static final String CAMPO_TIPO_DOC_RECEP_ASOC_DOC = "tipoDocReceptorAsociado";
    public static final String CAMPO_NRO_DOC_RECEP_ASOC_DOC = "numDocReceptorAsociado";
    public static final String CAMPO_NOMB_RECEP_ASOC_DOC = "nombreReceptorAsociado";
    public static final String CAMPO_TIPO_MONEDA_DOC = "tipoMoneda";
    public static final String CAMPO_TIPO_MONEDA_DEST_DOC = "tipoMonedaDestino";
    public static final String CAMPO_TIPO_CAMBIO_DEST_DOC = "tipoCambioDestino";
    public static final String CAMPO_TIPO_OPER_DOC = "tipoOperacion";
    public static final String CAMPO_SUST_DOC = "sustento";
    public static final String CAMPO_TIPO_MOTIV_NOTA_MODIF_DOC = "tipoMotivoNotaModificatoria";
    public static final String CAMPO_MONT_NETO_DOC = "mntNeto";
    public static final String CAMPO_MONT_EXE_DOC = "mntExe";
    public static final String CAMPO_MONT_EXO_DOC = "mntExo";
    public static final String CAMPO_MONT_DESC_GLOBAL_DOC = "mntDescuentoGlobal";
    public static final String CAMPO_MONT_TOT_IGV_DOC = "mntTotalIgv";
    public static final String CAMPO_MONT_TOT_ISC_DOC = "mntTotalIsc";
    public static final String CAMPO_MONT_TOTAL_DOC = "mntTotal";
    public static final String CAMPO_MONT_TOT_DESCUENTOS_DOC = "mntTotalDescuentos";
    public static final String CAMPO_MONT_TOT_GRAT_DOC = "mntTotalGrat";
    public static final String CAMPO_MONT_TOT_OTROS_DOC = "mntTotalOtros";
    public static final String CAMPO_MONT_TOT_OTROS_CARGOS_DOC = "mntTotalOtrosCargos";
    public static final String CAMPO_MONT_TOT_ANTICIP_DOC = "mntTotalAnticipos";
    public static final String CAMPO_FECHA_EMIS_DOC = "fechaEmision";
    public static final String CAMPO_FECHA_VTO_DOC = "fechaVencimiento";
    public static final String CAMPO_GLOSA_DOCUM_DOC = "glosaDocumento";
    public static final String CAMPO_CONT_ADJUN_DOC = "contieneAdjunto";
    public static final String CAMPO_TIPO_FORMAT_REPRES_IMPRES_DOC = "tipoFormatoRepresentacionImpresa";
    public static final String CAMPO_TIPO_MODALIDAD_EMIS_DOC = "tipoModalidadEmision";
    public static final String CAMPO_COD_CONTRATO_DOC = "codContrato";
    public static final String CAMPO_COD_AGENCIA_DOC = "codAgencia";
    public static final String CAMPO_COD_UBICAC_DOC = "codUbicacion";
    public static final String CAMPO_COD_ERROR_DOC = "codError";
    public static final String CAMPO_ID_ENVIO_DOC = "idEnvio";
    public static final String CAMPO_COD_ENVIO_DOC = "codEnvio";
    public static final String CAMPO_ID_CUENTA_DOC = "idCuenta";
    public static final String CAMPO_ID_TRANSAC_DOC = "idTransaccion";
    public static final String CAMPO_ESTADO_DOCUM_DOC = "estadoDocumento";
    public static final String CAMPO_ESTADO_EMIS_DOC = "estadoEmision";
    public static final String CAMPO_REENV_HAB_DOC = "reenvioHabilitado";
    public static final String CAMPO_TIPO_JUST_REENV_DOC = "tipoJustificacionReenvio";
    public static final String CAMPO_USUAR_CREAC_DOC = "usuarioCreacion";
    public static final String CAMPO_FECHA_CREAC_DOC = "fechaCreacion";
    public static final String CAMPO_FLAG_DOC = "flagDocElectronico";


    public static final String CREAR_TABLA_DOC_ELECT =
            "CREATE TABLE "+TABLA_DOC_ELECT+"("+
                    CAMPO_ID_EMISOR_DOC+" INTEGER, "+
                    CAMPO_SERIE_DOC+" TEXT, "+
                    CAMPO_CORRELATIVO_DOC+" INTEGER, "+
                    CAMPO_TIPO_DOCUM_DOC+" TEXT, "+
                    CAMPO_TIPO_DOC_EMISOR_DOC+" TEXT, "+
                    CAMPO_NUM_DOC_EMISOR_DOC+" TEXT, "+
                    CAMPO_ID_PUNTO_EMIS_DOC+" INTEGER NOT NULL, "+
                    CAMPO_ID_USUAR_DOC+" INTEGER NOT NULL, "+
                    CAMPO_ID_ELECT_DOC+" TEXT NOT NULL, "+
                    CAMPO_RUC_EMIS_PRINC_DOC+" TEXT NOT NULL, "+
                    CAMPO_NOM_EMIS_DOC+" TEXT NOT NULL, "+
                    CAMPO_NOM_COMERC_EMI_DOC+" TEXT, "+
                    CAMPO_DIR_ORI_DOC+" TEXT, "+
                    CAMPO_DIR_UBIG_DOC+" TEXT, "+
                    CAMPO_CORREO_NOT_EMI_DOC+" TEXT, "+
                    CAMPO_TIPO_DOC_RECEP_DOC+" TEXT NOT NULL, "+
                    CAMPO_NRO_DOC_RECEP_DOC+" TEXT NOT NULL, "+
                    CAMPO_NOMB_RECEP_DOC+" TEXT NOT NULL, "+
                    CAMPO_NOMB_COMERC_RECEP_DOC+" TEXT, "+
                    CAMPO_DIR_DEST_DOC+" TEXT, "+
                    CAMPO_CORREO_NOT_RECEP_DOC+" TEXT, "+
                    CAMPO_TIPO_DOC_RECEP_ASOC_DOC+" TEXT, "+
                    CAMPO_NRO_DOC_RECEP_ASOC_DOC+" TEXT, "+
                    CAMPO_NOMB_RECEP_ASOC_DOC+" TEXT, "+
                    CAMPO_TIPO_MONEDA_DOC+" TEXT NOT NULL, "+
                    CAMPO_TIPO_MONEDA_DEST_DOC+" TEXT NOT NULL, "+
                    CAMPO_TIPO_CAMBIO_DEST_DOC+" REAL, "+
                    CAMPO_TIPO_OPER_DOC+" TEXT, "+
                    CAMPO_SUST_DOC+" TEXT, "+
                    CAMPO_TIPO_MOTIV_NOTA_MODIF_DOC+" TEXT, "+
                    CAMPO_MONT_NETO_DOC+" REAL, "+
                    CAMPO_MONT_EXE_DOC+" REAL, "+
                    CAMPO_MONT_EXO_DOC+" REAL, "+
                    CAMPO_MONT_DESC_GLOBAL_DOC+" REAL, "+
                    CAMPO_MONT_TOT_IGV_DOC+" REAL, "+
                    CAMPO_MONT_TOT_ISC_DOC+" REAL, "+
                    CAMPO_MONT_TOT_DESCUENTOS_DOC+" REAL, "+
                    CAMPO_MONT_TOTAL_DOC+" REAL, "+
                    CAMPO_MONT_TOT_GRAT_DOC+" REAL, "+
                    CAMPO_MONT_TOT_OTROS_DOC+" REAL, "+
                    CAMPO_MONT_TOT_OTROS_CARGOS_DOC+" REAL, "+
                    CAMPO_MONT_TOT_ANTICIP_DOC+" REAL, "+
                    CAMPO_FECHA_EMIS_DOC+" TEXT NOT NULL, "+
                    CAMPO_FECHA_VTO_DOC+" TEXT, "+
                    CAMPO_GLOSA_DOCUM_DOC+" TEXT, "+
                    CAMPO_CONT_ADJUN_DOC+" INTEGER NOT NULL, "+
                    CAMPO_TIPO_FORMAT_REPRES_IMPRES_DOC+" TEXT NOT NULL, "+
                    CAMPO_TIPO_MODALIDAD_EMIS_DOC+" TEXT NOT NULL, "+
                    CAMPO_COD_CONTRATO_DOC+" TEXT, "+
                    CAMPO_COD_AGENCIA_DOC+" TEXT NOT NULL, "+
                    CAMPO_COD_UBICAC_DOC+" TEXT, "+
                    CAMPO_COD_ERROR_DOC+" TEXT, "+
                    CAMPO_ID_ENVIO_DOC+" TEXT NOT NULL, "+
                    CAMPO_COD_ENVIO_DOC+" TEXT NOT NULL, "+
                    CAMPO_ID_CUENTA_DOC+" INTEGER NOT NULL, "+
                    CAMPO_ID_TRANSAC_DOC+" TEXT NOT NULL, "+
                    CAMPO_ESTADO_DOCUM_DOC+" TEXT NOT NULL, "+
                    CAMPO_ESTADO_EMIS_DOC+" TEXT, "+
                    CAMPO_REENV_HAB_DOC+" INTEGER NOT NULL, "+
                    CAMPO_TIPO_JUST_REENV_DOC+" TEXT, "+
                    CAMPO_USUAR_CREAC_DOC+" TEXT, "+
                    CAMPO_FECHA_CREAC_DOC+" TEXT NOT NULL, "+
                    CAMPO_FLAG_DOC+" INTEGER NOT NULL, "+
                    "PRIMARY KEY("+CAMPO_ID_EMISOR_DOC+", "+CAMPO_SERIE_DOC+", "+CAMPO_CORRELATIVO_DOC+", "+CAMPO_TIPO_DOCUM_DOC+", "+CAMPO_TIPO_DOC_EMISOR_DOC+", "+CAMPO_NUM_DOC_EMISOR_DOC+"), "+
                    "FOREIGN KEY ("+CAMPO_ID_EMISOR_DOC+") REFERENCES "+TABLA_EMISOR+" ("+CAMPO_ID_EMISOR+") ON DELETE CASCADE ON UPDATE CASCADE, "+
                    "FOREIGN KEY ("+CAMPO_ID_PUNTO_EMIS_DOC+") REFERENCES "+TABLA_PUNTO_EMIS+" ("+CAMPO_ID_PUNTO_EMIS+") ON DELETE CASCADE ON UPDATE CASCADE, "+
                    "FOREIGN KEY ("+CAMPO_ID_USUAR_DOC+") REFERENCES "+TABLA_USUARIOS+" ("+CAMPO_ID_USER+") ON DELETE CASCADE ON UPDATE CASCADE, "+
                    "FOREIGN KEY ("+CAMPO_ID_CUENTA_DOC+") REFERENCES "+TABLA_GENERAL_CUENTA+" ("+CAMPO_ID_CUENTA+") ON DELETE CASCADE ON UPDATE CASCADE);";


    //TABLA documento_electronico_detalle
    public static final String TABLA_DOC_ELECT_DET = "`facturacion.documento_electronico_detalle`";
    public static final String CAMPO_ID_EMISOR_DET = "idEmisor";
    public static final String CAMPO_SERIE_DET = "serie";
    public static final String CAMPO_CORRELATIVO_DET = "correlativo";
    public static final String CAMPO_TIPO_DOCUM_DET = "tipoDocumento";
    public static final String CAMPO_TIPO_DOC_EMISOR_DET = "tipoDocEmisor";
    public static final String CAMPO_NUM_DOC_EMISOR_DET = "numDocEmisor";
    public static final String CAMPO_SECUENC_DET = "secuencial";
    public static final String CAMPO_CANT_ITEM_DET = "cantidadItem";
    public static final String CAMPO_UNID_MED_ITEM_DET = "unidadMedidaItem";
    public static final String CAMPO_COD_ITEM_DET = "codItem";
    public static final String CAMPO_NOMB_ITEM_DET = "nombreItem";
    public static final String CAMPO_PREC_ITEM_DET = "precioItem";
    public static final String CAMPO_PREC_ITEM_SIN_IGV_DET = "precioItemSinIgv";
    public static final String CAMPO_PREC_ITEM_REF_DET = "precioItemReferencia";
    public static final String CAMPO_MONTO_ITEM_DET = "montoItem";
    public static final String CAMPO_DESC_MONTO_DET = "descuentoMonto";
    public static final String CAMPO_COD_AFECT_IGV_DET = "codAfectacionIgv";
    public static final String CAMPO_TASA_IGV_DET = "tasaIgv";
    public static final String CAMPO_MONTO_IGV_DET = "montoIgv";
    public static final String CAMPO_COD_SIST_CALC_ISC_DET = "codSistemaCalculoIsc";
    public static final String CAMPO_TASA_ISC_DET = "tasaIsc";
    public static final String CAMPO_MONTO_ISC_DET = "montoIsc";
    public static final String CAMPO_ID_OPER_DET = "idOperacion";
    public static final String CAMPO_FLAG_DET = "flagDocElectDetalle";


    public static final String CREAR_TABLA_DOC_ELECT_DET =
            "CREATE TABLE "+TABLA_DOC_ELECT_DET+"("+
                    CAMPO_ID_EMISOR_DET+" INTEGER, "+
                    CAMPO_SERIE_DET+" TEXT, "+
                    CAMPO_CORRELATIVO_DET+" INTEGER, "+
                    CAMPO_TIPO_DOCUM_DET+" TEXT, "+
                    CAMPO_TIPO_DOC_EMISOR_DET+" TEXT, "+
                    CAMPO_NUM_DOC_EMISOR_DET+" TEXT, "+
                    CAMPO_SECUENC_DET+" INTEGER, "+
                    CAMPO_CANT_ITEM_DET+" REAL, "+
                    CAMPO_UNID_MED_ITEM_DET+" TEXT, "+
                    CAMPO_COD_ITEM_DET+" TEXT, "+
                    CAMPO_NOMB_ITEM_DET+" TEXT, "+
                    CAMPO_PREC_ITEM_DET+" REAL, "+
                    CAMPO_PREC_ITEM_SIN_IGV_DET+" REAL, "+
                    CAMPO_PREC_ITEM_REF_DET+" REAL, "+
                    CAMPO_MONTO_ITEM_DET+" REAL, "+
                    CAMPO_DESC_MONTO_DET+" REAL, "+
                    CAMPO_COD_AFECT_IGV_DET+" TEXT, "+
                    CAMPO_TASA_IGV_DET+" REAL, "+
                    CAMPO_MONTO_IGV_DET+" REAL, "+
                    CAMPO_COD_SIST_CALC_ISC_DET+" TEXT, "+
                    CAMPO_TASA_ISC_DET+" REAL, "+
                    CAMPO_MONTO_ISC_DET+" REAL, "+
                    CAMPO_ID_OPER_DET+" TEXT NOT NULL, "+
                    CAMPO_FLAG_DET+" INTEGER NOT NULL, "+
                    "PRIMARY KEY("+CAMPO_ID_EMISOR_DET+", "+CAMPO_SERIE_DET+", "+CAMPO_CORRELATIVO_DET+", "+CAMPO_TIPO_DOCUM_DET+", "+CAMPO_TIPO_DOC_EMISOR_DET+", "+CAMPO_NUM_DOC_EMISOR_DET+", "+CAMPO_SECUENC_DET+"), "+
                    "FOREIGN KEY ("+CAMPO_ID_EMISOR_DET+", "+CAMPO_SERIE_DET+", "+CAMPO_CORRELATIVO_DET+", "+CAMPO_TIPO_DOCUM_DET+", "+CAMPO_TIPO_DOC_EMISOR_DET+", "+CAMPO_NUM_DOC_EMISOR_DET+") REFERENCES "+TABLA_DOC_ELECT+" ("+CAMPO_ID_EMISOR_DOC+", "+CAMPO_SERIE_DOC+", "+CAMPO_CORRELATIVO_DOC+", "+CAMPO_TIPO_DOCUM_DOC+", "+CAMPO_TIPO_DOC_EMISOR_DOC+", "+CAMPO_NUM_DOC_EMISOR_DOC+") ON DELETE CASCADE ON UPDATE CASCADE);";


    //TABLA documento_electronico_impuesto
    public static final String TABLA_DOC_ELECT_IMP = "`facturacion.documento_electronico_impuesto`";
    public static final String CAMPO_ID_EMISOR_IMP = "idEmisor";
    public static final String CAMPO_SERIE_IMP = "serie";
    public static final String CAMPO_CORRELATIVO_IMP = "correlativo";
    public static final String CAMPO_TIPO_DOCUM_IMP = "tipoDocumento";
    public static final String CAMPO_TIPO_DOC_EMISOR_IMP = "tipoDocEmisor";
    public static final String CAMPO_NUM_DOC_EMISOR_IMP = "numDocEmisor";
    public static final String CAMPO_COD_IMP = "codImpuesto";
    public static final String CAMPO_MONTO_IMP = "montoImpuesto";
    public static final String CAMPO_TASA_IMP = "tasaImpuesto";
    public static final String CAMPO_FLAG_IMP = "flagDocElectImpuesto";


    public static final String CREAR_TABLA_DOC_ELECT_IMP =
            "CREATE TABLE "+TABLA_DOC_ELECT_IMP+"("+
                    CAMPO_ID_EMISOR_IMP+" INTEGER, "+
                    CAMPO_SERIE_IMP+" TEXT, "+
                    CAMPO_CORRELATIVO_IMP+" INTEGER, "+
                    CAMPO_TIPO_DOCUM_IMP+" TEXT, "+
                    CAMPO_TIPO_DOC_EMISOR_IMP+" TEXT, "+
                    CAMPO_NUM_DOC_EMISOR_IMP+" TEXT, "+
                    CAMPO_COD_IMP+" TEXT, "+
                    CAMPO_MONTO_IMP+" REAL NOT NULL, "+
                    CAMPO_TASA_IMP+" REAL NOT NULL, "+
                    CAMPO_FLAG_IMP+" INTEGER NOT NULL, "+
                    "PRIMARY KEY("+CAMPO_ID_EMISOR_IMP+", "+CAMPO_SERIE_IMP+", "+CAMPO_CORRELATIVO_IMP+", "+CAMPO_TIPO_DOCUM_IMP+", "+CAMPO_TIPO_DOC_EMISOR_IMP+", "+CAMPO_NUM_DOC_EMISOR_IMP+", "+CAMPO_COD_IMP+"), "+
                    "FOREIGN KEY ("+CAMPO_ID_EMISOR_IMP+", "+CAMPO_SERIE_IMP+", "+CAMPO_CORRELATIVO_IMP+", "+CAMPO_TIPO_DOCUM_IMP+", "+CAMPO_TIPO_DOC_EMISOR_IMP+", "+CAMPO_NUM_DOC_EMISOR_IMP+") REFERENCES "+TABLA_DOC_ELECT+" ("+CAMPO_ID_EMISOR_DOC+", "+CAMPO_SERIE_DOC+", "+CAMPO_CORRELATIVO_DOC+", "+CAMPO_TIPO_DOCUM_DOC+", "+CAMPO_TIPO_DOC_EMISOR_DOC+", "+CAMPO_NUM_DOC_EMISOR_DOC+") ON DELETE CASCADE ON UPDATE CASCADE);";

    //TABLA documento_electronico_datos_adicionales
    public static final String TABLA_DOC_ELECT_ADIC = "`emision.documento_electronico_datos_adicionales`";
    public static final String CAMPO_ID_EMISOR_ADIC = "idEmisor";
    public static final String CAMPO_SERIE_ADIC = "serie";
    public static final String CAMPO_CORRELATIVO_ADIC = "correlativo";
    public static final String CAMPO_TIPO_DOCUM_ADIC = "tipoDocumento";
    public static final String CAMPO_TIPO_DOC_EMISOR_ADIC = "tipoDocEmisor";
    public static final String CAMPO_NUM_DOC_EMISOR_ADIC = "numDocEmisor";
    public static final String CAMPO_VALOR_ADIC = "valor";
    public static final String CAMPO_DATOS_SCHEMA_ADIC = "datosSchema";
    public static final String CAMPO_FLAG_ADIC = "flagDocElectAdic";


    public static final String CREAR_TABLA_DOC_ELECT_ADIC =
            "CREATE TABLE "+TABLA_DOC_ELECT_ADIC+"("+
                    CAMPO_ID_EMISOR_ADIC+" INTEGER, "+
                    CAMPO_SERIE_ADIC+" TEXT, "+
                    CAMPO_CORRELATIVO_ADIC+" INTEGER, "+
                    CAMPO_TIPO_DOCUM_ADIC+" TEXT, "+
                    CAMPO_TIPO_DOC_EMISOR_ADIC+" TEXT, "+
                    CAMPO_NUM_DOC_EMISOR_ADIC+" TEXT, "+
                    CAMPO_VALOR_ADIC+" TEXT NOT NULL, "+
                    CAMPO_DATOS_SCHEMA_ADIC+" TEXT NOT NULL, "+
                    CAMPO_FLAG_ADIC+" INTEGER NOT NULL, "+
                    "PRIMARY KEY("+CAMPO_ID_EMISOR_ADIC+", "+CAMPO_SERIE_ADIC+", "+CAMPO_CORRELATIVO_ADIC+", "+CAMPO_TIPO_DOCUM_ADIC+", "+CAMPO_TIPO_DOC_EMISOR_ADIC+", "+CAMPO_NUM_DOC_EMISOR_ADIC+"), "+
                    "FOREIGN KEY ("+CAMPO_ID_EMISOR_ADIC+", "+CAMPO_SERIE_ADIC+", "+CAMPO_CORRELATIVO_ADIC+", "+CAMPO_TIPO_DOCUM_ADIC+", "+CAMPO_TIPO_DOC_EMISOR_ADIC+", "+CAMPO_NUM_DOC_EMISOR_ADIC+") REFERENCES "+TABLA_DOC_ELECT+" ("+CAMPO_ID_EMISOR_DOC+", "+CAMPO_SERIE_DOC+", "+CAMPO_CORRELATIVO_DOC+", "+CAMPO_TIPO_DOCUM_DOC+", "+CAMPO_TIPO_DOC_EMISOR_DOC+", "+CAMPO_NUM_DOC_EMISOR_DOC+") ON DELETE CASCADE ON UPDATE CASCADE);";


    //TABLA configuracion
    public static final String TABLA_CONFIGURACION = "`configuracion`";
    public static final String CAMPO_ID_EMISOR_CONFIG = "idEmisor";
    public static final String CAMPO_COD_CONFIG = "codConfiguracion";
    public static final String CAMPO_VALOR_CONFIG = "valor";
    public static final String CAMPO_COD_SERV = "codServicio";
    public static final String CAMPO_ACTIVO_CONFIG = "activo";
    public static final String CAMPO_USUARIO_EDIC_CONFIG = "usuarioEdicion";
    public static final String CAMPO_FECHA_EDIC_CONFIG = "fechaEdicion";
    public static final String CAMPO_FLAG_CONFIG = "flagConfiguracion";


    public static final String CREAR_TABLA_CONFIG =
            "CREATE TABLE "+TABLA_CONFIGURACION+"("+
                    CAMPO_ID_EMISOR_CONFIG+" INTEGER, "+
                    CAMPO_COD_CONFIG+" TEXT, "+
                    CAMPO_VALOR_CONFIG+" TEXT NOT NULL, "+
                    CAMPO_COD_SERV+" TEXT, "+
                    CAMPO_ACTIVO_CONFIG+" INTEGER NOT NULL, "+
                    CAMPO_USUARIO_EDIC_CONFIG+" TEXT, "+
                    CAMPO_FECHA_EDIC_CONFIG+" TEXT, "+
                    CAMPO_FLAG_CONFIG+" INTEGER NOT NULL, "+
                    "PRIMARY KEY(" + CAMPO_ID_EMISOR_CONFIG + ", " + CAMPO_COD_CONFIG + "), " +
                    "FOREIGN KEY (" + CAMPO_ID_EMISOR_CONFIG + ") REFERENCES " + TABLA_EMISOR + " (" + CAMPO_ID_EMISOR + ") ON DELETE CASCADE ON UPDATE CASCADE);";


    // Insertamos datos iniciales de Configuracion
    public static final String INSERT_INTO_CONFIG1 = "INSERT INTO " + Utilidades.TABLA_CONFIGURACION + " VALUES ('COMPROBANTES_PREDETERMINADO', '03', 'EMISION', 1, 'ksancho', '2018-01-17 18:06:41', 0)";
    public static final String INSERT_INTO_CONFIG2 = "INSERT INTO " + Utilidades.TABLA_CONFIGURACION + " VALUES ('IGV', '0.18', 'EMISION', 1, 'ksancho', '2018-01-17 18:06:41', 0)";
    public static final String INSERT_INTO_CONFIG3 = "INSERT INTO " + Utilidades.TABLA_CONFIGURACION + " VALUES ('MONEDA_LOCAL', 'PEN', 'EMISION', 1, 'ksancho', '2018-01-17 18:06:41', 0)";
    public static final String INSERT_INTO_CONFIG4 = "INSERT INTO " + Utilidades.TABLA_CONFIGURACION + " VALUES ('NOMBRE_EMISOR', 'Chifa YuTao', 'EMISION', 1, 'ksancho', '2018-01-17 18:06:41', 0)";
    public static final String INSERT_INTO_CONFIG5 = "INSERT INTO " + Utilidades.TABLA_CONFIGURACION + " VALUES ('NUMERO_DOCUMENTO_EMISOR', '20411554342', 'EMISION', 1, 'ksancho', '2018-01-17 18:06:41', 0)";
    public static final String INSERT_INTO_CONFIG6 = "INSERT INTO " + Utilidades.TABLA_CONFIGURACION + " VALUES ('PUNTO_EMISION_PREDETERMINADO', '3', 'EMISION', 1, 'ksancho', '2018-01-17 18:06:41', 0)";
    public static final String INSERT_INTO_CONFIG7 = "INSERT INTO " + Utilidades.TABLA_CONFIGURACION + " VALUES ('ROL_PREDETERMINADO', '01', 'EMISION', 1, 'ksancho', '2018-01-17 18:06:41', 0)";
    public static final String INSERT_INTO_CONFIG8 = "INSERT INTO " + Utilidades.TABLA_CONFIGURACION + " VALUES ('SERIE_BOLETA', 'B004', 'EMISION', 1, 'ksancho', '2018-01-17 18:06:41', 0)";
    public static final String INSERT_INTO_CONFIG9 = "INSERT INTO " + Utilidades.TABLA_CONFIGURACION + " VALUES ('SERIE_FACTURA', 'F106', 'EMISION', 1, 'ksancho', '2018-01-17 18:06:41', 0)";
    public static final String INSERT_INTO_CONFIG10 = "INSERT INTO " + Utilidades.TABLA_CONFIGURACION + " VALUES ('SERVICIO_PREDETERMINADO', '2', 'EMISION', 1, 'ksancho', '2018-01-17 18:06:41', 0)";
    public static final String INSERT_INTO_CONFIG11 = "INSERT INTO " + Utilidades.TABLA_CONFIGURACION + " VALUES ('TIPO_DOCUMENTO_EMISOR', '6', 'EMISION', 1, 'ksancho', '2018-01-17 18:06:41', 0)";


    //Tabla Coleccion Catalogo
    public static final String TABLA_COLECCION_CATALOGO = "`coleccion_catalogo`";
    public static final String CAMPO_COD_COLECC = "codColeccion";
    public static final String CAMPO_DESC_COLECC = "descripcion";
    public static final String CAMPO_COD_SERV_COLECC = "codServicio";
    public static final String CAMPO_ACTIVO_COLECC = "activo";
    public static final String CAMPO_USUARIO_CREAC_COLECC = "usuarioCreacion";
    public static final String CAMPO_FECHA_CREAC_COLECC = "fechaCreacion";
    public static final String CAMPO_USUARIO_EDIC_COLECC = "usuarioEdicion";
    public static final String CAMPO_FECHA_EDIC_COLECC = "fechaEdicion";
    public static final String CAMPO_FLAG_COLECC = "flagColeccionCatalogos";


    public static final String CREAR_TABLA_COLECCION_CATALOGO =
            "CREATE TABLE "+TABLA_COLECCION_CATALOGO+"("+
                    CAMPO_COD_COLECC+" TEXT PRIMARY KEY, "+
                    CAMPO_DESC_COLECC+" TEXT, "+
                    CAMPO_COD_SERV_COLECC+" TEXT, "+
                    CAMPO_ACTIVO_COLECC+" INTEGER, "+
                    CAMPO_USUARIO_CREAC_COLECC+" TEXT, "+
                    CAMPO_FECHA_CREAC_COLECC+" TEXT, "+
                    CAMPO_USUARIO_EDIC_COLECC+" TEXT, "+
                    CAMPO_FECHA_EDIC_COLECC+" TEXT, "+
                    CAMPO_FLAG_COLECC+" INTEGER NOT NULL);";

    //Inserciones Coleccion Catalogo
    public static final String INSERT_INTO_COLEC_CATALOG1 = "INSERT INTO " + Utilidades.TABLA_COLECCION_CATALOGO + " VALUES ('CodigoTipoDocumento', 'Codigos para los tipos de documentos electronicos', 'EMISION', 1, 'ksancho', '2018-01-17 18:06:41', '', '', 0)";
    public static final String INSERT_INTO_COLEC_CATALOG2 = "INSERT INTO " + Utilidades.TABLA_COLECCION_CATALOGO + " VALUES ('CodigoTipoMoneda', 'Codigos para el tipo de moneda usadas en las operaciones', 'EMISION', 1, 'ksancho', '2018-01-17 18:06:41', '', '', 0)";
    public static final String INSERT_INTO_COLEC_CATALOG3 = "INSERT INTO " + Utilidades.TABLA_COLECCION_CATALOGO + " VALUES ('CodigoTipoRol', 'Codiigos para los tipos de roles del sistema', 'EMISION', 1, 'ksancho', '2018-01-17 18:06:41', '', '', 0)";


    //Tabla Catalogo
    public static final String TABLA_CATALOGO = "`catalogo`";
    public static final String CAMPO_COD_COLECC_CATAL = "codColeccion";
    public static final String CAMPO_COD_CATAL = "codCatalogo";
    public static final String CAMPO_NOMB_CATAL = "nombre";
    public static final String CAMPO_DESC_CATAL = "descripcion";
    public static final String CAMPO_COD_SEC_CATAL = "codSecundario";
    public static final String CAMPO_ACTIVO_CATAL = "activo";
    public static final String CAMPO_USUARIO_CREAC_CATAL = "usuarioCreacion";
    public static final String CAMPO_FECHA_CREAC_CATAL = "fechaCreacion";
    public static final String CAMPO_USUARIO_EDIC_CATAL = "usuarioEdicion";
    public static final String CAMPO_FECHA_EDIC_CATAL = "fechaEdicion";
    public static final String CAMPO_FLAG_CATAL = "flagCatalogo";

    public static final String CREAR_TABLA_CATALOGO =
            "CREATE TABLE "+TABLA_CATALOGO+"("+
                    CAMPO_COD_COLECC_CATAL+" TEXT, "+
                    CAMPO_COD_CATAL+" TEXT, "+
                    CAMPO_NOMB_CATAL+" TEXT, "+
                    CAMPO_DESC_CATAL+" TEXT, "+
                    CAMPO_COD_SEC_CATAL+" TEXT, "+
                    CAMPO_ACTIVO_CATAL+" INTEGER, "+
                    CAMPO_USUARIO_CREAC_CATAL+" TEXT, "+
                    CAMPO_FECHA_CREAC_CATAL+" TEXT, "+
                    CAMPO_USUARIO_EDIC_CATAL+" TEXT, "+
                    CAMPO_FECHA_EDIC_CATAL+" TEXT, "+
                    CAMPO_FLAG_CATAL+" INTEGER NOT NULL, "+
                    "PRIMARY KEY("+CAMPO_COD_COLECC_CATAL+", "+CAMPO_COD_CATAL+"), "+
                    "FOREIGN KEY ("+CAMPO_COD_COLECC_CATAL+") REFERENCES "+TABLA_COLECCION_CATALOGO+" ("+CAMPO_COD_COLECC+") ON DELETE CASCADE ON UPDATE CASCADE);";


    // Inserciones Catalogo
    public static final String INSERT_INTO_CATALOG1 = "INSERT INTO " + Utilidades.TABLA_CATALOGO + " VALUES ('CodigoTipoDocumento', '01', 'FACTURA', '', '', 1, 'ksancho', '2018-01-17 18:06:41', '', '', 0)";
    public static final String INSERT_INTO_CATALOG2 = "INSERT INTO " + Utilidades.TABLA_CATALOGO + " VALUES ('CodigoTipoDocumento', '03', 'BOLETA DE VENTA', '', '', 1, 'ksancho', '2018-01-17 18:06:41', '', '', 0)";
    public static final String INSERT_INTO_CATALOG3 = "INSERT INTO " + Utilidades.TABLA_CATALOGO + " VALUES ('CodigoTipoMoneda', 'PEN', 'Nuevo Sol', '', '', 1, 'ksancho', '2018-01-17 18:06:41', '', '', 0)";
    public static final String INSERT_INTO_CATALOG4 = "INSERT INTO " + Utilidades.TABLA_CATALOGO + " VALUES ('CodigoTipoMoneda', 'USD', 'US Dollar', '', '', 1, 'ksancho', '2018-01-17 18:06:41', '', '', 0)";
    public static final String INSERT_INTO_CATALOG5 = "INSERT INTO " + Utilidades.TABLA_CATALOGO + " VALUES ('CodigoTipoRol', '01', 'Administrador', '', '', 1, 'ksancho', '2018-01-17 18:06:41', '', '', 0)";
    public static final String INSERT_INTO_CATALOG6 = "INSERT INTO " + Utilidades.TABLA_CATALOGO + " VALUES ('CodigoTipoRol', '02', 'Supervisor', '', '', 1, 'ksancho', '2018-01-17 18:06:41', '', '', 0)";
    public static final String INSERT_INTO_CATALOG7 = "INSERT INTO " + Utilidades.TABLA_CATALOGO + " VALUES ('CodigoTipoRol', '03', 'Vendedor', '', '', 1, 'ksancho', '2018-01-17 18:06:41', '', '', 0)";

    //Tabla Serie Correlativo
    public static final String TABLA_SERIE_CORRELATIVO = "`facturacion.documento_electronico_serie_correlativo`";
    public static final String CAMPO_ID_EMISOR_SERCORR = "idEmisor";
    public static final String CAMPO_TIPO_DOC_EMIS_SERCORR = "tipoDocEmisor";
    public static final String CAMPO_NUM_DOC_EMIS_SERCORR = "numDocEmisor";
    public static final String CAMPO_ID_PUNTO_EMIS_SERCORR = "idPuntoEmision";
    public static final String CAMPO_TIPO_DOC_SERCORR = "tipoDocumento";
    public static final String CAMPO_SERIE_SERCORR = "serie";
    public static final String CAMPO_CORRELATIVO_SERCORR = "correlativo";
    public static final String CAMPO_FLAG_SERCORR = "flagSerieCorrelativo";

    public static final String CREAR_TABLA_SERIE_CORRELATIVO =
            "CREATE TABLE "+TABLA_SERIE_CORRELATIVO+"("+
                    CAMPO_ID_EMISOR_SERCORR+" INTEGER, "+
                    CAMPO_TIPO_DOC_EMIS_SERCORR+" TEXT, "+
                    CAMPO_NUM_DOC_EMIS_SERCORR+" TEXT, "+
                    CAMPO_ID_PUNTO_EMIS_SERCORR+" INTEGER, "+
                    CAMPO_TIPO_DOC_SERCORR+" TEXT, "+
                    CAMPO_SERIE_SERCORR+" TEXT, "+
                    CAMPO_CORRELATIVO_SERCORR+" INTEGER NOT NULL, "+
                    CAMPO_FLAG_SERCORR+" INTEGER NOT NULL, "+
                    "PRIMARY KEY("+CAMPO_ID_EMISOR_SERCORR+", "+CAMPO_TIPO_DOC_EMIS_SERCORR+", "+CAMPO_NUM_DOC_EMIS_SERCORR+", "+CAMPO_ID_PUNTO_EMIS_SERCORR+", "+CAMPO_TIPO_DOC_SERCORR+", "+CAMPO_SERIE_SERCORR+"));";

    // Inserciones SERIE CORRELATIVO
    public static final String INSERT_INTO_SERIE_CORR_FACTURA = "INSERT INTO " + Utilidades.TABLA_SERIE_CORRELATIVO + " VALUES ("+ Constants.idEmisor+", '"+Constants.tipoDocEmisor+"', '"+Constants.numDocEmisor+"', "+Constants.idPuntoEmision+", '01', '"+Constants.serieFactura+"', 0, 0)";
    public static final String INSERT_INTO_SERIE_CORR_BOLETA = "INSERT INTO " + Utilidades.TABLA_SERIE_CORRELATIVO + " VALUES ("+Constants.idEmisor+", '"+Constants.tipoDocEmisor+"', '"+Constants.numDocEmisor+"', "+Constants.idPuntoEmision+", '03', '"+Constants.serieBoleta+"', 0, 0)";
    public static final String INSERT_INTO_SERIE_CORR_NC = "INSERT INTO " + Utilidades.TABLA_SERIE_CORRELATIVO + " VALUES ("+Constants.idEmisor+", '"+Constants.tipoDocEmisor+"', '"+Constants.numDocEmisor+"', "+Constants.idPuntoEmision+", '07', '"+Constants.serieNotaCre+"', 0, 0)";
    public static final String INSERT_INTO_SERIE_CORR_ND = "INSERT INTO " + Utilidades.TABLA_SERIE_CORRELATIVO + " VALUES ("+Constants.idEmisor+", '"+Constants.tipoDocEmisor+"', '"+Constants.numDocEmisor+"', "+Constants.idPuntoEmision+", '08', '"+Constants.serieNotaDeb+"', 0, 0)";

    //TABLA seguridad.consumidor_api -------------------------------------------------------------------------------
    public static final String TABLA_CONSUMIDOR_API = "`seguridad.consumidor_api`";
    public static final String CAMPO_ID_CONSUMIDOR = "idConsumidor";
    public static final String CAMPO_TIPO_CONSUMIDOR = "tipoConsumidor";
    public static final String CAMPO_KEY_CONSUM = "key";
    public static final String CAMPO_SECRET_CONSUM = "secret";
    public static final String CAMPO_SEED_CONSUM = "seed";
    public static final String CAMPO_ACTIVO_CONSUM = "activo";
    public static final String CAMPO_USUARIO_CREAC_CONSUM = "usuarioCreacion";
    public static final String CAMPO_FECHA_CREAC_CONSUM = "fechaCreacion";
    public static final String CAMPO_USUARIO_EDIC_CONSUM = "usuarioEdicion";
    public static final String CAMPO_FECHA_EDIC_CONSUM = "fechaEdicion";
    public static final String CAMPO_FLAG_CONSUM = "flagConsumidorApi";

    public static final String CREAR_TABLA_CONSUMIDOR_API =
            "CREATE TABLE " + TABLA_CONSUMIDOR_API + "(" +
                    CAMPO_ID_CONSUMIDOR + " INTEGER, " +
                    CAMPO_TIPO_CONSUMIDOR + " TEXT NOT NULL, " +
                    CAMPO_KEY_CONSUM + " TEXT NOT NULL, " +
                    CAMPO_SECRET_CONSUM + " TEXT NOT NULL, " +
                    CAMPO_SEED_CONSUM + " TEXT NOT NULL, " +
                    CAMPO_ACTIVO_CONSUM + " INTEGER NOT NULL, " +
                    CAMPO_USUARIO_CREAC_CONSUM + " TEXT, " +
                    CAMPO_FECHA_CREAC_CONSUM + " TEXT, " +
                    CAMPO_USUARIO_EDIC_CONSUM + " TEXT, " +
                    CAMPO_FECHA_EDIC_CONSUM + " TEXT, " +
                    CAMPO_FLAG_CONSUM + " INTEGER NOT NULL, " +
                    "PRIMARY KEY(" + CAMPO_ID_CONSUMIDOR + ", " + CAMPO_TIPO_CONSUMIDOR + "), " +
                    "FOREIGN KEY (" + CAMPO_ID_CONSUMIDOR + ") REFERENCES " + TABLA_EMISOR + " (" + CAMPO_ID_EMISOR + ") ON DELETE CASCADE ON UPDATE CASCADE);";

    //TABLA facturacion.emisor_token -------------------------------------------------------------------------------
    public static final String TABLA_EMISOR_TOCKEN = "`facturacion.emisor_token`";
    public static final String CAMPO_ID_EMISOR_TOCKEN = "idEmisor";
    public static final String CAMPO_TOCKEN_TOCKEN = "token";
    public static final String CAMPO_FECHA_EXPIRACION_TOCKEN = "fechaExpiracion";
    public static final String CAMPO_FECHA_EDIC_TOCKEN = "fechaEdicion";
    public static final String CAMPO_FLAG_TOCKEN = "flagTocken";

    public static final String CREAR_TABLA_EMISOR_TOCKEN =
            "CREATE TABLE " + TABLA_EMISOR_TOCKEN + "(" +
                    CAMPO_ID_EMISOR_TOCKEN + " INTEGER, " +
                    CAMPO_TOCKEN_TOCKEN + " TEXT NOT NULL, " +
                    CAMPO_FECHA_EXPIRACION_TOCKEN + " TEXT, " +
                    CAMPO_FECHA_EDIC_TOCKEN + " TEXT, " +
                    CAMPO_FLAG_TOCKEN + " INTEGER NOT NULL, " +
                    "PRIMARY KEY(" + CAMPO_ID_EMISOR_CATEG + "), " +
                    "FOREIGN KEY (" + CAMPO_ID_EMISOR_TOCKEN + ") REFERENCES " + TABLA_EMISOR + " (" + CAMPO_ID_EMISOR + ") ON DELETE CASCADE ON UPDATE CASCADE);";

    //TABLA facturacion.orden_atencion -------------------------------------------------------------------------------
    public static final String TABLA_ATTENTION_ORDER = "`facturacion.orden_atencion`";
    public static final String CAMPO_EMISOR_ID_AO = "idEmisor";
    public static final String CAMPO_ATTENTION_ORDER_ID_AO = "idOrdenAtencion";
    public static final String CAMPO_TABLE_NUMBER_AO = "numeroMesa";
    public static final String CAMPO_TOTAL_AMOUNT_AO = "montoTotal";
    public static final String CAMPO_USER_ATTENDED_ID_AO = "idUsuarioAtendio";
    public static final String CAMPO_STATE_AO = "state";
    public static final String CAMPO_USUARIO_CREAC_AO = "usuarioCreacion";
    public static final String CAMPO_FECHA_CREAC_AO = "fechaCreacion";
    public static final String CAMPO_USUARIO_EDIC_AO = "usuarioEdicion";
    public static final String CAMPO_FECHA_EDIC_AO = "fechaEdicion";
    public static final String CAMPO_FLAG_AO = "flagOrdenAtencion";

    public static final String CREAR_TABLA_ATTENTION_ORDER =
            "CREATE TABLE " + TABLA_ATTENTION_ORDER + "(" +
                    CAMPO_EMISOR_ID_AO + " INTEGER, " +
                    CAMPO_ATTENTION_ORDER_ID_AO + " INTEGER, " +
                    CAMPO_TABLE_NUMBER_AO + " INTEGER, " +
                    CAMPO_TOTAL_AMOUNT_AO + " REAL, " +
                    CAMPO_USER_ATTENDED_ID_AO + " INTEGER NOT NULL, " +
                    CAMPO_STATE_AO + " INTEGER, " +
                    CAMPO_USUARIO_CREAC_AO + " TEXT, " +
                    CAMPO_FECHA_CREAC_AO + " TEXT, " +
                    CAMPO_USUARIO_EDIC_AO + " TEXT, " +
                    CAMPO_FECHA_EDIC_AO + " TEXT, " +
                    CAMPO_FLAG_AO + " INTEGER NOT NULL, " +
                    "PRIMARY KEY(" + CAMPO_EMISOR_ID_AO + ", "+CAMPO_ATTENTION_ORDER_ID_AO+"), " +
                    "FOREIGN KEY (" + CAMPO_EMISOR_ID_AO + ") REFERENCES " + TABLA_EMISOR + " (" + CAMPO_ID_EMISOR + ") ON DELETE CASCADE ON UPDATE CASCADE);";


    //TABLA facturacion.orden_atencion_detalle -------------------------------------------------------------------------------
    public static final String TABLA_ATTENTION_ORDER_DETAIL = "`facturacion.orden_atencion_detalle`";
    public static final String CAMPO_EMISOR_ID_AOD = "idEmisor";
    public static final String CAMPO_ATTENTION_ORDER_ID_AOD = "idOrdenAtencion";
    public static final String CAMPO_ATTENTION_ORDER_DETAIL_ID_AOD = "idOrdenAtencionDetalle";
    public static final String CAMPO_PRODUCT_BUSSINESS_CODE_AOD = "codProducto";
    public static final String CAMPO_DESCRIPTION_AOD = "descripcion";
    public static final String CAMPO_UNIT_VALUE_AOD = "precioUnitario";
    public static final String CAMPO_QUANTITY_AOD = "cantidad";
    public static final String CAMPO_SUBTOTAL_AOD = "subtotal";
    public static final String CAMPO_FLAG_AOD = "flagOrdenAtencionDetalle";

    public static final String CREAR_TABLA_ATTENTION_ORDER_DETAIL =
            "CREATE TABLE " + TABLA_ATTENTION_ORDER_DETAIL + "(" +
                    CAMPO_EMISOR_ID_AOD + " INTEGER, " +
                    CAMPO_ATTENTION_ORDER_ID_AOD + " INTEGER, " +
                    CAMPO_ATTENTION_ORDER_DETAIL_ID_AOD + " INTEGER, " +
                    CAMPO_PRODUCT_BUSSINESS_CODE_AOD + " TEXT, " +
                    CAMPO_DESCRIPTION_AOD + " TEXT, " +
                    CAMPO_UNIT_VALUE_AOD + " REAL, " +
                    CAMPO_QUANTITY_AOD + " INTEGER, " +
                    CAMPO_SUBTOTAL_AOD + " REAL, " +
                    CAMPO_FLAG_AOD + " INTEGER NOT NULL, " +
                    "PRIMARY KEY(" + CAMPO_EMISOR_ID_AOD + ", " + CAMPO_ATTENTION_ORDER_ID_AOD + ", "+CAMPO_ATTENTION_ORDER_DETAIL_ID_AOD + "), " +
                    "FOREIGN KEY (" + CAMPO_EMISOR_ID_AOD + ", " + CAMPO_ATTENTION_ORDER_ID_AOD + ") REFERENCES " + TABLA_ATTENTION_ORDER + " (" + CAMPO_EMISOR_ID_AO + ", " + CAMPO_ATTENTION_ORDER_ID_AO + ") ON DELETE CASCADE ON UPDATE CASCADE);";

    //TABLA facturacion.orden_atencion -------------------------------------------------------------------------------
    public static final String TABLA_SALE = "`facturacion.venta`";
    public static final String CAMPO_EMISOR_ID_SL = "idEmisor";
    public static final String CAMPO_SALE_ID_SL = "idVenta";
    public static final String CAMPO_PAYMENT_TYPE_SL = "tipoPago";
    public static final String CAMPO_PAYMENT_AMOUNT_SL = "montoPago";
    public static final String CAMPO_CUSTOMER_ID_SL = "idCliente";
    public static final String CAMPO_USUARIO_CREAC_SL = "usuarioCreacion";
    public static final String CAMPO_FECHA_CREAC_SL = "fechaCreacion";
    public static final String CAMPO_FLAG_SL = "flagVenta";

    public static final String CREAR_TABLA_SALE =
            "CREATE TABLE " + TABLA_SALE + "(" +
                    CAMPO_EMISOR_ID_SL + " INTEGER, " +
                    CAMPO_SALE_ID_SL + " INTEGER, " +
                    CAMPO_PAYMENT_TYPE_SL + " TEXT, " +
                    CAMPO_PAYMENT_AMOUNT_SL + " REAL, " +
                    CAMPO_CUSTOMER_ID_SL + " INT, " +
                    CAMPO_USUARIO_CREAC_SL + " TEXT, " +
                    CAMPO_FECHA_CREAC_SL + " TEXT, " +
                    CAMPO_FLAG_SL + " INTEGER NOT NULL, " +
                    "PRIMARY KEY(" + CAMPO_EMISOR_ID_SL + ", "+CAMPO_SALE_ID_SL + "), " +
                    "FOREIGN KEY (" + CAMPO_EMISOR_ID_SL + ", " + CAMPO_SALE_ID_SL + ") REFERENCES " + TABLA_ATTENTION_ORDER + " (" + CAMPO_EMISOR_ID_AO + ", " + CAMPO_ATTENTION_ORDER_ID_AO + ") ON DELETE CASCADE ON UPDATE CASCADE);";












}









