package com.facturactiva.facturactivafreepos.sqlite;

import android.database.sqlite.SQLiteDatabase;

import com.facturactiva.facturactivafreepos.models.Configuracion;
import com.facturactiva.facturactivafreepos.utilities.Utilidades;

public class ConfiguracionesEntity {

    public static void insertConfiguracion(ConexionSQLiteHelper conn, Configuracion configuracion){

        SQLiteDatabase db = conn.getWritableDatabase();
        String insert = "INSERT INTO " + Utilidades.TABLA_CONFIGURACION + "(" +
                Utilidades.CAMPO_ID_EMISOR_CONFIG + ", " +
                Utilidades.CAMPO_COD_CONFIG + ", " +
                Utilidades.CAMPO_VALOR_CONFIG + ", " +
                Utilidades.CAMPO_COD_SERV + ", " +
                Utilidades.CAMPO_ACTIVO_CONFIG + ", " +
                Utilidades.CAMPO_USUARIO_EDIC_CONFIG + ", " +
                Utilidades.CAMPO_FECHA_EDIC_CONFIG + ", " +
                Utilidades.CAMPO_FLAG_CONFIG + ") VALUES(" +
                String.valueOf(configuracion.getIdEmisor()) + ", " +
                "'" + configuracion.getCodConfiguracion() + "', " +
                "'" + configuracion.getValor() + "', " +
                "'" + configuracion.getCodServicio() + "', " +
                String.valueOf(configuracion.getActivo()) + ", " +
                "'" + configuracion.getUsuarioEdicion() + "', " +
                "'" + configuracion.getFechaEdicion() + "', " +
                String.valueOf(0) + ")";

        db.execSQL(insert);
        db.close();

    }




}
