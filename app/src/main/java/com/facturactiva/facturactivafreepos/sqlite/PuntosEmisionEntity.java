package com.facturactiva.facturactivafreepos.sqlite;

import android.database.sqlite.SQLiteDatabase;

import com.facturactiva.facturactivafreepos.models.PuntoEmision;
import com.facturactiva.facturactivafreepos.utilities.Utilidades;

/**
 * Created by KEVIN on 26/03/2018.
 */

public class PuntosEmisionEntity {

    public static void insertPuntoEmision(ConexionSQLiteHelper conn, PuntoEmision puntoEmision){

        SQLiteDatabase db = conn.getWritableDatabase();
        String insert = "INSERT INTO " + Utilidades.TABLA_PUNTO_EMIS + "(" +
                Utilidades.CAMPO_ID_EMISOR_PUNTO_EMIS + ", " +
                Utilidades.CAMPO_TIPO_DOC_EMISOR_PUNTO_EMIS + ", " +
                Utilidades.CAMPO_NUM_DOC_EMISOR_PUNTO_EMIS + ", " +
                Utilidades.CAMPO_ID_PUNTO_EMIS + ", " +
                Utilidades.CAMPO_NOMBRE_PUNTO_EMIS + ", " +
                Utilidades.CAMPO_ACTIVO_PUNTO_EMIS + ", " +
                Utilidades.CAMPO_USUARIO_CREAC_PUNTO_EMIS + ", " +
                Utilidades.CAMPO_FECHA_CREAC_PUNTO_EMIS + ", " +
                Utilidades.CAMPO_USUARIO_EDIC_PUNTO_EMIS + ", " +
                Utilidades.CAMPO_FECHA_EDIC_PUNTO_EMIS + ", " +
                Utilidades.CAMPO_FLAG_PUNTO_EMIS + ") VALUES(" +
                String.valueOf(puntoEmision.getIdEmisor()) + ", " +
                "'" + puntoEmision.getTipoDocEmisor() + "', " +
                "'" + puntoEmision.getNumDocEmisor() + "', " +
                String.valueOf(puntoEmision.getIdPuntoEmision()) + ", " +
                "'" + puntoEmision.getNombre() + "', " +
                String.valueOf(puntoEmision.getActivo()) + ", " +
                "'" + puntoEmision.getUsuarioCreacion() + "', " +
                "'" + puntoEmision.getFechaCreacion() + "', " +
                "'" + puntoEmision.getUsuarioEdicion() + "', " +
                "'" + puntoEmision.getFechaEdicion() + "', " +
                String.valueOf(0) + ")";

        db.execSQL(insert);
        db.close();

    }












}
