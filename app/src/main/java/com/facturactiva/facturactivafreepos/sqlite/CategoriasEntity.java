package com.facturactiva.facturactivafreepos.sqlite;

import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.facturactiva.facturactivafreepos.models.Categoria;
import com.facturactiva.facturactivafreepos.utilities.Utilidades;

import java.util.ArrayList;

public class CategoriasEntity {

    public static void saveCategory(ConexionSQLiteHelper conn, Categoria categoria){

        SQLiteDatabase db = conn.getWritableDatabase();
        String insert = "INSERT INTO "+Utilidades.TABLA_CATEGORIA+"("+
                Utilidades.CAMPO_ID_EMISOR_CATEG+", "+
                Utilidades.CAMPO_COD_CATEGORIA+", "+
                Utilidades.CAMPO_DESCRIPCION+", "+
                Utilidades.CAMPO_ABREVIATURA+", "+
                Utilidades.CAMPO_ACTIVO_CATEG+", "+
                Utilidades.CAMPO_USUARIO_CREAC_CATEG+", "+
                Utilidades.CAMPO_FECHA_CREAC_CATEG+", "+
                Utilidades.CAMPO_USUARIO_EDIC_CATEG+", "+
                Utilidades.CAMPO_FECHA_EDIC_CATEG+", "+
                Utilidades.CAMPO_FLAG_CATEG+") VALUES("+
                String.valueOf(categoria.getIdEmisor())+", "+
                "'"+categoria.getCodCategoria()+"', "+
                "'"+categoria.getDescripcion()+"', "+
                "'"+categoria.getAbreviatura()+"', "+
                String.valueOf(categoria.getActivo())+", "+
                "'"+categoria.getUsuarioCreacion()+"', "+
                "'"+categoria.getFechaCreacion()+"', "+
                "'', "+
                "'', "+
                String.valueOf(0)+")";

        try{
            db.execSQL(insert);
        } catch (Exception e) {
            e.printStackTrace();
            Log.i("ERROR-ERROR","ERROR AL INSERTAR CATEGORÍA");
        }
        db.close();

    }
/*
    public static ArrayList<Categoria> getAllCategories(ConexionSQLiteHelper conn){


        ArrayList<Categoria> arrayListCategories = new ArrayList<Categoria>();





    }

    public static Categoria getCategoryByCode(ConexionSQLiteHelper conn, String codCategoria){

        SQLiteDatabase db = conn.getWritableDatabase();
        String insert = "INSERT INTO " + Utilidades.TABLA_DOC_ELECT_IMP + "(" +
                Utilidades.CAMPO_ID_EMISOR_IMP + ", " +
                Utilidades.CAMPO_SERIE_IMP + ", " +
                Utilidades.CAMPO_CORRELATIVO_IMP + ", " +
                Utilidades.CAMPO_TIPO_DOCUM_IMP + ", " +
                Utilidades.CAMPO_TIPO_DOC_EMISOR_IMP + ", " +
                Utilidades.CAMPO_NUM_DOC_EMISOR_IMP + ", " +
                Utilidades.CAMPO_COD_IMP + ", " +
                Utilidades.CAMPO_MONTO_IMP + ", " +
                Utilidades.CAMPO_TASA_IMP + ", " +
                Utilidades.CAMPO_FLAG_IMP + ") VALUES(" +
                String.valueOf(docElectronicoImpuesto.getIdEmisor()) + ", " +
                "'" + docElectronicoImpuesto.getSerie() + "', " +
                String.valueOf(docElectronicoImpuesto.getCorrelativo()) + ", " +
                "'" + docElectronicoImpuesto.getTipoDocumento() + "', " +
                "'" + docElectronicoImpuesto.getTipoDocEmisor() + "', " +
                "'" + docElectronicoImpuesto.getNumDocEmisor() + "', " +
                "'" + docElectronicoImpuesto.getCodImpuesto() + "', " +
                String.valueOf(docElectronicoImpuesto.getMontoImpuesto()) + ", " +
                String.valueOf(docElectronicoImpuesto.getTasaImpuesto()) + ", " +
                String.valueOf(0) + ")";

        try{
            db.execSQL(insert);
        } catch (Exception e) {
            e.printStackTrace();
            Log.i("PAGO-ERROR","ERROR AL INSERTAR EN DOC ELECTRONICO IMPUESTO");
        }
        db.close();

    }


*/
}
