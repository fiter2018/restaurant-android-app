package com.facturactiva.facturactivafreepos.sqlite;

import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.facturactiva.facturactivafreepos.models.DocElectronicoDetalle;
import com.facturactiva.facturactivafreepos.utilities.Utilidades;

public class DocElectronicoDetalleEntity {

    public static void insertDocElectDetalle(ConexionSQLiteHelper conn, DocElectronicoDetalle docElectronicoDetalle){

        SQLiteDatabase db = conn.getWritableDatabase();
        String insert = "INSERT INTO " + Utilidades.TABLA_DOC_ELECT_DET + "(" +
                Utilidades.CAMPO_ID_EMISOR_DET + ", " +
                Utilidades.CAMPO_SERIE_DET + ", " +
                Utilidades.CAMPO_CORRELATIVO_DET + ", " +
                Utilidades.CAMPO_TIPO_DOCUM_DET + ", " +
                Utilidades.CAMPO_TIPO_DOC_EMISOR_DET + ", " +
                Utilidades.CAMPO_NUM_DOC_EMISOR_DET + ", " +
                Utilidades.CAMPO_SECUENC_DET + ", " +
                Utilidades.CAMPO_CANT_ITEM_DET + ", " +
                Utilidades.CAMPO_UNID_MED_ITEM_DET + ", " +
                Utilidades.CAMPO_COD_ITEM_DET + ", " +
                Utilidades.CAMPO_NOMB_ITEM_DET + ", " +
                Utilidades.CAMPO_PREC_ITEM_DET + ", " +
                Utilidades.CAMPO_PREC_ITEM_SIN_IGV_DET + ", " +
                Utilidades.CAMPO_PREC_ITEM_REF_DET + ", " +
                Utilidades.CAMPO_MONTO_ITEM_DET + ", " +
                Utilidades.CAMPO_DESC_MONTO_DET + ", " +
                Utilidades.CAMPO_COD_AFECT_IGV_DET + ", " +
                Utilidades.CAMPO_TASA_IGV_DET + ", " +
                Utilidades.CAMPO_MONTO_IGV_DET + ", " +
                Utilidades.CAMPO_COD_SIST_CALC_ISC_DET + ", " +
                Utilidades.CAMPO_TASA_ISC_DET + ", " +
                Utilidades.CAMPO_MONTO_ISC_DET + ", " +
                Utilidades.CAMPO_ID_OPER_DET + ", " +
                Utilidades.CAMPO_FLAG_DET + ") VALUES(" +
                String.valueOf(docElectronicoDetalle.getIdEmisor()) + ", " +
                "'" + docElectronicoDetalle.getSerie() + "', " +
                String.valueOf(docElectronicoDetalle.getCorrelativo()) + ", " +
                "'" + docElectronicoDetalle.getTipoDocumento() + "', " +
                "'" + docElectronicoDetalle.getTipoDocEmisor() + "', " +
                "'" + docElectronicoDetalle.getNumDocEmisor() + "', " +
                String.valueOf(docElectronicoDetalle.getSecuencial()) + ", " +
                String.valueOf(docElectronicoDetalle.getCantidadItem()) + ", " +
                "'" + docElectronicoDetalle.getUnidadMedidaItem() + "', " +
                "'" + docElectronicoDetalle.getCodItem() + "', " +
                "'" + docElectronicoDetalle.getNombreItem() + "', " +
                String.valueOf(docElectronicoDetalle.getPrecioItem()) + ", " +
                String.valueOf(docElectronicoDetalle.getPrecioItemSinIgv()) + ", " +
                String.valueOf(docElectronicoDetalle.getPrecioItemReferencia()) + ", " +
                String.valueOf(docElectronicoDetalle.getMontoItem()) + ", " +
                String.valueOf(docElectronicoDetalle.getDescuentoMonto()) + ", " +
                "'" + docElectronicoDetalle.getCodAfectacionIgv() + "', " +
                String.valueOf(docElectronicoDetalle.getTasaIgv()) + ", " +
                String.valueOf(docElectronicoDetalle.getMontoIgv()) + ", " +
                "'" + docElectronicoDetalle.getCodSistemaCalculoIsc() + "', " +
                String.valueOf(docElectronicoDetalle.getTasaIsc()) + ", " +
                String.valueOf(docElectronicoDetalle.getMontoIsc()) + ", " +
                "'" + docElectronicoDetalle.getIdOperacion() + "', " +
                String.valueOf(0) + ")";

        try{
            db.execSQL(insert);
        } catch (Exception e) {
            e.printStackTrace();
            Log.i("PAGO-ERROR","ERROR AL INSERTAR EN DOC ELECTRONICO DETALLE");
        }
        db.close();

    }



}
