package com.facturactiva.facturactivafreepos.sqlite;

import android.database.sqlite.SQLiteDatabase;

import com.facturactiva.facturactivafreepos.models.User;
import com.facturactiva.facturactivafreepos.utilities.Utilidades;

public class UsuariosEntity {

    public static void insertUser(ConexionSQLiteHelper conn, User user){

        SQLiteDatabase db = conn.getWritableDatabase();
        String insert = "INSERT INTO " + Utilidades.TABLA_USUARIOS + "(" +
                Utilidades.CAMPO_ID_USER + ", " +
                Utilidades.CAMPO_NOMBRE_USER + ", " +
                Utilidades.CAMPO_DESC_USER + ", " +
                Utilidades.CAMPO_EMAIL_USER + ", " +
                Utilidades.CAMPO_PASS_USER + ", " +
                Utilidades.CAMPO_EMAIL_CONFIRM_USER + ", " +
                Utilidades.CAMPO_RENOV_PASS_USER + ", " +
                Utilidades.CAMPO_ID_PERS_USER + ", " +
                Utilidades.CAMPO_ID_CUENTA_USER + ", " +
                Utilidades.CAMPO_ID_EMISOR_USER + ", " +
                Utilidades.CAMPO_ID_TIPO_USUAR_USER + ", " +
                Utilidades.CAMPO_ACTIVO_USER + ", " +
                Utilidades.CAMPO_USUARIO_CREAC_USER + ", " +
                Utilidades.CAMPO_FECHA_CREAC_USER + ", " +
                Utilidades.CAMPO_USUARIO_EDIC_USER + ", " +
                Utilidades.CAMPO_FECHA_EDIC_USER + ", " +
                Utilidades.CAMPO_FLAG_USER + ") VALUES(" +
                String.valueOf(user.getIdUsuario()) + ", " +
                "'" + user.getNombre() + "', " +
                "'" + user.getDescripcion() + "', " +
                "'" + user.getEmail() + "', " +
                "'" + user.getPassword() + "', " +
                String.valueOf(user.getEmailConfirmado()) + ", " +
                String.valueOf(user.getRenovacionPassword()) + ", " +
                String.valueOf(user.getIdPersona()) + ", " +
                String.valueOf(user.getIdCuenta()) + ", " +
                String.valueOf(user.getIdEmisor()) + ", " +
                String.valueOf(user.getIdTipoUsuario()) + ", " +
                String.valueOf(user.getActivo()) + ", " +
                "'" + user.getUsuarioCreacion() + "', " +
                "'" + user.getFechaCreacion() + "', " +
                "'" + user.getUsuarioEdicion() + "', " +
                "'" + user.getFechaEdicion() + "', " +
                String.valueOf(0) + ")";

        db.execSQL(insert);
        db.close();

    }




}
