package com.facturactiva.facturactivafreepos.sqlite;

import android.content.SharedPreferences;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.facturactiva.facturactivafreepos.constants.Constants;
import com.facturactiva.facturactivafreepos.models.Comprobante;
import com.facturactiva.facturactivafreepos.models.DocElectronico;
import com.facturactiva.facturactivafreepos.utilities.Utilidades;

import org.w3c.dom.DOMConfiguration;

public class DocElectronicoEntity {

    public static void insertDocElect(ConexionSQLiteHelper conn, DocElectronico docElectronico){

        SQLiteDatabase db = conn.getWritableDatabase();
        String insert = "INSERT INTO " + Utilidades.TABLA_DOC_ELECT + "(" +
                Utilidades.CAMPO_ID_EMISOR_DOC + ", " +
                Utilidades.CAMPO_SERIE_DOC + ", " +
                Utilidades.CAMPO_CORRELATIVO_DOC + ", " +
                Utilidades.CAMPO_TIPO_DOCUM_DOC + ", " +
                Utilidades.CAMPO_TIPO_DOC_EMISOR_DOC + ", " +
                Utilidades.CAMPO_NUM_DOC_EMISOR_DOC + ", " +
                Utilidades.CAMPO_ID_PUNTO_EMIS_DOC + ", " +
                Utilidades.CAMPO_ID_USUAR_DOC + ", " +
                Utilidades.CAMPO_ID_ELECT_DOC + ", " +
                Utilidades.CAMPO_RUC_EMIS_PRINC_DOC + ", " +
                Utilidades.CAMPO_NOM_EMIS_DOC + ", " +
                Utilidades.CAMPO_NOM_COMERC_EMI_DOC + ", " +
                Utilidades.CAMPO_DIR_ORI_DOC + ", " +
                Utilidades.CAMPO_DIR_UBIG_DOC + ", " +
                Utilidades.CAMPO_CORREO_NOT_EMI_DOC + ", " +
                Utilidades.CAMPO_TIPO_DOC_RECEP_DOC + ", " +
                Utilidades.CAMPO_NRO_DOC_RECEP_DOC + ", " +
                Utilidades.CAMPO_NOMB_RECEP_DOC + ", " +
                Utilidades.CAMPO_NOMB_COMERC_RECEP_DOC + ", " +
                Utilidades.CAMPO_DIR_DEST_DOC + ", " +
                Utilidades.CAMPO_CORREO_NOT_RECEP_DOC + ", " +
                Utilidades.CAMPO_TIPO_DOC_RECEP_ASOC_DOC + ", " +
                Utilidades.CAMPO_NRO_DOC_RECEP_ASOC_DOC + ", " +
                Utilidades.CAMPO_NOMB_RECEP_ASOC_DOC + ", " +
                Utilidades.CAMPO_TIPO_MONEDA_DOC + ", " +
                Utilidades.CAMPO_TIPO_MONEDA_DEST_DOC + ", " +
                Utilidades.CAMPO_TIPO_CAMBIO_DEST_DOC + ", " +
                Utilidades.CAMPO_TIPO_OPER_DOC + ", " +
                Utilidades.CAMPO_SUST_DOC + ", " +
                Utilidades.CAMPO_TIPO_MOTIV_NOTA_MODIF_DOC + ", " +
                Utilidades.CAMPO_MONT_NETO_DOC + ", " +
                Utilidades.CAMPO_MONT_EXE_DOC + ", " +
                Utilidades.CAMPO_MONT_EXO_DOC + ", " +
                Utilidades.CAMPO_MONT_DESC_GLOBAL_DOC + ", " +
                Utilidades.CAMPO_MONT_TOT_IGV_DOC + ", " +
                Utilidades.CAMPO_MONT_TOT_ISC_DOC + ", " +
                Utilidades.CAMPO_MONT_TOTAL_DOC + ", " +
                Utilidades.CAMPO_MONT_TOT_DESCUENTOS_DOC + ", " +
                Utilidades.CAMPO_MONT_TOT_GRAT_DOC + ", " +
                Utilidades.CAMPO_MONT_TOT_OTROS_DOC + ", " +
                Utilidades.CAMPO_MONT_TOT_OTROS_CARGOS_DOC + ", " +
                Utilidades.CAMPO_MONT_TOT_ANTICIP_DOC + ", " +
                Utilidades.CAMPO_FECHA_EMIS_DOC + ", " +
                Utilidades.CAMPO_FECHA_VTO_DOC + ", " +
                Utilidades.CAMPO_GLOSA_DOCUM_DOC + ", " +
                Utilidades.CAMPO_CONT_ADJUN_DOC + ", " +
                Utilidades.CAMPO_TIPO_FORMAT_REPRES_IMPRES_DOC + ", " +
                Utilidades.CAMPO_TIPO_MODALIDAD_EMIS_DOC + ", " +
                Utilidades.CAMPO_COD_CONTRATO_DOC + ", " +
                Utilidades.CAMPO_COD_AGENCIA_DOC + ", " +
                Utilidades.CAMPO_COD_UBICAC_DOC + ", " +
                Utilidades.CAMPO_COD_ERROR_DOC + ", " +
                Utilidades.CAMPO_ID_ENVIO_DOC + ", " +
                Utilidades.CAMPO_COD_ENVIO_DOC + ", " +
                Utilidades.CAMPO_ID_CUENTA_DOC + ", " +
                Utilidades.CAMPO_ID_TRANSAC_DOC + ", " +
                Utilidades.CAMPO_ESTADO_DOCUM_DOC + ", " +
                Utilidades.CAMPO_ESTADO_EMIS_DOC + ", " +
                Utilidades.CAMPO_REENV_HAB_DOC + ", " +
                Utilidades.CAMPO_TIPO_JUST_REENV_DOC + ", " +
                Utilidades.CAMPO_USUAR_CREAC_DOC + ", " +
                Utilidades.CAMPO_FECHA_CREAC_DOC + ", " +
                Utilidades.CAMPO_FLAG_DOC + ") VALUES(" +
                String.valueOf(docElectronico.getIdEmisor()) + ", " +
                "'" + docElectronico.getSerie() + "', " +
                String.valueOf(docElectronico.getCorrelativo()) + ", " +
                "'" + docElectronico.getTipoDocumento() + "', " +
                "'" + docElectronico.getTipoDocEmisor() + "', " +
                "'" + docElectronico.getNumDocEmisor() + "', " +
                String.valueOf(docElectronico.getIdPuntoEmision()) + ", " +
                String.valueOf(docElectronico.getIdUsuario()) + ", " +
                "'" + docElectronico.getIdElectronico() + "', " +  //idElectronico
                "'" + docElectronico.getRucEmisorPrincipal() + "', " +  //rucEmisPrinc
                "'" + docElectronico.getNombreEmisor() + "', " +
                "'" + docElectronico.getNombreComercialEmisor() + "', " +  //NombreComercialEmisor
                "'" + docElectronico.getDireccionOrigen() + "', " +
                "'" + docElectronico.getDireccionUbigeo() + "', " +
                "'" + docElectronico.getCorreoNotificacionEmisor() + "', " +
                "'" + docElectronico.getTipoDocReceptor() + "', " +
                "'" + docElectronico.getNumDocReceptor() + "', " +
                "'" + docElectronico.getNombreReceptor() + "', " +
                "'" + docElectronico.getNombreComercialReceptor() + "', " +
                "'" + docElectronico.getDireccionDestino() + "', " +
                "'" + docElectronico.getCorreoNotificacionReceptor() + "', " +
                "'" + docElectronico.getTipoDocReceptorAsociado() + "', " +
                "'" + docElectronico.getNumDocReceptorAsociado() + "', " +
                "'" + docElectronico.getNombreReceptorAsociado() + "', " +
                "'" + docElectronico.getTipoMoneda() + "', " +
                "'" + docElectronico.getTipoMonedaDestino() + "', " +
                String.valueOf(docElectronico.getTipoCambioDestino()) + ", " +
                "'" + docElectronico.getTipoOperacion() + "', " +
                "'" + docElectronico.getSustento() + "', " +
                "'" + docElectronico.getTipoMotivoNotaModificatoria() + "', " +
                String.valueOf(docElectronico.getMntNeto()) + ", " +
                String.valueOf(docElectronico.getMntExe()) + ", " +
                String.valueOf(docElectronico.getMntExo()) + ", " +
                String.valueOf(docElectronico.getMntDescuentoGlobal()) + ", " +
                String.valueOf(docElectronico.getMntTotalIgv()) + ", " +
                String.valueOf(docElectronico.getMntTotalIsc()) + ", " +
                String.valueOf(docElectronico.getMntTotal()) + ", " +
                String.valueOf(docElectronico.getMntTotalDescuentos()) + ", " +
                String.valueOf(docElectronico.getMntTotalGrat()) + ", " +
                String.valueOf(docElectronico.getMntTotalOtros()) + ", " +
                String.valueOf(docElectronico.getMntTotalOtrosCargos()) + ", " +
                String.valueOf(docElectronico.getMntTotalAnticipos()) + ", " +
                "'" + docElectronico.getFechaEmision() + "', " +
                "'" + docElectronico.getFechaVencimiento() + "', " +
                "'" + docElectronico.getGlosaDocumento() + "', " +
                String.valueOf(docElectronico.getContieneAdjunto()) + ", " +
                "'" + docElectronico.getTipoFormatoRepresentacionImpresa() + "', " +
                "'" + docElectronico.getTipoModalidadEmision() + "', " +
                "'" + docElectronico.getCodContrato() + "', " +
                "'" + docElectronico.getCodAgencia() + "', " +
                "'" + docElectronico.getCodUbicacion() + "', " +
                "'" + docElectronico.getCodError() + "', " +
                "'" + docElectronico.getIdEnvio() + "', " +
                "'" + docElectronico.getCodEnvio() + "', " +
                String.valueOf(docElectronico.getIdCuenta()) + ", " +
                "'" + docElectronico.getIdTransaccion() + "', " +
                "'" + docElectronico.getEstadoDocumento() + "', " +
                "'" + docElectronico.getEstadoEmision() + "', " +
                String.valueOf(docElectronico.getReenvioHabilitado()) + ", " +
                "'" + docElectronico.getTipoJustificacionReenvio() + "', " +
                "'" + docElectronico.getUsuarioCreacion() + "', " +
                "'" + docElectronico.getFechaCreacion() + "', " +
                String.valueOf(0) + ")";

        try{
            db.execSQL(insert);
        } catch (Exception e) {
            e.printStackTrace();
            Log.i("PAGO-ERROR","ERROR AL INSERTAR EN DOC ELECTRONICO");
        }
        db.close();

    }

}
