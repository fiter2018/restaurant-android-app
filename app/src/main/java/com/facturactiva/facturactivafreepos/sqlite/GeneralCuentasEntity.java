package com.facturactiva.facturactivafreepos.sqlite;

import android.database.sqlite.SQLiteDatabase;

import com.facturactiva.facturactivafreepos.models.GeneralCuenta;
import com.facturactiva.facturactivafreepos.utilities.Utilidades;

/**
 * Created by KEVIN on 26/03/2018.
 */

public class GeneralCuentasEntity {

    public static void insertGeneralCuenta(ConexionSQLiteHelper conn, GeneralCuenta generalCuenta){

        SQLiteDatabase db = conn.getWritableDatabase();
        String insert = "INSERT INTO " + Utilidades.TABLA_GENERAL_CUENTA + "(" +
                Utilidades.CAMPO_ID_CUENTA + ", " +
                Utilidades.CAMPO_NOMBRE_CONTAC_CUENTA + ", " +
                Utilidades.CAMPO_EMAIL_CONTAC_CUENTA + ", " +
                Utilidades.CAMPO_TIPO_CUENTA + ", " +
                Utilidades.CAMPO_ACTIVO_CUENTA + ", " +
                Utilidades.CAMPO_USUARIO_CREAC_CUENTA + ", " +
                Utilidades.CAMPO_FECHA_CREAC_CUENTA + ", " +
                Utilidades.CAMPO_USUARIO_EDIC_CUENTA + ", " +
                Utilidades.CAMPO_FECHA_EDIC_CUENTA + ", " +
                Utilidades.CAMPO_FLAG_CUENTA + ") VALUES(" +
                String.valueOf(generalCuenta.getIdCuenta()) + ", " +
                "'" + generalCuenta.getNombreContacto() + "', " +
                "'" + generalCuenta.getEmailContacto() + "', " +
                "'" + generalCuenta.getTipoCuenta() + "', " +
                String.valueOf(generalCuenta.getActivo()) + ", " +
                "'" + generalCuenta.getUsuarioCreacion() + "', " +
                "'" + generalCuenta.getFechaCreacion() + "', " +
                "'" + generalCuenta.getUsuarioEdicion() + "', " +
                "'" + generalCuenta.getFechaEdicion() + "', " +
                String.valueOf(0) + ")";

        db.execSQL(insert);
        db.close();

    }


















}
