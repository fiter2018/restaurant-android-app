package com.facturactiva.facturactivafreepos.sqlite;

import android.database.sqlite.SQLiteDatabase;

import com.facturactiva.facturactivafreepos.models.SerieCorrelativo;
import com.facturactiva.facturactivafreepos.utilities.Utilidades;

public class SeriesCorrelativosEntity {

    public static void insertSerieCorrelativo(ConexionSQLiteHelper conn, SerieCorrelativo serieCorrelativo){

        SQLiteDatabase db = conn.getWritableDatabase();
        String insert = "INSERT INTO " + Utilidades.TABLA_SERIE_CORRELATIVO + "(" +
                Utilidades.CAMPO_ID_EMISOR_SERCORR + ", " +
                Utilidades.CAMPO_TIPO_DOC_EMIS_SERCORR + ", " +
                Utilidades.CAMPO_NUM_DOC_EMIS_SERCORR + ", " +
                Utilidades.CAMPO_ID_PUNTO_EMIS_SERCORR + ", " +
                Utilidades.CAMPO_TIPO_DOC_SERCORR + ", " +
                Utilidades.CAMPO_SERIE_SERCORR + ", " +
                Utilidades.CAMPO_CORRELATIVO_SERCORR + ", " +
                Utilidades.CAMPO_FLAG_SERCORR + ") VALUES(" +
                String.valueOf(serieCorrelativo.getIdEmisor()) + ", " +
                "'" + serieCorrelativo.getTipoDocEmisor() + "', " +
                "'" + serieCorrelativo.getNumDocEmisor() + "', " +
                String.valueOf(serieCorrelativo.getIdPuntoEmision()) + ", " +
                "'" + serieCorrelativo.getTipoDocumento() + "', " +
                "'" + serieCorrelativo.getSerie() + "', " +
                String.valueOf(serieCorrelativo.getCorrelativo()) + ", " +
                String.valueOf(0) + ")";

        db.execSQL(insert);
        db.close();

    }




}
