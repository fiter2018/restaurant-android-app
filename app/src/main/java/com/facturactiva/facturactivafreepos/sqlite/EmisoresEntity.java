package com.facturactiva.facturactivafreepos.sqlite;

import android.database.sqlite.SQLiteDatabase;

import com.facturactiva.facturactivafreepos.models.Emisor;
import com.facturactiva.facturactivafreepos.utilities.Utilidades;

public class EmisoresEntity {


    public static void insertEmisor(ConexionSQLiteHelper conn, Emisor emisor){

        SQLiteDatabase db = conn.getWritableDatabase();
        String insert = "INSERT INTO " + Utilidades.TABLA_EMISOR + "(" +
                Utilidades.CAMPO_ID_EMISOR + ", " +
                Utilidades.CAMPO_TIPO_DOC_EMISOR + ", " +
                Utilidades.CAMPO_NUM_DOC_EMISOR + ", " +
                Utilidades.CAMPO_NOMBRE_EMISOR + ", " +
                Utilidades.CAMPO_CORREO_NOTIF + ", " +
                Utilidades.CAMPO_ACTIVO + ", " +
                Utilidades.CAMPO_USUARIO_CREAC + ", " +
                Utilidades.CAMPO_FECHA_CREAC + ", " +
                Utilidades.CAMPO_USUARIO_EDIC + ", " +
                Utilidades.CAMPO_FECHA_EDIC + ", " +
                Utilidades.CAMPO_FLAG_EMISOR + ") VALUES(" +
                String.valueOf(emisor.getIdEmisor()) + ", " +
                "'" + emisor.getTipoDocEmisor() + "', " +
                "'" + emisor.getNumDocEmisor() + "', " +
                "'" + emisor.getNombreEmisor() + "', " +
                "'" + emisor.getCorreoNotif() + "', " +
                String.valueOf(emisor.getActivo()) + ", " +
                "'" + emisor.getUsuarioCreacion() + "', " +
                "'" + emisor.getFechaCreacion() + "', " +
                "'" + emisor.getUsuarioEdicion() + "', " +
                "'" + emisor.getFechaEdicion() + "', " +
                String.valueOf(0) + ")";

        db.execSQL(insert);
        db.close();

    }










}
