package com.facturactiva.facturactivafreepos.sqlite;

import android.content.ContentResolver;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;
import android.widget.Toast;

import com.facturactiva.facturactivafreepos.utilities.Utilidades;

public class ConexionSQLiteHelper extends SQLiteOpenHelper {

    //ContentResolver mContentResolver;
    Context context;


    public ConexionSQLiteHelper(Context context, String name, SQLiteDatabase.CursorFactory factory, int version) {
        super(context, name, factory, version);
        //mContentResolver = context.getContentResolver();
        this.context = context;

    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(Utilidades.CREAR_TABLA_EMISOR);
        db.execSQL(Utilidades.CREAR_TABLA_CATEGORIA);
        db.execSQL(Utilidades.CREAR_TABLA_PRODUCTO);
        db.execSQL(Utilidades.CREAR_TABLA_PERSONA);
        db.execSQL(Utilidades.CREAR_TABLA_CLIENTE);
        db.execSQL(Utilidades.CREAR_TABLA_PROVEEDOR);
        db.execSQL(Utilidades.CREAR_TABLA_CARRITO);
        db.execSQL(Utilidades.CREAR_TABLA_CUENTA);
        db.execSQL(Utilidades.CREAR_TABLA_PUNTO_EMISION);
        db.execSQL(Utilidades.CREAR_TABLA_TIPO_USUAR);
        db.execSQL(Utilidades.CREAR_TABLA_USUARIOS);
        db.execSQL(Utilidades.CREAR_TABLA_DOC_ELECT);
        db.execSQL(Utilidades.CREAR_TABLA_DOC_ELECT_DET);
        db.execSQL(Utilidades.CREAR_TABLA_DOC_ELECT_IMP);
        db.execSQL(Utilidades.CREAR_TABLA_DOC_ELECT_ADIC);
        db.execSQL(Utilidades.CREAR_TABLA_CONFIG);
        db.execSQL(Utilidades.CREAR_TABLA_COLECCION_CATALOGO);
        db.execSQL(Utilidades.CREAR_TABLA_CATALOGO);
        db.execSQL(Utilidades.CREAR_TABLA_SERIE_CORRELATIVO);
        db.execSQL(Utilidades.CREAR_TABLA_CONSUMIDOR_API);
        db.execSQL(Utilidades.CREAR_TABLA_EMISOR_TOCKEN);
        db.execSQL(Utilidades.CREAR_TABLA_ATTENTION_ORDER);
        db.execSQL(Utilidades.CREAR_TABLA_ATTENTION_ORDER_DETAIL);
        db.execSQL(Utilidades.CREAR_TABLA_SALE);

        try{
         /*
            db.execSQL(Utilidades.INSERT_INTO_EMISOR1);
            db.execSQL(Utilidades.INSERT_INTO_PERSONA1);
            db.execSQL(Utilidades.INSERT_INTO_PERSONA2);
            db.execSQL(Utilidades.INSERT_INTO_PERSONA3);
            db.execSQL(Utilidades.INSERT_INTO_PERSONA4);
            db.execSQL(Utilidades.INSERT_INTO_PERSONA5);
            db.execSQL(Utilidades.INSERT_INTO_GENERAL_CUENTA1);
            db.execSQL(Utilidades.INSERT_INTO_PUNTO_EMIS1);
            db.execSQL(Utilidades.INSERT_INTO_PUNTO_EMIS2);
         */
            db.execSQL(Utilidades.INSERT_INTO_TIPO_USUAR1);
            db.execSQL(Utilidades.INSERT_INTO_TIPO_USUAR2);
            db.execSQL(Utilidades.INSERT_INTO_TIPO_USUAR3);
            db.execSQL(Utilidades.INSERT_INTO_TIPO_USUAR4);
            db.execSQL(Utilidades.INSERT_INTO_TIPO_USUAR5);
         /*
            db.execSQL(Utilidades.INSERT_INTO_USUARIOS0);
            db.execSQL(Utilidades.INSERT_INTO_USUARIOS1);
            db.execSQL(Utilidades.INSERT_INTO_USUARIOS2);
            db.execSQL(Utilidades.INSERT_INTO_USUARIOS3);
            db.execSQL(Utilidades.INSERT_INTO_USUARIOS4);
         */

         /*
            db.execSQL(Utilidades.INSERT_INTO_CONFIG1);
            db.execSQL(Utilidades.INSERT_INTO_CONFIG2);
            db.execSQL(Utilidades.INSERT_INTO_CONFIG3);
            db.execSQL(Utilidades.INSERT_INTO_CONFIG4);
            db.execSQL(Utilidades.INSERT_INTO_CONFIG5);
            db.execSQL(Utilidades.INSERT_INTO_CONFIG6);
            db.execSQL(Utilidades.INSERT_INTO_CONFIG7);
            db.execSQL(Utilidades.INSERT_INTO_CONFIG8);
            db.execSQL(Utilidades.INSERT_INTO_CONFIG9);
            db.execSQL(Utilidades.INSERT_INTO_CONFIG10);
            db.execSQL(Utilidades.INSERT_INTO_CONFIG11);
         */

            db.execSQL(Utilidades.INSERT_INTO_COLEC_CATALOG1);
            db.execSQL(Utilidades.INSERT_INTO_COLEC_CATALOG2);
            db.execSQL(Utilidades.INSERT_INTO_COLEC_CATALOG3);
            db.execSQL(Utilidades.INSERT_INTO_CATALOG1);
            db.execSQL(Utilidades.INSERT_INTO_CATALOG2);
            db.execSQL(Utilidades.INSERT_INTO_CATALOG3);
            db.execSQL(Utilidades.INSERT_INTO_CATALOG4);
            db.execSQL(Utilidades.INSERT_INTO_CATALOG5);
            db.execSQL(Utilidades.INSERT_INTO_CATALOG6);
            db.execSQL(Utilidades.INSERT_INTO_CATALOG7);
/*
            db.execSQL(Utilidades.INSERT_INTO_SERIE_CORR_FACTURA);
            db.execSQL(Utilidades.INSERT_INTO_SERIE_CORR_BOLETA);
            db.execSQL(Utilidades.INSERT_INTO_SERIE_CORR_NC);
            db.execSQL(Utilidades.INSERT_INTO_SERIE_CORR_ND);
*/
         /*
            db.execSQL(Utilidades.INSERT_INTO_CATEGORIA1);
            db.execSQL(Utilidades.INSERT_INTO_CATEGORIA2);
            db.execSQL(Utilidades.INSERT_INTO_CATEGORIA3);
            db.execSQL(Utilidades.INSERT_INTO_PRODUCTO1);
            db.execSQL(Utilidades.INSERT_INTO_PRODUCTO2);
            db.execSQL(Utilidades.INSERT_INTO_PRODUCTO3);
            db.execSQL(Utilidades.INSERT_INTO_PRODUCTO4);
            db.execSQL(Utilidades.INSERT_INTO_PRODUCTO5);
            db.execSQL(Utilidades.INSERT_INTO_PRODUCTO6);
            db.execSQL(Utilidades.INSERT_INTO_PRODUCTO7);
            db.execSQL(Utilidades.INSERT_INTO_PRODUCTO8);
            db.execSQL(Utilidades.INSERT_INTO_PRODUCTO9);
            db.execSQL(Utilidades.INSERT_INTO_PRODUCTO10);
            db.execSQL(Utilidades.INSERT_INTO_PRODUCTO11);
            db.execSQL(Utilidades.INSERT_INTO_PRODUCTO12);
         */

        } catch (Exception e) {
            e.printStackTrace();
            Toast.makeText(context, "ERROR AL INSERTAR DATOS INICIALES", Toast.LENGTH_SHORT).show();
            Log.i("ERROR-SQLITE: ","ERROR AL INSERTAR DATOS INICIALES");
        }

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS "+ Utilidades.TABLA_EMISOR);
        db.execSQL("DROP TABLE IF EXISTS "+ Utilidades.TABLA_CATEGORIA);
        db.execSQL("DROP TABLE IF EXISTS "+ Utilidades.TABLA_PRODUCTO);
        db.execSQL("DROP TABLE IF EXISTS "+ Utilidades.TABLA_PERSONA);
        db.execSQL("DROP TABLE IF EXISTS "+ Utilidades.TABLA_CLIENTE);
        db.execSQL("DROP TABLE IF EXISTS "+ Utilidades.TABLA_PROVEEDOR);
        db.execSQL("DROP TABLE IF EXISTS "+ Utilidades.TABLA_CARRITO);
        db.execSQL("DROP TABLE IF EXISTS "+ Utilidades.TABLA_GENERAL_CUENTA);
        db.execSQL("DROP TABLE IF EXISTS "+ Utilidades.TABLA_PUNTO_EMIS);
        db.execSQL("DROP TABLE IF EXISTS "+ Utilidades.TABLA_TIPO_USUAR);
        db.execSQL("DROP TABLE IF EXISTS "+ Utilidades.TABLA_USUARIOS);
        db.execSQL("DROP TABLE IF EXISTS "+ Utilidades.TABLA_DOC_ELECT);
        db.execSQL("DROP TABLE IF EXISTS "+ Utilidades.TABLA_DOC_ELECT_DET);
        db.execSQL("DROP TABLE IF EXISTS "+ Utilidades.TABLA_DOC_ELECT_IMP);
        db.execSQL("DROP TABLE IF EXISTS "+ Utilidades.TABLA_DOC_ELECT_ADIC);
        db.execSQL("DROP TABLE IF EXISTS "+ Utilidades.TABLA_CONFIGURACION);
        db.execSQL("DROP TABLE IF EXISTS "+ Utilidades.TABLA_COLECCION_CATALOGO);
        db.execSQL("DROP TABLE IF EXISTS "+ Utilidades.TABLA_CATALOGO);
        db.execSQL("DROP TABLE IF EXISTS "+ Utilidades.TABLA_SERIE_CORRELATIVO);
        db.execSQL("DROP TABLE IF EXISTS "+ Utilidades.TABLA_CONSUMIDOR_API);
        db.execSQL("DROP TABLE IF EXISTS "+ Utilidades.TABLA_EMISOR_TOCKEN);
        db.execSQL("DROP TABLE IF EXISTS "+ Utilidades.TABLA_ATTENTION_ORDER);
        db.execSQL("DROP TABLE IF EXISTS "+ Utilidades.TABLA_ATTENTION_ORDER_DETAIL);
        db.execSQL("DROP TABLE IF EXISTS "+ Utilidades.TABLA_SALE);
        onCreate(db);

    }



}



