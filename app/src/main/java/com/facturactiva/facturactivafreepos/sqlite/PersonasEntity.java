package com.facturactiva.facturactivafreepos.sqlite;

import android.database.sqlite.SQLiteDatabase;

import com.facturactiva.facturactivafreepos.models.Persona;
import com.facturactiva.facturactivafreepos.utilities.Utilidades;

public class PersonasEntity {

    public static void insertPerson(ConexionSQLiteHelper conn, Persona persona){

        SQLiteDatabase db = conn.getWritableDatabase();
        String insert = "INSERT INTO " + Utilidades.TABLA_PERSONA + "(" +
                Utilidades.CAMPO_ID_PERS + ", " +
                Utilidades.CAMPO_NOMBRE_PERS + ", " +
                Utilidades.CAMPO_NOMBRES_PERS + ", " +
                Utilidades.CAMPO_AP_PATERNO_PERS + ", " +
                Utilidades.CAMPO_AP_MATERNO_PERS + ", " +
                Utilidades.CAMPO_DIREC_PRINC_PERS + ", " +
                Utilidades.CAMPO_DIREC_SECUND_PERS + ", " +
                Utilidades.CAMPO_COD_UBIG_PRINC_PERS + ", " +
                Utilidades.CAMPO_COD_UBIG_SECUND_PERS + ", " +
                Utilidades.CAMPO_COD_PAIS_PERS + ", " +
                Utilidades.CAMPO_TELEFONO_PERS + ", " +
                Utilidades.CAMPO_FAX_PERS + ", " +
                Utilidades.CAMPO_TIPO_PERSONA_PERS + ", " +
                Utilidades.CAMPO_TIPO_DOC_PRINC_PERS + ", " +
                Utilidades.CAMPO_NUM_DOC_PRINC_PERS + ", " +
                Utilidades.CAMPO_TIPO_DOC_SECUND_PERS + ", " +
                Utilidades.CAMPO_NUM_DOC_SECUND_PERS + ", " +
                Utilidades.CAMPO_ACTIVO_PERS + ", " +
                Utilidades.CAMPO_USUARIO_CREAC_PERS + ", " +
                Utilidades.CAMPO_FECHA_CREAC_PERS + ", " +
                Utilidades.CAMPO_USUARIO_EDIC_PERS + ", " +
                Utilidades.CAMPO_FECHA_EDIC_PERS + ", " +
                Utilidades.CAMPO_FLAG_PERS + ") VALUES(" +
                String.valueOf(persona.getIdPersona()) + ", " +
                "'" + persona.getNombre() + "', " +
                "'" + persona.getNombres() + "', " +
                "'" + persona.getApellidoPaterno() + "', " +
                "'" + persona.getApellidoMaterno() + "', " +
                "'" + persona.getDireccionPrincipal() + "', " +
                "'" + persona.getDireccionSecundaria() + "', " +
                "'" + persona.getCodUbigeoPrincipal() + "', " +
                "'" + persona.getCodUbigeoSecundario() + "', " +
                "'" + persona.getCodPais() + "', " +
                "'" + persona.getTelefono() + "', " +
                "'" + persona.getFax() + "', " +
                "'" + persona.getTipoPersona() + "', " +
                "'" + persona.getTipoDocPrincipal() + "', " +
                "'" + persona.getNumDocPrincipal() + "', " +
                "'" + persona.getTipoDocSecundario() + "', " +
                "'" + persona.getNumDocSecundario() + "', " +
                String.valueOf(persona.getActivo()) + ", " +
                "'" + persona.getUsuarioCreacion() + "', " +
                "'" + persona.getFechaCreacion() + "', " +
                "'" + persona.getUsuarioEdicion() + "', " +
                "'" + persona.getFechaEdicion() + "', " +
                String.valueOf(0) + ")";

        db.execSQL(insert);
        db.close();

    }























}
