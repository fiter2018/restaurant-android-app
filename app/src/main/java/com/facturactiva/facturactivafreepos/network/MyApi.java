package com.facturactiva.facturactivafreepos.network;


import java.util.List;
import java.util.Map;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Headers;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Path;
import retrofit2.http.Query;
import retrofit2.http.QueryMap;
import retrofit2.http.Url;

/**
 * @author cgonzales.
 */
public interface MyApi {

    @GET("/")
    Call<ResponseBody> fetchIpAddress();

    @GET("/posts")
    Call<ResponseBody> fetchPosts();

    @POST("/users")
    Call<ResponseBody> postUser(@Body RequestBody requestBody);

    @POST("/posts")
    Call<ResponseBody> postPost(@Body RequestBody requestBody);

    //Query single parameter
    @GET("/posts")
    Call<ResponseBody> fetchPostsByUserId(@Query("userId") int userId);

    // Query multiple parameters
    @GET("/posts")
    Call<ResponseBody> fetchPostsByUserIdAndPostId(@Query("userId") int userId, @Query("id") int postId);

    // Query multiple parameters with the same name
    @GET("/posts")
    Call<ResponseBody> fetchPostsByIds(@Query("id") List<Integer> ids);

    // Query: Null parameter
    @GET("/posts")
    Call<ResponseBody> fetchPostsByUserIdAndPostId(@Query("userId") String userId, @Query("id") String postId);

    // QueryMap: When you don't know what parameters to send
    @GET("/posts")
    Call<ResponseBody> fetchPostsByParams(@QueryMap Map<String, String> params);

    // Path: change part of url, putting a value in the url
    @GET("/posts/{id}")
    Call<ResponseBody> fetchPostById(@Path("id") int id); // can throw java.lang.IllegalArgumentException

    // How to send request url with different endpoint
    @GET("https://api.ipify.org")
    Call<ResponseBody> fetchIp();

    // How to send request url with different endpoint: DYNAMIC
    @GET
    Call<ResponseBody> sendRequest(@Url String url);

    // Uploading a file, multipart form data
    @Multipart
    @POST
    Call<ResponseBody> uploadFile(@Url String url, @Part MultipartBody.Part part);

    // Task for class
    @Multipart
    @POST
    Call<ResponseBody> ocr(@Url String url, @Part("apikey") RequestBody apiKey, @Part("language") RequestBody language, @Part MultipartBody.Part file);

    // Send form encoded data
    @FormUrlEncoded
    @POST("/posts")
    Call<ResponseBody> postPost(@Field("id") String id, @Field("userId") String userId, @Field("title") String title, @Field("body") String body);

    // Static header
    @Headers({"Content-Type: application/json",
            "User-Agent: MyRetrofit"})
    @GET("http://httpbin.org/get")
    Call<ResponseBody> sendRequestWithHeaders();

    // Get Access Tocken
    @Headers({"Content-Type: application/json"})
    @POST("/oauth2/token")
    Call<ResponseBody> fetchAccessTocken(@Body RequestBody requestBody);


    // Dynamic header
    @GET("http://httpbin.org/get")
    Call<ResponseBody> sendRequestWithHeaders(@Header("Content-Type") String contentType, @Header("User-Agent") String userAgent);

    // Simple Login
    @GET("https://httpbin.org/basic-auth/myusername/mypassword")
    Call<ResponseBody> auth(@Header("Authorization") String authorization);
/*
    // Use a Comment object instead ResponseBody object
    @GET("/comments/{id}")
    Call<Comment> fetchCommentById(@Path("id") int id);
*/
//    // Use JsonSchema2POJO
//    @GET("/users/{id}")
//    Call<User> fetchUserById(@Path("id") int id);

    /*

    // XML
    @GET("https://www.w3schools.com/xml/simple.xml")
    Call<BreakfastMenu> getMenu();

    */
}
