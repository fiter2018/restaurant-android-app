package com.facturactiva.facturactivafreepos;

import android.app.Application;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.facturactiva.facturactivafreepos.models.Cliente;
import com.facturactiva.facturactivafreepos.models.DocElectronico;
import com.facturactiva.facturactivafreepos.models.DocElectronicoDetalle;
import com.facturactiva.facturactivafreepos.network.HostingFacturactivaServices;

import java.util.ArrayList;

public class FacturactivaFreePOS extends Application {
    static FacturactivaFreePOS instance;
    HostingFacturactivaServices hostingFacturactivaServices;
    static DocElectronico docElectronico;
    static ArrayList<DocElectronicoDetalle> arrayListDocElectDetalle;
    static Cliente cliente;

    public FacturactivaFreePOS(){
        super();
        instance = this;
    }

    public static FacturactivaFreePOS getInstance(){
        return instance;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        AndroidNetworking.initialize(getApplicationContext());
        hostingFacturactivaServices = new HostingFacturactivaServices();


    }

    public static DocElectronico getDocElectronico() {
        return docElectronico;
    }

    public static void setDocElectronico(DocElectronico docElectronico) {
        FacturactivaFreePOS.docElectronico = docElectronico;
    }

    public static ArrayList<DocElectronicoDetalle> getArrayListDocElectDetalle() {
        return arrayListDocElectDetalle;
    }

    public static void setArrayListDocElectDetalle(ArrayList<DocElectronicoDetalle> arrayListDocElectDetalle) {
        FacturactivaFreePOS.arrayListDocElectDetalle = arrayListDocElectDetalle;
    }

    public static Cliente getCliente() {
        return cliente;
    }

    public static void setCliente(Cliente cliente) {
        FacturactivaFreePOS.cliente = cliente;
    }







}
