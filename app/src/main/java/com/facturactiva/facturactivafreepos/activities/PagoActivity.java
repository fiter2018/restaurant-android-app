package com.facturactiva.facturactivafreepos.activities;

import android.app.AlertDialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteStatement;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.facturactiva.facturactivafreepos.FacturactivaFreePOS;
import com.facturactiva.facturactivafreepos.R;
import com.facturactiva.facturactivafreepos.constants.Constants;
import com.facturactiva.facturactivafreepos.models.Cliente;
import com.facturactiva.facturactivafreepos.models.DocElectronico;
import com.facturactiva.facturactivafreepos.models.DocElectronicoDetalle;
import com.facturactiva.facturactivafreepos.models.DocElectronicoImpuesto;
import com.facturactiva.facturactivafreepos.models.Producto;
import com.facturactiva.facturactivafreepos.sqlite.ConexionSQLiteHelper;
import com.facturactiva.facturactivafreepos.sqlite.DocElectronicoDetalleEntity;
import com.facturactiva.facturactivafreepos.sqlite.DocElectronicoEntity;
import com.facturactiva.facturactivafreepos.sqlite.DocElectronicoImpuestoEntity;
import com.facturactiva.facturactivafreepos.utilities.Utilidades;
import com.facturactiva.facturactivafreepos.validations.ClientesValidations;
import com.facturactiva.facturactivafreepos.validations.PagosValidations;

import org.w3c.dom.DOMConfiguration;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

public class PagoActivity extends AppCompatActivity {

    TextView textViewMontoAPagar, textViewVuelto;
    EditText editTextMontoRecib;
    ImageView imageViewEfectivo, imageViewTarjeta;
    Button btnCobrar;
    int tipoPago;
    float montoAPagar, montoRecibido, vuelto;
    String montoAPagarCadena, montoRecibidoCadena;
    Spinner spinnerTipoComprob;
    ArrayList<String> arrayListTiposComprob;
    LinearLayout linearLayoutMontoRec, linearLayoutVuelto;
    ConexionSQLiteHelper conn;
    SharedPreferences sharedPreferences;

    //Controles del popup de ingreso de cliente carrito
    Spinner spinnerTipoPers;
    //Layout Persona Natural
    EditText editTextNomPNat,editTextDniPNat,editTextEmailPNat;
    Button btnGuardarPNat, btnCancelarPNat;
    //Layout Persona Juridica
    EditText editTextNomPJur,editTextRucPJur,editTextEmailPJur;
    Button btnGuardarPJur, btnCancelarPJur;
    ArrayList<String> tipoPersonaArrayList;
    LinearLayout linearLayoutPNat, linearLayoutPJurid;
    public Menu actionBarMenu;

    //SharedPreferences Data
    String idEmisor="";



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pago);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar_pago_activity);
        setSupportActionBar(toolbar);
        conn = new ConexionSQLiteHelper(PagoActivity.this, "facturactiva-service", null, 1);
        sharedPreferences = getSharedPreferences(LoginActivity.MyPREFERENCES, Context.MODE_PRIVATE);

        inicializarControles();
        eventosClick();

    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

    }

    public void inicializarControles(){

        String comprobantePredeterminado = sharedPreferences.getString("comprobantePredeterminado","comprobantePredeterminadoEnCasoDeNull");
        String igv = sharedPreferences.getString("igv","igvEnCasoDeNull");
        String monedaLocal = sharedPreferences.getString("monedaLocal","monedaLocalEnCasoDeNull");
        idEmisor = sharedPreferences.getString("idEmisor","idEmisorEnCasoDeNull");
        String tipoDocEmisor = sharedPreferences.getString("tipoDocEmisor","tipoDocEmisorEnCasoDeNull");
        String numDocEmisor = sharedPreferences.getString("numDocEmisor","numDocEmisorEnCasoDeNull");
        String nombreEmisor = sharedPreferences.getString("nombreEmisor","nombreEmisorEnCasoDeNull");
        String idPuntoEmision = sharedPreferences.getString("idPuntoEmision","idPuntoEmisionEnCasoDeNull");
        String puntoEmision = sharedPreferences.getString("puntoEmision","puntoEmisionEnCasoDeNull");
        String idUsuario = sharedPreferences.getString("idUsuario","idUsuarioEnCasoDeNull");
        String username = sharedPreferences.getString("username","usernameEnCasoDeNull");

        textViewMontoAPagar = (TextView) findViewById(R.id.textViewMontoAPagar);
        montoAPagar = getIntent().getFloatExtra("montoCobrar",0);
        textViewMontoAPagar.setText("S/."+String.valueOf(montoAPagar));
        textViewVuelto = (TextView) findViewById(R.id.textViewVuelto);   textViewVuelto.setText("0.0");
        tipoPago = 1; //1==efectivo, 2==tarjeta
        imageViewEfectivo = (ImageView) findViewById(R.id.imageViewEfectivo);
        imageViewTarjeta = (ImageView) findViewById(R.id.imageViewTarjeta);
        linearLayoutMontoRec = (LinearLayout) findViewById(R.id.linearMontoRecibido);
        linearLayoutVuelto = (LinearLayout) findViewById(R.id.linearVuelto);
        editTextMontoRecib = (EditText) findViewById(R.id.editTextMontoRecib);  editTextMontoRecib.setText(String.valueOf(montoAPagar));
        spinnerTipoComprob = (Spinner) findViewById(R.id.spinnerTipoComprob);
        btnCobrar = (Button) findViewById(R.id.btnCobrarPago);
        arrayListTiposComprob = new ArrayList<String>();
        arrayListTiposComprob.add("Seleccione");
        arrayListTiposComprob.add("Boleta");
        arrayListTiposComprob.add("Factura");
        ArrayAdapter<CharSequence> adapter = new ArrayAdapter(PagoActivity.this, android.R.layout.simple_spinner_item, arrayListTiposComprob);
        spinnerTipoComprob.setAdapter(adapter);
        if(comprobantePredeterminado.equals("03")){
            spinnerTipoComprob.setSelection(1);
        } else if(comprobantePredeterminado.equals("01")){
            spinnerTipoComprob.setSelection(2);
        }

        imageViewEfectivo.setBackgroundResource(R.drawable.btnes_tipo_pago_pressed);
        imageViewTarjeta.setBackgroundResource(R.drawable.btnes_tipo_pago);
        tipoPago = 1;
        editTextMontoRecib.requestFocus();
        editTextMontoRecib.addTextChangedListener(new TextWatcher() {
            public void afterTextChanged(Editable s) {
                try{
                    montoRecibidoCadena = s.toString();
                    if(montoRecibidoCadena.equals("")){
                        montoRecibidoCadena = "0";
                        editTextMontoRecib.setText("0.0");
                    }
                    montoRecibido = Float.parseFloat(montoRecibidoCadena);
                    vuelto = montoRecibido - montoAPagar;
                    textViewVuelto.setText(String.valueOf(vuelto));
                } catch (Exception e){
                    e.printStackTrace();
                    Toast.makeText(PagoActivity.this, "Error con valor en el input = "+s, Toast.LENGTH_SHORT).show();
                }
            }

            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}

            public void onTextChanged(CharSequence s, int start, int before, int count) {}
        });

    }

    public void insertSaleInDatabase(int tipoDeComprobante) {

        //Obteniendo datos a insertar ---------------------------------------------------------------------------------------------------------------------------
        String comprobantePredeterminado = sharedPreferences.getString("comprobantePredeterminado", "comprobantePredeterminadoEnCasoDeNull");
        String igv = sharedPreferences.getString("igv", "igvEnCasoDeNull");
        String monedaLocal = sharedPreferences.getString("monedaLocal", "monedaLocalEnCasoDeNull");
        //String idEmisor = sharedPreferences.getString("idEmisor", "idEmisorEnCasoDeNull");
        String tipoDocEmisor = sharedPreferences.getString("tipoDocEmisor", "tipoDocEmisorEnCasoDeNull");
        String numDocEmisor = sharedPreferences.getString("numDocEmisor", "numDocEmisorEnCasoDeNull");
        String nombreEmisor = sharedPreferences.getString("nombreEmisor", "nombreEmisorEnCasoDeNull");
        String idPuntoEmision = sharedPreferences.getString("idPuntoEmision", "idPuntoEmisionEnCasoDeNull");
        String puntoEmision = sharedPreferences.getString("puntoEmision", "puntoEmisionEnCasoDeNull");
        String idUsuario = sharedPreferences.getString("idUsuario", "idUsuarioEnCasoDeNull");
        String username = sharedPreferences.getString("username", "usernameEnCasoDeNull");
        String serieFactura = sharedPreferences.getString("serieFactura", "serieFacturaEnCasoDeNull");
        String correlativoFactura = sharedPreferences.getString("correlativoFactura", "correlativoFacturaEnCasoDeNull");
        String serieBoleta = sharedPreferences.getString("serieBoleta", "serieBoletaEnCasoDeNull");
        String correlativoBoleta = sharedPreferences.getString("correlativoBoleta", "correlativoBoletaEnCasoDeNull");
        String serieNC = sharedPreferences.getString("serieNC", "serieNCEnCasoDeNull");
        String correlativoNC = sharedPreferences.getString("correlativoNC", "correlativoNCEnCasoDeNull");
        String serieND = sharedPreferences.getString("serieND", "serieNDEnCasoDeNull");
        String correlativoND = sharedPreferences.getString("correlativoND", "correlativoNDEnCasoDeNull");

        String serie = "";
        int correlativo = 0;

        //datetime actual del sistema
        Date date = new Date();
        //DateFormat hourdateFormat0 = new SimpleDateFormat("HH:mm:ss dd/MM/yyyy");
        DateFormat hourdateFormat = new SimpleDateFormat("yyyy-MM-dd");
        //DateFormat hourdateFormat2 = DateFormat.getDateTimeInstance();
        DateFormat hourdateFormat2 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        DateFormat hourdateFormatSinGuiones = new SimpleDateFormat("yyyyMMdd");
        String fechaCreacDate = hourdateFormat.format(date);
        String fechaCreacDatetime = hourdateFormat2.format(date);
        String fechaSinGuiones = hourdateFormatSinGuiones.format(date);

        String tipoDeDocumento = "";
        if (tipoDeComprobante == 1) {
            tipoDeDocumento = "03";
            serie = serieBoleta;
            correlativo = Integer.parseInt(correlativoBoleta);
        } else if (tipoDeComprobante == 2) {
            tipoDeDocumento = "01";
            serie = serieFactura;
            correlativo = Integer.parseInt(correlativoFactura);
        } else if (tipoDeComprobante == 3) {
            tipoDeDocumento = "07";
            serie = serieNC;
            correlativo = Integer.parseInt(correlativoNC);
        } else if (tipoDeComprobante == 4) {
            tipoDeDocumento = "08";
            serie = serieND;
            correlativo = Integer.parseInt(correlativoND);
        }

        String tipoDocReceptor = "";
        String numDocReceptor = "";
        String nombreReceptor = "";
        String direccionDestino = "";
        String correoReceptor  = "";

        SQLiteDatabase db5 = conn.getReadableDatabase();
        Cursor cursor100 = db5.rawQuery("SELECT " + Utilidades.CAMPO_TIPO_DOC_PRINC_PERS + ", " + Utilidades.CAMPO_NUM_DOC_PRINC_PERS + ", " + Utilidades.CAMPO_NOMBRE_PERS + ", " + Utilidades.CAMPO_DIREC_PRINC_PERS + " FROM " + Utilidades.TABLA_PERSONA + " WHERE " + Utilidades.CAMPO_ID_PERS + "=-100",null);
        if(cursor100.getCount() == 1){
            while(cursor100.moveToNext()) {
                tipoDocReceptor = cursor100.getString(0);
                numDocReceptor = cursor100.getString(1);
                nombreReceptor = cursor100.getString(2);
                direccionDestino = cursor100.getString(3);
            }
        }
        cursor100.close();
        Cursor cursor101 = db5.rawQuery("SELECT " + Utilidades.CAMPO_EMAIL_CONTACTO_CLI + " FROM " + Utilidades.TABLA_CLIENTE + " WHERE " + Utilidades.CAMPO_ID_CLIENTE_CLI + "=-100",null);
        if(cursor101.getCount() == 1){
            while(cursor101.moveToNext()) {
                correoReceptor = cursor101.getString(0);
            }
        }
        cursor101.close();
        db5.close();

        //Armamos los objetos e insertamos en BD
        DocElectronico docElectronico = new DocElectronico();
        docElectronico.setIdEmisor(Integer.parseInt(idEmisor));
        docElectronico.setSerie(serie);
        docElectronico.setCorrelativo(correlativo);
        docElectronico.setTipoDocumento(tipoDeDocumento);
        docElectronico.setTipoDocEmisor(tipoDocEmisor);
        docElectronico.setNumDocEmisor(numDocEmisor);
        docElectronico.setIdPuntoEmision(Integer.parseInt(idPuntoEmision));
        docElectronico.setIdUsuario(Integer.parseInt(idUsuario));
        docElectronico.setIdElectronico(numDocEmisor+"-"+tipoDocEmisor+"-"+tipoDeDocumento+"-"+serie+"-"+String.valueOf(correlativo));
        docElectronico.setRucEmisorPrincipal(numDocEmisor);
        docElectronico.setNombreEmisor(nombreEmisor);
        docElectronico.setNombreComercialEmisor(nombreEmisor);
        docElectronico.setDireccionOrigen("");
        docElectronico.setDireccionUbigeo("");
        docElectronico.setCorreoNotificacionEmisor("");
        docElectronico.setTipoDocReceptor(tipoDocReceptor);
        docElectronico.setNumDocReceptor(numDocReceptor);
        docElectronico.setNombreReceptor(nombreReceptor);
        docElectronico.setNombreComercialReceptor("");
        docElectronico.setDireccionDestino(direccionDestino);
        docElectronico.setCorreoNotificacionReceptor("");
        docElectronico.setTipoDocReceptorAsociado("");
        docElectronico.setNumDocReceptorAsociado("");
        docElectronico.setNombreReceptorAsociado("");
        docElectronico.setTipoMoneda("PEN");
        docElectronico.setTipoMonedaDestino("PEN");
        docElectronico.setTipoCambioDestino(0);
        docElectronico.setTipoOperacion("");
        docElectronico.setSustento("");
        docElectronico.setTipoMotivoNotaModificatoria("");
        docElectronico.setMntNeto((float)(montoAPagar/(1+Constants.tasaIgv)));
        docElectronico.setMntExe(0);
        docElectronico.setMntExo(0);
        docElectronico.setMntDescuentoGlobal(0);
        docElectronico.setMntTotalIgv((float)(docElectronico.getMntNeto()*Constants.tasaIgv));
        docElectronico.setMntTotalIsc(0);
        docElectronico.setMntTotal(montoAPagar);
        docElectronico.setMntTotalDescuentos(0);
        docElectronico.setMntTotalGrat(0);
        docElectronico.setMntTotalOtros(0);
        docElectronico.setMntTotalOtrosCargos(0);
        docElectronico.setMntTotalAnticipos(0);
        docElectronico.setFechaEmision(fechaCreacDate);
        docElectronico.setFechaVencimiento("");
        docElectronico.setGlosaDocumento("");
        docElectronico.setContieneAdjunto(0);
        docElectronico.setTipoFormatoRepresentacionImpresa("TICKET");
        docElectronico.setTipoModalidadEmision("01");
        docElectronico.setCodContrato("");
        docElectronico.setCodAgencia("001");
        docElectronico.setCodUbicacion("");
        docElectronico.setCodError("");
        docElectronico.setIdEnvio("");
        docElectronico.setCodEnvio("");
        docElectronico.setIdCuenta(Integer.parseInt(idEmisor));
        docElectronico.setIdTransaccion(tipoDeDocumento+"-"+numDocEmisor+"-"+fechaSinGuiones+"-"+String.valueOf(correlativo));
        docElectronico.setEstadoDocumento("-");   //01: Activo, 02: Anulado, 03: Invalido
        docElectronico.setEstadoEmision("-");  // A: Aceptado, E: Enviado a Sunat, N: Envío erróneo, O: Aceptado con Observación, P: Pendiente de envío a Sunat, R: Rechazado
        docElectronico.setReenvioHabilitado(1);
        docElectronico.setTipoJustificacionReenvio("");
        docElectronico.setUsuarioCreacion(username);
        docElectronico.setFechaCreacion(fechaCreacDatetime);

        DocElectronicoEntity.insertDocElect(conn,docElectronico);


        int secuencial = 0;
        String codProducto = "";
        String descProducto = "";
        float valorUnitProducto = 0;
        int cantProductosItem = 0;
        DocElectronicoDetalle docElectronicoDetalle = null;
        ArrayList<DocElectronicoDetalle> arrayListDocElectDetalle = new ArrayList<DocElectronicoDetalle>();
        SQLiteDatabase db6 = conn.getReadableDatabase();
        Cursor cursor1000 = db6.rawQuery("SELECT * FROM " + Utilidades.TABLA_CARRITO + " WHERE " + Utilidades.CAMPO_ID_EMISOR_CARR + "=" + idEmisor,null);
        while(cursor1000.moveToNext()) {
            secuencial++;

            codProducto = cursor1000.getString(1);
            descProducto = cursor1000.getString(4);
            valorUnitProducto = cursor1000.getFloat(6);
            cantProductosItem = cursor1000.getInt(9);

            docElectronicoDetalle = new DocElectronicoDetalle();
            docElectronicoDetalle.setIdEmisor(Integer.parseInt(idEmisor));
            docElectronicoDetalle.setSerie(serie);
            docElectronicoDetalle.setCorrelativo(correlativo);
            docElectronicoDetalle.setTipoDocumento(tipoDeDocumento);
            docElectronicoDetalle.setTipoDocEmisor(tipoDocEmisor);
            docElectronicoDetalle.setNumDocEmisor(numDocEmisor);
            docElectronicoDetalle.setSecuencial(secuencial);
            docElectronicoDetalle.setCantidadItem(cantProductosItem);
            docElectronicoDetalle.setUnidadMedidaItem("NIU");
            docElectronicoDetalle.setCodItem(codProducto);
            docElectronicoDetalle.setNombreItem(descProducto);
            docElectronicoDetalle.setPrecioItem(valorUnitProducto);
            docElectronicoDetalle.setPrecioItemSinIgv((float)(valorUnitProducto/(1+Constants.tasaIgv)));
            docElectronicoDetalle.setPrecioItemReferencia(0);
            docElectronicoDetalle.setMontoItem(docElectronicoDetalle.getCantidadItem()*docElectronicoDetalle.getPrecioItemSinIgv());
            docElectronicoDetalle.setDescuentoMonto(0);
            docElectronicoDetalle.setCodAfectacionIgv("10");
            docElectronicoDetalle.setTasaIgv(Constants.tasaIgv);
            docElectronicoDetalle.setMontoIgv(docElectronicoDetalle.getMontoItem()*Constants.tasaIgv);
            docElectronicoDetalle.setCodSistemaCalculoIsc("");
            docElectronicoDetalle.setTasaIsc(0);
            docElectronicoDetalle.setMontoIsc(0);
            docElectronicoDetalle.setIdOperacion(idEmisor+"-"+serie+"-"+String.valueOf(correlativo)+"-"+String.valueOf(secuencial));

            DocElectronicoDetalleEntity.insertDocElectDetalle(conn,docElectronicoDetalle);
            arrayListDocElectDetalle.add(docElectronicoDetalle);

        }
        cursor1000.close();
        db6.close();


        DocElectronicoImpuesto docElectronicoImpuesto = new DocElectronicoImpuesto();
        docElectronicoImpuesto.setIdEmisor(Integer.parseInt(idEmisor));
        docElectronicoImpuesto.setSerie(serie);
        docElectronicoImpuesto.setCorrelativo(correlativo);
        docElectronicoImpuesto.setTipoDocumento(tipoDeDocumento);
        docElectronicoImpuesto.setTipoDocEmisor(tipoDocEmisor);
        docElectronicoImpuesto.setNumDocEmisor(numDocEmisor);
        docElectronicoImpuesto.setCodImpuesto("1000");
        docElectronicoImpuesto.setMontoImpuesto(docElectronico.getMntTotalIgv());
        docElectronicoImpuesto.setTasaImpuesto(Constants.tasaIgv);

        DocElectronicoImpuestoEntity.insertDocElectImpuesto(conn,docElectronicoImpuesto);


        Cliente clienteReceptor = new Cliente();
        clienteReceptor.setTipoDocPrincipal(tipoDocReceptor);
        clienteReceptor.setNumDocPrincipal(numDocReceptor);
        clienteReceptor.setNombre(nombreReceptor);
        clienteReceptor.setDireccionPrincipal(direccionDestino);
        clienteReceptor.setEmailContacto(correoReceptor);

        FacturactivaFreePOS.setDocElectronico(docElectronico);
        FacturactivaFreePOS.setArrayListDocElectDetalle(arrayListDocElectDetalle);
        FacturactivaFreePOS.setCliente(clienteReceptor);

        //Limpio Tabla Carrito, Persona y Cliente carritos
        SQLiteDatabase db = conn.getWritableDatabase();
        db.delete(Utilidades.TABLA_CARRITO,null,null);
        db.delete(Utilidades.TABLA_PERSONA,Utilidades.CAMPO_ID_PERS + "=-100",null);
        db.delete(Utilidades.TABLA_CLIENTE,Utilidades.CAMPO_ID_CLIENTE_CLI + "=-100",null);

        //Aumento en 1 correlativo en tabla CONFIG y en SharedPreferences

        SharedPreferences.Editor editor = sharedPreferences.edit();
        if(tipoDeDocumento.equals("01")){
            ContentValues cv = new ContentValues();
            cv.put(Utilidades.CAMPO_VALOR_CONFIG,correlativo+1);
            db.update(Utilidades.TABLA_CONFIGURACION, cv, Utilidades.CAMPO_ID_EMISOR_CONFIG+"="+idEmisor+" AND "+Utilidades.CAMPO_COD_CONFIG+"='correlativoFactura'", null);
            editor.putString("correlativoFactura",String.valueOf(correlativo+1));
        } else if (tipoDeDocumento.equals("03")){
            ContentValues cv = new ContentValues();
            cv.put(Utilidades.CAMPO_VALOR_CONFIG,correlativo+1);
            db.update(Utilidades.TABLA_CONFIGURACION, cv, Utilidades.CAMPO_ID_EMISOR_CONFIG+"="+idEmisor+" AND "+Utilidades.CAMPO_COD_CONFIG+"='correlativoBoleta'", null);
            editor.putString("correlativoBoleta",String.valueOf(correlativo+1));
        } else if (tipoDeDocumento.equals("07")){
            ContentValues cv = new ContentValues();
            cv.put(Utilidades.CAMPO_VALOR_CONFIG,correlativo+1);
            db.update(Utilidades.TABLA_CONFIGURACION, cv, Utilidades.CAMPO_ID_EMISOR_CONFIG+"="+idEmisor+" AND "+Utilidades.CAMPO_COD_CONFIG+"='correlativoNC'", null);
            editor.putString("correlativoNC",String.valueOf(correlativo+1));
        } else if (tipoDeDocumento.equals("08")){
            ContentValues cv = new ContentValues();
            cv.put(Utilidades.CAMPO_VALOR_CONFIG,correlativo+1);
            db.update(Utilidades.TABLA_CONFIGURACION, cv, Utilidades.CAMPO_ID_EMISOR_CONFIG+"="+idEmisor+" AND "+Utilidades.CAMPO_COD_CONFIG+"='correlativoND'", null);
            editor.putString("correlativoND",String.valueOf(correlativo+1));
        }

        db.close();
        editor.commit();

    }

    public void eventosClick(){
        imageViewEfectivo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                imageViewEfectivo.setBackgroundResource(R.drawable.btnes_tipo_pago_pressed);
                imageViewTarjeta.setBackgroundResource(R.drawable.btnes_tipo_pago);
                tipoPago = 1;
                editTextMontoRecib.setText(String.valueOf(montoAPagar));  editTextMontoRecib.setEnabled(true);
                editTextMontoRecib.requestFocus();

            }
        });

        imageViewTarjeta.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                imageViewTarjeta.setBackgroundResource(R.drawable.btnes_tipo_pago_pressed);
                imageViewEfectivo.setBackgroundResource(R.drawable.btnes_tipo_pago);
                tipoPago = 2;
                editTextMontoRecib.setText(String.valueOf(montoAPagar));  editTextMontoRecib.setEnabled(false);
                textViewVuelto.setText("0.0");

            }
        });

        btnCobrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(PagosValidations.validateFormularioPago(editTextMontoRecib,montoAPagar,spinnerTipoComprob)){

                    int tipoDeComprob = spinnerTipoComprob.getSelectedItemPosition();
                    SQLiteDatabase db = conn.getReadableDatabase();
                    Cursor cursor = db.rawQuery("SELECT * FROM "+Utilidades.TABLA_PERSONA+" WHERE "+Utilidades.CAMPO_ID_PERS+" = -100", null);

                    if((tipoDeComprob == 2) && (cursor.getCount() == 0)) { //Si escogió factura y No hay cliente carrito seteado despliego popup

                        //Muestra popup para ingreso de datos del receptor
                        AlertDialog.Builder builder = new AlertDialog.Builder(PagoActivity.this);
                        LayoutInflater inflater = getLayoutInflater();
                        View dialogView = inflater.inflate(R.layout.popup_ingresa_datos_receptor_carrito, null);
                        builder.setView(dialogView);

                        //inicializarComponentesPopUp();
                        linearLayoutPNat = (LinearLayout) dialogView.findViewById(R.id.linearLayoutPNaturalPopupCliCarr);
                        linearLayoutPJurid = (LinearLayout) dialogView.findViewById(R.id.linearLayoutPJuridicaPopupCliCarr);
                        spinnerTipoPers = (Spinner) dialogView.findViewById(R.id.spinnerTipoPersPopupCliCarr);

                        //Lleno Spinner
                        tipoPersonaArrayList = new ArrayList<String>();
                        tipoPersonaArrayList.add("SELECCIONE TIPO DE CLIENTE");
                        tipoPersonaArrayList.add("Persona Natural");
                        tipoPersonaArrayList.add("Persona Jurídica");
                        ArrayAdapter<CharSequence> adapter = new ArrayAdapter(PagoActivity.this, android.R.layout.simple_spinner_item, tipoPersonaArrayList);
                        spinnerTipoPers.setAdapter(adapter);
                        spinnerTipoPers.setSelection(2);
                        linearLayoutPNat.setVisibility(View.INVISIBLE);
                        linearLayoutPJurid.setVisibility(View.VISIBLE);
                        spinnerTipoPers.setEnabled(false);

                        editTextNomPJur = (EditText) dialogView.findViewById(R.id.editTextNomPJurPopupCliCarr);
                        editTextRucPJur = (EditText) dialogView.findViewById(R.id.editTextRucPJurPopupCliCarr);
                        editTextEmailPJur = (EditText) dialogView.findViewById(R.id.editTextEmailPJurPopupCliCarr);
                        btnGuardarPJur = (Button) dialogView.findViewById(R.id.btnGuardarPJurPopupCliCarr);
                        btnCancelarPJur = (Button) dialogView.findViewById(R.id.btnCancelarPJurPopupCliCarr);

                        final AlertDialog dialog = builder.create();

                        //Eventos Clic Guardar PJuridica
                        btnGuardarPJur.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {

                                if (ClientesValidations.validateIngresoCli(editTextNomPJur, editTextRucPJur, editTextEmailPJur, 2)) {
                                    //Recupera datos de persona
                                    String nombres = editTextNomPJur.getText().toString();
                                    String tipoPers = "";
                                    if (spinnerTipoPers.getSelectedItemPosition() == 1) {
                                        tipoPers = "02";
                                    }
                                    if (spinnerTipoPers.getSelectedItemPosition() == 2) {
                                        tipoPers = "01";
                                    }

                                    String telef = "";
                                    String tipoDocPrinc = "06";
                                    String numDocPrincip = editTextRucPJur.getText().toString();
                                    String tipoDocSec = "";
                                    String numDocSec = "";
                                    String direc = "";
                                    int activo = 1;

                                    //Obtenemos datetime actual del sistema
                                    Date date = new Date();
                                    //DateFormat hourdateFormat0 = new SimpleDateFormat("HH:mm:ss dd/MM/yyyy");
                                    DateFormat hourdateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
                                    //DateFormat hourdateFormat2 = DateFormat.getDateTimeInstance();
                                    String fechaCreac = hourdateFormat.format(date);

                                    String emailContac = editTextEmailPJur.getText().toString();

                                    //Inserta persona en tablas persona y cliente, con ID positivo y con ID -100 para agregar al carrito -----------------------------------------------------------------------
                                    SQLiteDatabase db = conn.getWritableDatabase();
                                    //Insertamos en tabla PERSONA recuperando el idPersona
                                    String insert = "INSERT INTO " + Utilidades.TABLA_PERSONA + "(" +
                                            Utilidades.CAMPO_NOMBRE_PERS + ", " +
                                            Utilidades.CAMPO_NOMBRES_PERS + ", " +
                                            Utilidades.CAMPO_AP_PATERNO_PERS + ", " +
                                            Utilidades.CAMPO_AP_MATERNO_PERS + ", " +
                                            Utilidades.CAMPO_DIREC_PRINC_PERS + ", " +
                                            Utilidades.CAMPO_DIREC_SECUND_PERS + ", " +
                                            Utilidades.CAMPO_COD_UBIG_PRINC_PERS + ", " +
                                            Utilidades.CAMPO_COD_UBIG_SECUND_PERS + ", " +
                                            Utilidades.CAMPO_COD_PAIS_PERS + ", " +
                                            Utilidades.CAMPO_TELEFONO_PERS + ", " +
                                            Utilidades.CAMPO_FAX_PERS + ", " +
                                            Utilidades.CAMPO_TIPO_PERSONA_PERS + ", " +
                                            Utilidades.CAMPO_TIPO_DOC_PRINC_PERS + ", " +
                                            Utilidades.CAMPO_NUM_DOC_PRINC_PERS + ", " +
                                            Utilidades.CAMPO_TIPO_DOC_SECUND_PERS + ", " +
                                            Utilidades.CAMPO_NUM_DOC_SECUND_PERS + ", " +
                                            Utilidades.CAMPO_ACTIVO_PERS + ", " +
                                            Utilidades.CAMPO_USUARIO_CREAC_PERS + ", " +
                                            Utilidades.CAMPO_FECHA_CREAC_PERS + ", " +
                                            Utilidades.CAMPO_USUARIO_EDIC_PERS + ", " +
                                            Utilidades.CAMPO_FECHA_EDIC_PERS + ", " +
                                            Utilidades.CAMPO_FLAG_PERS + ") VALUES(" +
                                            "'" + nombres + "', " +
                                            "'', " +
                                            "'', " +
                                            "'', " +
                                            "'" + direc + "', " +
                                            "'', " +
                                            "'', " +
                                            "'', " +
                                            "'', " +
                                            "'" + telef + "', " +
                                            "'', " +
                                            "'" + tipoPers + "', " +
                                            "'" + tipoDocPrinc + "', " +
                                            "'" + numDocPrincip + "', " +
                                            "'" + tipoDocSec + "', " +
                                            "'" + numDocSec + "', " +
                                            String.valueOf(activo) + ", " +
                                            "'" + NavDrawerActivity.login_email_usuario + "', " +
                                            "'" + fechaCreac + "', " +
                                            "'', " +
                                            "'', " +
                                            String.valueOf(0) + ")";

                                    db.execSQL(insert);

                                    int rowid = -10;
                                    Cursor cursor2 = db.rawQuery("SELECT last_insert_rowid()", null);
                                    while (cursor2.moveToNext()) {
                                        rowid = cursor2.getInt(0);
                                    }
                                    cursor2.close();
                                    //Insertamos el cliente en tabla CLIENTE
                                    String insert2 = "INSERT INTO " + Utilidades.TABLA_CLIENTE + "(" +
                                            Utilidades.CAMPO_ID_CLIENTE_CLI + ", " +
                                            Utilidades.CAMPO_EMAIL_CONTACTO_CLI + ", " +
                                            Utilidades.CAMPO_EMAIL_CONFIRMADO_CLI + ", " +
                                            Utilidades.CAMPO_ACTIVO_CLI + ", " +
                                            Utilidades.CAMPO_USUARIO_CREAC_CLI + ", " +
                                            Utilidades.CAMPO_FECHA_CREAC_CLI + ", " +
                                            Utilidades.CAMPO_USUARIO_EDIC_CLI + ", " +
                                            Utilidades.CAMPO_FECHA_EDIC_CLI + ", " +
                                            Utilidades.CAMPO_ID_EMISOR_CREAC_CLI + ", " +
                                            Utilidades.CAMPO_CONDICION_PAGO_CLI + ", " +
                                            Utilidades.CAMPO_FLAG_CLI + ") VALUES(" +
                                            String.valueOf(rowid) + ", " +
                                            "'" + emailContac + "', " +
                                            String.valueOf(0) + ", " +
                                            String.valueOf(activo) + ", " +
                                            "'" + NavDrawerActivity.login_email_usuario + "', " +
                                            "'" + fechaCreac + "', " +
                                            "'', " +
                                            "'', " +
                                            idEmisor + ", " +
                                            String.valueOf(0) + ", " +
                                            String.valueOf(0) + ")";

                                    db.execSQL(insert2);

                                    //Insertamos en tabla PERSONA con ID = -100
                                    String insert3 = "INSERT INTO " + Utilidades.TABLA_PERSONA + "(" +
                                            Utilidades.CAMPO_ID_PERS + ", " +
                                            Utilidades.CAMPO_NOMBRE_PERS + ", " +
                                            Utilidades.CAMPO_NOMBRES_PERS + ", " +
                                            Utilidades.CAMPO_AP_PATERNO_PERS + ", " +
                                            Utilidades.CAMPO_AP_MATERNO_PERS + ", " +
                                            Utilidades.CAMPO_DIREC_PRINC_PERS + ", " +
                                            Utilidades.CAMPO_DIREC_SECUND_PERS + ", " +
                                            Utilidades.CAMPO_COD_UBIG_PRINC_PERS + ", " +
                                            Utilidades.CAMPO_COD_UBIG_SECUND_PERS + ", " +
                                            Utilidades.CAMPO_COD_PAIS_PERS + ", " +
                                            Utilidades.CAMPO_TELEFONO_PERS + ", " +
                                            Utilidades.CAMPO_FAX_PERS + ", " +
                                            Utilidades.CAMPO_TIPO_PERSONA_PERS + ", " +
                                            Utilidades.CAMPO_TIPO_DOC_PRINC_PERS + ", " +
                                            Utilidades.CAMPO_NUM_DOC_PRINC_PERS + ", " +
                                            Utilidades.CAMPO_TIPO_DOC_SECUND_PERS + ", " +
                                            Utilidades.CAMPO_NUM_DOC_SECUND_PERS + ", " +
                                            Utilidades.CAMPO_ACTIVO_PERS + ", " +
                                            Utilidades.CAMPO_USUARIO_CREAC_PERS + ", " +
                                            Utilidades.CAMPO_FECHA_CREAC_PERS + ", " +
                                            Utilidades.CAMPO_USUARIO_EDIC_PERS + ", " +
                                            Utilidades.CAMPO_FECHA_EDIC_PERS + ", " +
                                            Utilidades.CAMPO_FLAG_PERS + ") VALUES(" +
                                            String.valueOf(-100) + ", " +
                                            "'" + nombres + "', " +
                                            "'', " +
                                            "'', " +
                                            "'', " +
                                            "'" + direc + "', " +
                                            "'', " +
                                            "'', " +
                                            "'', " +
                                            "'', " +
                                            "'" + telef + "', " +
                                            "'', " +
                                            "'" + tipoPers + "', " +
                                            "'" + tipoDocPrinc + "', " +
                                            "'" + numDocPrincip + "', " +
                                            "'" + tipoDocSec + "', " +
                                            "'" + numDocSec + "', " +
                                            String.valueOf(activo) + ", " +
                                            "'" + NavDrawerActivity.login_email_usuario + "', " +
                                            "'" + fechaCreac + "', " +
                                            "'', " +
                                            "'', " +
                                            String.valueOf(0) + ")";

                                    db.execSQL(insert3);

                                    //Insertamos en tabla CLIENTE con ID -100
                                    String insert4 = "INSERT INTO " + Utilidades.TABLA_CLIENTE + "(" +
                                            Utilidades.CAMPO_ID_CLIENTE_CLI + ", " +
                                            Utilidades.CAMPO_EMAIL_CONTACTO_CLI + ", " +
                                            Utilidades.CAMPO_EMAIL_CONFIRMADO_CLI + ", " +
                                            Utilidades.CAMPO_ACTIVO_CLI + ", " +
                                            Utilidades.CAMPO_USUARIO_CREAC_CLI + ", " +
                                            Utilidades.CAMPO_FECHA_CREAC_CLI + ", " +
                                            Utilidades.CAMPO_USUARIO_EDIC_CLI + ", " +
                                            Utilidades.CAMPO_FECHA_EDIC_CLI + ", " +
                                            Utilidades.CAMPO_ID_EMISOR_CREAC_CLI + ", " +
                                            Utilidades.CAMPO_CONDICION_PAGO_CLI + ", " +
                                            Utilidades.CAMPO_FLAG_CLI + ") VALUES(" +
                                            String.valueOf(-100) + ", " +
                                            "'" + emailContac + "', " +
                                            String.valueOf(0) + ", " +
                                            String.valueOf(activo) + ", " +
                                            "'" + NavDrawerActivity.login_email_usuario + "', " +
                                            "'" + fechaCreac + "', " +
                                            "'', " +
                                            "'', " +
                                            idEmisor + ", " +
                                            String.valueOf(0) + ", " +
                                            String.valueOf(0) + ")";

                                    db.execSQL(insert4);
                                    db.close();

                                    //Limpia controles y cambia imagen de persona en el actionBar  --------------------
                                    spinnerTipoPers.setSelection(0);
                                    editTextNomPJur.setText("");
                                    editTextRucPJur.setText("");
                                    editTextEmailPJur.setText("");

                                    //Cierra popup
                                    dialog.cancel();

                                    insertSaleInDatabase(tipoDeComprob);
                                    //Transacción Exitosa, pasa al siguiente activity - VentaRealizada
                                    Intent i = new Intent(PagoActivity.this, VentaRealizadaActivity.class);
                                    //i.putExtra("montoCobrar", montoCobrar);
                                    startActivity(i);
                                    finish();
                                }
                            }
                        });

                        btnCancelarPJur.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                dialog.cancel();
                            }
                        });

                        dialog.show();

                        // -----------------------------------------------------------------------------
                        cursor.close();
                        db.close();
                        return;
                    }

                    cursor.close();
                    db.close();

                    insertSaleInDatabase(tipoDeComprob);
                    Intent i = new Intent(PagoActivity.this, VentaRealizadaActivity.class);
                    //i.putExtra("montoCobrar", montoCobrar);
                    startActivity(i);
                    finish();

                }
            }
        });

    }

    private void alert(String s) {
        Toast.makeText(PagoActivity.this,s,Toast.LENGTH_SHORT).show();
    }








}
