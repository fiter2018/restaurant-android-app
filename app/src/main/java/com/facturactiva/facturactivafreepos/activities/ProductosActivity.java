package com.facturactiva.facturactivafreepos.activities;

import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.facturactiva.facturactivafreepos.R;
import com.facturactiva.facturactivafreepos.adapters.ListaProductosAdapter;
import com.facturactiva.facturactivafreepos.models.Categoria;
import com.facturactiva.facturactivafreepos.models.Producto;
import com.facturactiva.facturactivafreepos.sqlite.CategoriasEntity;
import com.facturactiva.facturactivafreepos.sqlite.ConexionSQLiteHelper;
import com.facturactiva.facturactivafreepos.utilities.Utilidades;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

public class ProductosActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener{

    NavigationView navigationView;
    Button btnSiLogout, btnNoLogout, btnOkCerrarApp, btnCancelCerrarApp, btnNuevCategor, btnGuardarCateg, btnCancelarCateg;
    SharedPreferences sharedPreferences;
    String login_way;
    public static String nombreEmpresa="", login_email_usuario="", login_pass_usuario="";
    public static int idEmisor = -1;
    public Menu actionBarMenu;

    Spinner spinnerCategInProdActiv;
    RecyclerView productosRecyclerView;
    ListaProductosAdapter listaProductosAdapter;
    RecyclerView.LayoutManager layoutManagerProductos;
    ArrayList<Producto> arrayListProductos;
    private static String TAG = "FacturactivaFreePOS";
    private int spanCount = 2;
    EditText editTextCodCateg, editTextDescCateg, editTextAbrevCateg;
    ArrayList<Categoria> arrayListValoresCategorias;
    ArrayList<String> arrayListMostrarCategorias;
    public ConexionSQLiteHelper conn;

    //Contextual Action Mode Variables
    public boolean inActionMode = false;
    public TextView textViewCounter;
    private ArrayList<Producto> selectedProducts = new ArrayList<Producto>();
    public Toolbar toolbar;





    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_productos);
        toolbar = (Toolbar) findViewById(R.id.toolbar_productos);
        setSupportActionBar(toolbar);

        conn = new ConexionSQLiteHelper(ProductosActivity.this, "facturactiva-service", null, 1);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab_productos);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(ProductosActivity.this, AgregarProductoActivity.class);
                startActivity(i);
            }
        });

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout_productos);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        navigationView = (NavigationView) findViewById(R.id.nav_view_productos);
        navigationView.setNavigationItemSelectedListener(this);

        sharedPreferences = getSharedPreferences(LoginActivity.MyPREFERENCES, Context.MODE_PRIVATE);

        login_way = sharedPreferences.getString("enter_mode","EnterModeEnCasoDeNull");

        if(login_way.equals("login")) {
            login_email_usuario = sharedPreferences.getString("username","UsernameEnCasoDeNull");
            login_pass_usuario = sharedPreferences.getString("password","PasswordEnCasoDeNull");
            nombreEmpresa = sharedPreferences.getString("nombreEmisor","NombreEmpresaEnCasoDeNull");
            idEmisor = Integer.parseInt( sharedPreferences.getString("idEmisor","idEmisorEnCasoDeNull"));

        }

        if(login_way.equals("login")){
            Menu menu = navigationView.getMenu();
            MenuItem itemProductos = menu.findItem(R.id.nav_productos);
            itemProductos.setChecked(true);
            inicializarDatosUsuario(navigationView);
            inicializarControles();

            eventosClick();
            consultarListasCategorias();
            ArrayAdapter<CharSequence> adapter = new ArrayAdapter(ProductosActivity.this, android.R.layout.simple_spinner_item, arrayListMostrarCategorias);
            spinnerCategInProdActiv.setAdapter(adapter);

            spinnerCategInProdActiv.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                    if(position != 0){
                        String codCateg = arrayListValoresCategorias.get(position-1).getCodCategoria();
                        consultarListasProductosByCategoria(codCateg);
                    } else {
                        consultarListasProductos();
                    }

                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {

                }
            });
            consultarListasProductos();

            /*
            productosRecyclerView.addOnItemTouchListener(new RecyclerItemClickListener(this, productosRecyclerView, new RecyclerItemClickListener.OnItemClickListener() {

                @Override
                public void onItemClick(View view, int position) {
                    if (isMultiSelect){
                        //if multiple selection is enabled then select item on single click else perform normal click on item.
                        multiSelect(position);
                    } else {
                        Toast.makeText(ProductosActivity.this, "CLICKEADO SIN MULTISELECT AT POSITION: "+String.valueOf(position), Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onItemLongClick(View view, int position) {
                    if (!isMultiSelect){
                        selectedIdEmisorCodProductStrings = new ArrayList<String>();
                        isMultiSelect = true;

                        if (actionMode == null){
                            actionMode = startActionMode(ProductosActivity.this); //show ActionMode.
                        }
                    }

                    multiSelect(position);
                }
            }));
            */

        }

    }

    @Override
    protected void onResume() {
        super.onResume();
        consultarListasProductos();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onBackPressed() {

        if(inActionMode){
            finishActionMode();
        } else {
            DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout_productos);
            if (drawer.isDrawerOpen(GravityCompat.START)) {
                drawer.closeDrawer(GravityCompat.START);
            } else {

                AlertDialog.Builder builder = new AlertDialog.Builder(ProductosActivity.this);
                LayoutInflater inflater = getLayoutInflater();
                View dialogView = inflater.inflate(R.layout.popup_salir_aplicacion, null);
                builder.setView(dialogView);

                //inicializarComponentesPopUp();
                btnOkCerrarApp = (Button) dialogView.findViewById(R.id.btn_ok_cerrar_app);
                btnCancelCerrarApp = (Button) dialogView.findViewById(R.id.btn_cancel_cerrar_app);

                final AlertDialog dialog = builder.create();

                //Eventos Clic
                btnOkCerrarApp.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        //moveTaskToBack(true);
                        //dialog.cancel();
                        //Process.killProcess(Process.myPid());
                        //System.exit(0);
                        finishAffinity();

                    }
                });

                btnCancelCerrarApp.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.cancel();
                    }
                });

                dialog.show();

                //---------------------------------------------------------------------------
            }
        }

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.multiselected_items_menu, menu);
        actionBarMenu = menu;
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_delete_multiselected) {
            if(inActionMode){




            }
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();
        if (id == R.id.nav_ordenar) {
            Intent i = new Intent(ProductosActivity.this, NavDrawerActivity.class);
            startActivity(i);
            finish();
        } else if (id == R.id.nav_comprobantes) {
            Intent i = new Intent(ProductosActivity.this, ComprobantesActivity.class);
            startActivity(i);
            finish();
        } else if (id == R.id.nav_productos) {

        } else if (id == R.id.nav_administrar) {
            Intent i = new Intent(ProductosActivity.this, AdministrarActivity.class);
            startActivity(i);
            finish();
        } else if (id == R.id.nav_configuracion) {
            Intent i = new Intent(ProductosActivity.this, ConfiguracionActivity.class);
            startActivity(i);
            finish();
        } else if (id == R.id.nav_logout){

            AlertDialog.Builder builder = new AlertDialog.Builder(ProductosActivity.this);
            LayoutInflater inflater = getLayoutInflater();
            View dialogView = inflater.inflate(R.layout.popup_cerrar_cesion,null);
            builder.setView(dialogView);

            //inicializarComponentesPopUp();
            btnSiLogout = (Button) dialogView.findViewById(R.id.btn_si_logout);
            btnNoLogout = (Button) dialogView.findViewById(R.id.btn_no_logout);

            final AlertDialog dialog = builder.create();

            //Eventos Click
            btnSiLogout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    cerrarSesion();
                    finish();
                    dialog.cancel();
                }
            });

            btnNoLogout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialog.cancel();
                    Menu menu = navigationView.getMenu();
                    MenuItem itemProductos = menu.findItem(R.id.nav_productos);
                    itemProductos.setChecked(true);
                }
            });

            dialog.show();

        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout_productos);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);

        /*Toast.makeText(this, "onConfigurationChanged()", Toast.LENGTH_SHORT).show();
        sharedpreferences = getSharedPreferences(LoginActivity.MyPREFERENCES, Context.MODE_PRIVATE);

        login_way = sharedpreferences.getString("enter_mode","EnterModeEnCasoDeNull");

        if (login_way.equals("login")) {
            Menu menu = navigationView.getMenu();
            MenuItem itemVentas = menu.findItem(R.id.nav_ordenar);
            MenuItem itemComprobantes = menu.findItem(R.id.nav_comprobantes);
            MenuItem itemProductos = menu.findItem(R.id.nav_productos);
            MenuItem itemComentarios = menu.findItem(R.id.nav_comentarios);
            MenuItem itemAdministrar = menu.findItem(R.id.nav_administrar);
            MenuItem itemConfiguracion = menu.findItem(R.id.nav_configuracion);
            MenuItem itemSoporte = menu.findItem(R.id.nav_soporte);

            if (itemEmisiones.isChecked()) {
                FragmentManager fragmentManager = getSupportFragmentManager();
                fragmentManager.beginTransaction().replace(R.id.contenedor_nav_drawer, new EmisionesFragment()).commit();
            } else if (itemConsultas.isChecked()) {
                FragmentManager fragmentManager = getSupportFragmentManager();
                fragmentManager.beginTransaction().replace(R.id.contenedor_nav_drawer, new ConsultasFragment()).commit();
            } else if(itemAdministrar.isChecked()) {
                FragmentManager fragmentManager = getSupportFragmentManager();
                fragmentManager.beginTransaction().replace(R.id.contenedor_nav_drawer, new AdministrarFragment()).commit();
            }
        }

*/
        alert("onConfigurationChanged");
    }

    private void inicializarDatosUsuario(NavigationView navigationView) {
        View hview = navigationView.getHeaderView(0);
        ImageView imageViewLogoEmpresa = (ImageView) hview.findViewById(R.id.imageViewLogoEmpresa);   imageViewLogoEmpresa.setImageResource(R.drawable.imagen_empresa);
        TextView nombEmpresaMenuLat = (TextView) hview.findViewById(R.id.textViewNomEmpLatMenu);
        String parteNombreEmpresa = "";
        if(nombreEmpresa.length() > 25){
            parteNombreEmpresa = nombreEmpresa.substring(0,24);
            nombEmpresaMenuLat.setText(parteNombreEmpresa);
        } else {
            nombEmpresaMenuLat.setText(nombreEmpresa);
        }
        TextView nombUsuarioMenuLat = (TextView) hview.findViewById(R.id.textViewUserLatMenu);    nombUsuarioMenuLat.setText(login_email_usuario);
    }

    private void alert(String s) {
        Toast.makeText(ProductosActivity.this,s,Toast.LENGTH_SHORT).show();
    }

    public void cerrarSesion(){
        SharedPreferences sharedpreferences = getSharedPreferences(LoginActivity.MyPREFERENCES, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedpreferences.edit();
        editor.clear();
        editor.commit();
    }

    public void inicializarControles(){
        textViewCounter = (TextView) findViewById(R.id.textViewProductosToobarLabel);
        textViewCounter.setVisibility(View.INVISIBLE);
        spinnerCategInProdActiv = (Spinner) findViewById(R.id.spinnerCategProdActiv);
        btnNuevCategor = (Button) findViewById(R.id.btnNuevaCategProducActiv);
        productosRecyclerView = (RecyclerView) findViewById(R.id.productosRecyclerView);
    }

    public void eventosClick(){

        btnNuevCategor.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                AlertDialog.Builder builder = new AlertDialog.Builder(ProductosActivity.this);
                LayoutInflater inflater = getLayoutInflater();
                View dialogView = inflater.inflate(R.layout.popup_nuev_categ_prod_activ,null);
                builder.setView(dialogView);

                //inicializarComponentesPopUp();
                editTextDescCateg = (EditText) dialogView.findViewById(R.id.editTextDescCategPopup);
                editTextAbrevCateg = (EditText) dialogView.findViewById(R.id.editTextAbrevCategPopup);
                btnGuardarCateg = (Button) dialogView.findViewById(R.id.btnGuardarCategPopup);
                btnCancelarCateg = (Button) dialogView.findViewById(R.id.btnCancelarCategPopup);

                final AlertDialog dialog = builder.create();

                //eventoClicksPopUp();
                btnGuardarCateg.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        String codCateg = editTextAbrevCateg.getText().toString();
                        String descCateg = editTextDescCateg.getText().toString();
                        String abrevCateg = editTextAbrevCateg.getText().toString();
                        //Obtenemos datetime actual
                        Date date = new Date();
                        DateFormat hourdateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss"); //DateFormat hourdateFormat2 = DateFormat.getDateTimeInstance();
                        String fechaCreac = hourdateFormat.format(date);

                        Categoria categoria = new Categoria();
                        categoria.setIdEmisor(idEmisor);
                        categoria.setCodCategoria(codCateg);
                        categoria.setDescripcion(descCateg);
                        categoria.setAbreviatura(abrevCateg);
                        categoria.setActivo(1);
                        categoria.setUsuarioCreacion(login_email_usuario);
                        categoria.setFechaCreacion(fechaCreac);
                        categoria.setUsuarioEdicion("");
                        categoria.setFechaEdicion("");

                        CategoriasEntity.saveCategory(conn,categoria);

                        editTextDescCateg.setText("");
                        editTextAbrevCateg.setText("");
                        dialog.cancel();
                        alert("Categoría creada exitosamente!");
                        consultarListasCategorias();
                        ArrayAdapter<CharSequence> adapter = new ArrayAdapter(ProductosActivity.this, android.R.layout.simple_spinner_item, arrayListMostrarCategorias);
                        spinnerCategInProdActiv.setAdapter(adapter);

                    }
                });

                btnCancelarCateg.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.cancel();
                    }
                });

                dialog.show();

            }
        });


    }

    private void consultarListasCategorias() {

        SQLiteDatabase db = conn.getReadableDatabase();

        arrayListValoresCategorias = new ArrayList<Categoria>();
        Categoria categoria = null;

        //Select * from
        Cursor cursor = db.rawQuery("SELECT * FROM "+ Utilidades.TABLA_CATEGORIA + " WHERE " + Utilidades.CAMPO_ID_EMISOR_CATEG + "=" + String.valueOf(idEmisor), null);
        while(cursor.moveToNext()){
            categoria = new Categoria();
            categoria.setIdEmisor(cursor.getInt(0));
            categoria.setCodCategoria(cursor.getString(1));
            categoria.setDescripcion(cursor.getString(2));
            categoria.setAbreviatura(cursor.getString(3));
            categoria.setActivo(cursor.getInt(4));
            categoria.setUsuarioCreacion(cursor.getString(5));
            categoria.setFechaCreacion(cursor.getString(6));
            categoria.setUsuarioEdicion(cursor.getString(7));
            categoria.setFechaEdicion(cursor.getString(8));

            arrayListValoresCategorias.add(categoria);
        }
        cursor.close();
        db.close();
        arrayListMostrarCategorias = new ArrayList<String>();
        arrayListMostrarCategorias.add("Seleccione");

        for (int i=0; i<arrayListValoresCategorias.size(); i++){
            arrayListMostrarCategorias.add(arrayListValoresCategorias.get(i).getAbreviatura());
        }

    }

    private void consultarListasProductos() {

        arrayListProductos = new ArrayList<>();

        //LayoutManager
        layoutManagerProductos = new LinearLayoutManager(this);
        productosRecyclerView.setLayoutManager(layoutManagerProductos);

        SQLiteDatabase db = conn.getReadableDatabase();

        Producto producto = null;
        //Select * from
        Cursor cursor = db.rawQuery("SELECT * FROM "+ Utilidades.TABLA_PRODUCTO + " WHERE " + Utilidades.CAMPO_ID_EMISOR_PROD + "=" + String.valueOf(idEmisor) + " ORDER BY " + Utilidades.CAMPO_NRO_PLATO_CARTA_PROD + " ASC", null);
        while(cursor.moveToNext()){
            producto = new Producto();
            producto.setIdEmisor(cursor.getInt(0));
            producto.setCodProducto(cursor.getString(1));
            producto.setCodCategoria(cursor.getString(2));
            producto.setNroPlatoCarta(cursor.getInt(3));
            producto.setDescripcion(cursor.getString(4));
            producto.setTipoMoneda(cursor.getString(5));
            producto.setValorUnitario(cursor.getFloat(6));
            producto.setActivo(cursor.getInt(7));
            producto.setImagePath(cursor.getString(8));
            producto.setUsuarioCreacion(cursor.getString(9));
            producto.setFechaCreacion(cursor.getString(10));
            producto.setUsuarioEdicion(cursor.getString(11));
            producto.setFechaEdicion(cursor.getString(12));

            arrayListProductos.add(producto);
        }
        cursor.close();
        db.close();
        //Adapter
        listaProductosAdapter = new ListaProductosAdapter(arrayListProductos, ProductosActivity.this);
        productosRecyclerView.setAdapter(listaProductosAdapter);



    }

    private void consultarListasProductosByCategoria(String codCategoria) {

        arrayListProductos = new ArrayList<>();

        //LayoutManager
        layoutManagerProductos = new LinearLayoutManager(this);
        productosRecyclerView.setLayoutManager(layoutManagerProductos);

        SQLiteDatabase db = conn.getReadableDatabase();

        Producto producto = null;

        //Select * from
        Cursor cursor = db.rawQuery("SELECT * FROM "+ Utilidades.TABLA_PRODUCTO + " WHERE "+ Utilidades.CAMPO_COD_CATEG_PROD + "='" + codCategoria +"' AND " + Utilidades.CAMPO_ID_EMISOR_PROD + "=" + String.valueOf(idEmisor) + " AND " + Utilidades.CAMPO_ACTIVO_PROD + "=1" + " ORDER BY " + Utilidades.CAMPO_NRO_PLATO_CARTA_PROD + " ASC", null);
        while(cursor.moveToNext()){
            producto = new Producto();
            producto.setIdEmisor(cursor.getInt(0));
            producto.setCodProducto(cursor.getString(1));
            producto.setCodCategoria(cursor.getString(2));
            producto.setNroPlatoCarta(cursor.getInt(3));
            producto.setDescripcion(cursor.getString(4));
            producto.setTipoMoneda(cursor.getString(5));
            producto.setValorUnitario(cursor.getFloat(6));
            producto.setActivo(cursor.getInt(7));
            producto.setImagePath(cursor.getString(8));
            producto.setUsuarioCreacion(cursor.getString(9));
            producto.setFechaCreacion(cursor.getString(10));
            producto.setUsuarioEdicion(cursor.getString(11));
            producto.setFechaEdicion(cursor.getString(12));

            arrayListProductos.add(producto);
        }
        cursor.close();
        db.close();
        //Adapter
        listaProductosAdapter = new ListaProductosAdapter(arrayListProductos, ProductosActivity.this);
        productosRecyclerView.setAdapter(listaProductosAdapter);

    }

    public boolean activeContextualActionMode(View view, int position) {

        if(inActionMode){
            //Nothing
            Toast.makeText(this, "Doing Nothing", Toast.LENGTH_SHORT).show();
        } else {
            //Active Contextual Action Mode
            navigationView.setVisibility(View.GONE);
            toolbar.setBackgroundColor(getResources().getColor(R.color.toolbar_action_mode));
            MenuItem menuItemPerson = actionBarMenu.findItem(R.id.action_delete_multiselected);
            menuItemPerson.setVisible(true);
            inActionMode = true;
            selectedProducts.add(arrayListProductos.get(position));
            listaProductosAdapter.setSelectedProducts(selectedProducts,1, position);
            textViewCounter.setText(selectedProducts.size()+" selected items");
            textViewCounter.setVisibility(View.VISIBLE);
        }

        return true;
    }

    public void prepareSelection(View view, int position){
        if(inActionMode){
            ColorDrawable colorDrawable = (ColorDrawable) view.getBackground();
            if(colorDrawable.getColor() == ContextCompat.getColor(this,R.color.red_color)){
                selectedProducts.remove(arrayListProductos.get(position));
                listaProductosAdapter.setSelectedProducts(selectedProducts,2, position);
                textViewCounter.setText(selectedProducts.size()+" productos seleccionados");
                if(selectedProducts.size() == 0){
                    inActionMode = false;
                    textViewCounter.setVisibility(View.INVISIBLE);
                    MenuItem menuItemPerson = actionBarMenu.findItem(R.id.action_delete_multiselected);
                    menuItemPerson.setVisible(false);
                }
            }
            if(colorDrawable.getColor() == ContextCompat.getColor(this,android.R.color.transparent)){
                selectedProducts.add(arrayListProductos.get(position));
                listaProductosAdapter.setSelectedProducts(selectedProducts,1, position);
                textViewCounter.setText(selectedProducts.size()+" productos seleccionados");
            }
        } else {
            Toast.makeText(this, "No estamos en Action Mode", Toast.LENGTH_SHORT).show();
        }

    }

    public void finishActionMode(){
        inActionMode = false;
        selectedProducts.clear();
        textViewCounter.setText(selectedProducts.size() + "productos seleccionados");
        textViewCounter.setVisibility(View.INVISIBLE);
        MenuItem menuItemPerson = actionBarMenu.findItem(R.id.action_delete_multiselected);
        menuItemPerson.setVisible(false);
        listaProductosAdapter.setSelectedProducts(selectedProducts,3,0);
    }




}






























