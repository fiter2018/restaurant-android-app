package com.facturactiva.facturactivafreepos.activities;

import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.Toast;

import com.facturactiva.facturactivafreepos.R;
import com.facturactiva.facturactivafreepos.adapters.ListaClientesAdapter;
import com.facturactiva.facturactivafreepos.adapters.ListaProductosCarritoAdapter;
import com.facturactiva.facturactivafreepos.constants.Constants;
import com.facturactiva.facturactivafreepos.models.Cliente;
import com.facturactiva.facturactivafreepos.models.Producto;
import com.facturactiva.facturactivafreepos.models.ProductoCarrito;
import com.facturactiva.facturactivafreepos.sqlite.ConexionSQLiteHelper;
import com.facturactiva.facturactivafreepos.utilities.Utilidades;
import com.facturactiva.facturactivafreepos.validations.ClientesValidations;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

public class TicketActualActivity extends AppCompatActivity {


    RecyclerView recyclerViewProductosCarr;
    ListaProductosCarritoAdapter listaProductosCarrAdapter;
    RecyclerView.LayoutManager layoutManagerProductosCarr;
    ArrayList<ProductoCarrito> arrayListProductosCarr;
    Button btnCobrar;
    public ConexionSQLiteHelper conn;
    public static float montoCobrar;
    SharedPreferences sharedPreferences;
    public static String nombreEmpresa="", login_email_usuario="", login_pass_usuario="", login_way="";
    public static int idEmisor = -1;

    //Controles del popup de ingreso de cliente carrito
    Spinner spinnerTipoPers;
    //Layout Persona Natural
    EditText editTextNomPNat,editTextDniPNat,editTextEmailPNat;
    Button btnGuardarPNat, btnCancelarPNat;
    //Layout Persona Juridica
    EditText editTextNomPJur,editTextRucPJur,editTextEmailPJur;
    Button btnGuardarPJur, btnCancelarPJur;
    ArrayList<String> tipoPersonaArrayList;
    LinearLayout linearLayoutPNat, linearLayoutPJurid;
    public Menu actionBarMenu;

    //Controles popup borrar todoo carrito
    Button btnSiBorrarTodoCarrito, btnNoBorrarTodoCarrito;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ticket_actual);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar_ticket_actual_activity);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        conn = new ConexionSQLiteHelper(TicketActualActivity.this, "facturactiva-service", null, 1);

        sharedPreferences = getSharedPreferences(LoginActivity.MyPREFERENCES, Context.MODE_PRIVATE);

        login_way = sharedPreferences.getString("enter_mode","EnterModeEnCasoDeNull");

        if(login_way.equals("login")) {
            login_email_usuario = sharedPreferences.getString("username","UsernameEnCasoDeNull");
            login_pass_usuario = sharedPreferences.getString("password","PasswordEnCasoDeNull");
            nombreEmpresa = sharedPreferences.getString("nombreEmisor","NombreEmpresaEnCasoDeNull");
            idEmisor = Integer.parseInt( sharedPreferences.getString("idEmisor","idEmisorEnCasoDeNull"));
        }

        inicializarControles();
        listarProductosCarrito();
        //Seteo monto en botón
        btnCobrar.setText("Cobrar  S/."+String.valueOf(montoCobrar));
        eventosClick();

    }

    @Override
    protected void onResume() {
        super.onResume();

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.ticket_actual_options_menu, menu);
        actionBarMenu = menu;

        SQLiteDatabase db1 = conn.getReadableDatabase();
        //Consulto si el cliente carrito está seteado
        Cursor cursor = db1.rawQuery("SELECT * FROM " + Utilidades.TABLA_PERSONA + " WHERE " + Utilidades.CAMPO_ID_PERS + " = -100", null);
        if(cursor.getCount() == 1){
            //Seteo al cliente carrito imagen
            MenuItem menuItemPerson = actionBarMenu.findItem(R.id.action_person_ticket_act);
            menuItemPerson.setIcon(R.drawable.user_cheked_icon);
        } else {

            MenuItem menuItemPerson = actionBarMenu.findItem(R.id.action_person_ticket_act);
            menuItemPerson.setIcon(R.drawable.add_user_icon);

        }
        cursor.close();

        //Consulto si el hay algun producto en el carrito
        Cursor cursor2 = db1.rawQuery("SELECT * FROM " + Utilidades.TABLA_CARRITO, null);
        if(cursor2.getCount() == 0){  //Si no hay productos en el carrito
            //Seteo INVISIBLE tachito de basura que borra todoo el ticket
            MenuItem menuItemTacho = actionBarMenu.findItem(R.id.action_delete_carrito);
            menuItemTacho.setVisible(false);
        } else { //Si hay productos en el carrito
            //Seteo VISIBLE tachito de basura que borra todoo el ticket
            MenuItem menuItemTacho = actionBarMenu.findItem(R.id.action_delete_carrito);
            menuItemTacho.setVisible(true);
        }
        cursor2.close();
        db1.close();

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_person_ticket_act) {

            SQLiteDatabase db1 = conn.getReadableDatabase();
            //Consulto si el cliente carrito está seteado
            Cursor cursor10 = db1.rawQuery("SELECT * FROM " + Utilidades.TABLA_PERSONA + " WHERE " + Utilidades.CAMPO_ID_PERS + " = -100", null);

            if (cursor10.getCount() == 0) {   //Cliente carrito No seteado
                //Muestra popup para ingreso de datos del receptor
                AlertDialog.Builder builder = new AlertDialog.Builder(TicketActualActivity.this);
                LayoutInflater inflater = getLayoutInflater();
                View dialogView = inflater.inflate(R.layout.popup_ingresa_datos_receptor_carrito, null);
                builder.setView(dialogView);

                //inicializarComponentesPopUp();
                linearLayoutPNat = (LinearLayout) dialogView.findViewById(R.id.linearLayoutPNaturalPopupCliCarr);
                linearLayoutPJurid = (LinearLayout) dialogView.findViewById(R.id.linearLayoutPJuridicaPopupCliCarr);
                spinnerTipoPers = (Spinner) dialogView.findViewById(R.id.spinnerTipoPersPopupCliCarr);

                //Lleno Spinner
                tipoPersonaArrayList = new ArrayList<String>();
                tipoPersonaArrayList.add("SELECCIONE TIPO DE CLIENTE");
                tipoPersonaArrayList.add("Persona Natural");
                tipoPersonaArrayList.add("Persona Jurídica");
                ArrayAdapter<CharSequence> adapter = new ArrayAdapter(TicketActualActivity.this, android.R.layout.simple_spinner_item, tipoPersonaArrayList);
                spinnerTipoPers.setAdapter(adapter);
                spinnerTipoPers.setSelection(1);
                linearLayoutPNat.setVisibility(View.VISIBLE);
                linearLayoutPJurid.setVisibility(View.INVISIBLE);

                editTextNomPNat = (EditText) dialogView.findViewById(R.id.editTextNomPNatPopupCliCarr);
                editTextDniPNat = (EditText) dialogView.findViewById(R.id.editTextDniPNatPopupCliCarr);
                editTextEmailPNat = (EditText) dialogView.findViewById(R.id.editTextEmailPNatPopupCliCarr);
                btnGuardarPNat = (Button) dialogView.findViewById(R.id.btnGuardarPNatPopupCliCarr);
                btnCancelarPNat = (Button) dialogView.findViewById(R.id.btnCancelarPNatPopupCliCarr);

                editTextNomPJur = (EditText) dialogView.findViewById(R.id.editTextNomPJurPopupCliCarr);
                editTextRucPJur = (EditText) dialogView.findViewById(R.id.editTextRucPJurPopupCliCarr);
                editTextEmailPJur = (EditText) dialogView.findViewById(R.id.editTextEmailPJurPopupCliCarr);
                btnGuardarPJur = (Button) dialogView.findViewById(R.id.btnGuardarPJurPopupCliCarr);
                btnCancelarPJur = (Button) dialogView.findViewById(R.id.btnCancelarPJurPopupCliCarr);

                final AlertDialog dialog = builder.create();

                //Eventos Click
                spinnerTipoPers.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                        if (position == 0) {
                            linearLayoutPNat.setVisibility(View.INVISIBLE);
                            linearLayoutPJurid.setVisibility(View.INVISIBLE);
                        }
                        if (position == 1) {
                            linearLayoutPNat.setVisibility(View.VISIBLE);
                            linearLayoutPJurid.setVisibility(View.INVISIBLE);
                        }
                        if (position == 2) {
                            linearLayoutPNat.setVisibility(View.INVISIBLE);
                            linearLayoutPJurid.setVisibility(View.VISIBLE);
                        }

                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> parent) {

                    }
                });

                //Eventos Clic Guardar PNatural
                btnGuardarPNat.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        if(ClientesValidations.validateIngresoCli(editTextNomPNat, editTextDniPNat, editTextEmailPNat, 1)) {
                            //Recupera datos de persona
                            String nombres = editTextNomPNat.getText().toString();
                            String tipoPers = "";
                            if (spinnerTipoPers.getSelectedItemPosition() == 1) {
                                tipoPers = "02";
                            }
                            if (spinnerTipoPers.getSelectedItemPosition() == 2) {
                                tipoPers = "01";
                            }

                            String telef = "";
                            String tipoDocPrinc = "1";
                            String numDocPrincip = editTextDniPNat.getText().toString();
                            String tipoDocSec = "";
                            String numDocSec = "";
                            String direc = "";
                            int activo = 1;

                            //Obtenemos datetime actual del sistema
                            Date date = new Date();
                            //DateFormat hourdateFormat0 = new SimpleDateFormat("HH:mm:ss dd/MM/yyyy");
                            DateFormat hourdateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
                            //DateFormat hourdateFormat2 = DateFormat.getDateTimeInstance();
                            String fechaCreac = hourdateFormat.format(date);

                            String emailContac = editTextEmailPNat.getText().toString();

                            //Inserta persona en tablas persona y cliente, con ID positivo y con ID -100 para agregar al carrito -----------------------------------------------------------------------
                            SQLiteDatabase db = conn.getWritableDatabase();
                            //Insertamos en tabla PERSONA recuperando el idPersona
                            String insert = "INSERT INTO " + Utilidades.TABLA_PERSONA + "(" +
                                    Utilidades.CAMPO_NOMBRE_PERS + ", " +
                                    Utilidades.CAMPO_NOMBRES_PERS + ", " +
                                    Utilidades.CAMPO_AP_PATERNO_PERS + ", " +
                                    Utilidades.CAMPO_AP_MATERNO_PERS + ", " +
                                    Utilidades.CAMPO_DIREC_PRINC_PERS + ", " +
                                    Utilidades.CAMPO_DIREC_SECUND_PERS + ", " +
                                    Utilidades.CAMPO_COD_UBIG_PRINC_PERS + ", " +
                                    Utilidades.CAMPO_COD_UBIG_SECUND_PERS + ", " +
                                    Utilidades.CAMPO_COD_PAIS_PERS + ", " +
                                    Utilidades.CAMPO_TELEFONO_PERS + ", " +
                                    Utilidades.CAMPO_FAX_PERS + ", " +
                                    Utilidades.CAMPO_TIPO_PERSONA_PERS + ", " +
                                    Utilidades.CAMPO_TIPO_DOC_PRINC_PERS + ", " +
                                    Utilidades.CAMPO_NUM_DOC_PRINC_PERS + ", " +
                                    Utilidades.CAMPO_TIPO_DOC_SECUND_PERS + ", " +
                                    Utilidades.CAMPO_NUM_DOC_SECUND_PERS + ", " +
                                    Utilidades.CAMPO_ACTIVO_PERS + ", " +
                                    Utilidades.CAMPO_USUARIO_CREAC_PERS + ", " +
                                    Utilidades.CAMPO_FECHA_CREAC_PERS + ", " +
                                    Utilidades.CAMPO_USUARIO_EDIC_PERS + ", " +
                                    Utilidades.CAMPO_FECHA_EDIC_PERS + ", " +
                                    Utilidades.CAMPO_FLAG_PERS + ") VALUES(" +
                                    "'" + nombres + "', " +
                                    "'', " +
                                    "'', " +
                                    "'', " +
                                    "'" + direc + "', " +
                                    "'', " +
                                    "'', " +
                                    "'', " +
                                    "'', " +
                                    "'" + telef + "', " +
                                    "'', " +
                                    "'" + tipoPers + "', " +
                                    "'" + tipoDocPrinc + "', " +
                                    "'" + numDocPrincip + "', " +
                                    "'" + tipoDocSec + "', " +
                                    "'" + numDocSec + "', " +
                                    String.valueOf(activo) + ", " +
                                    "'" + NavDrawerActivity.login_email_usuario + "', " +
                                    "'" + fechaCreac + "', " +
                                    "'', " +
                                    "'', " +
                                    String.valueOf(0) + ")";

                            db.execSQL(insert);

                            int rowid = -10;
                            Cursor cursor = db.rawQuery("SELECT last_insert_rowid()", null);
                            while (cursor.moveToNext()) {
                                rowid = cursor.getInt(0);
                            }

                            //Insertamos el cliente en tabla CLIENTE
                            String insert2 = "INSERT INTO " + Utilidades.TABLA_CLIENTE + "(" +
                                    Utilidades.CAMPO_ID_CLIENTE_CLI + ", " +
                                    Utilidades.CAMPO_EMAIL_CONTACTO_CLI + ", " +
                                    Utilidades.CAMPO_EMAIL_CONFIRMADO_CLI + ", " +
                                    Utilidades.CAMPO_ACTIVO_CLI + ", " +
                                    Utilidades.CAMPO_USUARIO_CREAC_CLI + ", " +
                                    Utilidades.CAMPO_FECHA_CREAC_CLI + ", " +
                                    Utilidades.CAMPO_USUARIO_EDIC_CLI + ", " +
                                    Utilidades.CAMPO_FECHA_EDIC_CLI + ", " +
                                    Utilidades.CAMPO_ID_EMISOR_CREAC_CLI + ", " +
                                    Utilidades.CAMPO_CONDICION_PAGO_CLI + ", " +
                                    Utilidades.CAMPO_FLAG_CLI + ") VALUES(" +
                                    String.valueOf(rowid) + ", " +
                                    "'" + emailContac + "', " +
                                    String.valueOf(0) + ", " +
                                    String.valueOf(activo) + ", " +
                                    "'" + NavDrawerActivity.login_email_usuario + "', " +
                                    "'" + fechaCreac + "', " +
                                    "'', " +
                                    "'', " +
                                    String.valueOf(Constants.idEmisor) + ", " +
                                    String.valueOf(0) + ", " +
                                    String.valueOf(0) + ")";

                            db.execSQL(insert2);

                            //Insertamos en tabla PERSONA con ID = -100
                            String insert3 = "INSERT INTO " + Utilidades.TABLA_PERSONA + "(" +
                                    Utilidades.CAMPO_ID_PERS + ", " +
                                    Utilidades.CAMPO_NOMBRE_PERS + ", " +
                                    Utilidades.CAMPO_NOMBRES_PERS + ", " +
                                    Utilidades.CAMPO_AP_PATERNO_PERS + ", " +
                                    Utilidades.CAMPO_AP_MATERNO_PERS + ", " +
                                    Utilidades.CAMPO_DIREC_PRINC_PERS + ", " +
                                    Utilidades.CAMPO_DIREC_SECUND_PERS + ", " +
                                    Utilidades.CAMPO_COD_UBIG_PRINC_PERS + ", " +
                                    Utilidades.CAMPO_COD_UBIG_SECUND_PERS + ", " +
                                    Utilidades.CAMPO_COD_PAIS_PERS + ", " +
                                    Utilidades.CAMPO_TELEFONO_PERS + ", " +
                                    Utilidades.CAMPO_FAX_PERS + ", " +
                                    Utilidades.CAMPO_TIPO_PERSONA_PERS + ", " +
                                    Utilidades.CAMPO_TIPO_DOC_PRINC_PERS + ", " +
                                    Utilidades.CAMPO_NUM_DOC_PRINC_PERS + ", " +
                                    Utilidades.CAMPO_TIPO_DOC_SECUND_PERS + ", " +
                                    Utilidades.CAMPO_NUM_DOC_SECUND_PERS + ", " +
                                    Utilidades.CAMPO_ACTIVO_PERS + ", " +
                                    Utilidades.CAMPO_USUARIO_CREAC_PERS + ", " +
                                    Utilidades.CAMPO_FECHA_CREAC_PERS + ", " +
                                    Utilidades.CAMPO_USUARIO_EDIC_PERS + ", " +
                                    Utilidades.CAMPO_FECHA_EDIC_PERS + ", " +
                                    Utilidades.CAMPO_FLAG_PERS + ") VALUES(" +
                                    String.valueOf(-100) + ", " +
                                    "'" + nombres + "', " +
                                    "'', " +
                                    "'', " +
                                    "'', " +
                                    "'" + direc + "', " +
                                    "'', " +
                                    "'', " +
                                    "'', " +
                                    "'', " +
                                    "'" + telef + "', " +
                                    "'', " +
                                    "'" + tipoPers + "', " +
                                    "'" + tipoDocPrinc + "', " +
                                    "'" + numDocPrincip + "', " +
                                    "'" + tipoDocSec + "', " +
                                    "'" + numDocSec + "', " +
                                    String.valueOf(activo) + ", " +
                                    "'" + NavDrawerActivity.login_email_usuario + "', " +
                                    "'" + fechaCreac + "', " +
                                    "'', " +
                                    "'', " +
                                    String.valueOf(0) + ")";

                            db.execSQL(insert3);

                            //Insertamos en tabla CLIENTE con ID -100
                            String insert4 = "INSERT INTO " + Utilidades.TABLA_CLIENTE + "(" +
                                    Utilidades.CAMPO_ID_CLIENTE_CLI + ", " +
                                    Utilidades.CAMPO_EMAIL_CONTACTO_CLI + ", " +
                                    Utilidades.CAMPO_EMAIL_CONFIRMADO_CLI + ", " +
                                    Utilidades.CAMPO_ACTIVO_CLI + ", " +
                                    Utilidades.CAMPO_USUARIO_CREAC_CLI + ", " +
                                    Utilidades.CAMPO_FECHA_CREAC_CLI + ", " +
                                    Utilidades.CAMPO_USUARIO_EDIC_CLI + ", " +
                                    Utilidades.CAMPO_FECHA_EDIC_CLI + ", " +
                                    Utilidades.CAMPO_ID_EMISOR_CREAC_CLI + ", " +
                                    Utilidades.CAMPO_CONDICION_PAGO_CLI + ", " +
                                    Utilidades.CAMPO_FLAG_CLI + ") VALUES(" +
                                    String.valueOf(-100) + ", " +
                                    "'" + emailContac + "', " +
                                    String.valueOf(0) + ", " +
                                    String.valueOf(activo) + ", " +
                                    "'" + NavDrawerActivity.login_email_usuario + "', " +
                                    "'" + fechaCreac + "', " +
                                    "'', " +
                                    "'', " +
                                    String.valueOf(Constants.idEmisor) + ", " +
                                    String.valueOf(0) + ", " +
                                    String.valueOf(0) + ")";

                            db.execSQL(insert4);
                            db.close();

                            //Limpia controles y cambia imagen de persona en el actionBar  --------------------
                            spinnerTipoPers.setSelection(0);
                            editTextNomPNat.setText("");
                            editTextDniPNat.setText("");
                            editTextEmailPNat.setText("");
                            MenuItem menuItemPerson = actionBarMenu.findItem(R.id.action_person_ticket_act);
                            menuItemPerson.setIcon(R.drawable.user_cheked_icon);

                            //Cierra popup
                            dialog.cancel();

                        }

                    }
                });

                btnCancelarPNat.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.cancel();
                    }
                });

                //Eventos Clic Guardar PJuridica
                btnGuardarPJur.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        if(ClientesValidations.validateIngresoCli(editTextNomPJur, editTextRucPJur, editTextEmailPJur, 2)) {
                            //Recupera datos de persona
                            String nombres = editTextNomPJur.getText().toString();
                            String tipoPers = "";
                            if (spinnerTipoPers.getSelectedItemPosition() == 1) {
                                tipoPers = "02";
                            }
                            if (spinnerTipoPers.getSelectedItemPosition() == 2) {
                                tipoPers = "01";
                            }

                            String telef = "";
                            String tipoDocPrinc = "6";
                            String numDocPrincip = editTextRucPJur.getText().toString();
                            String tipoDocSec = "";
                            String numDocSec = "";
                            String direc = "";
                            int activo = 1;

                            //Obtenemos datetime actual del sistema
                            Date date = new Date();
                            //DateFormat hourdateFormat0 = new SimpleDateFormat("HH:mm:ss dd/MM/yyyy");
                            DateFormat hourdateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
                            //DateFormat hourdateFormat2 = DateFormat.getDateTimeInstance();
                            String fechaCreac = hourdateFormat.format(date);

                            String emailContac = editTextEmailPJur.getText().toString();

                            //Inserta persona en tablas persona y cliente, con ID positivo y con ID -100 para agregar al carrito -----------------------------------------------------------------------
                            SQLiteDatabase db = conn.getWritableDatabase();
                            //Insertamos en tabla PERSONA recuperando el idPersona
                            String insert = "INSERT INTO " + Utilidades.TABLA_PERSONA + "(" +
                                    Utilidades.CAMPO_NOMBRE_PERS + ", " +
                                    Utilidades.CAMPO_NOMBRES_PERS + ", " +
                                    Utilidades.CAMPO_AP_PATERNO_PERS + ", " +
                                    Utilidades.CAMPO_AP_MATERNO_PERS + ", " +
                                    Utilidades.CAMPO_DIREC_PRINC_PERS + ", " +
                                    Utilidades.CAMPO_DIREC_SECUND_PERS + ", " +
                                    Utilidades.CAMPO_COD_UBIG_PRINC_PERS + ", " +
                                    Utilidades.CAMPO_COD_UBIG_SECUND_PERS + ", " +
                                    Utilidades.CAMPO_COD_PAIS_PERS + ", " +
                                    Utilidades.CAMPO_TELEFONO_PERS + ", " +
                                    Utilidades.CAMPO_FAX_PERS + ", " +
                                    Utilidades.CAMPO_TIPO_PERSONA_PERS + ", " +
                                    Utilidades.CAMPO_TIPO_DOC_PRINC_PERS + ", " +
                                    Utilidades.CAMPO_NUM_DOC_PRINC_PERS + ", " +
                                    Utilidades.CAMPO_TIPO_DOC_SECUND_PERS + ", " +
                                    Utilidades.CAMPO_NUM_DOC_SECUND_PERS + ", " +
                                    Utilidades.CAMPO_ACTIVO_PERS + ", " +
                                    Utilidades.CAMPO_USUARIO_CREAC_PERS + ", " +
                                    Utilidades.CAMPO_FECHA_CREAC_PERS + ", " +
                                    Utilidades.CAMPO_USUARIO_EDIC_PERS + ", " +
                                    Utilidades.CAMPO_FECHA_EDIC_PERS + ", " +
                                    Utilidades.CAMPO_FLAG_PERS + ") VALUES(" +
                                    "'" + nombres + "', " +
                                    "'', " +
                                    "'', " +
                                    "'', " +
                                    "'" + direc + "', " +
                                    "'', " +
                                    "'', " +
                                    "'', " +
                                    "'', " +
                                    "'" + telef + "', " +
                                    "'', " +
                                    "'" + tipoPers + "', " +
                                    "'" + tipoDocPrinc + "', " +
                                    "'" + numDocPrincip + "', " +
                                    "'" + tipoDocSec + "', " +
                                    "'" + numDocSec + "', " +
                                    String.valueOf(activo) + ", " +
                                    "'" + NavDrawerActivity.login_email_usuario + "', " +
                                    "'" + fechaCreac + "', " +
                                    "'', " +
                                    "'', " +
                                    String.valueOf(0) + ")";

                            db.execSQL(insert);

                            int rowid = -10;
                            Cursor cursor = db.rawQuery("SELECT last_insert_rowid()", null);
                            while (cursor.moveToNext()) {
                                rowid = cursor.getInt(0);
                            }

                            //Insertamos el cliente en tabla CLIENTE
                            String insert2 = "INSERT INTO " + Utilidades.TABLA_CLIENTE + "(" +
                                    Utilidades.CAMPO_ID_CLIENTE_CLI + ", " +
                                    Utilidades.CAMPO_EMAIL_CONTACTO_CLI + ", " +
                                    Utilidades.CAMPO_EMAIL_CONFIRMADO_CLI + ", " +
                                    Utilidades.CAMPO_ACTIVO_CLI + ", " +
                                    Utilidades.CAMPO_USUARIO_CREAC_CLI + ", " +
                                    Utilidades.CAMPO_FECHA_CREAC_CLI + ", " +
                                    Utilidades.CAMPO_USUARIO_EDIC_CLI + ", " +
                                    Utilidades.CAMPO_FECHA_EDIC_CLI + ", " +
                                    Utilidades.CAMPO_ID_EMISOR_CREAC_CLI + ", " +
                                    Utilidades.CAMPO_CONDICION_PAGO_CLI + ", " +
                                    Utilidades.CAMPO_FLAG_CLI + ") VALUES(" +
                                    String.valueOf(rowid) + ", " +
                                    "'" + emailContac + "', " +
                                    String.valueOf(0) + ", " +
                                    String.valueOf(activo) + ", " +
                                    "'" + NavDrawerActivity.login_email_usuario + "', " +
                                    "'" + fechaCreac + "', " +
                                    "'', " +
                                    "'', " +
                                    String.valueOf(Constants.idEmisor) + ", " +
                                    String.valueOf(0) + ", " +
                                    String.valueOf(0) + ")";

                            db.execSQL(insert2);

                            //Insertamos en tabla PERSONA con ID = -100
                            String insert3 = "INSERT INTO " + Utilidades.TABLA_PERSONA + "(" +
                                    Utilidades.CAMPO_ID_PERS + ", " +
                                    Utilidades.CAMPO_NOMBRE_PERS + ", " +
                                    Utilidades.CAMPO_NOMBRES_PERS + ", " +
                                    Utilidades.CAMPO_AP_PATERNO_PERS + ", " +
                                    Utilidades.CAMPO_AP_MATERNO_PERS + ", " +
                                    Utilidades.CAMPO_DIREC_PRINC_PERS + ", " +
                                    Utilidades.CAMPO_DIREC_SECUND_PERS + ", " +
                                    Utilidades.CAMPO_COD_UBIG_PRINC_PERS + ", " +
                                    Utilidades.CAMPO_COD_UBIG_SECUND_PERS + ", " +
                                    Utilidades.CAMPO_COD_PAIS_PERS + ", " +
                                    Utilidades.CAMPO_TELEFONO_PERS + ", " +
                                    Utilidades.CAMPO_FAX_PERS + ", " +
                                    Utilidades.CAMPO_TIPO_PERSONA_PERS + ", " +
                                    Utilidades.CAMPO_TIPO_DOC_PRINC_PERS + ", " +
                                    Utilidades.CAMPO_NUM_DOC_PRINC_PERS + ", " +
                                    Utilidades.CAMPO_TIPO_DOC_SECUND_PERS + ", " +
                                    Utilidades.CAMPO_NUM_DOC_SECUND_PERS + ", " +
                                    Utilidades.CAMPO_ACTIVO_PERS + ", " +
                                    Utilidades.CAMPO_USUARIO_CREAC_PERS + ", " +
                                    Utilidades.CAMPO_FECHA_CREAC_PERS + ", " +
                                    Utilidades.CAMPO_USUARIO_EDIC_PERS + ", " +
                                    Utilidades.CAMPO_FECHA_EDIC_PERS + ", " +
                                    Utilidades.CAMPO_FLAG_PERS + ") VALUES(" +
                                    String.valueOf(-100) + ", " +
                                    "'" + nombres + "', " +
                                    "'', " +
                                    "'', " +
                                    "'', " +
                                    "'" + direc + "', " +
                                    "'', " +
                                    "'', " +
                                    "'', " +
                                    "'', " +
                                    "'" + telef + "', " +
                                    "'', " +
                                    "'" + tipoPers + "', " +
                                    "'" + tipoDocPrinc + "', " +
                                    "'" + numDocPrincip + "', " +
                                    "'" + tipoDocSec + "', " +
                                    "'" + numDocSec + "', " +
                                    String.valueOf(activo) + ", " +
                                    "'" + NavDrawerActivity.login_email_usuario + "', " +
                                    "'" + fechaCreac + "', " +
                                    "'', " +
                                    "'', " +
                                    String.valueOf(0) + ")";

                            db.execSQL(insert3);

                            //Insertamos en tabla CLIENTE con ID -100
                            String insert4 = "INSERT INTO " + Utilidades.TABLA_CLIENTE + "(" +
                                    Utilidades.CAMPO_ID_CLIENTE_CLI + ", " +
                                    Utilidades.CAMPO_EMAIL_CONTACTO_CLI + ", " +
                                    Utilidades.CAMPO_EMAIL_CONFIRMADO_CLI + ", " +
                                    Utilidades.CAMPO_ACTIVO_CLI + ", " +
                                    Utilidades.CAMPO_USUARIO_CREAC_CLI + ", " +
                                    Utilidades.CAMPO_FECHA_CREAC_CLI + ", " +
                                    Utilidades.CAMPO_USUARIO_EDIC_CLI + ", " +
                                    Utilidades.CAMPO_FECHA_EDIC_CLI + ", " +
                                    Utilidades.CAMPO_ID_EMISOR_CREAC_CLI + ", " +
                                    Utilidades.CAMPO_CONDICION_PAGO_CLI + ", " +
                                    Utilidades.CAMPO_FLAG_CLI + ") VALUES(" +
                                    String.valueOf(-100) + ", " +
                                    "'" + emailContac + "', " +
                                    String.valueOf(0) + ", " +
                                    String.valueOf(activo) + ", " +
                                    "'" + NavDrawerActivity.login_email_usuario + "', " +
                                    "'" + fechaCreac + "', " +
                                    "'', " +
                                    "'', " +
                                    String.valueOf(Constants.idEmisor) + ", " +
                                    String.valueOf(0) + ", " +
                                    String.valueOf(0) + ")";

                            db.execSQL(insert4);
                            db.close();

                            //Limpia controles y cambia imagen de persona en el actionBar  --------------------
                            spinnerTipoPers.setSelection(0);
                            editTextNomPJur.setText("");
                            editTextRucPJur.setText("");
                            editTextEmailPJur.setText("");
                            MenuItem menuItemPerson = actionBarMenu.findItem(R.id.action_person_ticket_act);
                            menuItemPerson.setIcon(R.drawable.user_cheked_icon);

                            //Cierra popup
                            dialog.cancel();

                        }

                    }
                });


                btnCancelarPJur.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.cancel();
                    }
                });

                dialog.show();

                //---------------------------------------------------------------------------

                return true;
            } else {



                return true;
            }
        } else if((id == R.id.action_delete_carrito)){
            AlertDialog.Builder builder = new AlertDialog.Builder(TicketActualActivity.this);
            LayoutInflater inflater = getLayoutInflater();
            View dialogView = inflater.inflate(R.layout.popup_borrar_todo_carrito,null);
            builder.setView(dialogView);

            //inicializarComponentesPopUp();
            btnSiBorrarTodoCarrito = (Button) dialogView.findViewById(R.id.btn_si_borrar_todo_carrito);
            btnNoBorrarTodoCarrito = (Button) dialogView.findViewById(R.id.btn_no_borrar_todo_carrito);

            final AlertDialog dialog = builder.create();

            //Eventos Clic
            btnSiBorrarTodoCarrito.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    SQLiteDatabase db = conn.getWritableDatabase();
                    db.delete(Utilidades.TABLA_CARRITO,null,null);
                    db.close();

                    listarProductosCarrito();
                    btnCobrar.setText("Cobrar  S/."+montoCobrar);

                    dialog.cancel();
                }
            });

            btnNoBorrarTodoCarrito.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialog.cancel();
                }
            });

            dialog.show();

            return true;
        }

        return super.onOptionsItemSelected(item);
    }



    private void inicializarControles() {

        recyclerViewProductosCarr = (RecyclerView) findViewById(R.id.recyclerViewProductosCarrito);
        btnCobrar = (Button) findViewById(R.id.btnCarritoCobrar);

    }




    private void eventosClick() {

        btnCobrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                //Consulto si hay productos en carrito de venta
                SQLiteDatabase db = conn.getReadableDatabase();
                Cursor cursor3 = db.rawQuery("SELECT * FROM "+ Utilidades.TABLA_CARRITO, null);
                //Consulto si el cliente carrito está seteado
                Cursor cursor4 = db.rawQuery("SELECT * FROM "+Utilidades.TABLA_PERSONA+" WHERE "+Utilidades.CAMPO_ID_PERS+" = -100", null);

                if(cursor3.getCount() == 0){
                    alert("No hay productos en Carrito de Venta");
                    return;
                }

                if(((cursor3.getCount() > 0) && (cursor4.getCount() == 0)) && (montoCobrar > 750)){

                    alert("Monto mayor a  S/.750. Debe ingresar datos del cliente.");
                    //Muestra popup para ingreso de datos del receptor
                    AlertDialog.Builder builder = new AlertDialog.Builder(TicketActualActivity.this);
                    LayoutInflater inflater = getLayoutInflater();
                    View dialogView = inflater.inflate(R.layout.popup_ingresa_datos_receptor_carrito,null);
                    builder.setView(dialogView);

                    //inicializarComponentesPopUp();
                    linearLayoutPNat = (LinearLayout) dialogView.findViewById(R.id.linearLayoutPNaturalPopupCliCarr);
                    linearLayoutPJurid = (LinearLayout) dialogView.findViewById(R.id.linearLayoutPJuridicaPopupCliCarr);
                    spinnerTipoPers = (Spinner) dialogView.findViewById(R.id.spinnerTipoPersPopupCliCarr);

                    //Lleno Spinner
                    tipoPersonaArrayList = new ArrayList<String>();
                    tipoPersonaArrayList.add("SELECCIONE TIPO DE CLIENTE");
                    tipoPersonaArrayList.add("Persona Natural");
                    tipoPersonaArrayList.add("Persona Jurídica");
                    ArrayAdapter<CharSequence> adapter = new ArrayAdapter(TicketActualActivity.this, android.R.layout.simple_spinner_item, tipoPersonaArrayList);
                    spinnerTipoPers.setAdapter(adapter);
                    spinnerTipoPers.setSelection(1);
                    linearLayoutPNat.setVisibility(View.VISIBLE);
                    linearLayoutPJurid.setVisibility(View.INVISIBLE);

                    editTextNomPNat = (EditText) dialogView.findViewById(R.id.editTextNomPNatPopupCliCarr);
                    editTextDniPNat = (EditText) dialogView.findViewById(R.id.editTextDniPNatPopupCliCarr);
                    editTextEmailPNat = (EditText) dialogView.findViewById(R.id.editTextEmailPNatPopupCliCarr);
                    btnGuardarPNat = (Button) dialogView.findViewById(R.id.btnGuardarPNatPopupCliCarr);
                    btnCancelarPNat = (Button) dialogView.findViewById(R.id.btnCancelarPNatPopupCliCarr);

                    editTextNomPJur = (EditText) dialogView.findViewById(R.id.editTextNomPJurPopupCliCarr);
                    editTextRucPJur = (EditText) dialogView.findViewById(R.id.editTextRucPJurPopupCliCarr);
                    editTextEmailPJur = (EditText) dialogView.findViewById(R.id.editTextEmailPJurPopupCliCarr);
                    btnGuardarPJur = (Button) dialogView.findViewById(R.id.btnGuardarPJurPopupCliCarr);
                    btnCancelarPJur = (Button) dialogView.findViewById(R.id.btnCancelarPJurPopupCliCarr);

                    final AlertDialog dialog = builder.create();

                    //Eventos Click
                    spinnerTipoPers.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                        @Override
                        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                            if (position==0) {
                                linearLayoutPNat.setVisibility(View.INVISIBLE);
                                linearLayoutPJurid.setVisibility(View.INVISIBLE);
                            }
                            if (position==1) {
                                linearLayoutPNat.setVisibility(View.VISIBLE);
                                linearLayoutPJurid.setVisibility(View.INVISIBLE);
                            }
                            if (position==2) {
                                linearLayoutPNat.setVisibility(View.INVISIBLE);
                                linearLayoutPJurid.setVisibility(View.VISIBLE);
                            }

                        }

                        @Override
                        public void onNothingSelected(AdapterView<?> parent) {

                        }
                    });

                    //Eventos Clic Guardar PNatural
                    btnGuardarPNat.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {

                            if(ClientesValidations.validateIngresoCli(editTextNomPNat,editTextDniPNat,editTextEmailPNat,1)) {
                                //Recupera datos de persona
                                String nombres = editTextNomPNat.getText().toString();
                                String tipoPers = "";
                                if (spinnerTipoPers.getSelectedItemPosition() == 1) {
                                    tipoPers = "02";
                                }
                                if (spinnerTipoPers.getSelectedItemPosition() == 2) {
                                    tipoPers = "01";
                                }

                                String telef = "";
                                String tipoDocPrinc = "1";
                                String numDocPrincip = editTextDniPNat.getText().toString();
                                String tipoDocSec = "";
                                String numDocSec = "";
                                String direc = "";
                                int activo = 1;

                                //Obtenemos datetime actual del sistema
                                Date date = new Date();
                                //DateFormat hourdateFormat0 = new SimpleDateFormat("HH:mm:ss dd/MM/yyyy");
                                DateFormat hourdateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
                                //DateFormat hourdateFormat2 = DateFormat.getDateTimeInstance();
                                String fechaCreac = hourdateFormat.format(date);

                                String emailContac = editTextEmailPNat.getText().toString();

                                //Inserta persona en tablas persona y cliente, con ID positivo y con ID -100 para agregar al carrito -----------------------------------------------------------------------
                                SQLiteDatabase db = conn.getWritableDatabase();
                                //Insertamos en tabla PERSONA recuperando el idPersona
                                String insert = "INSERT INTO " + Utilidades.TABLA_PERSONA + "(" +
                                        Utilidades.CAMPO_NOMBRE_PERS + ", " +
                                        Utilidades.CAMPO_NOMBRES_PERS + ", " +
                                        Utilidades.CAMPO_AP_PATERNO_PERS + ", " +
                                        Utilidades.CAMPO_AP_MATERNO_PERS + ", " +
                                        Utilidades.CAMPO_DIREC_PRINC_PERS + ", " +
                                        Utilidades.CAMPO_DIREC_SECUND_PERS + ", " +
                                        Utilidades.CAMPO_COD_UBIG_PRINC_PERS + ", " +
                                        Utilidades.CAMPO_COD_UBIG_SECUND_PERS + ", " +
                                        Utilidades.CAMPO_COD_PAIS_PERS + ", " +
                                        Utilidades.CAMPO_TELEFONO_PERS + ", " +
                                        Utilidades.CAMPO_FAX_PERS + ", " +
                                        Utilidades.CAMPO_TIPO_PERSONA_PERS + ", " +
                                        Utilidades.CAMPO_TIPO_DOC_PRINC_PERS + ", " +
                                        Utilidades.CAMPO_NUM_DOC_PRINC_PERS + ", " +
                                        Utilidades.CAMPO_TIPO_DOC_SECUND_PERS + ", " +
                                        Utilidades.CAMPO_NUM_DOC_SECUND_PERS + ", " +
                                        Utilidades.CAMPO_ACTIVO_PERS + ", " +
                                        Utilidades.CAMPO_USUARIO_CREAC_PERS + ", " +
                                        Utilidades.CAMPO_FECHA_CREAC_PERS + ", " +
                                        Utilidades.CAMPO_USUARIO_EDIC_PERS + ", " +
                                        Utilidades.CAMPO_FECHA_EDIC_PERS + ", " +
                                        Utilidades.CAMPO_FLAG_PERS + ") VALUES(" +
                                        "'" + nombres + "', " +
                                        "'', " +
                                        "'', " +
                                        "'', " +
                                        "'" + direc + "', " +
                                        "'', " +
                                        "'', " +
                                        "'', " +
                                        "'', " +
                                        "'" + telef + "', " +
                                        "'', " +
                                        "'" + tipoPers + "', " +
                                        "'" + tipoDocPrinc + "', " +
                                        "'" + numDocPrincip + "', " +
                                        "'" + tipoDocSec + "', " +
                                        "'" + numDocSec + "', " +
                                        String.valueOf(activo) + ", " +
                                        "'" + NavDrawerActivity.login_email_usuario + "', " +
                                        "'" + fechaCreac + "', " +
                                        "'', " +
                                        "'', " +
                                        String.valueOf(0) + ")";

                                db.execSQL(insert);

                                int rowid = -10;
                                Cursor cursor = db.rawQuery("SELECT last_insert_rowid()", null);
                                while (cursor.moveToNext()) {
                                    rowid = cursor.getInt(0);
                                }

                                //Insertamos el cliente en tabla CLIENTE
                                String insert2 = "INSERT INTO " + Utilidades.TABLA_CLIENTE + "(" +
                                        Utilidades.CAMPO_ID_CLIENTE_CLI + ", " +
                                        Utilidades.CAMPO_EMAIL_CONTACTO_CLI + ", " +
                                        Utilidades.CAMPO_EMAIL_CONFIRMADO_CLI + ", " +
                                        Utilidades.CAMPO_ACTIVO_CLI + ", " +
                                        Utilidades.CAMPO_USUARIO_CREAC_CLI + ", " +
                                        Utilidades.CAMPO_FECHA_CREAC_CLI + ", " +
                                        Utilidades.CAMPO_USUARIO_EDIC_CLI + ", " +
                                        Utilidades.CAMPO_FECHA_EDIC_CLI + ", " +
                                        Utilidades.CAMPO_ID_EMISOR_CREAC_CLI + ", " +
                                        Utilidades.CAMPO_CONDICION_PAGO_CLI + ", " +
                                        Utilidades.CAMPO_FLAG_CLI + ") VALUES(" +
                                        String.valueOf(rowid) + ", " +
                                        "'" + emailContac + "', " +
                                        String.valueOf(0) + ", " +
                                        String.valueOf(activo) + ", " +
                                        "'" + NavDrawerActivity.login_email_usuario + "', " +
                                        "'" + fechaCreac + "', " +
                                        "'', " +
                                        "'', " +
                                        String.valueOf(NavDrawerActivity.idEmisor) + ", " +
                                        String.valueOf(0) + ", " +
                                        String.valueOf(0) + ")";

                                db.execSQL(insert2);

                                //Insertamos en tabla PERSONA con ID = -100
                                String insert3 = "INSERT INTO " + Utilidades.TABLA_PERSONA + "(" +
                                        Utilidades.CAMPO_ID_PERS + ", " +
                                        Utilidades.CAMPO_NOMBRE_PERS + ", " +
                                        Utilidades.CAMPO_NOMBRES_PERS + ", " +
                                        Utilidades.CAMPO_AP_PATERNO_PERS + ", " +
                                        Utilidades.CAMPO_AP_MATERNO_PERS + ", " +
                                        Utilidades.CAMPO_DIREC_PRINC_PERS + ", " +
                                        Utilidades.CAMPO_DIREC_SECUND_PERS + ", " +
                                        Utilidades.CAMPO_COD_UBIG_PRINC_PERS + ", " +
                                        Utilidades.CAMPO_COD_UBIG_SECUND_PERS + ", " +
                                        Utilidades.CAMPO_COD_PAIS_PERS + ", " +
                                        Utilidades.CAMPO_TELEFONO_PERS + ", " +
                                        Utilidades.CAMPO_FAX_PERS + ", " +
                                        Utilidades.CAMPO_TIPO_PERSONA_PERS + ", " +
                                        Utilidades.CAMPO_TIPO_DOC_PRINC_PERS + ", " +
                                        Utilidades.CAMPO_NUM_DOC_PRINC_PERS + ", " +
                                        Utilidades.CAMPO_TIPO_DOC_SECUND_PERS + ", " +
                                        Utilidades.CAMPO_NUM_DOC_SECUND_PERS + ", " +
                                        Utilidades.CAMPO_ACTIVO_PERS + ", " +
                                        Utilidades.CAMPO_USUARIO_CREAC_PERS + ", " +
                                        Utilidades.CAMPO_FECHA_CREAC_PERS + ", " +
                                        Utilidades.CAMPO_USUARIO_EDIC_PERS + ", " +
                                        Utilidades.CAMPO_FECHA_EDIC_PERS + ", " +
                                        Utilidades.CAMPO_FLAG_PERS + ") VALUES(" +
                                        String.valueOf(-100) + ", " +
                                        "'" + nombres + "', " +
                                        "'', " +
                                        "'', " +
                                        "'', " +
                                        "'" + direc + "', " +
                                        "'', " +
                                        "'', " +
                                        "'', " +
                                        "'', " +
                                        "'" + telef + "', " +
                                        "'', " +
                                        "'" + tipoPers + "', " +
                                        "'" + tipoDocPrinc + "', " +
                                        "'" + numDocPrincip + "', " +
                                        "'" + tipoDocSec + "', " +
                                        "'" + numDocSec + "', " +
                                        String.valueOf(activo) + ", " +
                                        "'" + NavDrawerActivity.login_email_usuario + "', " +
                                        "'" + fechaCreac + "', " +
                                        "'', " +
                                        "'', " +
                                        String.valueOf(0) + ")";

                                db.execSQL(insert3);

                                //Insertamos en tabla CLIENTE con ID -100
                                String insert4 = "INSERT INTO " + Utilidades.TABLA_CLIENTE + "(" +
                                        Utilidades.CAMPO_ID_CLIENTE_CLI + ", " +
                                        Utilidades.CAMPO_EMAIL_CONTACTO_CLI + ", " +
                                        Utilidades.CAMPO_EMAIL_CONFIRMADO_CLI + ", " +
                                        Utilidades.CAMPO_ACTIVO_CLI + ", " +
                                        Utilidades.CAMPO_USUARIO_CREAC_CLI + ", " +
                                        Utilidades.CAMPO_FECHA_CREAC_CLI + ", " +
                                        Utilidades.CAMPO_USUARIO_EDIC_CLI + ", " +
                                        Utilidades.CAMPO_FECHA_EDIC_CLI + ", " +
                                        Utilidades.CAMPO_ID_EMISOR_CREAC_CLI + ", " +
                                        Utilidades.CAMPO_CONDICION_PAGO_CLI + ", " +
                                        Utilidades.CAMPO_FLAG_CLI + ") VALUES(" +
                                        String.valueOf(-100) + ", " +
                                        "'" + emailContac + "', " +
                                        String.valueOf(0) + ", " +
                                        String.valueOf(activo) + ", " +
                                        "'" + NavDrawerActivity.login_email_usuario + "', " +
                                        "'" + fechaCreac + "', " +
                                        "'', " +
                                        "'', " +
                                        String.valueOf(NavDrawerActivity.idEmisor) + ", " +
                                        String.valueOf(0) + ", " +
                                        String.valueOf(0) + ")";

                                db.execSQL(insert4);
                                db.close();

                                //Limpia controles y cambia imagen de persona en el actionBar  --------------------
                                spinnerTipoPers.setSelection(0);
                                editTextNomPNat.setText("");
                                editTextDniPNat.setText("");
                                editTextEmailPNat.setText("");
                                MenuItem menuItemPerson = actionBarMenu.findItem(R.id.action_person_ticket_act);
                                menuItemPerson.setIcon(R.drawable.user_cheked_icon);

                                //Cierra popup
                                dialog.cancel();

                                //Transacción Exitosa, pasa al siguiente activity - Pago
                                Intent i = new Intent(TicketActualActivity.this, PagoActivity.class);
                                i.putExtra("montoCobrar",montoCobrar);
                                startActivity(i);
                                finish();

                            }


                        }
                    });

                    btnCancelarPNat.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            dialog.cancel();
                        }
                    });

                    //Eventos Clic Guardar PJuridica
                    btnGuardarPJur.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {

                            if(ClientesValidations.validateIngresoCli(editTextNomPJur,editTextRucPJur,editTextEmailPJur,2)) {
                                //Recupera datos de persona
                                String nombres = editTextNomPJur.getText().toString();
                                String tipoPers = "";
                                if (spinnerTipoPers.getSelectedItemPosition() == 1) {
                                    tipoPers = "02";
                                }
                                if (spinnerTipoPers.getSelectedItemPosition() == 2) {
                                    tipoPers = "01";
                                }

                                String telef = "";
                                String tipoDocPrinc = "6";
                                String numDocPrincip = editTextRucPJur.getText().toString();
                                String tipoDocSec = "";
                                String numDocSec = "";
                                String direc = "";
                                int activo = 1;

                                //Obtenemos datetime actual del sistema
                                Date date = new Date();
                                //DateFormat hourdateFormat0 = new SimpleDateFormat("HH:mm:ss dd/MM/yyyy");
                                DateFormat hourdateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
                                //DateFormat hourdateFormat2 = DateFormat.getDateTimeInstance();
                                String fechaCreac = hourdateFormat.format(date);

                                String emailContac = editTextEmailPJur.getText().toString();

                                //Inserta persona en tablas persona y cliente, con ID positivo y con ID -100 para agregar al carrito -----------------------------------------------------------------------
                                SQLiteDatabase db = conn.getWritableDatabase();
                                //Insertamos en tabla PERSONA recuperando el idPersona
                                String insert = "INSERT INTO " + Utilidades.TABLA_PERSONA + "(" +
                                        Utilidades.CAMPO_NOMBRE_PERS + ", " +
                                        Utilidades.CAMPO_NOMBRES_PERS + ", " +
                                        Utilidades.CAMPO_AP_PATERNO_PERS + ", " +
                                        Utilidades.CAMPO_AP_MATERNO_PERS + ", " +
                                        Utilidades.CAMPO_DIREC_PRINC_PERS + ", " +
                                        Utilidades.CAMPO_DIREC_SECUND_PERS + ", " +
                                        Utilidades.CAMPO_COD_UBIG_PRINC_PERS + ", " +
                                        Utilidades.CAMPO_COD_UBIG_SECUND_PERS + ", " +
                                        Utilidades.CAMPO_COD_PAIS_PERS + ", " +
                                        Utilidades.CAMPO_TELEFONO_PERS + ", " +
                                        Utilidades.CAMPO_FAX_PERS + ", " +
                                        Utilidades.CAMPO_TIPO_PERSONA_PERS + ", " +
                                        Utilidades.CAMPO_TIPO_DOC_PRINC_PERS + ", " +
                                        Utilidades.CAMPO_NUM_DOC_PRINC_PERS + ", " +
                                        Utilidades.CAMPO_TIPO_DOC_SECUND_PERS + ", " +
                                        Utilidades.CAMPO_NUM_DOC_SECUND_PERS + ", " +
                                        Utilidades.CAMPO_ACTIVO_PERS + ", " +
                                        Utilidades.CAMPO_USUARIO_CREAC_PERS + ", " +
                                        Utilidades.CAMPO_FECHA_CREAC_PERS + ", " +
                                        Utilidades.CAMPO_USUARIO_EDIC_PERS + ", " +
                                        Utilidades.CAMPO_FECHA_EDIC_PERS + ", " +
                                        Utilidades.CAMPO_FLAG_PERS + ") VALUES(" +
                                        "'" + nombres + "', " +
                                        "'', " +
                                        "'', " +
                                        "'', " +
                                        "'" + direc + "', " +
                                        "'', " +
                                        "'', " +
                                        "'', " +
                                        "'', " +
                                        "'" + telef + "', " +
                                        "'', " +
                                        "'" + tipoPers + "', " +
                                        "'" + tipoDocPrinc + "', " +
                                        "'" + numDocPrincip + "', " +
                                        "'" + tipoDocSec + "', " +
                                        "'" + numDocSec + "', " +
                                        String.valueOf(activo) + ", " +
                                        "'" + NavDrawerActivity.login_email_usuario + "', " +
                                        "'" + fechaCreac + "', " +
                                        "'', " +
                                        "'', " +
                                        String.valueOf(0) + ")";

                                db.execSQL(insert);

                                int rowid = -10;
                                Cursor cursor = db.rawQuery("SELECT last_insert_rowid()", null);
                                while (cursor.moveToNext()) {
                                    rowid = cursor.getInt(0);
                                }

                                //Insertamos el cliente en tabla CLIENTE
                                String insert2 = "INSERT INTO " + Utilidades.TABLA_CLIENTE + "(" +
                                        Utilidades.CAMPO_ID_CLIENTE_CLI + ", " +
                                        Utilidades.CAMPO_EMAIL_CONTACTO_CLI + ", " +
                                        Utilidades.CAMPO_EMAIL_CONFIRMADO_CLI + ", " +
                                        Utilidades.CAMPO_ACTIVO_CLI + ", " +
                                        Utilidades.CAMPO_USUARIO_CREAC_CLI + ", " +
                                        Utilidades.CAMPO_FECHA_CREAC_CLI + ", " +
                                        Utilidades.CAMPO_USUARIO_EDIC_CLI + ", " +
                                        Utilidades.CAMPO_FECHA_EDIC_CLI + ", " +
                                        Utilidades.CAMPO_ID_EMISOR_CREAC_CLI + ", " +
                                        Utilidades.CAMPO_CONDICION_PAGO_CLI + ", " +
                                        Utilidades.CAMPO_FLAG_CLI + ") VALUES(" +
                                        String.valueOf(rowid) + ", " +
                                        "'" + emailContac + "', " +
                                        String.valueOf(0) + ", " +
                                        String.valueOf(activo) + ", " +
                                        "'" + NavDrawerActivity.login_email_usuario + "', " +
                                        "'" + fechaCreac + "', " +
                                        "'', " +
                                        "'', " +
                                        String.valueOf(NavDrawerActivity.idEmisor) + ", " +
                                        String.valueOf(0) + ", " +
                                        String.valueOf(0) + ")";

                                db.execSQL(insert2);

                                //Insertamos en tabla PERSONA con ID = -100
                                String insert3 = "INSERT INTO " + Utilidades.TABLA_PERSONA + "(" +
                                        Utilidades.CAMPO_ID_PERS + ", " +
                                        Utilidades.CAMPO_NOMBRE_PERS + ", " +
                                        Utilidades.CAMPO_NOMBRES_PERS + ", " +
                                        Utilidades.CAMPO_AP_PATERNO_PERS + ", " +
                                        Utilidades.CAMPO_AP_MATERNO_PERS + ", " +
                                        Utilidades.CAMPO_DIREC_PRINC_PERS + ", " +
                                        Utilidades.CAMPO_DIREC_SECUND_PERS + ", " +
                                        Utilidades.CAMPO_COD_UBIG_PRINC_PERS + ", " +
                                        Utilidades.CAMPO_COD_UBIG_SECUND_PERS + ", " +
                                        Utilidades.CAMPO_COD_PAIS_PERS + ", " +
                                        Utilidades.CAMPO_TELEFONO_PERS + ", " +
                                        Utilidades.CAMPO_FAX_PERS + ", " +
                                        Utilidades.CAMPO_TIPO_PERSONA_PERS + ", " +
                                        Utilidades.CAMPO_TIPO_DOC_PRINC_PERS + ", " +
                                        Utilidades.CAMPO_NUM_DOC_PRINC_PERS + ", " +
                                        Utilidades.CAMPO_TIPO_DOC_SECUND_PERS + ", " +
                                        Utilidades.CAMPO_NUM_DOC_SECUND_PERS + ", " +
                                        Utilidades.CAMPO_ACTIVO_PERS + ", " +
                                        Utilidades.CAMPO_USUARIO_CREAC_PERS + ", " +
                                        Utilidades.CAMPO_FECHA_CREAC_PERS + ", " +
                                        Utilidades.CAMPO_USUARIO_EDIC_PERS + ", " +
                                        Utilidades.CAMPO_FECHA_EDIC_PERS + ", " +
                                        Utilidades.CAMPO_FLAG_PERS + ") VALUES(" +
                                        String.valueOf(-100) + ", " +
                                        "'" + nombres + "', " +
                                        "'', " +
                                        "'', " +
                                        "'', " +
                                        "'" + direc + "', " +
                                        "'', " +
                                        "'', " +
                                        "'', " +
                                        "'', " +
                                        "'" + telef + "', " +
                                        "'', " +
                                        "'" + tipoPers + "', " +
                                        "'" + tipoDocPrinc + "', " +
                                        "'" + numDocPrincip + "', " +
                                        "'" + tipoDocSec + "', " +
                                        "'" + numDocSec + "', " +
                                        String.valueOf(activo) + ", " +
                                        "'" + NavDrawerActivity.login_email_usuario + "', " +
                                        "'" + fechaCreac + "', " +
                                        "'', " +
                                        "'', " +
                                        String.valueOf(0) + ")";

                                db.execSQL(insert3);

                                //Insertamos en tabla CLIENTE con ID -100
                                String insert4 = "INSERT INTO " + Utilidades.TABLA_CLIENTE + "(" +
                                        Utilidades.CAMPO_ID_CLIENTE_CLI + ", " +
                                        Utilidades.CAMPO_EMAIL_CONTACTO_CLI + ", " +
                                        Utilidades.CAMPO_EMAIL_CONFIRMADO_CLI + ", " +
                                        Utilidades.CAMPO_ACTIVO_CLI + ", " +
                                        Utilidades.CAMPO_USUARIO_CREAC_CLI + ", " +
                                        Utilidades.CAMPO_FECHA_CREAC_CLI + ", " +
                                        Utilidades.CAMPO_USUARIO_EDIC_CLI + ", " +
                                        Utilidades.CAMPO_FECHA_EDIC_CLI + ", " +
                                        Utilidades.CAMPO_ID_EMISOR_CREAC_CLI + ", " +
                                        Utilidades.CAMPO_CONDICION_PAGO_CLI + ", " +
                                        Utilidades.CAMPO_FLAG_CLI + ") VALUES(" +
                                        String.valueOf(-100) + ", " +
                                        "'" + emailContac + "', " +
                                        String.valueOf(0) + ", " +
                                        String.valueOf(activo) + ", " +
                                        "'" + NavDrawerActivity.login_email_usuario + "', " +
                                        "'" + fechaCreac + "', " +
                                        "'', " +
                                        "'', " +
                                        String.valueOf(NavDrawerActivity.idEmisor) + ", " +
                                        String.valueOf(0) + ", " +
                                        String.valueOf(0) + ")";

                                db.execSQL(insert4);
                                db.close();

                                //Limpia controles y cambia imagen de persona en el actionBar  --------------------
                                spinnerTipoPers.setSelection(0);
                                editTextNomPJur.setText("");
                                editTextRucPJur.setText("");
                                editTextEmailPJur.setText("");
                                MenuItem menuItemPerson = actionBarMenu.findItem(R.id.action_person_ticket_act);
                                menuItemPerson.setIcon(R.drawable.user_cheked_icon);

                                //Cierra popup
                                dialog.cancel();

                                //Transacción Exitosa, pasa al siguiente activity - Pago
                                Intent i = new Intent(TicketActualActivity.this, PagoActivity.class);
                                i.putExtra("montoCobrar",montoCobrar);
                                startActivity(i);
                                finish();

                            }

                        }
                    });


                    btnCancelarPJur.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            dialog.cancel();
                        }
                    });

                    dialog.show();

                    // -----------------------------------------------------------------------------
                    return;
                }

                cursor3.close();
                cursor4.close();
                //Transacción Exitosa, pasa al siguiente activity - Pago
                Intent i = new Intent(TicketActualActivity.this, PagoActivity.class);
                i.putExtra("montoCobrar",montoCobrar);
                startActivity(i);
                finish();


            }
        });


    }

    private void listarProductosCarrito() {

        arrayListProductosCarr = new ArrayList<>();

        //LayoutManager
        layoutManagerProductosCarr = new LinearLayoutManager(this);
        recyclerViewProductosCarr.setLayoutManager(layoutManagerProductosCarr);

        SQLiteDatabase db = conn.getReadableDatabase();

        ProductoCarrito productoCarr = null;
        montoCobrar = 0;
        //Select * from
        Cursor cursor = db.rawQuery("SELECT * FROM "+ Utilidades.TABLA_CARRITO + " WHERE " + Utilidades.CAMPO_ID_EMISOR_CARR + "=" + String.valueOf(NavDrawerActivity.idEmisor), null);
        while(cursor.moveToNext()){
            productoCarr = new ProductoCarrito();
            productoCarr.setIdEmisor(cursor.getInt(0));
            productoCarr.setCodProducto(cursor.getString(1));
            productoCarr.setCodCategoria(cursor.getString(2));
            productoCarr.setNroPlatoCarta(cursor.getInt(3));
            productoCarr.setDescripcion(cursor.getString(4));
            productoCarr.setTipoMoneda(cursor.getString(5));
            productoCarr.setValorUnitario(cursor.getFloat(6));
            productoCarr.setActivo(cursor.getInt(7));
            productoCarr.setImagePath(cursor.getString(8));
            productoCarr.setCantidad(cursor.getInt(9));

            montoCobrar += productoCarr.getValorUnitario()*productoCarr.getCantidad();
            arrayListProductosCarr.add(productoCarr);
        }
        cursor.close();
        db.close();

        //Adapter
        listaProductosCarrAdapter = new ListaProductosCarritoAdapter(arrayListProductosCarr, conn,TicketActualActivity.this, btnCobrar, montoCobrar, idEmisor);
        recyclerViewProductosCarr.setAdapter(listaProductosCarrAdapter);

        recyclerViewProductosCarr.setItemAnimator(new DefaultItemAnimator());

    }

    private void alert(String s) {
        Toast.makeText(TicketActualActivity.this,s,Toast.LENGTH_SHORT).show();
    }


}
