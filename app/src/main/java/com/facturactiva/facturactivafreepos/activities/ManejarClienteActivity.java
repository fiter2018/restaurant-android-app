package com.facturactiva.facturactivafreepos.activities;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.Toast;

import com.facturactiva.facturactivafreepos.R;
import com.facturactiva.facturactivafreepos.constants.Constants;
import com.facturactiva.facturactivafreepos.models.Cliente;
import com.facturactiva.facturactivafreepos.sqlite.ConexionSQLiteHelper;
import com.facturactiva.facturactivafreepos.utilities.Utilidades;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

public class ManejarClienteActivity extends AppCompatActivity {

    Spinner spinnerTipoPers;
    //Layout Persona Natural
    EditText editTextNomPNat,editTextDirecPNat,editTextDniPNat,editTextRucPNat,editTextEmailPNat,editTextTelefPNat;
    Button btnGuardarPNat;
    //Layout Persona Juridica
    EditText editTextNomPJur,editTextRucPJur,editTextDirecPJur,editTextEmailPJur,editTextTelefPJur;
    Button btnGuardarPJur;
    ArrayList<String> tipoPersonaArrayList;
    LinearLayout linearLayoutPNat, linearLayoutPJurid;
    public ConexionSQLiteHelper conn = new ConexionSQLiteHelper(ManejarClienteActivity.this, "facturactiva-service", null, 1);


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_manejar_cliente);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar_manejar_cliente_activity);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        inicializarControles();
        eventosClick();
    }


    private void inicializarControles() {

        linearLayoutPNat = (LinearLayout) findViewById(R.id.linearLayoutPNaturalMan);
        linearLayoutPJurid = (LinearLayout) findViewById(R.id.linearLayoutPJuridicaMan);

        spinnerTipoPers = (Spinner) findViewById(R.id.spinnerTipoPersMan);

        //Lleno Spinner
        tipoPersonaArrayList = new ArrayList<String>();
        tipoPersonaArrayList.add("SELECCIONE TIPO DE CLIENTE");
        tipoPersonaArrayList.add("Persona Natural");
        tipoPersonaArrayList.add("Persona Jurídica");
        ArrayAdapter<CharSequence> adapter = new ArrayAdapter(ManejarClienteActivity.this, android.R.layout.simple_spinner_item, tipoPersonaArrayList);
        spinnerTipoPers.setAdapter(adapter);

        editTextNomPNat = (EditText) findViewById(R.id.editTextNomPNatMan);
        //editTextApPatPNat = (EditText) findViewById(R.id.editTextApPatPNatCrud);
        //editTextApMatPNat = (EditText) findViewById(R.id.editTextApMatPNatCrud);
        editTextDniPNat = (EditText) findViewById(R.id.editTextDniPNatMan);
        editTextDirecPNat = (EditText) findViewById(R.id.editTextDirecPNatMan);
        editTextEmailPNat = (EditText) findViewById(R.id.editTextEmailPNatMan);
        editTextTelefPNat = (EditText) findViewById(R.id.editTextTelefPNatMan);
        btnGuardarPNat = (Button) findViewById(R.id.btnAceptPNatMan);

        editTextNomPJur = (EditText) findViewById(R.id.editTextNomPJurMan);
        editTextRucPJur = (EditText) findViewById(R.id.editTextRucPJurMan);
        editTextDirecPJur = (EditText) findViewById(R.id.editTextDirecPJurMan);
        editTextEmailPJur = (EditText) findViewById(R.id.editTextEmailPJurMan);
        editTextTelefPJur = (EditText) findViewById(R.id.editTextTelefPJurMan);
        btnGuardarPJur = (Button) findViewById(R.id.btnAceptPJuridMan);

        //Limpiamos campos --------------------------------------------------------
        spinnerTipoPers.setSelection(0);
        editTextNomPNat.setText("");
        editTextDniPNat.setText("");
        editTextDirecPNat.setText("");
        editTextEmailPNat.setText("");
        editTextTelefPNat.setText("");

        editTextNomPJur.setText("");
        editTextRucPJur.setText("");
        editTextDirecPJur.setText("");
        editTextEmailPJur.setText("");
        editTextTelefPJur.setText("");

    }

    private void eventosClick() {

        spinnerTipoPers.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                if (position==0) {
                    linearLayoutPNat.setVisibility(View.INVISIBLE);
                    linearLayoutPJurid.setVisibility(View.INVISIBLE);
                }
                if (position==1) {
                    linearLayoutPNat.setVisibility(View.VISIBLE);
                    linearLayoutPJurid.setVisibility(View.INVISIBLE);
                }
                if (position==2) {
                    linearLayoutPNat.setVisibility(View.INVISIBLE);
                    linearLayoutPJurid.setVisibility(View.VISIBLE);
                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        btnGuardarPNat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                //Capturamos la data que insertaremos
                String nombres = editTextNomPNat.getText().toString();
                String tipoPers="";
                if(spinnerTipoPers.getSelectedItemPosition()==1){
                    tipoPers = "02";
                }
                if(spinnerTipoPers.getSelectedItemPosition()==2){
                    tipoPers = "01";
                }

                String telef = editTextTelefPNat.getText().toString();

                String tipoDocPrinc = "1";
                String numDocPrincip = editTextDniPNat.getText().toString();
                String tipoDocSec = "";
                String numDocSec = "";
                String direc = editTextDirecPNat.getText().toString();

                int activo = 1;

                //Obtenemos datetime actual del sistema
                Date date = new Date();
                //DateFormat hourdateFormat0 = new SimpleDateFormat("HH:mm:ss dd/MM/yyyy");
                DateFormat hourdateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
                //DateFormat hourdateFormat2 = DateFormat.getDateTimeInstance();
                String fechaCreac = hourdateFormat.format(date);

                String emailContac = editTextEmailPNat.getText().toString();

                //Insertamos cliente en las tablas correspondientes de la BD --------------

                SQLiteDatabase db = conn.getWritableDatabase();
                //Insertamos en tabla PERSONA recuperando el idPersona
                //INSERT INTO general.persona(idPersona, nombre, nombres, apPaterno, apMaterno, direcPrinc, direcSecund, codUbigPrinc, codUbigSec, codPais, telef, fax, tipoPers, tipoDocPrinc, numDocPrinc, tipoDocSec, numDocSec, activo, usuarCreac, fechaCreac, usuarEdic, fechaEdic)
                //  values()
                String insert = "INSERT INTO "+ Utilidades.TABLA_PERSONA+"("+
                        Utilidades.CAMPO_NOMBRE_PERS+", "+
                        Utilidades.CAMPO_NOMBRES_PERS+", "+
                        Utilidades.CAMPO_AP_PATERNO_PERS+", "+
                        Utilidades.CAMPO_AP_MATERNO_PERS+", "+
                        Utilidades.CAMPO_DIREC_PRINC_PERS+", "+
                        Utilidades.CAMPO_DIREC_SECUND_PERS+", "+
                        Utilidades.CAMPO_COD_UBIG_PRINC_PERS+", "+
                        Utilidades.CAMPO_COD_UBIG_SECUND_PERS+", "+
                        Utilidades.CAMPO_COD_PAIS_PERS+", "+
                        Utilidades.CAMPO_TELEFONO_PERS+", "+
                        Utilidades.CAMPO_FAX_PERS+", "+
                        Utilidades.CAMPO_TIPO_PERSONA_PERS+", "+
                        Utilidades.CAMPO_TIPO_DOC_PRINC_PERS+", "+
                        Utilidades.CAMPO_NUM_DOC_PRINC_PERS+", "+
                        Utilidades.CAMPO_TIPO_DOC_SECUND_PERS+", "+
                        Utilidades.CAMPO_NUM_DOC_SECUND_PERS+", "+
                        Utilidades.CAMPO_ACTIVO_PERS+", "+
                        Utilidades.CAMPO_USUARIO_CREAC_PERS+", "+
                        Utilidades.CAMPO_FECHA_CREAC_PERS+", "+
                        Utilidades.CAMPO_USUARIO_EDIC_PERS+", "+
                        Utilidades.CAMPO_FECHA_EDIC_PERS+", "+
                        Utilidades.CAMPO_FLAG_PERS+") VALUES("+
                        "'"+nombres+"', "+
                        "'', "+
                        "'', "+
                        "'', "+
                        "'"+direc+"', "+
                        "'', "+
                        "'', "+
                        "'', "+
                        "'', "+
                        "'"+telef+"', "+
                        "'', "+
                        "'"+tipoPers+"', "+
                        "'"+tipoDocPrinc+"', "+
                        "'"+numDocPrincip+"', "+
                        "'"+tipoDocSec+"', "+
                        "'"+numDocSec+"', "+
                        String.valueOf(activo)+", "+
                        "'"+NavDrawerActivity.login_email_usuario+"', "+
                        "'"+fechaCreac+"', "+
                        "'', "+
                        "'', "+
                        String.valueOf(0)+")";

                db.execSQL(insert);

                int rowid= -10;
                Cursor cursor = db.rawQuery("SELECT last_insert_rowid()", null);
                while(cursor.moveToNext()) {
                    rowid = cursor.getInt(0);
                }

                db.close();

                //Insertamos el cliente en tabla CLIENTE
                SQLiteDatabase db2 = conn.getWritableDatabase();
                String insert2 = "INSERT INTO "+ Utilidades.TABLA_CLIENTE+"("+
                        Utilidades.CAMPO_ID_CLIENTE_CLI+", "+
                        Utilidades.CAMPO_EMAIL_CONTACTO_CLI+", "+
                        Utilidades.CAMPO_EMAIL_CONFIRMADO_CLI+", "+
                        Utilidades.CAMPO_ACTIVO_CLI+", "+
                        Utilidades.CAMPO_USUARIO_CREAC_CLI+", "+
                        Utilidades.CAMPO_FECHA_CREAC_CLI+", "+
                        Utilidades.CAMPO_USUARIO_EDIC_CLI+", "+
                        Utilidades.CAMPO_FECHA_EDIC_CLI+", "+
                        Utilidades.CAMPO_ID_EMISOR_CREAC_CLI+", "+
                        Utilidades.CAMPO_CONDICION_PAGO_CLI+", "+
                        Utilidades.CAMPO_FLAG_CLI+") VALUES("+
                        String.valueOf(rowid)+", "+
                        "'"+emailContac+"', "+
                        String.valueOf(0)+", "+
                        String.valueOf(activo)+", "+
                        "'"+NavDrawerActivity.login_email_usuario+"', "+
                        "'"+fechaCreac+"', "+
                        "'', "+
                        "'', "+
                        String.valueOf(Constants.idEmisor)+", "+
                        String.valueOf(0)+", "+
                        String.valueOf(0)+")";

                db2.execSQL(insert2);
                db2.close();

                //-------------------------------------------------------------------------
                //Limpiamos campos --------------------------------------------------------
                spinnerTipoPers.setSelection(0);
                editTextNomPNat.setText("");
                editTextDniPNat.setText("");
                editTextDirecPNat.setText("");
                editTextEmailPNat.setText("");
                editTextTelefPNat.setText("");
                alert("Cliente agregado exitosamente con rowid = "+rowid);
                finish();
                //-------------------------------------------------------------------------

            }
        });

        btnGuardarPJur.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Insertamos cliente en las tablas correspondientes de la BD --------------
                //Capturamos la data que insertaremos

                String nombres = editTextNomPJur.getText().toString();
                String tipoPers="";
                if(spinnerTipoPers.getSelectedItemPosition()==1){
                    tipoPers = "02";
                }
                if(spinnerTipoPers.getSelectedItemPosition()==2){
                    tipoPers = "01";
                }

                String telef = editTextTelefPJur.getText().toString();

                String tipoDocPrinc = "6";
                String numDocPrincip = editTextRucPJur.getText().toString();
                String tipoDocSec = "";
                String numDocSec = "";
                String direc = editTextDirecPJur.getText().toString();

                int activo = 1;

                //Obtenemos datetime actual del sistema
                Date date = new Date();
                //DateFormat hourdateFormat0 = new SimpleDateFormat("HH:mm:ss dd/MM/yyyy");
                DateFormat hourdateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
                //DateFormat hourdateFormat2 = DateFormat.getDateTimeInstance();
                String fechaCreac = hourdateFormat.format(date);

                String emailContac = editTextEmailPJur.getText().toString();

                //Insertamos cliente en las tablas correspondientes de la BD --------------

                SQLiteDatabase db = conn.getWritableDatabase();
                //Insertamos en tabla PERSONA recuperando el idPersona
                //INSERT INTO general.persona(idPersona, nombre, nombres, apPaterno, apMaterno, direcPrinc, direcSecund, codUbigPrinc, codUbigSec, codPais, telef, fax, tipoPers, tipoDocPrinc, numDocPrinc, tipoDocSec, numDocSec, activo, usuarCreac, fechaCreac, usuarEdic, fechaEdic)
                //  values()
                String insert = "INSERT INTO "+ Utilidades.TABLA_PERSONA+"("+
                        Utilidades.CAMPO_NOMBRE_PERS+", "+
                        Utilidades.CAMPO_NOMBRES_PERS+", "+
                        Utilidades.CAMPO_AP_PATERNO_PERS+", "+
                        Utilidades.CAMPO_AP_MATERNO_PERS+", "+
                        Utilidades.CAMPO_DIREC_PRINC_PERS+", "+
                        Utilidades.CAMPO_DIREC_SECUND_PERS+", "+
                        Utilidades.CAMPO_COD_UBIG_PRINC_PERS+", "+
                        Utilidades.CAMPO_COD_UBIG_SECUND_PERS+", "+
                        Utilidades.CAMPO_COD_PAIS_PERS+", "+
                        Utilidades.CAMPO_TELEFONO_PERS+", "+
                        Utilidades.CAMPO_FAX_PERS+", "+
                        Utilidades.CAMPO_TIPO_PERSONA_PERS+", "+
                        Utilidades.CAMPO_TIPO_DOC_PRINC_PERS+", "+
                        Utilidades.CAMPO_NUM_DOC_PRINC_PERS+", "+
                        Utilidades.CAMPO_TIPO_DOC_SECUND_PERS+", "+
                        Utilidades.CAMPO_NUM_DOC_SECUND_PERS+", "+
                        Utilidades.CAMPO_ACTIVO_PERS+", "+
                        Utilidades.CAMPO_USUARIO_CREAC_PERS+", "+
                        Utilidades.CAMPO_FECHA_CREAC_PERS+", "+
                        Utilidades.CAMPO_USUARIO_EDIC_PERS+", "+
                        Utilidades.CAMPO_FECHA_EDIC_PERS+", "+
                        Utilidades.CAMPO_FLAG_PERS+") VALUES("+
                        "'"+nombres+"', "+
                        "'', "+
                        "'', "+
                        "'', "+
                        "'"+direc+"', "+
                        "'', "+
                        "'', "+
                        "'', "+
                        "'', "+
                        "'"+telef+"', "+
                        "'', "+
                        "'"+tipoPers+"', "+
                        "'"+tipoDocPrinc+"', "+
                        "'"+numDocPrincip+"', "+
                        "'"+tipoDocSec+"', "+
                        "'"+numDocSec+"', "+
                        String.valueOf(activo)+", "+
                        "'"+NavDrawerActivity.login_email_usuario+"', "+
                        "'"+fechaCreac+"', "+
                        "'', "+
                        "'', "+
                        String.valueOf(0)+")";

                db.execSQL(insert);

                int rowid= -10;
                Cursor cursor = db.rawQuery("SELECT last_insert_rowid()", null);
                while(cursor.moveToNext()) {
                    rowid = cursor.getInt(0);
                }

                db.close();

                //Insertamos el cliente en tabla CLIENTE
                SQLiteDatabase db2 = conn.getWritableDatabase();
                String insert2 = "INSERT INTO "+ Utilidades.TABLA_CLIENTE+"("+
                        Utilidades.CAMPO_ID_CLIENTE_CLI+", "+
                        Utilidades.CAMPO_EMAIL_CONTACTO_CLI+", "+
                        Utilidades.CAMPO_EMAIL_CONFIRMADO_CLI+", "+
                        Utilidades.CAMPO_ACTIVO_CLI+", "+
                        Utilidades.CAMPO_USUARIO_CREAC_CLI+", "+
                        Utilidades.CAMPO_FECHA_CREAC_CLI+", "+
                        Utilidades.CAMPO_USUARIO_EDIC_CLI+", "+
                        Utilidades.CAMPO_FECHA_EDIC_CLI+", "+
                        Utilidades.CAMPO_ID_EMISOR_CREAC_CLI+", "+
                        Utilidades.CAMPO_CONDICION_PAGO_CLI+", "+
                        Utilidades.CAMPO_FLAG_CLI+") VALUES("+
                        String.valueOf(rowid)+", "+
                        "'"+emailContac+"', "+
                        String.valueOf(0)+", "+
                        String.valueOf(activo)+", "+
                        "'"+NavDrawerActivity.login_email_usuario+"', "+
                        "'"+fechaCreac+"', "+
                        "'', "+
                        "'', "+
                        String.valueOf(Constants.idEmisor)+", "+
                        String.valueOf(0)+", "+
                        String.valueOf(0)+")";

                db2.execSQL(insert2);
                db2.close();

                //Armo objeto cliente receptor temporal
                NavDrawerActivity.clienteCarrito = new Cliente();
                NavDrawerActivity.clienteCarrito.setIdPersona(rowid);
                NavDrawerActivity.clienteCarrito.setNombre(nombres);
                NavDrawerActivity.clienteCarrito.setNombres("");
                NavDrawerActivity.clienteCarrito.setApellidoPaterno("");
                NavDrawerActivity.clienteCarrito.setApellidoMaterno("");
                NavDrawerActivity.clienteCarrito.setDireccionPrincipal(direc);
                NavDrawerActivity.clienteCarrito.setDireccionSecundaria("");
                NavDrawerActivity.clienteCarrito.setCodUbigeoPrincipal("");
                NavDrawerActivity.clienteCarrito.setCodUbigeoSecundario("");
                NavDrawerActivity.clienteCarrito.setCodPais("");
                NavDrawerActivity.clienteCarrito.setTelefono(telef);
                NavDrawerActivity.clienteCarrito.setFax("");
                NavDrawerActivity.clienteCarrito.setTipoPersona(tipoPers);
                NavDrawerActivity.clienteCarrito.setTipoDocPrincipal(tipoDocPrinc);
                NavDrawerActivity.clienteCarrito.setNumDocPrincipal(numDocPrincip);
                NavDrawerActivity.clienteCarrito.setTipoDocSecundario(tipoDocSec);
                NavDrawerActivity.clienteCarrito.setNumDocSecundario(numDocSec);
                NavDrawerActivity.clienteCarrito.setActivo(activo);
                NavDrawerActivity.clienteCarrito.setUsuarioCreacion(NavDrawerActivity.login_email_usuario);
                NavDrawerActivity.clienteCarrito.setFechaCreacion(fechaCreac);
                NavDrawerActivity.clienteCarrito.setUsuarioEdicion("");
                NavDrawerActivity.clienteCarrito.setFechaEdicion("");

                NavDrawerActivity.clienteCarrito.setIdCliente(rowid);
                NavDrawerActivity.clienteCarrito.setEmailContacto(emailContac);
                NavDrawerActivity.clienteCarrito.setEmailConfirmado(0);
                NavDrawerActivity.clienteCarrito.setActivo(activo);
                NavDrawerActivity.clienteCarrito.setUsuarioCreacion(NavDrawerActivity.login_email_usuario);
                NavDrawerActivity.clienteCarrito.setFechaCreacion(fechaCreac);
                NavDrawerActivity.clienteCarrito.setUsuarioEdicion("");
                NavDrawerActivity.clienteCarrito.setFechaEdicion("");
                NavDrawerActivity.clienteCarrito.setIdEmisorCreacion(Constants.idEmisor);
                NavDrawerActivity.clienteCarrito.setCondicionPago(0);

                //----------------------------------------
                //Limpiamos
                spinnerTipoPers.setSelection(0);
                editTextNomPJur.setText("");
                editTextRucPJur.setText("");
                editTextDirecPJur.setText("");
                editTextEmailPJur.setText("");
                editTextTelefPJur.setText("");
                alert("Cliente agregado exitosamente!");
                finish();
                //-----------------------------------------

            }
        });


    }

    private void alert(String s) {
        Toast.makeText(ManejarClienteActivity.this,s,Toast.LENGTH_SHORT).show();
    }




}
