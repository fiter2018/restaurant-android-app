package com.facturactiva.facturactivafreepos.activities;

import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;

import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;


import com.facturactiva.facturactivafreepos.R;
import com.facturactiva.facturactivafreepos.adapters.ListaProductosNavDrawerAdapter;
import com.facturactiva.facturactivafreepos.constants.Constants;
import com.facturactiva.facturactivafreepos.models.Categoria;
import com.facturactiva.facturactivafreepos.models.Cliente;
import com.facturactiva.facturactivafreepos.models.Producto;
import com.facturactiva.facturactivafreepos.models.ProductoCarrito;
import com.facturactiva.facturactivafreepos.sqlite.ConexionSQLiteHelper;
import com.facturactiva.facturactivafreepos.utilities.Utilidades;
import com.facturactiva.facturactivafreepos.validations.ClientesValidations;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import static android.content.res.Configuration.ORIENTATION_PORTRAIT;

public class NavDrawerActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {

    private TextView textViewCorreo, textViewNombre;
    public static String nombreEmpresa="", login_email_usuario="", login_pass_usuario="";
    public static int idEmisor = -1;
    public static String login_way="", rotado="No", itemSelectedLastTime="";
    public NavigationView navigationView;
    public static String TAG = "FacturadorPOS";
    public static String modeAlert = "";
    SharedPreferences sharedpreferences;
    Button btnSiLogout, btnNoLogout, btnOkCerrarApp, btnCancelCerrarApp, btnBuscarProduc, btnTicket;

    ImageView imageViewTicketActual, imageViewCobrar1, imageViewCobrar2, imageViewBuscarProd, imageViewSearchCli, imageViewAddCli;
    public TextView textViewCantItems;
    TextView textViewMontoCobrar, textViewNomCliCarr, textViewTipoDocCliCarr, textViewNroDocCliCarr;
    ImageButton imageButtonBuscarProd;
    Spinner spinnerCategInProdActiv;
    TextView textViewClientes;
    RecyclerView productosRecyclerView;
    ListaProductosNavDrawerAdapter listaProductosAdapter;
    RecyclerView.LayoutManager layoutManagerProductos;
    ArrayList<Producto> arrayListProductos;
    private static long animationDuration = 1000; //miliseconds
    private int spanCount = 2;
    ArrayList<Categoria> arrayListValoresCategorias;
    ArrayList<String> arrayListMostrarCategorias;
    public ConexionSQLiteHelper conn;
    public static Cliente clienteCarrito = null;
    public LinearLayout linearNoCliCarr, linearCliCarr;
    public static int numProducts = 0;
    public static ArrayList<Producto> arrayListProductosCarrito;
    public Context context = NavDrawerActivity.this;
    public float montoCobrar = 0;

    //Controles del popup de ingreso de cliente carrito
    Spinner spinnerTipoPers;
    //Layout Persona Natural
    EditText editTextNomPNat,editTextDniPNat,editTextEmailPNat;
    Button btnGuardarPNat, btnCancelarPNat;
    //Layout Persona Juridica
    EditText editTextNomPJur,editTextRucPJur,editTextEmailPJur;
    Button btnGuardarPJur, btnCancelarPJur;
    ArrayList<String> tipoPersonaArrayList;
    LinearLayout linearLayoutPNat, linearLayoutPJurid;
    public Menu actionBarMenu;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_nav_drawer);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar_nav_drawer);
        setSupportActionBar(toolbar);
        Log.i("TAG","onCreate() NAV DRAWER");


        conn = new ConexionSQLiteHelper(NavDrawerActivity.this, "facturactiva-service", null, 1);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout_nav_drawer);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        navigationView = (NavigationView) findViewById(R.id.nav_view_nav_drawer);
        navigationView.setNavigationItemSelectedListener(this);

        sharedpreferences = getSharedPreferences(LoginActivity.MyPREFERENCES, Context.MODE_PRIVATE);

        login_way = sharedpreferences.getString("enter_mode","EnterModeEnCasoDeNull");

        if(login_way.equals("login")) {
            login_email_usuario = sharedpreferences.getString("username","UsernameEnCasoDeNull");
            login_pass_usuario = sharedpreferences.getString("password","PasswordEnCasoDeNull");
            nombreEmpresa = sharedpreferences.getString("nombreEmisor","NombreEmpresaEnCasoDeNull");
            idEmisor = Integer.parseInt( sharedpreferences.getString("idEmisor","idEmisorEnCasoDeNull"));
        }

        if(login_way.equals("login")){
            Menu menu = navigationView.getMenu();
            MenuItem itemVentas = menu.findItem(R.id.nav_ordenar);
            itemVentas.setChecked(true);
            inicializarDatosUsuario(navigationView);
            inicializarControles();
            consultaCarritoYClienteYSeteaValores();

            eventosClick();
            consultarListasCategorias();
            ArrayAdapter<CharSequence> adapter = new ArrayAdapter(NavDrawerActivity.this, android.R.layout.simple_spinner_item, arrayListMostrarCategorias);
            spinnerCategInProdActiv.setAdapter(adapter);

            spinnerCategInProdActiv.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                    if(position != 0){
                        String codCateg = arrayListValoresCategorias.get(position-1).getCodCategoria();
                        consultarListasProductosByCategoria(codCateg);
                    } else {
                        consultarListasProductos();
                    }


                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {

                }
            });
            consultarListasProductos();


        }


    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    protected void onResume() {
        super.onResume();
        consultaCarritoYClienteYSeteaValores();
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout_nav_drawer);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {

            AlertDialog.Builder builder = new AlertDialog.Builder(NavDrawerActivity.this);
            LayoutInflater inflater = getLayoutInflater();
            View dialogView = inflater.inflate(R.layout.popup_salir_aplicacion,null);
            builder.setView(dialogView);

            //inicializarComponentesPopUp();
            btnOkCerrarApp = (Button) dialogView.findViewById(R.id.btn_ok_cerrar_app);
            btnCancelCerrarApp = (Button) dialogView.findViewById(R.id.btn_cancel_cerrar_app);

            final AlertDialog dialog = builder.create();

            //Eventos Clic
            btnOkCerrarApp.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    //moveTaskToBack(true);
                    //dialog.cancel();
                    //Process.killProcess(Process.myPid());
                    //System.exit(0);
                    finishAffinity();

                }
            });

            btnCancelCerrarApp.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialog.cancel();
                }
            });

            dialog.show();

            //---------------------------------------------------------------------------
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.nav_drawer_options_menu, menu);
        actionBarMenu = menu;

        SQLiteDatabase db1 = conn.getReadableDatabase();
        //Consulto si el cliente carrito está seteado
        Cursor cursor = db1.rawQuery("SELECT * FROM " + Utilidades.TABLA_PERSONA + " WHERE " + Utilidades.CAMPO_ID_PERS + " = -100", null);
        if(cursor.getCount() == 1){
            //Seteo al cliente carrito imagen
            MenuItem menuItemPerson = actionBarMenu.findItem(R.id.action_person_nav_drawer);
            menuItemPerson.setIcon(R.drawable.user_cheked_icon);

        } else {

            MenuItem menuItemPerson = actionBarMenu.findItem(R.id.action_person_nav_drawer);
            menuItemPerson.setIcon(R.drawable.add_user_icon);
        }
        cursor.close();
        db1.close();

/*

        //SearchView
        MenuItem menuItemSeachPerson = actionBarMenu.findItem(R.id.action_person_nav_drawer).getSubMenu().findItem(R.id.action_person_nav_drawer_buscar);
        SearchView searchView = (SearchView) menuItemSeachPerson.getActionView();

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {

                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {


                return false;
            }
        });
*/
/*
        MenuItem menuItemSearch = actionBarMenu.findItem(R.id.action_search_prueba);
        SearchView searchView = (SearchView) menuItemSearch.getActionView();

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {

                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {


                return false;
            }
        });

*/

        //Recuperamos el dato pasado por el activity anterior
        sharedpreferences = getSharedPreferences(LoginActivity.MyPREFERENCES, Context.MODE_PRIVATE);

        login_way = sharedpreferences.getString("enter_mode","EnterModeEnCasoDeNull");
        //Decidimos Items de OptionsMenu
        if(login_way.equals("login")){
            /*MenuItem optionItemConocenos1 = menu.findItem(R.id.optionItemConocenos1);  optionItemConocenos1.setVisible(false);
            MenuItem optionItemConocenos2 = menu.findItem(R.id.optionItemConocenos2);  optionItemConocenos2.setVisible(false);
            MenuItem optionItemConocenos3 = menu.findItem(R.id.optionItemConocenos3);  optionItemConocenos3.setVisible(false);*/
        }
        if(login_way.equals("conocenos")){
           /* MenuItem optionItemLogin1 = menu.findItem(R.id.optionItemLogin1);  optionItemLogin1.setVisible(false);
            MenuItem optionItemLogin2 = menu.findItem(R.id.optionItemLogin2);  optionItemLogin2.setVisible(false);
            MenuItem optionItemLogin3 = menu.findItem(R.id.optionItemLogin3);  optionItemLogin3.setVisible(false);*/
        }
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_person_nav_drawer) {

            SQLiteDatabase db1 = conn.getReadableDatabase();
            //Consulto si el cliente carrito está seteado
            Cursor cursor10 = db1.rawQuery("SELECT * FROM " + Utilidades.TABLA_PERSONA + " WHERE " + Utilidades.CAMPO_ID_PERS + " = -100", null);

            if (cursor10.getCount() == 0) {
                MenuItem menuItemBuscarCli = item.getSubMenu().findItem(R.id.action_person_nav_drawer_buscar);  menuItemBuscarCli.setVisible(false);
                MenuItem menuItemAgregarCli = item.getSubMenu().findItem(R.id.action_person_nav_drawer_agregar);  menuItemAgregarCli.setVisible(true);
                MenuItem menuItemEliminarCli = item.getSubMenu().findItem(R.id.action_person_nav_drawer_eliminar);  menuItemEliminarCli.setVisible(false);

            } else {
                MenuItem menuItemBuscarCli = item.getSubMenu().findItem(R.id.action_person_nav_drawer_buscar);  menuItemBuscarCli.setVisible(false);
                MenuItem menuItemAgregarCli = item.getSubMenu().findItem(R.id.action_person_nav_drawer_agregar);  menuItemAgregarCli.setVisible(false);
                MenuItem menuItemEliminarCli = item.getSubMenu().findItem(R.id.action_person_nav_drawer_eliminar);  menuItemEliminarCli.setVisible(true);
            }
            cursor10.close();
            db1.close();


        } else if (id == R.id.action_cobrar_nav_drawer){

            SQLiteDatabase db2 = conn.getReadableDatabase();
            ProductoCarrito productoCarr = null;
            montoCobrar = 0;
            //Consulto si hay algun producto en el carrito
            Cursor cursor2 = db2.rawQuery("SELECT * FROM " + Utilidades.TABLA_CARRITO, null);
            while(cursor2.moveToNext()){
                productoCarr = new ProductoCarrito();
                productoCarr.setIdEmisor(cursor2.getInt(0));
                productoCarr.setCodProducto(cursor2.getString(1));
                productoCarr.setCodCategoria(cursor2.getString(2));
                productoCarr.setNroPlatoCarta(cursor2.getInt(3));
                productoCarr.setDescripcion(cursor2.getString(4));
                productoCarr.setTipoMoneda(cursor2.getString(5));
                productoCarr.setValorUnitario(cursor2.getFloat(6));
                productoCarr.setActivo(cursor2.getInt(7));
                productoCarr.setImagePath(cursor2.getString(8));
                productoCarr.setCantidad(cursor2.getInt(9));

                montoCobrar += productoCarr.getValorUnitario()*productoCarr.getCantidad();
            }
            Cursor cursorCli = db2.rawQuery("SELECT * FROM "+Utilidades.TABLA_PERSONA+" WHERE "+Utilidades.CAMPO_ID_PERS+" = -100", null);

            if(cursor2.getCount() == 0){
                alert("No hay productos en Carrito de Venta");
                return true;
            }

            if(((cursor2.getCount() > 0) && (cursorCli.getCount() == 0)) && (montoCobrar > 750)){

                alert("Monto mayor a  S/.750. Debe ingresar datos del cliente.");
                //Muestra popup para ingreso de datos del receptor
                AlertDialog.Builder builder = new AlertDialog.Builder(NavDrawerActivity.this);
                LayoutInflater inflater = getLayoutInflater();
                View dialogView = inflater.inflate(R.layout.popup_ingresa_datos_receptor_carrito,null);
                builder.setView(dialogView);

                //inicializarComponentesPopUp();
                linearLayoutPNat = (LinearLayout) dialogView.findViewById(R.id.linearLayoutPNaturalPopupCliCarr);
                linearLayoutPJurid = (LinearLayout) dialogView.findViewById(R.id.linearLayoutPJuridicaPopupCliCarr);
                spinnerTipoPers = (Spinner) dialogView.findViewById(R.id.spinnerTipoPersPopupCliCarr);

                //Lleno Spinner
                tipoPersonaArrayList = new ArrayList<String>();
                tipoPersonaArrayList.add("SELECCIONE TIPO DE CLIENTE");
                tipoPersonaArrayList.add("Persona Natural");
                tipoPersonaArrayList.add("Persona Jurídica");
                ArrayAdapter<CharSequence> adapter = new ArrayAdapter(NavDrawerActivity.this, android.R.layout.simple_spinner_item, tipoPersonaArrayList);
                spinnerTipoPers.setAdapter(adapter);
                spinnerTipoPers.setSelection(1);
                linearLayoutPNat.setVisibility(View.VISIBLE);
                linearLayoutPJurid.setVisibility(View.INVISIBLE);

                editTextNomPNat = (EditText) dialogView.findViewById(R.id.editTextNomPNatPopupCliCarr);
                editTextDniPNat = (EditText) dialogView.findViewById(R.id.editTextDniPNatPopupCliCarr);
                editTextEmailPNat = (EditText) dialogView.findViewById(R.id.editTextEmailPNatPopupCliCarr);
                btnGuardarPNat = (Button) dialogView.findViewById(R.id.btnGuardarPNatPopupCliCarr);
                btnCancelarPNat = (Button) dialogView.findViewById(R.id.btnCancelarPNatPopupCliCarr);

                editTextNomPJur = (EditText) dialogView.findViewById(R.id.editTextNomPJurPopupCliCarr);
                editTextRucPJur = (EditText) dialogView.findViewById(R.id.editTextRucPJurPopupCliCarr);
                editTextEmailPJur = (EditText) dialogView.findViewById(R.id.editTextEmailPJurPopupCliCarr);
                btnGuardarPJur = (Button) dialogView.findViewById(R.id.btnGuardarPJurPopupCliCarr);
                btnCancelarPJur = (Button) dialogView.findViewById(R.id.btnCancelarPJurPopupCliCarr);

                final AlertDialog dialog = builder.create();

                //Eventos Click
                spinnerTipoPers.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                        if (position==0) {
                            linearLayoutPNat.setVisibility(View.INVISIBLE);
                            linearLayoutPJurid.setVisibility(View.INVISIBLE);
                        }
                        if (position==1) {
                            linearLayoutPNat.setVisibility(View.VISIBLE);
                            linearLayoutPJurid.setVisibility(View.INVISIBLE);
                        }
                        if (position==2) {
                            linearLayoutPNat.setVisibility(View.INVISIBLE);
                            linearLayoutPJurid.setVisibility(View.VISIBLE);
                        }

                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> parent) {

                    }
                });

                //Eventos Clic Guardar PNatural
                btnGuardarPNat.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        if(ClientesValidations.validateIngresoCli(editTextNomPNat,editTextDniPNat,editTextEmailPNat,1)) {
                            //Recupera datos de persona
                            String nombres = editTextNomPNat.getText().toString();
                            String tipoPers = "";
                            if (spinnerTipoPers.getSelectedItemPosition() == 1) {
                                tipoPers = "02";
                            }
                            if (spinnerTipoPers.getSelectedItemPosition() == 2) {
                                tipoPers = "01";
                            }

                            String telef = "";
                            String tipoDocPrinc = "1";
                            String numDocPrincip = editTextDniPNat.getText().toString();
                            String tipoDocSec = "";
                            String numDocSec = "";
                            String direc = "";
                            int activo = 1;

                            //Obtenemos datetime actual del sistema
                            Date date = new Date();
                            //DateFormat hourdateFormat0 = new SimpleDateFormat("HH:mm:ss dd/MM/yyyy");
                            DateFormat hourdateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
                            //DateFormat hourdateFormat2 = DateFormat.getDateTimeInstance();
                            String fechaCreac = hourdateFormat.format(date);

                            String emailContac = editTextEmailPNat.getText().toString();

                            //Inserta persona en tablas persona y cliente, con ID positivo y con ID -100 para agregar al carrito -----------------------------------------------------------------------
                            SQLiteDatabase db = conn.getWritableDatabase();
                            //Insertamos en tabla PERSONA recuperando el idPersona
                            String insert = "INSERT INTO " + Utilidades.TABLA_PERSONA + "(" +
                                    Utilidades.CAMPO_NOMBRE_PERS + ", " +
                                    Utilidades.CAMPO_NOMBRES_PERS + ", " +
                                    Utilidades.CAMPO_AP_PATERNO_PERS + ", " +
                                    Utilidades.CAMPO_AP_MATERNO_PERS + ", " +
                                    Utilidades.CAMPO_DIREC_PRINC_PERS + ", " +
                                    Utilidades.CAMPO_DIREC_SECUND_PERS + ", " +
                                    Utilidades.CAMPO_COD_UBIG_PRINC_PERS + ", " +
                                    Utilidades.CAMPO_COD_UBIG_SECUND_PERS + ", " +
                                    Utilidades.CAMPO_COD_PAIS_PERS + ", " +
                                    Utilidades.CAMPO_TELEFONO_PERS + ", " +
                                    Utilidades.CAMPO_FAX_PERS + ", " +
                                    Utilidades.CAMPO_TIPO_PERSONA_PERS + ", " +
                                    Utilidades.CAMPO_TIPO_DOC_PRINC_PERS + ", " +
                                    Utilidades.CAMPO_NUM_DOC_PRINC_PERS + ", " +
                                    Utilidades.CAMPO_TIPO_DOC_SECUND_PERS + ", " +
                                    Utilidades.CAMPO_NUM_DOC_SECUND_PERS + ", " +
                                    Utilidades.CAMPO_ACTIVO_PERS + ", " +
                                    Utilidades.CAMPO_USUARIO_CREAC_PERS + ", " +
                                    Utilidades.CAMPO_FECHA_CREAC_PERS + ", " +
                                    Utilidades.CAMPO_USUARIO_EDIC_PERS + ", " +
                                    Utilidades.CAMPO_FECHA_EDIC_PERS + ", " +
                                    Utilidades.CAMPO_FLAG_PERS + ") VALUES(" +
                                    "'" + nombres + "', " +
                                    "'', " +
                                    "'', " +
                                    "'', " +
                                    "'" + direc + "', " +
                                    "'', " +
                                    "'', " +
                                    "'', " +
                                    "'', " +
                                    "'" + telef + "', " +
                                    "'', " +
                                    "'" + tipoPers + "', " +
                                    "'" + tipoDocPrinc + "', " +
                                    "'" + numDocPrincip + "', " +
                                    "'" + tipoDocSec + "', " +
                                    "'" + numDocSec + "', " +
                                    String.valueOf(activo) + ", " +
                                    "'" + NavDrawerActivity.login_email_usuario + "', " +
                                    "'" + fechaCreac + "', " +
                                    "'', " +
                                    "'', " +
                                    String.valueOf(0) + ")";

                            db.execSQL(insert);

                            int rowid = -10;
                            Cursor cursor = db.rawQuery("SELECT last_insert_rowid()", null);
                            while (cursor.moveToNext()) {
                                rowid = cursor.getInt(0);
                            }

                            //Insertamos el cliente en tabla CLIENTE
                            String insert2 = "INSERT INTO " + Utilidades.TABLA_CLIENTE + "(" +
                                    Utilidades.CAMPO_ID_CLIENTE_CLI + ", " +
                                    Utilidades.CAMPO_EMAIL_CONTACTO_CLI + ", " +
                                    Utilidades.CAMPO_EMAIL_CONFIRMADO_CLI + ", " +
                                    Utilidades.CAMPO_ACTIVO_CLI + ", " +
                                    Utilidades.CAMPO_USUARIO_CREAC_CLI + ", " +
                                    Utilidades.CAMPO_FECHA_CREAC_CLI + ", " +
                                    Utilidades.CAMPO_USUARIO_EDIC_CLI + ", " +
                                    Utilidades.CAMPO_FECHA_EDIC_CLI + ", " +
                                    Utilidades.CAMPO_ID_EMISOR_CREAC_CLI + ", " +
                                    Utilidades.CAMPO_CONDICION_PAGO_CLI + ", " +
                                    Utilidades.CAMPO_FLAG_CLI + ") VALUES(" +
                                    String.valueOf(rowid) + ", " +
                                    "'" + emailContac + "', " +
                                    String.valueOf(0) + ", " +
                                    String.valueOf(activo) + ", " +
                                    "'" + NavDrawerActivity.login_email_usuario + "', " +
                                    "'" + fechaCreac + "', " +
                                    "'', " +
                                    "'', " +
                                    String.valueOf(Constants.idEmisor) + ", " +
                                    String.valueOf(0) + ", " +
                                    String.valueOf(0) + ")";

                            db.execSQL(insert2);

                            //Insertamos en tabla PERSONA con ID = -100
                            String insert3 = "INSERT INTO " + Utilidades.TABLA_PERSONA + "(" +
                                    Utilidades.CAMPO_ID_PERS + ", " +
                                    Utilidades.CAMPO_NOMBRE_PERS + ", " +
                                    Utilidades.CAMPO_NOMBRES_PERS + ", " +
                                    Utilidades.CAMPO_AP_PATERNO_PERS + ", " +
                                    Utilidades.CAMPO_AP_MATERNO_PERS + ", " +
                                    Utilidades.CAMPO_DIREC_PRINC_PERS + ", " +
                                    Utilidades.CAMPO_DIREC_SECUND_PERS + ", " +
                                    Utilidades.CAMPO_COD_UBIG_PRINC_PERS + ", " +
                                    Utilidades.CAMPO_COD_UBIG_SECUND_PERS + ", " +
                                    Utilidades.CAMPO_COD_PAIS_PERS + ", " +
                                    Utilidades.CAMPO_TELEFONO_PERS + ", " +
                                    Utilidades.CAMPO_FAX_PERS + ", " +
                                    Utilidades.CAMPO_TIPO_PERSONA_PERS + ", " +
                                    Utilidades.CAMPO_TIPO_DOC_PRINC_PERS + ", " +
                                    Utilidades.CAMPO_NUM_DOC_PRINC_PERS + ", " +
                                    Utilidades.CAMPO_TIPO_DOC_SECUND_PERS + ", " +
                                    Utilidades.CAMPO_NUM_DOC_SECUND_PERS + ", " +
                                    Utilidades.CAMPO_ACTIVO_PERS + ", " +
                                    Utilidades.CAMPO_USUARIO_CREAC_PERS + ", " +
                                    Utilidades.CAMPO_FECHA_CREAC_PERS + ", " +
                                    Utilidades.CAMPO_USUARIO_EDIC_PERS + ", " +
                                    Utilidades.CAMPO_FECHA_EDIC_PERS + ", " +
                                    Utilidades.CAMPO_FLAG_PERS + ") VALUES(" +
                                    String.valueOf(-100) + ", " +
                                    "'" + nombres + "', " +
                                    "'', " +
                                    "'', " +
                                    "'', " +
                                    "'" + direc + "', " +
                                    "'', " +
                                    "'', " +
                                    "'', " +
                                    "'', " +
                                    "'" + telef + "', " +
                                    "'', " +
                                    "'" + tipoPers + "', " +
                                    "'" + tipoDocPrinc + "', " +
                                    "'" + numDocPrincip + "', " +
                                    "'" + tipoDocSec + "', " +
                                    "'" + numDocSec + "', " +
                                    String.valueOf(activo) + ", " +
                                    "'" + NavDrawerActivity.login_email_usuario + "', " +
                                    "'" + fechaCreac + "', " +
                                    "'', " +
                                    "'', " +
                                    String.valueOf(0) + ")";

                            db.execSQL(insert3);

                            //Insertamos en tabla CLIENTE con ID -100
                            String insert4 = "INSERT INTO " + Utilidades.TABLA_CLIENTE + "(" +
                                    Utilidades.CAMPO_ID_CLIENTE_CLI + ", " +
                                    Utilidades.CAMPO_EMAIL_CONTACTO_CLI + ", " +
                                    Utilidades.CAMPO_EMAIL_CONFIRMADO_CLI + ", " +
                                    Utilidades.CAMPO_ACTIVO_CLI + ", " +
                                    Utilidades.CAMPO_USUARIO_CREAC_CLI + ", " +
                                    Utilidades.CAMPO_FECHA_CREAC_CLI + ", " +
                                    Utilidades.CAMPO_USUARIO_EDIC_CLI + ", " +
                                    Utilidades.CAMPO_FECHA_EDIC_CLI + ", " +
                                    Utilidades.CAMPO_ID_EMISOR_CREAC_CLI + ", " +
                                    Utilidades.CAMPO_CONDICION_PAGO_CLI + ", " +
                                    Utilidades.CAMPO_FLAG_CLI + ") VALUES(" +
                                    String.valueOf(-100) + ", " +
                                    "'" + emailContac + "', " +
                                    String.valueOf(0) + ", " +
                                    String.valueOf(activo) + ", " +
                                    "'" + NavDrawerActivity.login_email_usuario + "', " +
                                    "'" + fechaCreac + "', " +
                                    "'', " +
                                    "'', " +
                                    String.valueOf(Constants.idEmisor) + ", " +
                                    String.valueOf(0) + ", " +
                                    String.valueOf(0) + ")";

                            db.execSQL(insert4);
                            db.close();

                            //Limpia controles y cambia imagen de persona en el actionBar  --------------------
                            spinnerTipoPers.setSelection(0);
                            editTextNomPNat.setText("");
                            editTextDniPNat.setText("");
                            editTextEmailPNat.setText("");
                            MenuItem menuItemPerson = actionBarMenu.findItem(R.id.action_person_ticket_act);
                            menuItemPerson.setIcon(R.drawable.user_cheked_icon);

                            //Cierra popup
                            dialog.cancel();

                            //Transacción Exitosa, pasa al siguiente activity - Pago
                            Intent i = new Intent(NavDrawerActivity.this, PagoActivity.class);
                            i.putExtra("montoCobrar",montoCobrar);
                            startActivity(i);

                        }


                    }
                });

                btnCancelarPNat.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.cancel();
                    }
                });

                //Eventos Clic Guardar PJuridica
                btnGuardarPJur.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        if(ClientesValidations.validateIngresoCli(editTextNomPJur,editTextRucPJur,editTextEmailPJur,2)) {
                            //Recupera datos de persona
                            String nombres = editTextNomPJur.getText().toString();
                            String tipoPers = "";
                            if (spinnerTipoPers.getSelectedItemPosition() == 1) {
                                tipoPers = "02";
                            }
                            if (spinnerTipoPers.getSelectedItemPosition() == 2) {
                                tipoPers = "01";
                            }

                            String telef = "";
                            String tipoDocPrinc = "6";
                            String numDocPrincip = editTextRucPJur.getText().toString();
                            String tipoDocSec = "";
                            String numDocSec = "";
                            String direc = "";
                            int activo = 1;

                            //Obtenemos datetime actual del sistema
                            Date date = new Date();
                            //DateFormat hourdateFormat0 = new SimpleDateFormat("HH:mm:ss dd/MM/yyyy");
                            DateFormat hourdateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
                            //DateFormat hourdateFormat2 = DateFormat.getDateTimeInstance();
                            String fechaCreac = hourdateFormat.format(date);

                            String emailContac = editTextEmailPJur.getText().toString();

                            //Inserta persona en tablas persona y cliente, con ID positivo y con ID -100 para agregar al carrito -----------------------------------------------------------------------
                            SQLiteDatabase db = conn.getWritableDatabase();
                            //Insertamos en tabla PERSONA recuperando el idPersona
                            String insert = "INSERT INTO " + Utilidades.TABLA_PERSONA + "(" +
                                    Utilidades.CAMPO_NOMBRE_PERS + ", " +
                                    Utilidades.CAMPO_NOMBRES_PERS + ", " +
                                    Utilidades.CAMPO_AP_PATERNO_PERS + ", " +
                                    Utilidades.CAMPO_AP_MATERNO_PERS + ", " +
                                    Utilidades.CAMPO_DIREC_PRINC_PERS + ", " +
                                    Utilidades.CAMPO_DIREC_SECUND_PERS + ", " +
                                    Utilidades.CAMPO_COD_UBIG_PRINC_PERS + ", " +
                                    Utilidades.CAMPO_COD_UBIG_SECUND_PERS + ", " +
                                    Utilidades.CAMPO_COD_PAIS_PERS + ", " +
                                    Utilidades.CAMPO_TELEFONO_PERS + ", " +
                                    Utilidades.CAMPO_FAX_PERS + ", " +
                                    Utilidades.CAMPO_TIPO_PERSONA_PERS + ", " +
                                    Utilidades.CAMPO_TIPO_DOC_PRINC_PERS + ", " +
                                    Utilidades.CAMPO_NUM_DOC_PRINC_PERS + ", " +
                                    Utilidades.CAMPO_TIPO_DOC_SECUND_PERS + ", " +
                                    Utilidades.CAMPO_NUM_DOC_SECUND_PERS + ", " +
                                    Utilidades.CAMPO_ACTIVO_PERS + ", " +
                                    Utilidades.CAMPO_USUARIO_CREAC_PERS + ", " +
                                    Utilidades.CAMPO_FECHA_CREAC_PERS + ", " +
                                    Utilidades.CAMPO_USUARIO_EDIC_PERS + ", " +
                                    Utilidades.CAMPO_FECHA_EDIC_PERS + ", " +
                                    Utilidades.CAMPO_FLAG_PERS + ") VALUES(" +
                                    "'" + nombres + "', " +
                                    "'', " +
                                    "'', " +
                                    "'', " +
                                    "'" + direc + "', " +
                                    "'', " +
                                    "'', " +
                                    "'', " +
                                    "'', " +
                                    "'" + telef + "', " +
                                    "'', " +
                                    "'" + tipoPers + "', " +
                                    "'" + tipoDocPrinc + "', " +
                                    "'" + numDocPrincip + "', " +
                                    "'" + tipoDocSec + "', " +
                                    "'" + numDocSec + "', " +
                                    String.valueOf(activo) + ", " +
                                    "'" + NavDrawerActivity.login_email_usuario + "', " +
                                    "'" + fechaCreac + "', " +
                                    "'', " +
                                    "'', " +
                                    String.valueOf(0) + ")";

                            db.execSQL(insert);

                            int rowid = -10;
                            Cursor cursor = db.rawQuery("SELECT last_insert_rowid()", null);
                            while (cursor.moveToNext()) {
                                rowid = cursor.getInt(0);
                            }

                            //Insertamos el cliente en tabla CLIENTE
                            String insert2 = "INSERT INTO " + Utilidades.TABLA_CLIENTE + "(" +
                                    Utilidades.CAMPO_ID_CLIENTE_CLI + ", " +
                                    Utilidades.CAMPO_EMAIL_CONTACTO_CLI + ", " +
                                    Utilidades.CAMPO_EMAIL_CONFIRMADO_CLI + ", " +
                                    Utilidades.CAMPO_ACTIVO_CLI + ", " +
                                    Utilidades.CAMPO_USUARIO_CREAC_CLI + ", " +
                                    Utilidades.CAMPO_FECHA_CREAC_CLI + ", " +
                                    Utilidades.CAMPO_USUARIO_EDIC_CLI + ", " +
                                    Utilidades.CAMPO_FECHA_EDIC_CLI + ", " +
                                    Utilidades.CAMPO_ID_EMISOR_CREAC_CLI + ", " +
                                    Utilidades.CAMPO_CONDICION_PAGO_CLI + ", " +
                                    Utilidades.CAMPO_FLAG_CLI + ") VALUES(" +
                                    String.valueOf(rowid) + ", " +
                                    "'" + emailContac + "', " +
                                    String.valueOf(0) + ", " +
                                    String.valueOf(activo) + ", " +
                                    "'" + NavDrawerActivity.login_email_usuario + "', " +
                                    "'" + fechaCreac + "', " +
                                    "'', " +
                                    "'', " +
                                    String.valueOf(Constants.idEmisor) + ", " +
                                    String.valueOf(0) + ", " +
                                    String.valueOf(0) + ")";

                            db.execSQL(insert2);

                            //Insertamos en tabla PERSONA con ID = -100
                            String insert3 = "INSERT INTO " + Utilidades.TABLA_PERSONA + "(" +
                                    Utilidades.CAMPO_ID_PERS + ", " +
                                    Utilidades.CAMPO_NOMBRE_PERS + ", " +
                                    Utilidades.CAMPO_NOMBRES_PERS + ", " +
                                    Utilidades.CAMPO_AP_PATERNO_PERS + ", " +
                                    Utilidades.CAMPO_AP_MATERNO_PERS + ", " +
                                    Utilidades.CAMPO_DIREC_PRINC_PERS + ", " +
                                    Utilidades.CAMPO_DIREC_SECUND_PERS + ", " +
                                    Utilidades.CAMPO_COD_UBIG_PRINC_PERS + ", " +
                                    Utilidades.CAMPO_COD_UBIG_SECUND_PERS + ", " +
                                    Utilidades.CAMPO_COD_PAIS_PERS + ", " +
                                    Utilidades.CAMPO_TELEFONO_PERS + ", " +
                                    Utilidades.CAMPO_FAX_PERS + ", " +
                                    Utilidades.CAMPO_TIPO_PERSONA_PERS + ", " +
                                    Utilidades.CAMPO_TIPO_DOC_PRINC_PERS + ", " +
                                    Utilidades.CAMPO_NUM_DOC_PRINC_PERS + ", " +
                                    Utilidades.CAMPO_TIPO_DOC_SECUND_PERS + ", " +
                                    Utilidades.CAMPO_NUM_DOC_SECUND_PERS + ", " +
                                    Utilidades.CAMPO_ACTIVO_PERS + ", " +
                                    Utilidades.CAMPO_USUARIO_CREAC_PERS + ", " +
                                    Utilidades.CAMPO_FECHA_CREAC_PERS + ", " +
                                    Utilidades.CAMPO_USUARIO_EDIC_PERS + ", " +
                                    Utilidades.CAMPO_FECHA_EDIC_PERS + ", " +
                                    Utilidades.CAMPO_FLAG_PERS + ") VALUES(" +
                                    String.valueOf(-100) + ", " +
                                    "'" + nombres + "', " +
                                    "'', " +
                                    "'', " +
                                    "'', " +
                                    "'" + direc + "', " +
                                    "'', " +
                                    "'', " +
                                    "'', " +
                                    "'', " +
                                    "'" + telef + "', " +
                                    "'', " +
                                    "'" + tipoPers + "', " +
                                    "'" + tipoDocPrinc + "', " +
                                    "'" + numDocPrincip + "', " +
                                    "'" + tipoDocSec + "', " +
                                    "'" + numDocSec + "', " +
                                    String.valueOf(activo) + ", " +
                                    "'" + NavDrawerActivity.login_email_usuario + "', " +
                                    "'" + fechaCreac + "', " +
                                    "'', " +
                                    "'', " +
                                    String.valueOf(0) + ")";

                            db.execSQL(insert3);

                            //Insertamos en tabla CLIENTE con ID -100
                            String insert4 = "INSERT INTO " + Utilidades.TABLA_CLIENTE + "(" +
                                    Utilidades.CAMPO_ID_CLIENTE_CLI + ", " +
                                    Utilidades.CAMPO_EMAIL_CONTACTO_CLI + ", " +
                                    Utilidades.CAMPO_EMAIL_CONFIRMADO_CLI + ", " +
                                    Utilidades.CAMPO_ACTIVO_CLI + ", " +
                                    Utilidades.CAMPO_USUARIO_CREAC_CLI + ", " +
                                    Utilidades.CAMPO_FECHA_CREAC_CLI + ", " +
                                    Utilidades.CAMPO_USUARIO_EDIC_CLI + ", " +
                                    Utilidades.CAMPO_FECHA_EDIC_CLI + ", " +
                                    Utilidades.CAMPO_ID_EMISOR_CREAC_CLI + ", " +
                                    Utilidades.CAMPO_CONDICION_PAGO_CLI + ", " +
                                    Utilidades.CAMPO_FLAG_CLI + ") VALUES(" +
                                    String.valueOf(-100) + ", " +
                                    "'" + emailContac + "', " +
                                    String.valueOf(0) + ", " +
                                    String.valueOf(activo) + ", " +
                                    "'" + NavDrawerActivity.login_email_usuario + "', " +
                                    "'" + fechaCreac + "', " +
                                    "'', " +
                                    "'', " +
                                    String.valueOf(Constants.idEmisor) + ", " +
                                    String.valueOf(0) + ", " +
                                    String.valueOf(0) + ")";

                            db.execSQL(insert4);
                            db.close();

                            //Limpia controles y cambia imagen de persona en el actionBar  --------------------
                            spinnerTipoPers.setSelection(0);
                            editTextNomPJur.setText("");
                            editTextRucPJur.setText("");
                            editTextEmailPJur.setText("");
                            MenuItem menuItemPerson = actionBarMenu.findItem(R.id.action_person_ticket_act);
                            menuItemPerson.setIcon(R.drawable.user_cheked_icon);

                            //Cierra popup
                            dialog.cancel();

                            //Transacción Exitosa, pasa al siguiente activity - Pago
                            Intent i = new Intent(NavDrawerActivity.this, PagoActivity.class);
                            i.putExtra("montoCobrar",montoCobrar);
                            startActivity(i);

                        }

                    }
                });


                btnCancelarPJur.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.cancel();
                    }
                });

                dialog.show();

                // -----------------------------------------------------------------------------
                return true;
            }


            //Transacción Exitosa, pasa al siguiente activity - Pago
            Intent i = new Intent(NavDrawerActivity.this, PagoActivity.class);
            i.putExtra("montoCobrar",montoCobrar);
            startActivity(i);
            return true;

        } else if(id == R.id.action_person_nav_drawer_agregar) {

                //Muestra popup para ingreso de datos del receptor
                AlertDialog.Builder builder = new AlertDialog.Builder(NavDrawerActivity.this);
                LayoutInflater inflater = getLayoutInflater();
                View dialogView = inflater.inflate(R.layout.popup_ingresa_datos_receptor_carrito,null);
                builder.setView(dialogView);

                //inicializarComponentesPopUp();
                linearLayoutPNat = (LinearLayout) dialogView.findViewById(R.id.linearLayoutPNaturalPopupCliCarr);
                linearLayoutPJurid = (LinearLayout) dialogView.findViewById(R.id.linearLayoutPJuridicaPopupCliCarr);
                spinnerTipoPers = (Spinner) dialogView.findViewById(R.id.spinnerTipoPersPopupCliCarr);

                //Lleno Spinner
                tipoPersonaArrayList = new ArrayList<String>();
                tipoPersonaArrayList.add("SELECCIONE TIPO DE CLIENTE");
                tipoPersonaArrayList.add("Persona Natural");
                tipoPersonaArrayList.add("Persona Jurídica");
                ArrayAdapter<CharSequence> adapter = new ArrayAdapter(NavDrawerActivity.this, android.R.layout.simple_spinner_item, tipoPersonaArrayList);
                spinnerTipoPers.setAdapter(adapter);
                spinnerTipoPers.setSelection(1);
                linearLayoutPNat.setVisibility(View.VISIBLE);
                linearLayoutPJurid.setVisibility(View.INVISIBLE);

                editTextNomPNat = (EditText) dialogView.findViewById(R.id.editTextNomPNatPopupCliCarr);
                editTextDniPNat = (EditText) dialogView.findViewById(R.id.editTextDniPNatPopupCliCarr);
                editTextEmailPNat = (EditText) dialogView.findViewById(R.id.editTextEmailPNatPopupCliCarr);
                btnGuardarPNat = (Button) dialogView.findViewById(R.id.btnGuardarPNatPopupCliCarr);
                btnCancelarPNat = (Button) dialogView.findViewById(R.id.btnCancelarPNatPopupCliCarr);

                editTextNomPJur = (EditText) dialogView.findViewById(R.id.editTextNomPJurPopupCliCarr);
                editTextRucPJur = (EditText) dialogView.findViewById(R.id.editTextRucPJurPopupCliCarr);
                editTextEmailPJur = (EditText) dialogView.findViewById(R.id.editTextEmailPJurPopupCliCarr);
                btnGuardarPJur = (Button) dialogView.findViewById(R.id.btnGuardarPJurPopupCliCarr);
                btnCancelarPJur = (Button) dialogView.findViewById(R.id.btnCancelarPJurPopupCliCarr);

                final AlertDialog dialog = builder.create();

                //Eventos Click
                spinnerTipoPers.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                        if (position==0) {
                            linearLayoutPNat.setVisibility(View.INVISIBLE);
                            linearLayoutPJurid.setVisibility(View.INVISIBLE);
                        }
                        if (position==1) {
                            linearLayoutPNat.setVisibility(View.VISIBLE);
                            linearLayoutPJurid.setVisibility(View.INVISIBLE);
                        }
                        if (position==2) {
                            linearLayoutPNat.setVisibility(View.INVISIBLE);
                            linearLayoutPJurid.setVisibility(View.VISIBLE);
                        }

                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> parent) {

                    }
                });

                //Eventos Clic Guardar PNatural
                btnGuardarPNat.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {


                        //Recupera datos de persona
                        String nombres = editTextNomPNat.getText().toString();
                        String tipoPers="";
                        if(spinnerTipoPers.getSelectedItemPosition()==1){
                            tipoPers = "02";
                        }
                        if(spinnerTipoPers.getSelectedItemPosition()==2){
                            tipoPers = "01";
                        }

                        String telef = "";
                        String tipoDocPrinc = "1";
                        String numDocPrincip = editTextDniPNat.getText().toString();
                        String tipoDocSec = "";
                        String numDocSec = "";
                        String direc = "";
                        int activo = 1;

                        //Obtenemos datetime actual del sistema
                        Date date = new Date();
                        //DateFormat hourdateFormat0 = new SimpleDateFormat("HH:mm:ss dd/MM/yyyy");
                        DateFormat hourdateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
                        //DateFormat hourdateFormat2 = DateFormat.getDateTimeInstance();
                        String fechaCreac = hourdateFormat.format(date);

                        String emailContac = editTextEmailPNat.getText().toString();




                        //Inserta persona en tablas persona y cliente, con ID positivo y con ID -100 para agregar al carrito -----------------------------------------------------------------------
                        SQLiteDatabase db = conn.getWritableDatabase();
                        //Insertamos en tabla PERSONA recuperando el idPersona
                        String insert = "INSERT INTO "+ Utilidades.TABLA_PERSONA+"("+
                                Utilidades.CAMPO_NOMBRE_PERS+", "+
                                Utilidades.CAMPO_NOMBRES_PERS+", "+
                                Utilidades.CAMPO_AP_PATERNO_PERS+", "+
                                Utilidades.CAMPO_AP_MATERNO_PERS+", "+
                                Utilidades.CAMPO_DIREC_PRINC_PERS+", "+
                                Utilidades.CAMPO_DIREC_SECUND_PERS+", "+
                                Utilidades.CAMPO_COD_UBIG_PRINC_PERS+", "+
                                Utilidades.CAMPO_COD_UBIG_SECUND_PERS+", "+
                                Utilidades.CAMPO_COD_PAIS_PERS+", "+
                                Utilidades.CAMPO_TELEFONO_PERS+", "+
                                Utilidades.CAMPO_FAX_PERS+", "+
                                Utilidades.CAMPO_TIPO_PERSONA_PERS+", "+
                                Utilidades.CAMPO_TIPO_DOC_PRINC_PERS+", "+
                                Utilidades.CAMPO_NUM_DOC_PRINC_PERS+", "+
                                Utilidades.CAMPO_TIPO_DOC_SECUND_PERS+", "+
                                Utilidades.CAMPO_NUM_DOC_SECUND_PERS+", "+
                                Utilidades.CAMPO_ACTIVO_PERS+", "+
                                Utilidades.CAMPO_USUARIO_CREAC_PERS+", "+
                                Utilidades.CAMPO_FECHA_CREAC_PERS+", "+
                                Utilidades.CAMPO_USUARIO_EDIC_PERS+", "+
                                Utilidades.CAMPO_FECHA_EDIC_PERS+", "+
                                Utilidades.CAMPO_FLAG_PERS+") VALUES("+
                                "'"+nombres+"', "+
                                "'', "+
                                "'', "+
                                "'', "+
                                "'"+direc+"', "+
                                "'', "+
                                "'', "+
                                "'', "+
                                "'', "+
                                "'"+telef+"', "+
                                "'', "+
                                "'"+tipoPers+"', "+
                                "'"+tipoDocPrinc+"', "+
                                "'"+numDocPrincip+"', "+
                                "'"+tipoDocSec+"', "+
                                "'"+numDocSec+"', "+
                                String.valueOf(activo)+", "+
                                "'"+NavDrawerActivity.login_email_usuario+"', "+
                                "'"+fechaCreac+"', "+
                                "'', "+
                                "'', "+
                                String.valueOf(0)+")";

                        db.execSQL(insert);

                        int rowid= -10;
                        Cursor cursor = db.rawQuery("SELECT last_insert_rowid()", null);
                        while(cursor.moveToNext()) {
                            rowid = cursor.getInt(0);
                        }

                        //Insertamos el cliente en tabla CLIENTE
                        String insert2 = "INSERT INTO "+ Utilidades.TABLA_CLIENTE+"("+
                                Utilidades.CAMPO_ID_CLIENTE_CLI+", "+
                                Utilidades.CAMPO_EMAIL_CONTACTO_CLI+", "+
                                Utilidades.CAMPO_EMAIL_CONFIRMADO_CLI+", "+
                                Utilidades.CAMPO_ACTIVO_CLI+", "+
                                Utilidades.CAMPO_USUARIO_CREAC_CLI+", "+
                                Utilidades.CAMPO_FECHA_CREAC_CLI+", "+
                                Utilidades.CAMPO_USUARIO_EDIC_CLI+", "+
                                Utilidades.CAMPO_FECHA_EDIC_CLI+", "+
                                Utilidades.CAMPO_ID_EMISOR_CREAC_CLI+", "+
                                Utilidades.CAMPO_CONDICION_PAGO_CLI+", "+
                                Utilidades.CAMPO_FLAG_CLI+") VALUES("+
                                String.valueOf(rowid)+", "+
                                "'"+emailContac+"', "+
                                String.valueOf(0)+", "+
                                String.valueOf(activo)+", "+
                                "'"+NavDrawerActivity.login_email_usuario+"', "+
                                "'"+fechaCreac+"', "+
                                "'', "+
                                "'', "+
                                String.valueOf(Constants.idEmisor)+", "+
                                String.valueOf(0)+", "+
                                String.valueOf(0)+")";

                        db.execSQL(insert2);

                        //Insertamos en tabla PERSONA con ID = -100
                        String insert3 = "INSERT INTO "+ Utilidades.TABLA_PERSONA+"("+
                                Utilidades.CAMPO_ID_PERS+", "+
                                Utilidades.CAMPO_NOMBRE_PERS+", "+
                                Utilidades.CAMPO_NOMBRES_PERS+", "+
                                Utilidades.CAMPO_AP_PATERNO_PERS+", "+
                                Utilidades.CAMPO_AP_MATERNO_PERS+", "+
                                Utilidades.CAMPO_DIREC_PRINC_PERS+", "+
                                Utilidades.CAMPO_DIREC_SECUND_PERS+", "+
                                Utilidades.CAMPO_COD_UBIG_PRINC_PERS+", "+
                                Utilidades.CAMPO_COD_UBIG_SECUND_PERS+", "+
                                Utilidades.CAMPO_COD_PAIS_PERS+", "+
                                Utilidades.CAMPO_TELEFONO_PERS+", "+
                                Utilidades.CAMPO_FAX_PERS+", "+
                                Utilidades.CAMPO_TIPO_PERSONA_PERS+", "+
                                Utilidades.CAMPO_TIPO_DOC_PRINC_PERS+", "+
                                Utilidades.CAMPO_NUM_DOC_PRINC_PERS+", "+
                                Utilidades.CAMPO_TIPO_DOC_SECUND_PERS+", "+
                                Utilidades.CAMPO_NUM_DOC_SECUND_PERS+", "+
                                Utilidades.CAMPO_ACTIVO_PERS+", "+
                                Utilidades.CAMPO_USUARIO_CREAC_PERS+", "+
                                Utilidades.CAMPO_FECHA_CREAC_PERS+", "+
                                Utilidades.CAMPO_USUARIO_EDIC_PERS+", "+
                                Utilidades.CAMPO_FECHA_EDIC_PERS+", "+
                                Utilidades.CAMPO_FLAG_PERS+") VALUES("+
                                String.valueOf(-100)+", "+
                                "'"+nombres+"', "+
                                "'', "+
                                "'', "+
                                "'', "+
                                "'"+direc+"', "+
                                "'', "+
                                "'', "+
                                "'', "+
                                "'', "+
                                "'"+telef+"', "+
                                "'', "+
                                "'"+tipoPers+"', "+
                                "'"+tipoDocPrinc+"', "+
                                "'"+numDocPrincip+"', "+
                                "'"+tipoDocSec+"', "+
                                "'"+numDocSec+"', "+
                                String.valueOf(activo)+", "+
                                "'"+NavDrawerActivity.login_email_usuario+"', "+
                                "'"+fechaCreac+"', "+
                                "'', "+
                                "'', "+
                                String.valueOf(0)+")";

                        db.execSQL(insert3);

                        //Insertamos en tabla CLIENTE con ID -100
                        String insert4 = "INSERT INTO "+ Utilidades.TABLA_CLIENTE+"("+
                                Utilidades.CAMPO_ID_CLIENTE_CLI+", "+
                                Utilidades.CAMPO_EMAIL_CONTACTO_CLI+", "+
                                Utilidades.CAMPO_EMAIL_CONFIRMADO_CLI+", "+
                                Utilidades.CAMPO_ACTIVO_CLI+", "+
                                Utilidades.CAMPO_USUARIO_CREAC_CLI+", "+
                                Utilidades.CAMPO_FECHA_CREAC_CLI+", "+
                                Utilidades.CAMPO_USUARIO_EDIC_CLI+", "+
                                Utilidades.CAMPO_FECHA_EDIC_CLI+", "+
                                Utilidades.CAMPO_ID_EMISOR_CREAC_CLI+", "+
                                Utilidades.CAMPO_CONDICION_PAGO_CLI+", "+
                                Utilidades.CAMPO_FLAG_CLI+") VALUES("+
                                String.valueOf(-100)+", "+
                                "'"+emailContac+"', "+
                                String.valueOf(0)+", "+
                                String.valueOf(activo)+", "+
                                "'"+NavDrawerActivity.login_email_usuario+"', "+
                                "'"+fechaCreac+"', "+
                                "'', "+
                                "'', "+
                                String.valueOf(Constants.idEmisor)+", "+
                                String.valueOf(0)+", "+
                                String.valueOf(0)+")";

                        db.execSQL(insert4);
                        db.close();

                        //Limpia controles y cambia imagen de persona en el actionBar  --------------------
                        spinnerTipoPers.setSelection(0);
                        editTextNomPNat.setText("");
                        editTextDniPNat.setText("");
                        editTextEmailPNat.setText("");
                        MenuItem menuItemPerson = actionBarMenu.findItem(R.id.action_person_nav_drawer);
                        menuItemPerson.setIcon(R.drawable.user_cheked_icon);

                        //Cierra popup
                        dialog.cancel();

                    }
                });

                btnCancelarPNat.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.cancel();
                    }
                });

                //Eventos Clic Guardar PJuridica
                btnGuardarPJur.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        //Recupera datos de persona
                        String nombres = editTextNomPJur.getText().toString();
                        String tipoPers="";
                        if(spinnerTipoPers.getSelectedItemPosition()==1){
                            tipoPers = "02";
                        }
                        if(spinnerTipoPers.getSelectedItemPosition()==2){
                            tipoPers = "01";
                        }

                        String telef = "";
                        String tipoDocPrinc = "6";
                        String numDocPrincip = editTextRucPJur.getText().toString();
                        String tipoDocSec = "";
                        String numDocSec = "";
                        String direc = "";
                        int activo = 1;

                        //Obtenemos datetime actual del sistema
                        Date date = new Date();
                        //DateFormat hourdateFormat0 = new SimpleDateFormat("HH:mm:ss dd/MM/yyyy");
                        DateFormat hourdateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
                        //DateFormat hourdateFormat2 = DateFormat.getDateTimeInstance();
                        String fechaCreac = hourdateFormat.format(date);

                        String emailContac = editTextEmailPJur.getText().toString();

                        //Inserta persona en tablas persona y cliente, con ID positivo y con ID -100 para agregar al carrito -----------------------------------------------------------------------
                        SQLiteDatabase db = conn.getWritableDatabase();
                        //Insertamos en tabla PERSONA recuperando el idPersona
                        String insert = "INSERT INTO "+ Utilidades.TABLA_PERSONA+"("+
                                Utilidades.CAMPO_NOMBRE_PERS+", "+
                                Utilidades.CAMPO_NOMBRES_PERS+", "+
                                Utilidades.CAMPO_AP_PATERNO_PERS+", "+
                                Utilidades.CAMPO_AP_MATERNO_PERS+", "+
                                Utilidades.CAMPO_DIREC_PRINC_PERS+", "+
                                Utilidades.CAMPO_DIREC_SECUND_PERS+", "+
                                Utilidades.CAMPO_COD_UBIG_PRINC_PERS+", "+
                                Utilidades.CAMPO_COD_UBIG_SECUND_PERS+", "+
                                Utilidades.CAMPO_COD_PAIS_PERS+", "+
                                Utilidades.CAMPO_TELEFONO_PERS+", "+
                                Utilidades.CAMPO_FAX_PERS+", "+
                                Utilidades.CAMPO_TIPO_PERSONA_PERS+", "+
                                Utilidades.CAMPO_TIPO_DOC_PRINC_PERS+", "+
                                Utilidades.CAMPO_NUM_DOC_PRINC_PERS+", "+
                                Utilidades.CAMPO_TIPO_DOC_SECUND_PERS+", "+
                                Utilidades.CAMPO_NUM_DOC_SECUND_PERS+", "+
                                Utilidades.CAMPO_ACTIVO_PERS+", "+
                                Utilidades.CAMPO_USUARIO_CREAC_PERS+", "+
                                Utilidades.CAMPO_FECHA_CREAC_PERS+", "+
                                Utilidades.CAMPO_USUARIO_EDIC_PERS+", "+
                                Utilidades.CAMPO_FECHA_EDIC_PERS+", "+
                                Utilidades.CAMPO_FLAG_PERS+") VALUES("+
                                "'"+nombres+"', "+
                                "'', "+
                                "'', "+
                                "'', "+
                                "'"+direc+"', "+
                                "'', "+
                                "'', "+
                                "'', "+
                                "'', "+
                                "'"+telef+"', "+
                                "'', "+
                                "'"+tipoPers+"', "+
                                "'"+tipoDocPrinc+"', "+
                                "'"+numDocPrincip+"', "+
                                "'"+tipoDocSec+"', "+
                                "'"+numDocSec+"', "+
                                String.valueOf(activo)+", "+
                                "'"+NavDrawerActivity.login_email_usuario+"', "+
                                "'"+fechaCreac+"', "+
                                "'', "+
                                "'', "+
                                String.valueOf(0)+")";

                        db.execSQL(insert);

                        int rowid= -10;
                        Cursor cursor = db.rawQuery("SELECT last_insert_rowid()", null);
                        while(cursor.moveToNext()) {
                            rowid = cursor.getInt(0);
                        }

                        //Insertamos el cliente en tabla CLIENTE
                        String insert2 = "INSERT INTO "+ Utilidades.TABLA_CLIENTE+"("+
                                Utilidades.CAMPO_ID_CLIENTE_CLI+", "+
                                Utilidades.CAMPO_EMAIL_CONTACTO_CLI+", "+
                                Utilidades.CAMPO_EMAIL_CONFIRMADO_CLI+", "+
                                Utilidades.CAMPO_ACTIVO_CLI+", "+
                                Utilidades.CAMPO_USUARIO_CREAC_CLI+", "+
                                Utilidades.CAMPO_FECHA_CREAC_CLI+", "+
                                Utilidades.CAMPO_USUARIO_EDIC_CLI+", "+
                                Utilidades.CAMPO_FECHA_EDIC_CLI+", "+
                                Utilidades.CAMPO_ID_EMISOR_CREAC_CLI+", "+
                                Utilidades.CAMPO_CONDICION_PAGO_CLI+", "+
                                Utilidades.CAMPO_FLAG_CLI+") VALUES("+
                                String.valueOf(rowid)+", "+
                                "'"+emailContac+"', "+
                                String.valueOf(0)+", "+
                                String.valueOf(activo)+", "+
                                "'"+NavDrawerActivity.login_email_usuario+"', "+
                                "'"+fechaCreac+"', "+
                                "'', "+
                                "'', "+
                                String.valueOf(Constants.idEmisor)+", "+
                                String.valueOf(0)+", "+
                                String.valueOf(0)+")";

                        db.execSQL(insert2);

                        //Insertamos en tabla PERSONA con ID = -100
                        String insert3 = "INSERT INTO "+ Utilidades.TABLA_PERSONA+"("+
                                Utilidades.CAMPO_ID_PERS+", "+
                                Utilidades.CAMPO_NOMBRE_PERS+", "+
                                Utilidades.CAMPO_NOMBRES_PERS+", "+
                                Utilidades.CAMPO_AP_PATERNO_PERS+", "+
                                Utilidades.CAMPO_AP_MATERNO_PERS+", "+
                                Utilidades.CAMPO_DIREC_PRINC_PERS+", "+
                                Utilidades.CAMPO_DIREC_SECUND_PERS+", "+
                                Utilidades.CAMPO_COD_UBIG_PRINC_PERS+", "+
                                Utilidades.CAMPO_COD_UBIG_SECUND_PERS+", "+
                                Utilidades.CAMPO_COD_PAIS_PERS+", "+
                                Utilidades.CAMPO_TELEFONO_PERS+", "+
                                Utilidades.CAMPO_FAX_PERS+", "+
                                Utilidades.CAMPO_TIPO_PERSONA_PERS+", "+
                                Utilidades.CAMPO_TIPO_DOC_PRINC_PERS+", "+
                                Utilidades.CAMPO_NUM_DOC_PRINC_PERS+", "+
                                Utilidades.CAMPO_TIPO_DOC_SECUND_PERS+", "+
                                Utilidades.CAMPO_NUM_DOC_SECUND_PERS+", "+
                                Utilidades.CAMPO_ACTIVO_PERS+", "+
                                Utilidades.CAMPO_USUARIO_CREAC_PERS+", "+
                                Utilidades.CAMPO_FECHA_CREAC_PERS+", "+
                                Utilidades.CAMPO_USUARIO_EDIC_PERS+", "+
                                Utilidades.CAMPO_FECHA_EDIC_PERS+", "+
                                Utilidades.CAMPO_FLAG_PERS+") VALUES("+
                                String.valueOf(-100)+", "+
                                "'"+nombres+"', "+
                                "'', "+
                                "'', "+
                                "'', "+
                                "'"+direc+"', "+
                                "'', "+
                                "'', "+
                                "'', "+
                                "'', "+
                                "'"+telef+"', "+
                                "'', "+
                                "'"+tipoPers+"', "+
                                "'"+tipoDocPrinc+"', "+
                                "'"+numDocPrincip+"', "+
                                "'"+tipoDocSec+"', "+
                                "'"+numDocSec+"', "+
                                String.valueOf(activo)+", "+
                                "'"+NavDrawerActivity.login_email_usuario+"', "+
                                "'"+fechaCreac+"', "+
                                "'', "+
                                "'', "+
                                String.valueOf(0)+")";

                        db.execSQL(insert3);

                        //Insertamos en tabla CLIENTE con ID -100
                        String insert4 = "INSERT INTO "+ Utilidades.TABLA_CLIENTE+"("+
                                Utilidades.CAMPO_ID_CLIENTE_CLI+", "+
                                Utilidades.CAMPO_EMAIL_CONTACTO_CLI+", "+
                                Utilidades.CAMPO_EMAIL_CONFIRMADO_CLI+", "+
                                Utilidades.CAMPO_ACTIVO_CLI+", "+
                                Utilidades.CAMPO_USUARIO_CREAC_CLI+", "+
                                Utilidades.CAMPO_FECHA_CREAC_CLI+", "+
                                Utilidades.CAMPO_USUARIO_EDIC_CLI+", "+
                                Utilidades.CAMPO_FECHA_EDIC_CLI+", "+
                                Utilidades.CAMPO_ID_EMISOR_CREAC_CLI+", "+
                                Utilidades.CAMPO_CONDICION_PAGO_CLI+", "+
                                Utilidades.CAMPO_FLAG_CLI+") VALUES("+
                                String.valueOf(-100)+", "+
                                "'"+emailContac+"', "+
                                String.valueOf(0)+", "+
                                String.valueOf(activo)+", "+
                                "'"+NavDrawerActivity.login_email_usuario+"', "+
                                "'"+fechaCreac+"', "+
                                "'', "+
                                "'', "+
                                String.valueOf(Constants.idEmisor)+", "+
                                String.valueOf(0)+", "+
                                String.valueOf(0)+")";

                        db.execSQL(insert4);
                        db.close();

                        //Limpia controles y cambia imagen de persona en el actionBar  --------------------
                        spinnerTipoPers.setSelection(0);
                        editTextNomPJur.setText("");
                        editTextRucPJur.setText("");
                        editTextEmailPJur.setText("");
                        MenuItem menuItemPerson = actionBarMenu.findItem(R.id.action_person_nav_drawer);
                        menuItemPerson.setIcon(R.drawable.user_cheked_icon);

                        //Cierra popup
                        dialog.cancel();

                    }
                });


                btnCancelarPJur.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.cancel();
                    }
                });

                dialog.show();

                //---------------------------------------------------------------------------


                return true;
            //}



        } else if(id == R.id.action_person_nav_drawer_buscar) {



            return true;
        } else if(id == R.id.action_person_nav_drawer_eliminar) {
            //Eliminamos cliente carrito(id= -100 en tablas cliente y persona), al eliminar la persona con id=-100 se elimina por cascada el cliente de id=-100
            SQLiteDatabase db = conn.getWritableDatabase();
            db.delete(Utilidades.TABLA_PERSONA,Utilidades.CAMPO_ID_PERS+"=-100",null);
            db.delete(Utilidades.TABLA_CLIENTE,Utilidades.CAMPO_ID_CLIENTE_CLI+"=-100"+" AND "+Utilidades.CAMPO_ID_EMISOR_CREAC_CLI+"="+Constants.idEmisor,null);
            db.close();

            MenuItem menuItemPerson = actionBarMenu.findItem(R.id.action_person_nav_drawer);
            menuItemPerson.setIcon(R.drawable.add_user_icon);

            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();
        if (id == R.id.nav_ordenar) {

        } else if (id == R.id.nav_pedidos) {
            Intent i = new Intent(NavDrawerActivity.this, ComprobantesActivity.class);
            startActivity(i);
            finish();
        } else if (id == R.id.nav_cobranzas) {
            Intent i = new Intent(NavDrawerActivity.this, ComprobantesActivity.class);
            startActivity(i);
            finish();
        } else if (id == R.id.nav_productos) {
            Intent i = new Intent(NavDrawerActivity.this, ProductosActivity.class);
            startActivity(i);
            finish();
        } else if (id == R.id.nav_administrar) {
            Intent i = new Intent(NavDrawerActivity.this, AdministrarActivity.class);
            startActivity(i);
            finish();
        } else if (id == R.id.nav_configuracion) {
            Intent i = new Intent(NavDrawerActivity.this, ConfiguracionActivity.class);
            startActivity(i);
            finish();
        } else if (id == R.id.nav_logout){

            AlertDialog.Builder builder = new AlertDialog.Builder(NavDrawerActivity.this);
            LayoutInflater inflater = getLayoutInflater();
            View dialogView = inflater.inflate(R.layout.popup_cerrar_cesion,null);
            builder.setView(dialogView);

            //inicializarComponentesPopUp();
            btnSiLogout = (Button) dialogView.findViewById(R.id.btn_si_logout);
            btnNoLogout = (Button) dialogView.findViewById(R.id.btn_no_logout);

            final AlertDialog dialog = builder.create();

            //Eventos Click
            btnSiLogout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    cerrarSesion();
                    finish();
                    dialog.cancel();
                }
            });

            btnNoLogout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialog.cancel();
                    Menu menu = navigationView.getMenu();
                    MenuItem itemVentas = menu.findItem(R.id.nav_ordenar);
                    itemVentas.setChecked(true);
                }
            });

            dialog.show();

        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout_nav_drawer);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);

        spanCount = newConfig.orientation == ORIENTATION_PORTRAIT ? 2 : 3;
        ((GridLayoutManager)layoutManagerProductos).setSpanCount(spanCount);

        /*Toast.makeText(this, "onConfigurationChanged()", Toast.LENGTH_SHORT).show();
        sharedpreferences = getSharedPreferences(LoginActivity.MyPREFERENCES, Context.MODE_PRIVATE);

        login_way = sharedpreferences.getString("enter_mode","EnterModeEnCasoDeNull");

        if (login_way.equals("login")) {
            Menu menu = navigationView.getMenu();
            MenuItem itemVentas = menu.findItem(R.id.nav_ordenar);
            MenuItem itemComprobantes = menu.findItem(R.id.nav_comprobantes);
            MenuItem itemProductos = menu.findItem(R.id.nav_productos);
            MenuItem itemComentarios = menu.findItem(R.id.nav_comentarios);
            MenuItem itemAdministrar = menu.findItem(R.id.nav_administrar);
            MenuItem itemConfiguracion = menu.findItem(R.id.nav_configuracion);
            MenuItem itemSoporte = menu.findItem(R.id.nav_soporte);

            if (itemEmisiones.isChecked()) {
                FragmentManager fragmentManager = getSupportFragmentManager();
                fragmentManager.beginTransaction().replace(R.id.contenedor_nav_drawer, new EmisionesFragment()).commit();
            } else if (itemConsultas.isChecked()) {
                FragmentManager fragmentManager = getSupportFragmentManager();
                fragmentManager.beginTransaction().replace(R.id.contenedor_nav_drawer, new ConsultasFragment()).commit();
            } else if(itemAdministrar.isChecked()) {
                FragmentManager fragmentManager = getSupportFragmentManager();
                fragmentManager.beginTransaction().replace(R.id.contenedor_nav_drawer, new AdministrarFragment()).commit();
            }
        }

*/
        alert("onConfigurationChanged");
    }

    private void limpiaCarritoYCliente() {
        //Borra cliente de tabla persona y de tabla cliente, luego setea el actionIcon de No Cliente
        SQLiteDatabase db = conn.getWritableDatabase();
        String whereClause = Utilidades.CAMPO_ID_PERS+" = ?";
        String whereArgs[] = {String.valueOf(-100)};
        db.delete(Utilidades.TABLA_PERSONA, whereClause, whereArgs);

        String whereClause1 = Utilidades.CAMPO_ID_CLIENTE_CLI + " = ";
        String whereArgs1[] = null;
        db.delete(Utilidades.TABLA_CLIENTE, whereClause1, whereArgs1);

        //Borra productos de tabla carrito y setea 0 en textViewCantProducts

    }

    private void consultaCarritoYClienteYSeteaValores(){

        // =============================================================================== PRODUCTOS CARRITO =====================================================================================
        SQLiteDatabase db = conn.getReadableDatabase();
        int cantidadProductos = 0;
        Cursor cursor3 = db.rawQuery("SELECT * FROM "+ Utilidades.TABLA_CARRITO + " WHERE " + Utilidades.CAMPO_ID_EMISOR_CARR + "=" + String.valueOf(idEmisor), null);
        while(cursor3.moveToNext()){
            cantidadProductos += cursor3.getInt(9); //Acumulo las cantidades de cada uno de los productos del carrito
        }
        //Seteo cantidad total de productos en carrito
        textViewCantItems.setText(String.valueOf(cantidadProductos));
        cursor3.close();
        db.close();

    }


    /*public void setActionBarTitle(String title) {
        getSupportActionBar().setTitle(title);
    }*/

    private void inicializarDatosUsuario(NavigationView navigationView) {
        View hview = navigationView.getHeaderView(0);
        ImageView imageViewLogoEmpresa = (ImageView) hview.findViewById(R.id.imageViewLogoEmpresa);   imageViewLogoEmpresa.setImageResource(R.drawable.imagen_empresa);
        TextView nombEmpresaMenuLat = (TextView) hview.findViewById(R.id.textViewNomEmpLatMenu);
        String parteNombreEmpresa = "";
        if(nombreEmpresa.length() > 25){
            parteNombreEmpresa = nombreEmpresa.substring(0,24);
            nombEmpresaMenuLat.setText(parteNombreEmpresa);
        } else {
            nombEmpresaMenuLat.setText(nombreEmpresa);
        }
        TextView nombUsuarioMenuLat = (TextView) hview.findViewById(R.id.textViewUserLatMenu);    nombUsuarioMenuLat.setText(login_email_usuario);
    }

    private void alert(String s) {
        Toast.makeText(NavDrawerActivity.this,s,Toast.LENGTH_SHORT).show();
    }

    public void cerrarSesion(){
        SharedPreferences sharedpreferences = getSharedPreferences(LoginActivity.MyPREFERENCES, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedpreferences.edit();
        editor.clear();
        editor.commit();
    }

    public void inicializarControles(){
        imageViewTicketActual = (ImageView) findViewById(R.id.btnTicket);
        imageViewCobrar1 = (ImageView) findViewById(R.id.imageViewCobrar1);
        linearNoCliCarr = (LinearLayout) findViewById(R.id.linearDatosNoClienteCarr);
        linearCliCarr = (LinearLayout) findViewById(R.id.linearDatosClienteCarr);
        textViewClientes = (TextView) findViewById(R.id.textViewCliente);
        imageViewSearchCli = (ImageView) findViewById(R.id.imageViewSearchCli);
        imageViewAddCli = (ImageView) findViewById(R.id.imageViewAddCli);
        textViewNomCliCarr = (TextView) findViewById(R.id.textViewNomCliCarr);
        textViewTipoDocCliCarr = (TextView) findViewById(R.id.textViewTipoDocCliCarr);
        textViewNroDocCliCarr = (TextView) findViewById(R.id.textViewNroDocCliCarr);
        textViewCantItems = (TextView) findViewById(R.id.textViewCantItems);
        //textViewMontoCobrar = (TextView) findViewById(R.id.textViewMontoCobrar);   String montoInicialCobrar= "0.00"; textViewMontoCobrar.setText(montoInicialCobrar);
        //imageButtonBuscarProd = (Button) findViewById(R.id.imageBtnBuscarProd);
        imageViewBuscarProd = (ImageView) findViewById(R.id.imageBtnBuscarProd);
        spinnerCategInProdActiv = (Spinner) findViewById(R.id.spinnerCategVentAct);
        productosRecyclerView = (RecyclerView) findViewById(R.id.productosRecyclerViewVentasAct);

        /*
        //Consultar cliente de tabla temporal

        //Decide visibilidad de controles
        if(Hay cliente seteado){
            muestro vista cliente seteado
        } else {
            Muestro vista sin cliente
        }

        //Consultar cantidad de items en tabla temporal
        //#ProductosSeteados = db.exec...
        if(#ProductosSeteados>0){
            muestro cantidad en cajita
        } else {
            muestro 0
        }
        */

    }

    public void eventosClick(){
        imageViewTicketActual.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent i = new Intent(NavDrawerActivity.this, TicketActualActivity.class);
                startActivity(i);

            }
        });

        imageViewBuscarProd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {



            }
        });

    }

    private void consultarListasCategorias() {
        SQLiteDatabase db = conn.getReadableDatabase();

        Categoria categoria = null;
        arrayListValoresCategorias = new ArrayList<Categoria>();
        //Select * from
        Cursor cursor = db.rawQuery("SELECT * FROM "+ Utilidades.TABLA_CATEGORIA + " WHERE " + Utilidades.CAMPO_ID_EMISOR_CATEG + "=" + String.valueOf(idEmisor), null);
        while(cursor.moveToNext()){
            categoria = new Categoria();
            categoria.setIdEmisor(cursor.getInt(0));
            categoria.setCodCategoria(cursor.getString(1));
            categoria.setDescripcion(cursor.getString(2));
            categoria.setAbreviatura(cursor.getString(3));
            categoria.setActivo(cursor.getInt(4));
            categoria.setUsuarioCreacion(cursor.getString(5));
            categoria.setFechaCreacion(cursor.getString(6));
            categoria.setUsuarioEdicion(cursor.getString(7));
            categoria.setFechaEdicion(cursor.getString(8));

            arrayListValoresCategorias.add(categoria);
        }
        cursor.close();
        db.close();
        obtenerListaMostrarCategorias();
    }

    private void obtenerListaMostrarCategorias() {
        arrayListMostrarCategorias = new ArrayList<String>();
        arrayListMostrarCategorias.add("Seleccione");

        for (int i=0; i<arrayListValoresCategorias.size(); i++){
            arrayListMostrarCategorias.add(arrayListValoresCategorias.get(i).getAbreviatura());
        }

    }

    private void consultarListasProductos() {

        arrayListProductos = new ArrayList<>();

        //LayoutManager
        spanCount = getResources().getConfiguration().orientation == ORIENTATION_PORTRAIT ? 2 : 3;
        layoutManagerProductos = new GridLayoutManager(this, spanCount);
        productosRecyclerView.setLayoutManager(layoutManagerProductos);

        SQLiteDatabase db = conn.getReadableDatabase();

        Producto producto = null;
        //Select * from
        Cursor cursor = db.rawQuery("SELECT * FROM "+ Utilidades.TABLA_PRODUCTO + " WHERE " + Utilidades.CAMPO_ID_EMISOR_PROD + "=" + String.valueOf(idEmisor) + " ORDER BY " + Utilidades.CAMPO_NRO_PLATO_CARTA_PROD + " ASC", null);
        while(cursor.moveToNext()){
            producto = new Producto();
            producto.setIdEmisor(cursor.getInt(0));
            producto.setCodProducto(cursor.getString(1));
            producto.setCodCategoria(cursor.getString(2));
            producto.setNroPlatoCarta(cursor.getInt(3));
            producto.setDescripcion(cursor.getString(4));
            producto.setTipoMoneda(cursor.getString(5));
            producto.setValorUnitario(cursor.getFloat(6));
            producto.setActivo(cursor.getInt(7));
            producto.setImagePath(cursor.getString(8));
            producto.setUsuarioCreacion(cursor.getString(9));
            producto.setFechaCreacion(cursor.getString(10));
            producto.setUsuarioEdicion(cursor.getString(11));
            producto.setFechaEdicion(cursor.getString(12));

            arrayListProductos.add(producto);
        }
        cursor.close();
        db.close();

        //Adapter
        listaProductosAdapter = new ListaProductosNavDrawerAdapter(arrayListProductos, textViewCantItems, context, conn);
        productosRecyclerView.setAdapter(listaProductosAdapter);

    }

    private void consultarListasProductosByCategoria(String codCategoria) {

        arrayListProductos = new ArrayList<>();

        //LayoutManager
        spanCount = getResources().getConfiguration().orientation == ORIENTATION_PORTRAIT ? 2 : 3;
        layoutManagerProductos = new GridLayoutManager(this, spanCount);
        productosRecyclerView.setLayoutManager(layoutManagerProductos);

        SQLiteDatabase db = conn.getReadableDatabase();

        Producto producto = null;

        //Select * from
        Cursor cursor = db.rawQuery("SELECT * FROM "+ Utilidades.TABLA_PRODUCTO + " WHERE "+ Utilidades.CAMPO_COD_CATEG_PROD + "='" + codCategoria +"' AND " + Utilidades.CAMPO_ID_EMISOR_PROD + "=" + String.valueOf(idEmisor) + " ORDER BY " + Utilidades.CAMPO_NRO_PLATO_CARTA_PROD + " ASC", null);
        while(cursor.moveToNext()){
            producto = new Producto();
            producto.setIdEmisor(cursor.getInt(0));
            producto.setCodProducto(cursor.getString(1));
            producto.setCodCategoria(cursor.getString(2));
            producto.setNroPlatoCarta(cursor.getInt(3));
            producto.setDescripcion(cursor.getString(4));
            producto.setTipoMoneda(cursor.getString(5));
            producto.setValorUnitario(cursor.getFloat(6));
            producto.setActivo(cursor.getInt(7));
            producto.setImagePath(cursor.getString(8));
            producto.setUsuarioCreacion(cursor.getString(9));
            producto.setFechaCreacion(cursor.getString(10));
            producto.setUsuarioEdicion(cursor.getString(11));
            producto.setFechaEdicion(cursor.getString(12));

            arrayListProductos.add(producto);
        }
        cursor.close();
        db.close();

        //Adapter
        listaProductosAdapter = new ListaProductosNavDrawerAdapter(arrayListProductos, textViewCantItems, context, conn);
        productosRecyclerView.setAdapter(listaProductosAdapter);

    }

/*
    public static void moveItem(ImageView imageViewVolador){

        ObjectAnimator animatorX = ObjectAnimator.ofFloat(imageViewVolador, "x", 200f);
        ObjectAnimator animatorY = ObjectAnimator.ofFloat(imageViewVolador, "y", 140f);
        ObjectAnimator alphaAnimation = ObjectAnimator.ofFloat(imageViewVolador, View.ALPHA, 1.0f, 0.0f);
        ObjectAnimator rotateAnimation = ObjectAnimator.ofFloat(imageViewVolador,"rotation",0f, 250f);
        animatorX.setDuration(animationDuration);  animatorY.setDuration(animationDuration);  alphaAnimation.setDuration(animationDuration);  rotateAnimation.setDuration(animationDuration);
        AnimatorSet animatorSet = new AnimatorSet();
        animatorSet.playTogether(animatorX,animatorY,rotateAnimation,alphaAnimation);
        animatorSet.start();

    }
*/


}
