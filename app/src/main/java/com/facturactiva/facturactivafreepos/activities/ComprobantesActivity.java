package com.facturactiva.facturactivafreepos.activities;

import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.facturactiva.facturactivafreepos.R;
import com.facturactiva.facturactivafreepos.adapters.ListaComprobantesAdapter;
import com.facturactiva.facturactivafreepos.constants.Constants;
import com.facturactiva.facturactivafreepos.models.AdditionalData;
import com.facturactiva.facturactivafreepos.models.Comprobante;
import com.facturactiva.facturactivafreepos.models.Detail;
import com.facturactiva.facturactivafreepos.models.Discount;
import com.facturactiva.facturactivafreepos.models.DocElectronico;
import com.facturactiva.facturactivafreepos.models.Document;
import com.facturactiva.facturactivafreepos.models.Tax;
import com.facturactiva.facturactivafreepos.network.MyApi;
import com.facturactiva.facturactivafreepos.sqlite.ConexionSQLiteHelper;
import com.facturactiva.facturactivafreepos.utilities.Utilidades;

import java.util.ArrayList;

import retrofit2.Retrofit;

import static com.facturactiva.facturactivafreepos.network.HostingFacturactivaServices.API_BASE_URL;

public class ComprobantesActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {

    NavigationView navigationView;
    Button btnSiLogout, btnNoLogout, btnOkCerrarApp, btnCancelCerrarApp;
    SharedPreferences sharedPreferences;
    String login_way;
    public static String nombreEmpresa="", login_email_usuario="", login_pass_usuario="";
    public static int idEmisor = -1;

    LinearLayout linearLayoutFiltros,linearLayoutBotones,linearListadoComprobantes;
    ListaComprobantesAdapter listaComprobantesAdapter;
    RecyclerView.LayoutManager layoutManagerComprobantes;
    RecyclerView recyclerComprobantes;
    Button btnEmitirComprobantes;
    ArrayList<DocElectronico> arrayListDocElectronicos;
    ConexionSQLiteHelper conn;




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_comprobantes);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar_comprobantes);
        setSupportActionBar(toolbar);
        conn = new ConexionSQLiteHelper(ComprobantesActivity.this, "facturactiva-service", null, 1);
        sharedPreferences = getSharedPreferences(LoginActivity.MyPREFERENCES, Context.MODE_PRIVATE);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout_comprobantes);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();
        navigationView = (NavigationView) findViewById(R.id.nav_view_comprobantes);
        navigationView.setNavigationItemSelectedListener(this);

        login_way = sharedPreferences.getString("enter_mode","EnterModeEnCasoDeNull");

        if(login_way.equals("login")) {
            login_email_usuario = sharedPreferences.getString("username","UsernameEnCasoDeNull");
            login_pass_usuario = sharedPreferences.getString("password","PasswordEnCasoDeNull");
            nombreEmpresa = sharedPreferences.getString("nombreEmisor","NombreEmpresaEnCasoDeNull");
            idEmisor = Integer.parseInt( sharedPreferences.getString("idEmisor","idEmisorEnCasoDeNull"));
        }

        if(login_way.equals("login")){
            Menu menu = navigationView.getMenu();
            MenuItem itemComprobantes = menu.findItem(R.id.nav_comprobantes);
            itemComprobantes.setChecked(true);
            inicializarDatosUsuario(navigationView);
            inicializarControles();
            listarComprobantes();
            eventosClick();

        }


    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout_comprobantes);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {

            AlertDialog.Builder builder = new AlertDialog.Builder(ComprobantesActivity.this);
            LayoutInflater inflater = getLayoutInflater();
            View dialogView = inflater.inflate(R.layout.popup_salir_aplicacion,null);
            builder.setView(dialogView);

            //inicializarComponentesPopUp();
            btnOkCerrarApp = (Button) dialogView.findViewById(R.id.btn_ok_cerrar_app);
            btnCancelCerrarApp = (Button) dialogView.findViewById(R.id.btn_cancel_cerrar_app);

            final AlertDialog dialog = builder.create();

            //Eventos Clic
            btnOkCerrarApp.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    //moveTaskToBack(true);
                    //dialog.cancel();
                    //Process.killProcess(Process.myPid());
                    //System.exit(0);
                    finishAffinity();

                }
            });

            btnCancelCerrarApp.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialog.cancel();
                }
            });

            dialog.show();

            //---------------------------------------------------------------------------
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.comprobantes_options_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        /*if (id == R.id.action_settings) {
            return true;
        }*/

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();
        if (id == R.id.nav_ordenar) {
            Intent i = new Intent(ComprobantesActivity.this, NavDrawerActivity.class);
            startActivity(i);
            finish();
        } else if (id == R.id.nav_comprobantes) {

        } else if (id == R.id.nav_productos) {
            Intent i = new Intent(ComprobantesActivity.this, ProductosActivity.class);
            startActivity(i);
            finish();
        } else if (id == R.id.nav_administrar) {
            Intent i = new Intent(ComprobantesActivity.this, AdministrarActivity.class);
            startActivity(i);
            finish();
        } else if (id == R.id.nav_configuracion) {
            Intent i = new Intent(ComprobantesActivity.this, ConfiguracionActivity.class);
            startActivity(i);
            finish();
        } else if (id == R.id.nav_logout){

            AlertDialog.Builder builder = new AlertDialog.Builder(ComprobantesActivity.this);
            LayoutInflater inflater = getLayoutInflater();
            View dialogView = inflater.inflate(R.layout.popup_cerrar_cesion,null);
            builder.setView(dialogView);

            //inicializarComponentesPopUp();
            btnSiLogout = (Button) dialogView.findViewById(R.id.btn_si_logout);
            btnNoLogout = (Button) dialogView.findViewById(R.id.btn_no_logout);

            final AlertDialog dialog = builder.create();

            //Eventos Click
            btnSiLogout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    cerrarSesion();
                    finish();
                    dialog.cancel();
                }
            });

            btnNoLogout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialog.cancel();
                    Menu menu = navigationView.getMenu();
                    MenuItem itemComprobantes = menu.findItem(R.id.nav_comprobantes);
                    itemComprobantes.setChecked(true);
                }
            });

            dialog.show();

        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout_comprobantes);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        /*Toast.makeText(this, "onConfigurationChanged()", Toast.LENGTH_SHORT).show();
        sharedpreferences = getSharedPreferences(LoginActivity.MyPREFERENCES, Context.MODE_PRIVATE);

        login_way = sharedpreferences.getString("enter_mode","EnterModeEnCasoDeNull");

        if (login_way.equals("login")) {
            Menu menu = navigationView.getMenu();
            MenuItem itemVentas = menu.findItem(R.id.nav_ordenar);
            MenuItem itemComprobantes = menu.findItem(R.id.nav_comprobantes);
            MenuItem itemProductos = menu.findItem(R.id.nav_productos);
            MenuItem itemComentarios = menu.findItem(R.id.nav_comentarios);
            MenuItem itemAdministrar = menu.findItem(R.id.nav_administrar);
            MenuItem itemConfiguracion = menu.findItem(R.id.nav_configuracion);
            MenuItem itemSoporte = menu.findItem(R.id.nav_soporte);

            if (itemEmisiones.isChecked()) {
                FragmentManager fragmentManager = getSupportFragmentManager();
                fragmentManager.beginTransaction().replace(R.id.contenedor_nav_drawer, new EmisionesFragment()).commit();
            } else if (itemConsultas.isChecked()) {
                FragmentManager fragmentManager = getSupportFragmentManager();
                fragmentManager.beginTransaction().replace(R.id.contenedor_nav_drawer, new ConsultasFragment()).commit();
            } else if(itemAdministrar.isChecked()) {
                FragmentManager fragmentManager = getSupportFragmentManager();
                fragmentManager.beginTransaction().replace(R.id.contenedor_nav_drawer, new AdministrarFragment()).commit();
            }
        }

*/
        alert("onConfigurationChanged");
    }

    public void inicializarControles(){
        //linearLayoutFiltros = (LinearLayout) findViewById(R.id.linearFitros);
        //linearLayoutBotones = (LinearLayout) findViewById(R.id.linearBotones);
        //linearListadoComprobantes = (LinearLayout) findViewById(R.id.linearListadoComprobantes);
        recyclerComprobantes = (RecyclerView) findViewById(R.id.recyclerComprobantes);
        btnEmitirComprobantes = (Button) findViewById(R.id.btnEmitirComprobantes);

    }

    public void listarComprobantes(){

        layoutManagerComprobantes = new LinearLayoutManager(this);
        recyclerComprobantes.setLayoutManager(layoutManagerComprobantes);

        DocElectronico docElectronico = null;
        arrayListDocElectronicos = new ArrayList<DocElectronico>();
        SQLiteDatabase db = conn.getReadableDatabase();
        Cursor cursor = db.rawQuery("SELECT " + Utilidades.CAMPO_SERIE_DOC + ", " + Utilidades.CAMPO_CORRELATIVO_DOC + ", " + Utilidades.CAMPO_TIPO_DOCUM_DOC + ", " + Utilidades.CAMPO_FECHA_EMIS_DOC + ", " + Utilidades.CAMPO_MONT_TOTAL_DOC + " FROM " + Utilidades.TABLA_DOC_ELECT + " WHERE " + Utilidades.CAMPO_ID_EMISOR_DOC + "=" + String.valueOf(idEmisor),null);
        while(cursor.moveToNext()) {
            docElectronico = new DocElectronico();
            docElectronico.setSerie(cursor.getString(0));
            docElectronico.setCorrelativo(cursor.getInt(1));
            docElectronico.setTipoDocumento(cursor.getString(2));
            docElectronico.setFechaEmision(cursor.getString(3));
            docElectronico.setMntTotal(cursor.getFloat(4));

            arrayListDocElectronicos.add(docElectronico);
        }
        cursor.close();
        db.close();

        //Adapter
        listaComprobantesAdapter = new ListaComprobantesAdapter(arrayListDocElectronicos, ComprobantesActivity.this);
        recyclerComprobantes.setAdapter(listaComprobantesAdapter);

    }

    public void eventosClick(){

        btnEmitirComprobantes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                //Con el key y secret de facturactiva, la empresa emisora genera un tocken cuando inicia la aplicación
                Retrofit retrofit = new Retrofit.Builder()
                        .baseUrl(API_BASE_URL)
                        .build();

                MyApi service = retrofit.create(MyApi.class); //Creamos el servicio de Retrofit


                ArrayList<Comprobante> arrayListComprobantes = new ArrayList<>();
                SQLiteDatabase db = conn.getReadableDatabase();
                Cursor cursorDocElectronicos = db.rawQuery("SELECT * FROM " + Utilidades.TABLA_DOC_ELECT + " WHERE " + Utilidades.CAMPO_ID_EMISOR_DOC + "=" + String.valueOf(idEmisor) + " AND " + Utilidades.CAMPO_ESTADO_EMIS_DOC + "='-'",null);
                if(cursorDocElectronicos.getCount() > 0) {

                    while(cursorDocElectronicos.moveToNext()) {

                        Comprobante comprobante = new Comprobante();
                        comprobante.setTipoDocumento(cursorDocElectronicos.getString(3));
                        comprobante.setIdTransaccion(cursorDocElectronicos.getString(8));
                        comprobante.setFechaEmision(cursorDocElectronicos.getString(42));
                        comprobante.setCorreoReceptor("");

                        Document document = new Document();
                        document.setSerie(cursorDocElectronicos.getString(1));
                        document.setCorrelativo(cursorDocElectronicos.getInt(2));
                        document.setNombreEmisor(cursorDocElectronicos.getString(10));
                        document.setTipoDocEmisor(cursorDocElectronicos.getString(4));
                        document.setNumDocEmisor(cursorDocElectronicos.getString(5));
                        document.setDireccionOrigen(cursorDocElectronicos.getString(12));
                        document.setDireccionUbigeo(cursorDocElectronicos.getString(13));
                        document.setNombreComercialEmisor(cursorDocElectronicos.getString(11));
                        document.setTipoDocReceptor(cursorDocElectronicos.getString(15));
                        document.setNumDocReceptor(cursorDocElectronicos.getString(16));
                        document.setNombreReceptor(cursorDocElectronicos.getString(17));
                        document.setNombreComercialReceptor(cursorDocElectronicos.getString(18));
                        document.setTipoDocReceptorAsociado(cursorDocElectronicos.getString(21));
                        document.setNumDocReceptorAsociado(cursorDocElectronicos.getString(22));
                        document.setNombreReceptorAsociado(cursorDocElectronicos.getString(23));
                        document.setDireccionDestino(cursorDocElectronicos.getString(19));
                        document.setTipoMoneda(cursorDocElectronicos.getString(24));
                        document.setSustento(cursorDocElectronicos.getString(28));
                        document.setTipoMotivoNotaModificatoria(cursorDocElectronicos.getString(29));
                        document.setMntNeto(cursorDocElectronicos.getFloat(30));
                        document.setMntTotalIgv(cursorDocElectronicos.getFloat(34));
                        document.setMntTotal(cursorDocElectronicos.getFloat(37));
                        document.setFechaVencimiento(cursorDocElectronicos.getString(43));
                        document.setGlosaDocumento(cursorDocElectronicos.getString(44));
                        document.setCodContrato(cursorDocElectronicos.getString(48));
                        document.setCodCentroCosto("");
                        document.setTipoCambioDestino(cursorDocElectronicos.getFloat(26));
                        document.setTipoFormatoRepresentacionImpresa(cursorDocElectronicos.getString(46));

                        comprobante.setDocumento(document);

                        //Arma arrayList de Taxes del presente documento y setea en Comprobante
                        ArrayList<Tax> arrayListTaxes = new ArrayList<>();
                        Cursor cursorTaxesByDocElectronico = db.rawQuery("SELECT * FROM " + Utilidades.TABLA_DOC_ELECT_IMP + " WHERE " + Utilidades.CAMPO_ID_EMISOR_IMP + "=" + String.valueOf(idEmisor) + " AND " + Utilidades.CAMPO_TIPO_DOCUM_IMP + "=" +"'"+ comprobante.getTipoDocumento() + "'" +" AND " + Utilidades.CAMPO_SERIE_IMP + "=" +"'"+ document.getSerie() +"'"+ " AND " + Utilidades.CAMPO_CORRELATIVO_IMP + "=" + String.valueOf(document.getCorrelativo()),null);
                        if (cursorTaxesByDocElectronico.getCount() > 0) {

                            while(cursorTaxesByDocElectronico.moveToNext()) {

                                Tax tax = new Tax();
                                tax.setCodImpuesto("1000");
                                tax.setMontoImpuesto(document.getMntTotalIgv());
                                tax.setTasaImpuesto(Constants.tasaIgv);

                                arrayListTaxes.add(tax);
                            }

                        }

                        cursorTaxesByDocElectronico.close();
                        comprobante.setImpuesto(arrayListTaxes);

                        //Arma arrayList de Detalles del presente documento y setea en Comprobante
                        ArrayList<Detail> arrayListDetails = new ArrayList<>();
                        Cursor cursorDetailsByDocElectronico = db.rawQuery("SELECT * FROM " + Utilidades.TABLA_DOC_ELECT_DET + " WHERE " + Utilidades.CAMPO_ID_EMISOR_DET + "=" + String.valueOf(idEmisor) + " AND " + Utilidades.CAMPO_TIPO_DOCUM_DET + "=" +"'"+ comprobante.getTipoDocumento() +"'"+ " AND " + Utilidades.CAMPO_SERIE_DET + "=" +"'"+ document.getSerie() +"'"+ " AND " + Utilidades.CAMPO_CORRELATIVO_DET + "=" + String.valueOf(document.getCorrelativo()),null);
                        if (cursorDetailsByDocElectronico.getCount() > 0) {

                            while(cursorDetailsByDocElectronico.moveToNext()) {

                                Detail detail = new Detail();
                                detail.setCantidadItem(cursorDetailsByDocElectronico.getFloat(7));
                                detail.setUnidadMedidaItem(cursorDetailsByDocElectronico.getString(8));
                                detail.setCodItem(cursorDetailsByDocElectronico.getString(9));
                                detail.setNombreItem(cursorDetailsByDocElectronico.getString(10));
                                detail.setPrecioItem(cursorDetailsByDocElectronico.getFloat(11));
                                detail.setPrecioItemSinIgv(cursorDetailsByDocElectronico.getFloat(12));
                                detail.setMontoItem(cursorDetailsByDocElectronico.getFloat(14));
                                detail.setDescuentoMonto(cursorDetailsByDocElectronico.getFloat(15));
                                detail.setCodAfectacionIgv(cursorDetailsByDocElectronico.getString(16));
                                detail.setTasaIgv(cursorDetailsByDocElectronico.getFloat(17));
                                detail.setMontoIgv(cursorDetailsByDocElectronico.getFloat(18));
                                detail.setCodSistemaCalculoIsc(cursorDetailsByDocElectronico.getString(19));
                                detail.setMontoIsc(cursorDetailsByDocElectronico.getFloat(21));
                                detail.setTasaIsc(cursorDetailsByDocElectronico.getFloat(20));
                                detail.setPrecioItemReferencia(cursorDetailsByDocElectronico.getFloat(13));
                                detail.setIdOperacion(cursorDetailsByDocElectronico.getString(22));

                                arrayListDetails.add(detail);
                            }

                        }

                        cursorDetailsByDocElectronico.close();
                        comprobante.setDetalle(arrayListDetails);

                        Discount discount = new Discount();
                        AdditionalData additionalData = new AdditionalData();
                        comprobante.setDescuento(discount);
                        comprobante.setDatosAdicionales(additionalData);

                        arrayListComprobantes.add(comprobante);

                    }

                    /*
                    //Realizar envío de comprobantes(arrayListComprobantes)

                    final String json = "{\n"
                            + "\t\"id\": 100,\n"
                            + "\t\"name\": \"Cristhian Gonzales\"\n"
                            + "}";

                    final RequestBody requestBody = RequestBody.create(MediaType.parse("application/json"), json);
                    service.fetchAccessTocken(requestBody).enqueue(new Callback<ResponseBody>() {
                        @Override
                        public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {





                        }

                        @Override
                        public void onFailure(Call<ResponseBody> call, Throwable t) {






                        }
                    });
                    */


                    Toast.makeText(ComprobantesActivity.this, "Emitiendo comprobantes...", Toast.LENGTH_SHORT).show();


                } else {
                    Toast.makeText(ComprobantesActivity.this, "No hay comprobantes por emitir", Toast.LENGTH_LONG).show();
                }

                cursorDocElectronicos.close();

            }
        });

    }

    private void inicializarDatosUsuario(NavigationView navigationView) {
        View hview = navigationView.getHeaderView(0);
        ImageView imageViewLogoEmpresa = (ImageView) hview.findViewById(R.id.imageViewLogoEmpresa);   imageViewLogoEmpresa.setImageResource(R.drawable.imagen_empresa);
        TextView nombEmpresaMenuLat = (TextView) hview.findViewById(R.id.textViewNomEmpLatMenu);
        String parteNombreEmpresa = "";
        if(nombreEmpresa.length() > 25){
            parteNombreEmpresa = nombreEmpresa.substring(0,24);
            nombEmpresaMenuLat.setText(parteNombreEmpresa);
        } else {
            nombEmpresaMenuLat.setText(nombreEmpresa);
        }
        TextView nombUsuarioMenuLat = (TextView) hview.findViewById(R.id.textViewUserLatMenu);    nombUsuarioMenuLat.setText(login_email_usuario);
    }

    private void alert(String s) {
        Toast.makeText(ComprobantesActivity.this,s,Toast.LENGTH_SHORT).show();
    }

    public void cerrarSesion(){
        SharedPreferences sharedpreferences = getSharedPreferences(LoginActivity.MyPREFERENCES, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedpreferences.edit();
        editor.clear();
        editor.commit();
    }



}
