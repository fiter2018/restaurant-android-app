package com.facturactiva.facturactivafreepos.activities;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;

import com.facturactiva.facturactivafreepos.R;
import com.facturactiva.facturactivafreepos.adapters.ListaClientesAdapter;
import com.facturactiva.facturactivafreepos.constants.Constants;
import com.facturactiva.facturactivafreepos.models.Cliente;
import com.facturactiva.facturactivafreepos.sqlite.ConexionSQLiteHelper;
import com.facturactiva.facturactivafreepos.utilities.Utilidades;

import java.util.ArrayList;

public class ClientesActivity extends AppCompatActivity {

    RecyclerView recyclerViewClientes;
    ListaClientesAdapter listaClientesAdapter;
    RecyclerView.LayoutManager layoutManagerClientes;
    ArrayList<Cliente> arrayListClientes;
    ConexionSQLiteHelper conn;
    SharedPreferences sharedPreferences;
    int idEmisor = 0;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_clientes);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar_clientes);
        setSupportActionBar(toolbar);
        conn = new ConexionSQLiteHelper(ClientesActivity.this, "facturactiva-service", null, 1);
        sharedPreferences = getSharedPreferences(LoginActivity.MyPREFERENCES, Context.MODE_PRIVATE);



        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab_clientes);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(ClientesActivity.this, AgregarClienteCrudActivity.class);
                startActivity(i);
            }
        });
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        inicializarControles();
        eventosClick();
        listarClientes();

    }

    @Override
    protected void onResume() {
        super.onResume();
        listarClientes();
    }

    private void listarClientes() {

        arrayListClientes = new ArrayList<>();

        //LayoutManager
        layoutManagerClientes = new LinearLayoutManager(this);
        recyclerViewClientes.setLayoutManager(layoutManagerClientes);

        SQLiteDatabase db = conn.getReadableDatabase();

        Cliente cliente = null;
        //Select * from
        Cursor cursor = db.rawQuery("SELECT * FROM "+ Utilidades.TABLA_CLIENTE + " WHERE " + Utilidades.CAMPO_ID_EMISOR_CREAC_CLI + "=" + String.valueOf(idEmisor) + " AND "+Utilidades.CAMPO_ID_CLIENTE_CLI + ">0", null);
        while(cursor.moveToNext()){
            cliente = new Cliente();
            cliente.setIdCliente(cursor.getInt(0));
            cliente.setEmailContacto(cursor.getString(1));
            cliente.setEmailConfirmado(cursor.getInt(2));
            cliente.setActivo(cursor.getInt(3));
            cliente.setUsuarioCreacion(cursor.getString(4));
            cliente.setFechaCreacion(cursor.getString(5));
            cliente.setUsuarioEdicion(cursor.getString(6));
            cliente.setFechaEdicion(cursor.getString(7));
            cliente.setIdEmisorCreacion(cursor.getInt(8));
            cliente.setCondicionPago(cursor.getInt(9));

            //Traemos los datos Persona de cada cliente
            Cursor cursor2 = db.rawQuery("SELECT * FROM " + Utilidades.TABLA_PERSONA + " WHERE " + Utilidades.CAMPO_ID_PERS + "=" + cliente.getIdCliente(), null);
            while(cursor2.moveToNext()){
                cliente.setIdPersona(cursor2.getInt(0));
                cliente.setNombre(cursor2.getString(1));
                cliente.setNombres(cursor2.getString(2));
                cliente.setApellidoPaterno(cursor2.getString(3));
                cliente.setApellidoMaterno(cursor2.getString(4));
                cliente.setDireccionPrincipal(cursor2.getString(5));
                cliente.setDireccionSecundaria(cursor2.getString(6));
                cliente.setCodUbigeoPrincipal(cursor2.getString(7));
                cliente.setCodUbigeoSecundario(cursor2.getString(8));
                cliente.setCodPais(cursor2.getString(9));
                cliente.setTelefono(cursor2.getString(10));
                cliente.setFax(cursor2.getString(11));
                cliente.setTipoPersona(cursor2.getString(12));
                cliente.setTipoDocPrincipal(cursor2.getString(13));
                cliente.setNumDocPrincipal(cursor2.getString(14));
                cliente.setTipoDocSecundario(cursor2.getString(15));
                cliente.setNumDocSecundario(cursor2.getString(16));
                cliente.setActivo(cursor2.getInt(17));
                cliente.setUsuarioCreacion(cursor2.getString(18));
                cliente.setFechaCreacion(cursor2.getString(19));
                cliente.setUsuarioEdicion(cursor2.getString(20));
                cliente.setFechaEdicion(cursor2.getString(21));
            }
            cursor2.close();
            arrayListClientes.add(cliente);
        }
        cursor.close();
        db.close();
        //Adapter
        listaClientesAdapter = new ListaClientesAdapter(arrayListClientes);
        recyclerViewClientes.setAdapter(listaClientesAdapter);

    }

    private void eventosClick() {


    }

    private void inicializarControles() {
        recyclerViewClientes = (RecyclerView) findViewById(R.id.recyclerViewClientesActiv);
        idEmisor = Integer.parseInt(sharedPreferences.getString("idEmisor","idEmisorEnCasoDeNull"));
    }


}
