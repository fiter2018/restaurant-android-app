package com.facturactiva.facturactivafreepos.activities;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.facturactiva.facturactivafreepos.R;
import com.facturactiva.facturactivafreepos.models.Configuracion;
import com.facturactiva.facturactivafreepos.models.Emisor;
import com.facturactiva.facturactivafreepos.models.GeneralCuenta;
import com.facturactiva.facturactivafreepos.models.Persona;
import com.facturactiva.facturactivafreepos.models.PuntoEmision;
import com.facturactiva.facturactivafreepos.models.SerieCorrelativo;
import com.facturactiva.facturactivafreepos.models.User;
import com.facturactiva.facturactivafreepos.sqlite.ConexionSQLiteHelper;
import com.facturactiva.facturactivafreepos.sqlite.ConfiguracionesEntity;
import com.facturactiva.facturactivafreepos.sqlite.EmisoresEntity;
import com.facturactiva.facturactivafreepos.sqlite.GeneralCuentasEntity;
import com.facturactiva.facturactivafreepos.sqlite.PersonasEntity;
import com.facturactiva.facturactivafreepos.sqlite.PuntosEmisionEntity;
import com.facturactiva.facturactivafreepos.sqlite.SeriesCorrelativosEntity;
import com.facturactiva.facturactivafreepos.sqlite.UsuariosEntity;
import com.facturactiva.facturactivafreepos.utilities.Utilidades;
import com.facturactiva.facturactivafreepos.utilities.Utils;
import com.facturactiva.facturactivafreepos.validations.RegistroActivityValidations;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import static com.facturactiva.facturactivafreepos.activities.LoginActivity.MyPREFERENCES;

public class RegisterActivity extends AppCompatActivity {

    EditText editTextEmail, editTextPass, editTextRazonSoc, editTextRuc;
    Button btnAceptRegistro;
    ConexionSQLiteHelper conn;
    public static final String MyPREFERENCES = "MyPrefs";
    SharedPreferences sharedpreferences;
    Context context;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar_register_Activity);
        setSupportActionBar(toolbar);
        sharedpreferences = getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
        conn = new ConexionSQLiteHelper(RegisterActivity.this, "facturactiva-service", null, 1);
        context = RegisterActivity.this;

        inicializarComponentes();
        eventosClick();



    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        conn.close();
    }

    private void inicializarComponentes() {
        editTextEmail = (EditText) findViewById(R.id.editTextEmailUser);
        editTextPass = (EditText) findViewById(R.id.editTextPassUser);
        editTextRazonSoc = (EditText) findViewById(R.id.editTextRazonSocial);
        editTextRuc = (EditText) findViewById(R.id.editTextRuc);
        btnAceptRegistro = (Button) findViewById(R.id.btnAceptRegistro);

    }

    private void eventosClick() {

        btnAceptRegistro.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(RegistroActivityValidations.validateRegistroUser(editTextEmail,editTextPass,editTextRazonSoc,editTextRuc,context,conn)){

                    //Generamos idEmisor aleatorio que esté disponible
                    SQLiteDatabase db = conn.getReadableDatabase();
                    int idEmisorAleatorio = 0;
                    boolean disponible = false;
                    while(!disponible){
                        idEmisorAleatorio = (int) (Math.random()*100+1);  //Numero aleatorio entre 1 y 100
                        Cursor cursor = db.rawQuery("SELECT * FROM " + Utilidades.TABLA_EMISOR + " WHERE " + Utilidades.CAMPO_ID_EMISOR + "=" + String.valueOf(idEmisorAleatorio), null);
                        if(cursor.getCount() == 0){
                            disponible = true;
                        }
                        cursor.close();
                    }

                    int maxIdPersona = 0;
                    Cursor cursor10 = db.rawQuery("SELECT MAX(" + Utilidades.CAMPO_ID_PERS + ") FROM " + Utilidades.TABLA_PERSONA,null);
                    while(cursor10.moveToNext()){
                        maxIdPersona = cursor10.getInt(0);
                    }
                    cursor10.close();

                    db.close();

                    try {
                        //Guardamos en BD lo del Emisor y lo del usuario
                        Emisor emisor = new Emisor(idEmisorAleatorio, "6", editTextRuc.getText().toString(), editTextRazonSoc.getText().toString(), editTextEmail.getText().toString(), 1, editTextEmail.getText().toString(), Utils.getActualSystemDatetime(), "", "");
                        GeneralCuenta generalCuenta = new GeneralCuenta(idEmisorAleatorio, editTextRazonSoc.getText().toString(), editTextEmail.getText().toString(), "01", 1, editTextEmail.getText().toString(), Utils.getActualSystemDatetime(), "", "");
                        PuntoEmision puntoEmision = new PuntoEmision(idEmisorAleatorio, "6", editTextRuc.getText().toString(), 1, "PUNTO DE EMISION 1", 1, editTextEmail.getText().toString(), Utils.getActualSystemDatetime(), "", "");
                        Persona personaE = new Persona(maxIdPersona + 1, editTextRazonSoc.getText().toString(), "", "", "", "", "", "", "", "", "", "", "01", "06", editTextRuc.getText().toString(), "", "", 1, editTextEmail.getText().toString(), Utils.getActualSystemDatetime(), "", "");
                        Persona personaU = new Persona(maxIdPersona + 2, editTextEmail.getText().toString(), "", "", "", "", "", "", "", "", "", "", "02", "01", "42563178", "", "", 1, editTextEmail.getText().toString(), Utils.getActualSystemDatetime(), "", "");
                        User user = new User(maxIdPersona + 2, editTextEmail.getText().toString(), "", editTextEmail.getText().toString(), editTextPass.getText().toString(), 1, 1, maxIdPersona + 2, idEmisorAleatorio, idEmisorAleatorio, 1, 1, editTextEmail.getText().toString(), Utils.getActualSystemDatetime(), "", "");
                        Configuracion configuracion1 = new Configuracion(idEmisorAleatorio, "comprobantePredeterminado", "03", "EMISION", 1, "", "");
                        Configuracion configuracion2 = new Configuracion(idEmisorAleatorio, "igv", "0.18", "EMISION", 1, "", "");
                        Configuracion configuracion3 = new Configuracion(idEmisorAleatorio, "monedaLocal", "PEN", "EMISION", 1, "", "");
                        Configuracion configuracion4 = new Configuracion(idEmisorAleatorio, "tipoDocEmisor", "6", "EMISION", 1, "", "");
                        Configuracion configuracion5 = new Configuracion(idEmisorAleatorio, "numDocEmisor", editTextRuc.getText().toString(), "EMISION", 1, "", "");
                        Configuracion configuracion6 = new Configuracion(idEmisorAleatorio, "nombreEmisor", editTextRazonSoc.getText().toString(), "EMISION", 1, "", "");
                        Configuracion configuracion7 = new Configuracion(idEmisorAleatorio, "idPuntoEmision", "1", "EMISION", 1, "", "");
                        Configuracion configuracion8 = new Configuracion(idEmisorAleatorio, "nombrePuntoEmision", "PUNTO DE EMISIÓN 01", "EMISION", 1, "", "");
                        Configuracion configuracion9 = new Configuracion(idEmisorAleatorio, "serieFactura", "F001", "EMISION", 1, "", "");
                        Configuracion configuracion10 = new Configuracion(idEmisorAleatorio, "correlativoFactura", "1", "EMISION", 1, "", "");
                        Configuracion configuracion11 = new Configuracion(idEmisorAleatorio, "serieBoleta", "B001", "EMISION", 1, "", "");
                        Configuracion configuracion12 = new Configuracion(idEmisorAleatorio, "correlativoBoleta", "1", "EMISION", 1, "", "");
                        Configuracion configuracion13 = new Configuracion(idEmisorAleatorio, "serieNC", "B100", "EMISION", 1, "", "");
                        Configuracion configuracion14 = new Configuracion(idEmisorAleatorio, "correlativoNC", "1", "EMISION", 1, "", "");
                        Configuracion configuracion15 = new Configuracion(idEmisorAleatorio, "serieND", "B100", "EMISION", 1, "", "");
                        Configuracion configuracion16 = new Configuracion(idEmisorAleatorio, "correlativoND", "1", "EMISION", 1, "", "");
                        SerieCorrelativo serieCorrelativo1 = new SerieCorrelativo(idEmisorAleatorio, "6", editTextRuc.getText().toString(),1,"01","F001",0);
                        SerieCorrelativo serieCorrelativo2 = new SerieCorrelativo(idEmisorAleatorio, "6", editTextRuc.getText().toString(),1,"03","B001",0);
                        SerieCorrelativo serieCorrelativo3 = new SerieCorrelativo(idEmisorAleatorio, "6", editTextRuc.getText().toString(),1,"07","B100",0);
                        SerieCorrelativo serieCorrelativo4 = new SerieCorrelativo(idEmisorAleatorio, "6", editTextRuc.getText().toString(),1,"08","B100",0);

                        EmisoresEntity.insertEmisor(conn, emisor);
                        GeneralCuentasEntity.insertGeneralCuenta(conn, generalCuenta);
                        PuntosEmisionEntity.insertPuntoEmision(conn, puntoEmision);
                        PersonasEntity.insertPerson(conn, personaE);
                        PersonasEntity.insertPerson(conn, personaU);
                        UsuariosEntity.insertUser(conn, user);
                        ConfiguracionesEntity.insertConfiguracion(conn,configuracion1);
                        ConfiguracionesEntity.insertConfiguracion(conn,configuracion2);
                        ConfiguracionesEntity.insertConfiguracion(conn,configuracion3);
                        ConfiguracionesEntity.insertConfiguracion(conn,configuracion4);
                        ConfiguracionesEntity.insertConfiguracion(conn,configuracion5);
                        ConfiguracionesEntity.insertConfiguracion(conn,configuracion6);
                        ConfiguracionesEntity.insertConfiguracion(conn,configuracion7);
                        ConfiguracionesEntity.insertConfiguracion(conn,configuracion8);
                        ConfiguracionesEntity.insertConfiguracion(conn,configuracion9);
                        ConfiguracionesEntity.insertConfiguracion(conn,configuracion10);
                        ConfiguracionesEntity.insertConfiguracion(conn,configuracion11);
                        ConfiguracionesEntity.insertConfiguracion(conn,configuracion12);
                        ConfiguracionesEntity.insertConfiguracion(conn,configuracion13);
                        ConfiguracionesEntity.insertConfiguracion(conn,configuracion14);
                        ConfiguracionesEntity.insertConfiguracion(conn,configuracion15);
                        ConfiguracionesEntity.insertConfiguracion(conn,configuracion16);
                        SeriesCorrelativosEntity.insertSerieCorrelativo(conn,serieCorrelativo1);
                        SeriesCorrelativosEntity.insertSerieCorrelativo(conn,serieCorrelativo2);
                        SeriesCorrelativosEntity.insertSerieCorrelativo(conn,serieCorrelativo3);
                        SeriesCorrelativosEntity.insertSerieCorrelativo(conn,serieCorrelativo4);

                        //Guardar en sharedPreferences todoo lo del usuario y lo del emisor

                        SharedPreferences.Editor editor = sharedpreferences.edit();
                        editor.putString("comprobantePredeterminado", "03");
                        editor.putString("igv", "0.18");
                        editor.putString("monedaLocal", "PEN");
                        editor.putString("idEmisor", String.valueOf(idEmisorAleatorio));
                        editor.putString("tipoDocEmisor", "6");
                        editor.putString("numDocEmisor",editTextRuc.getText().toString());
                        editor.putString("nombreEmisor",editTextRazonSoc.getText().toString());
                        editor.putString("idPuntoEmision","1");
                        editor.putString("puntoEmision","PUNTO DE EMISION 01");
                        editor.putString("idUsuario",String.valueOf(maxIdPersona + 2));
                        editor.putString("username",editTextEmail.getText().toString());
                        editor.putString("password","123456");
                        editor.putString("serieFactura","F001");
                        editor.putString("correlativoFactura","1");
                        editor.putString("serieBoleta","B001");
                        editor.putString("correlativoBoleta","1");
                        editor.putString("serieNC","B100");
                        editor.putString("correlativoNC","1");
                        editor.putString("serieND","B100");
                        editor.putString("correlativoND","1");
                        editor.putString("enter_mode","login");
                        editor.commit();

                    } catch (Exception e){
                        e.printStackTrace();
                        Toast.makeText(RegisterActivity.this, "ERROR AL INTENTAR INSERTAR DATOS DE EMISOR-USUARIO", Toast.LENGTH_LONG).show();
                        Log.i("ERROR-REGISTRO","ERROR AL INTENTAR INSERTAR DATOS DE EMISOR-USUARIO");
                    }

                    Intent i = new Intent(RegisterActivity.this, NavDrawerActivity.class);
                    String enterMode = "login";
                    i.putExtra("enter_mode",enterMode);
                    startActivity(i);
                    finish();

                }
            }
        });

    }

}
