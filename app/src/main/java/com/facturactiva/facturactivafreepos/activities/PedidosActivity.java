package com.facturactiva.facturactivafreepos.activities;

import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;

import com.facturactiva.facturactivafreepos.R;
import com.facturactiva.facturactivafreepos.sqlite.ConexionSQLiteHelper;

public class PedidosActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    Button btnSiLogout, btnNoLogout, btnOkCerrarApp, btnCancelCerrarApp;
    public static String nombreEmpresa="", login_email_usuario="", login_pass_usuario="";
    public static int idEmisor = -1;
    public static String login_way="", rotado="No", itemSelectedLastTime="";
    public static String TAG = "FacturadorPOS";
    SharedPreferences sharedpreferences;
    public ConexionSQLiteHelper conn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pedidos);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar_pedidos);
        setSupportActionBar(toolbar);
        conn = new ConexionSQLiteHelper(PedidosActivity.this, "facturactiva-service", null, 1);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout_pedidos);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view_pedidos);
        navigationView.setNavigationItemSelectedListener(this);

        login_way = sharedpreferences.getString("enter_mode","EnterModeEnCasoDeNull");

        if(login_way.equals("login")) {
            login_email_usuario = sharedpreferences.getString("username","UsernameEnCasoDeNull");
            login_pass_usuario = sharedpreferences.getString("password","PasswordEnCasoDeNull");
            nombreEmpresa = sharedpreferences.getString("nombreEmisor","NombreEmpresaEnCasoDeNull");
            idEmisor = Integer.parseInt( sharedpreferences.getString("idEmisor","idEmisorEnCasoDeNull"));
        }

        if(login_way.equals("login")){
            Menu menu = navigationView.getMenu();
            MenuItem itemVentas = menu.findItem(R.id.nav_pedidos);
            itemVentas.setChecked(true);
            inicializarDatosUsuario(navigationView);
            inicializarControles();
            consultaCarritoYClienteYSeteaValores();

            eventosClick();
            consultarListasCategorias();
            ArrayAdapter<CharSequence> adapter = new ArrayAdapter(NavDrawerActivity.this, android.R.layout.simple_spinner_item, arrayListMostrarCategorias);
            spinnerCategInProdActiv.setAdapter(adapter);

            spinnerCategInProdActiv.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                    if(position != 0){
                        String codCateg = arrayListValoresCategorias.get(position-1).getCodCategoria();
                        consultarListasProductosByCategoria(codCateg);
                    } else {
                        consultarListasProductos();
                        coosultevagsssbvgdvsnbsdsddd1u7365235165362533
                                46554564-
                    }


                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {

                }
            });
            consultarListasProductos();


        }






    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout_pedidos);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.pedidos_options_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
/*
        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }
*/
        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();
        if (id == R.id.nav_ordenar) {
            Intent i = new Intent(PedidosActivity.this, NavDrawerActivity.class);
            startActivity(i);
            finish();
        } else if (id == R.id.nav_pedidos) {

        } else if (id == R.id.nav_cobranzas) {
            Intent i = new Intent(PedidosActivity.this, ComprobantesActivity.class);
            startActivity(i);
            finish();
        } else if (id == R.id.nav_productos) {
            Intent i = new Intent(PedidosActivity.this, ProductosActivity.class);
            startActivity(i);
            finish();
        } else if (id == R.id.nav_administrar) {
            Intent i = new Intent(PedidosActivity.this, AdministrarActivity.class);
            startActivity(i);
            finish();
        } else if (id == R.id.nav_configuracion) {
            Intent i = new Intent(PedidosActivity.this, ConfiguracionActivity.class);
            startActivity(i);
            finish();
        } else if (id == R.id.nav_logout){

            AlertDialog.Builder builder = new AlertDialog.Builder(PedidosActivity.this);
            LayoutInflater inflater = getLayoutInflater();
            View dialogView = inflater.inflate(R.layout.popup_cerrar_cesion,null);
            builder.setView(dialogView);

            //inicializarComponentesPopUp();
            btnSiLogout = (Button) dialogView.findViewById(R.id.btn_si_logout);
            btnNoLogout = (Button) dialogView.findViewById(R.id.btn_no_logout);

            final AlertDialog dialog = builder.create();

            //Eventos Click
            btnSiLogout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    cerrarSesion();
                    finish();
                    dialog.cancel();
                }
            });

            btnNoLogout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialog.cancel();
                    Menu menu = navigationView.getMenu();
                    MenuItem itemVentas = menu.findItem(R.id.nav_ordenar);
                    itemVentas.setChecked(true);
                }
            });

            dialog.show();

        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout_nav_drawer);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }


    public void cerrarSesion(){
        SharedPreferences sharedpreferences = getSharedPreferences(LoginActivity.MyPREFERENCES, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedpreferences.edit();
        editor.clear();
        editor.commit();
    }

}
