package com.facturactiva.facturactivafreepos.activities;

import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.view.LayoutInflater;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.facturactiva.facturactivafreepos.R;

public class ConfiguracionActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    NavigationView navigationView;
    Button btnSiLogout, btnNoLogout, btnOkCerrarApp, btnCancelCerrarApp;
    SharedPreferences sharedPreferences;
    String login_way;
    public static String nombreEmpresa="", login_email_usuario="", login_pass_usuario="";
    public static int idEmisor = -1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_configuracion);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar_configuracion);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab_configuracion);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout_configuracion);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        navigationView = (NavigationView) findViewById(R.id.nav_view_configuracion);
        navigationView.setNavigationItemSelectedListener(this);

        sharedPreferences = getSharedPreferences(LoginActivity.MyPREFERENCES, Context.MODE_PRIVATE);

        login_way = sharedPreferences.getString("enter_mode","EnterModeEnCasoDeNull");

        if(login_way.equals("login")) {
            login_email_usuario = sharedPreferences.getString("username","UsernameEnCasoDeNull");
            login_pass_usuario = sharedPreferences.getString("password","PasswordEnCasoDeNull");
            nombreEmpresa = sharedPreferences.getString("nombreEmisor","NombreEmpresaEnCasoDeNull");
            idEmisor = Integer.parseInt( sharedPreferences.getString("idEmisor","idEmisorEnCasoDeNull"));
        }

        if(login_way.equals("login")){
            Menu menu = navigationView.getMenu();
            MenuItem itemConfiguracion = menu.findItem(R.id.nav_configuracion);
            itemConfiguracion.setChecked(true);
            inicializarDatosUsuario(navigationView);
        }

    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout_configuracion);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {

            AlertDialog.Builder builder = new AlertDialog.Builder(ConfiguracionActivity.this);
            LayoutInflater inflater = getLayoutInflater();
            View dialogView = inflater.inflate(R.layout.popup_salir_aplicacion,null);
            builder.setView(dialogView);

            //inicializarComponentesPopUp();
            btnOkCerrarApp = (Button) dialogView.findViewById(R.id.btn_ok_cerrar_app);
            btnCancelCerrarApp = (Button) dialogView.findViewById(R.id.btn_cancel_cerrar_app);

            final AlertDialog dialog = builder.create();

            //Eventos Clic
            btnOkCerrarApp.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    //moveTaskToBack(true);
                    //dialog.cancel();
                    //Process.killProcess(Process.myPid());
                    //System.exit(0);
                    finishAffinity();

                }
            });

            btnCancelCerrarApp.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialog.cancel();
                }
            });

            dialog.show();

            //---------------------------------------------------------------------------
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.configuracion_options_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        /*if (id == R.id.action_settings) {
            return true;
        }
*/
        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();
        if (id == R.id.nav_ordenar) {
            Intent i = new Intent(ConfiguracionActivity.this, NavDrawerActivity.class);
            startActivity(i);
            finish();
        } else if (id == R.id.nav_comprobantes) {
            Intent i = new Intent(ConfiguracionActivity.this, ComprobantesActivity.class);
            startActivity(i);
            finish();
        } else if (id == R.id.nav_productos) {
            Intent i = new Intent(ConfiguracionActivity.this, ProductosActivity.class);
            startActivity(i);
            finish();
        } else if (id == R.id.nav_administrar) {
            Intent i = new Intent(ConfiguracionActivity.this, AdministrarActivity.class);
            startActivity(i);
            finish();
        } else if (id == R.id.nav_configuracion) {

        } else if (id == R.id.nav_logout){

            AlertDialog.Builder builder = new AlertDialog.Builder(ConfiguracionActivity.this);
            LayoutInflater inflater = getLayoutInflater();
            View dialogView = inflater.inflate(R.layout.popup_cerrar_cesion,null);
            builder.setView(dialogView);

            //inicializarComponentesPopUp();
            btnSiLogout = (Button) dialogView.findViewById(R.id.btn_si_logout);
            btnNoLogout = (Button) dialogView.findViewById(R.id.btn_no_logout);

            final AlertDialog dialog = builder.create();

            //Eventos Click
            btnSiLogout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    cerrarSesion();
                    finish();
                    dialog.cancel();
                }
            });

            btnNoLogout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialog.cancel();
                    Menu menu = navigationView.getMenu();
                    MenuItem itemConfiguracion = menu.findItem(R.id.nav_configuracion);
                    itemConfiguracion.setChecked(true);
                }
            });

            dialog.show();

        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout_configuracion);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        /*Toast.makeText(this, "onConfigurationChanged()", Toast.LENGTH_SHORT).show();
        sharedpreferences = getSharedPreferences(LoginActivity.MyPREFERENCES, Context.MODE_PRIVATE);

        login_way = sharedpreferences.getString("enter_mode","EnterModeEnCasoDeNull");

        if (login_way.equals("login")) {
            Menu menu = navigationView.getMenu();
            MenuItem itemVentas = menu.findItem(R.id.nav_ordenar);
            MenuItem itemComprobantes = menu.findItem(R.id.nav_comprobantes);
            MenuItem itemProductos = menu.findItem(R.id.nav_productos);
            MenuItem itemComentarios = menu.findItem(R.id.nav_comentarios);
            MenuItem itemAdministrar = menu.findItem(R.id.nav_administrar);
            MenuItem itemConfiguracion = menu.findItem(R.id.nav_configuracion);
            MenuItem itemSoporte = menu.findItem(R.id.nav_soporte);

            if (itemEmisiones.isChecked()) {
                FragmentManager fragmentManager = getSupportFragmentManager();
                fragmentManager.beginTransaction().replace(R.id.contenedor_nav_drawer, new EmisionesFragment()).commit();
            } else if (itemConsultas.isChecked()) {
                FragmentManager fragmentManager = getSupportFragmentManager();
                fragmentManager.beginTransaction().replace(R.id.contenedor_nav_drawer, new ConsultasFragment()).commit();
            } else if(itemAdministrar.isChecked()) {
                FragmentManager fragmentManager = getSupportFragmentManager();
                fragmentManager.beginTransaction().replace(R.id.contenedor_nav_drawer, new AdministrarFragment()).commit();
            }
        }

*/
        alert("onConfigurationChanged");
    }

    private void inicializarDatosUsuario(NavigationView navigationView) {
        View hview = navigationView.getHeaderView(0);
        ImageView imageViewLogoEmpresa = (ImageView) hview.findViewById(R.id.imageViewLogoEmpresa);   imageViewLogoEmpresa.setImageResource(R.drawable.imagen_empresa);
        TextView nombEmpresaMenuLat = (TextView) hview.findViewById(R.id.textViewNomEmpLatMenu);
        String parteNombreEmpresa = "";
        if(nombreEmpresa.length() > 25){
            parteNombreEmpresa = nombreEmpresa.substring(0,24);
            nombEmpresaMenuLat.setText(parteNombreEmpresa);
        } else {
            nombEmpresaMenuLat.setText(nombreEmpresa);
        }
        TextView nombUsuarioMenuLat = (TextView) hview.findViewById(R.id.textViewUserLatMenu);    nombUsuarioMenuLat.setText(login_email_usuario);
    }

    private void alert(String s) {
        Toast.makeText(ConfiguracionActivity.this,s,Toast.LENGTH_SHORT).show();
    }

    public void cerrarSesion(){
        SharedPreferences sharedpreferences = getSharedPreferences(LoginActivity.MyPREFERENCES, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedpreferences.edit();
        editor.clear();
        editor.commit();
    }



}
