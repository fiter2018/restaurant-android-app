package com.facturactiva.facturactivafreepos.activities;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.facturactiva.facturactivafreepos.R;
import com.facturactiva.facturactivafreepos.network.HostingFacturactivaServices;
import com.facturactiva.facturactivafreepos.sqlite.ConexionSQLiteHelper;
import com.facturactiva.facturactivafreepos.utilities.Utilidades;
import com.facturactiva.facturactivafreepos.validations.LoginActivityValidations;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;


public class LoginActivity extends AppCompatActivity {

    private Button buttonSignIn, buttonConocenos;
    private EditText editTextEmail;
    private EditText editTextPass;
    private TextView textViewAquiRegister;
    public static String TAG = "FacturadorPOS";

    public ConexionSQLiteHelper conn;
    public Context context;

    //Shared Preferences--------------------------------------
    public static final String MyPREFERENCES = "MyPrefs";
    public static final String username = "username";
    public static final String password = "password";
    SharedPreferences sharedpreferences;
    //--------------------------------------------------

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        conn = new ConexionSQLiteHelper(LoginActivity.this, "facturactiva-service", null, 1);
        context = LoginActivity.this;

/*        ActionBar actionBar = getSupportActionBar();
        actionBar.hide();*/
        inicializarComponentes();
        sharedpreferences = getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
        String correo = sharedpreferences.getString("username","usernameEnCasoDeNull");
        if(!correo.equals("usernameEnCasoDeNull")){
            Intent i = new Intent(LoginActivity.this,NavDrawerActivity.class);
            startActivity(i);
        }
        eventoClicks();
    }

    @Override
    protected void onStart() {
        super.onStart();

    }

    @Override
    protected void onResume() {
        super.onResume();
        editTextPass = (EditText) findViewById(R.id.editTextPass);   editTextPass.setText("");
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        conn.close();
    }

    private void alert(String s) {
        Toast.makeText(LoginActivity.this,s,Toast.LENGTH_SHORT).show();
    }

    //Método de encriptación SHA1---------------------------------------------------------------
    private static String convertToHex(byte[] data) {
        StringBuilder buf = new StringBuilder();
        for (byte b : data) {
            int halfbyte = (b >>> 4) & 0x0F;
            int two_halfs = 0;
            do {
                buf.append((0 <= halfbyte) && (halfbyte <= 9) ? (char) ('0' + halfbyte) : (char) ('a' + (halfbyte - 10)));
                halfbyte = b & 0x0F;
            } while (two_halfs++ < 1);
        }
        return buf.toString();
    }

    public static String SHA1(String textToConvert) throws NoSuchAlgorithmException, UnsupportedEncodingException {
        MessageDigest md = MessageDigest.getInstance("SHA-1");
        byte[] textBytes = textToConvert.getBytes("UTF-8");
        md.update(textBytes, 0, textBytes.length);
        byte[] sha1hash = md.digest();
        return convertToHex(sha1hash);
    }
    //------------------------------------------------------------------------------------------

    private void eventoClicks() {
        buttonSignIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(LoginActivityValidations.validateLogin(editTextEmail,editTextPass,conn,context)) {
                    /*String tag = "login";
                    final String email = editTextEmail.getText().toString().trim();   //CorreoSinEncriptar
                    final String pass = editTextPass.getText().toString().trim();     //PasswordSinEncriptar
                    String varStatic = "f3bd7fd075e3d803d9c4b4db36816dec518fbc70";   //Variable estática utilizada en el encriptado
                    String userSHA1 = "", claveSHA1 = "", userBase64 = "", claveBase64 = "";

                    try {
                        //Encriptando SHA1
                        userSHA1 = SHA1(varStatic + email);
                        claveSHA1 = SHA1(varStatic + pass);

                        //Encriptando Base64
                        byte[] data = email.getBytes("UTF-8");
                        userBase64 = Base64.encodeToString(data, Base64.DEFAULT);
                        byte[] data2 = pass.getBytes("UTF-8");
                        claveBase64 = Base64.encodeToString(data2, Base64.DEFAULT);

                    } catch (NoSuchAlgorithmException e) {
                        e.printStackTrace();
                    } catch (UnsupportedEncodingException e) {
                        e.printStackTrace();
                    }
                    String varApp = "O";
                    String varNavName = "";
                    String varNavVer = "";
                    String varNavVendor = "";
                    String varNacPais = "";
                    String varNacRegion = "";
                    String varNacCiudad = "";
                    String decGpsLat = "";
                    String decGpsLng = "";
                    String varOSLang = "";
                    String varOSName = "";
                    String varOSProcess = "";
                    String varOSPlatform = "";
                    String varIPPublica = "";
                    String varIPServidor = "";
                    String varMacAddress = "";
                    String intSwCookie = "";
                    String intSwMovil = "";

                    //Envío el request GET
                    AndroidNetworking.get(HostingFacturactivaServices.UNIVERSAL_URL)
                            .setTag(TAG)
                            .setPriority(Priority.LOW)
                            .addQueryParameter("tag", "login")
                            .addQueryParameter("varUsuarioEnc", userSHA1)
                            .addQueryParameter("varContrasenia", claveSHA1)
                            .addQueryParameter("varAPP", varApp)
                            .addQueryParameter("varUsuario", userBase64)
                            .addQueryParameter("varContraseniaes", claveBase64)
                            .addQueryParameter("varNavname", varNavName)
                            .addQueryParameter("varNavver", varNavVer)
                            .addQueryParameter("varNavvendor", varNavVendor)
                            .addQueryParameter("varNacpais", varNacPais)
                            .addQueryParameter("varNacregion", varNacRegion)
                            .addQueryParameter("varNacciudad", varNacCiudad)
                            .addQueryParameter("decGpslat", decGpsLat)
                            .addQueryParameter("decGpslng", decGpsLng)
                            .addQueryParameter("varOslang", varOSLang)
                            .addQueryParameter("varOsname",varOSName)
                            .addQueryParameter("varOsproccess", varOSProcess)
                            .addQueryParameter("varOsplatform", varOSPlatform)
                            .addQueryParameter("varIppublica", varIPPublica)
                            .addQueryParameter("varIpservidor", varIPServidor)
                            .addQueryParameter("varMacaddress", varMacAddress)
                            .addQueryParameter("intSwcookie", intSwCookie)
                            .addQueryParameter("intSwmovil", intSwMovil)
                            .build()
                            .getAsJSONObject(new JSONObjectRequestListener() {
                                @Override
                                public void onResponse(JSONObject response) {
                                    try {

                                        if((response.getString("message")).equals("NO OK")) {
                                            Log.d(TAG, response.getString("message"));
                                            alert("Correo y/o password incorrectos");
                                            return;
                                        }

                                        SharedPreferences.Editor editor = sharedpreferences.edit();

                                        editor.putString(username, email);
                                        editor.putString(password, pass);
                                        editor.putString("enter_mode","login");
                                        editor.putString("user_id",response.getJSONObject("data").getString("id"));
                                        editor.putString("user_codigo",response.getJSONObject("data").getString("codigo"));
                                        editor.putString("user_correo",response.getJSONObject("data").getString("correo"));
                                        editor.putString("user_nombre",response.getJSONObject("data").getString("nombre"));
                                        editor.putString("user_apellido",response.getJSONObject("data").getString("apellido"));
                                        editor.putString("user_dni",response.getJSONObject("data").getString("dni"));
                                        editor.putString("user_salon",response.getJSONObject("data").getString("salon"));
                                        editor.putString("user_colegio",response.getJSONObject("data").getString("colegio"));
                                        editor.putString("user_grado",response.getJSONObject("data").getString("grado"));
                                        editor.putString("user_codGrado",response.getJSONObject("data").getString("codgrado"));
                                        editor.putString("user_idSalon",String.valueOf(response.getJSONObject("data").getInt("idsalon")));
                                        editor.putString("user_idPerfil",response.getJSONObject("data").getString("idPerfil"));
                                        editor.putString("user_idPerfiles",response.getJSONObject("data").getString("idperfiles"));
                                        editor.putString("user_tutor",response.getJSONObject("data").getString("tutor"));
                                        editor.putString("user_imgCache",response.getJSONObject("data").getString("imgcache"));

                                        editor.commit();

                                        Intent i = new Intent(LoginActivity.this, NavDrawerActivity.class);
                                        String enterMode = "login";
                                        i.putExtra("enter_mode",enterMode);
                                        startActivity(i);

                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }


                                }

                                @Override
                                public void onError(ANError anError) {
                                    Log.d(TAG, anError.getLocalizedMessage());
                                    alert("No hay respuesta del hosting");
                                }
                            });
*/
                    //login(email, pass);
                    //editTextPass.setText("");

                    String email = editTextEmail.getText().toString();
                    String pass = editTextPass.getText().toString();
                    int idUsuario = 0;
                    int idEmisor = 0;

                    //Trae idEmisor del usuario logueado
                    SQLiteDatabase db = conn.getReadableDatabase();
                    Cursor cursor = db.rawQuery("SELECT " + Utilidades.CAMPO_ID_USER + ", " + Utilidades.CAMPO_ID_EMISOR_USER + " FROM " + Utilidades.TABLA_USUARIOS + " WHERE " + Utilidades.CAMPO_EMAIL_USER + "='" + email + "' AND " + Utilidades.CAMPO_PASS_USER + "='" + pass + "'", null);
                    while(cursor.moveToNext()) {
                        idUsuario = cursor.getInt(0);
                        idEmisor = cursor.getInt(1);
                    }
                    cursor.close();

                    //Trae configuracion del Emisor y lo pone en SharedPreferences
                    String codConfig = "";
                    String valorConfig = "";

                    String comprobantePredeterminado = "";
                    String igv = "";
                    String monedaLocal = "";
                    String tipoDocEmisor = "";
                    String numDocEmisor = "";
                    String nombreEmisor = "";
                    String idPuntoEmision = "";
                    String nombrePuntoEmision = "";

                    Cursor cursor1 = db.rawQuery("SELECT " + Utilidades.CAMPO_COD_CONFIG + ", " + Utilidades.CAMPO_VALOR_CONFIG + " FROM " + Utilidades.TABLA_CONFIGURACION + " WHERE " + Utilidades.CAMPO_ID_EMISOR_CONFIG + "=" + String.valueOf(idEmisor),null);
                    while(cursor1.moveToNext()){
                        codConfig = cursor1.getString(0);
                        valorConfig = cursor1.getString(1);
                        switch (codConfig) {
                            case "comprobantePredeterminado":
                                comprobantePredeterminado = valorConfig;
                                break;
                            case "igv":
                                igv = valorConfig;
                                break;
                            case "monedaLocal":
                                monedaLocal = valorConfig;
                                break;
                            case "tipoDocEmisor":
                                tipoDocEmisor = valorConfig;
                                break;
                            case "numDocEmisor":
                                numDocEmisor = valorConfig;
                                break;
                            case "nombreEmisor":
                                nombreEmisor = valorConfig;
                                break;
                            case "idPuntoEmision":
                                idPuntoEmision = valorConfig;
                                break;
                            case "nombrePuntoEmision":
                                nombrePuntoEmision = valorConfig;
                                break;
                        }
                    }
                    cursor1.close();

                    String tipoDocum = "";
                    String serieTemp = "";
                    int correlativoTemp = 0;

                    String serieFactura = "";
                    int correlativoFactura = 0;
                    String serieBoleta = "";
                    int correlativoBoleta = 0;
                    String serieNC = "";
                    int correlativoNC = 0;
                    String serieND = "";
                    int correlativoND = 0;

                    Cursor cursor10 = db.rawQuery("SELECT " + Utilidades.CAMPO_TIPO_DOC_SERCORR + ", " + Utilidades.CAMPO_SERIE_SERCORR + ", " + Utilidades.CAMPO_CORRELATIVO_SERCORR + " FROM " + Utilidades.TABLA_SERIE_CORRELATIVO + " WHERE " + Utilidades.CAMPO_ID_EMISOR_SERCORR + "=" + String.valueOf(idEmisor),null);
                    while(cursor10.moveToNext()){
                        tipoDocum = cursor10.getString(0);
                        serieTemp = cursor10.getString(1);
                        correlativoTemp = cursor10.getInt(2);
                        switch (tipoDocum) {
                            case "01":
                                serieFactura = serieTemp;
                                correlativoFactura = correlativoTemp;
                                break;
                            case "03":
                                serieBoleta = serieTemp;
                                correlativoBoleta = correlativoTemp;
                                break;
                            case "07":
                                serieNC = serieTemp;
                                correlativoNC = correlativoTemp;
                                break;
                            case "08":
                                serieND = serieTemp;
                                correlativoND = correlativoTemp;
                                break;
                        }
                    }
                    cursor10.close();
                    db.close();

                    SharedPreferences.Editor editor = sharedpreferences.edit();
                    editor.putString("comprobantePredeterminado", comprobantePredeterminado);
                    editor.putString("igv", igv);
                    editor.putString("monedaLocal", monedaLocal);
                    editor.putString("idEmisor", String.valueOf(idEmisor));
                    editor.putString("tipoDocEmisor", tipoDocEmisor);
                    editor.putString("numDocEmisor",numDocEmisor);
                    editor.putString("nombreEmisor",nombreEmisor);
                    editor.putString("idPuntoEmision",idPuntoEmision);
                    editor.putString("puntoEmision",nombrePuntoEmision);
                    editor.putString("idUsuario",String.valueOf(idUsuario));
                    editor.putString("username",email);
                    editor.putString("password",pass);
                    editor.putString("serieFactura",serieFactura);
                    editor.putString("correlativoFactura",String.valueOf(correlativoFactura));
                    editor.putString("serieBoleta",serieBoleta);
                    editor.putString("correlativoBoleta",String.valueOf(correlativoBoleta));
                    editor.putString("serieNC",serieNC);
                    editor.putString("correlativoNC",String.valueOf(correlativoNC));
                    editor.putString("serieND",serieND);
                    editor.putString("correlativoND",String.valueOf(correlativoND));
                    editor.putString("enter_mode","login");
                    editor.commit();

                    Intent i = new Intent(LoginActivity.this, NavDrawerActivity.class);
                    String enterMode = "login";
                    i.putExtra("enter_mode",enterMode);
                    startActivity(i);

                }
            }
        });

        textViewAquiRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(LoginActivity.this,RegisterActivity.class);
                startActivity(i);
            }
        });


    }


    private void inicializarComponentes() {
        editTextEmail = (EditText) findViewById(R.id.editTextEmail); editTextEmail.setText("");
        editTextPass = (EditText) findViewById(R.id.editTextPass);   editTextPass.setText("");
        buttonSignIn = (Button) findViewById(R.id.buttonSignin);
        textViewAquiRegister = (TextView) findViewById(R.id.textViewAquiLogin);
    }









}
