package com.facturactiva.facturactivafreepos.activities;

import android.app.AlertDialog;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.facturactiva.facturactivafreepos.FacturactivaFreePOS;
import com.facturactiva.facturactivafreepos.R;
import com.facturactiva.facturactivafreepos.adapters.ListaProductosVentaRealAdapter;
import com.facturactiva.facturactivafreepos.models.DocElectronicoDetalle;
import com.facturactiva.facturactivafreepos.validations.VentaRealizadaActivityValidations;

import java.util.ArrayList;

public class VentaRealizadaActivity extends AppCompatActivity {


    RecyclerView recyclerViewProductosVenta;
    ArrayList<DocElectronicoDetalle> arrayListDocElectDetalle;
    ListaProductosVentaRealAdapter listaProductosVentaRealAdapter;
    RecyclerView.LayoutManager layoutManagerProductosVentaReal;


    Button btnbtnNuevaVenta, btnImprimirComprob, btnEnviarComprobCorreo;
    TextView textViewNombEmpresaVentaReal,textViewRucEmisorVentaReal,textViewTipoDocVentaReal,textViewSerieCorrelVentaReal,textViewNombReceptVentaReal,textViewTipoDocReceptorVentaReal,textViewNumDocReceptVentaReal,textViewEmailReceptVentaReal,textViewMntNeto,textViewTotalIGV,textViewMntTotal;
    Button btnAceptPopup, btnCancelPopup;
    EditText editTextCorreoPopup;






    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_venta_realizada);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar_venta_realizada);
        setSupportActionBar(toolbar);


        inicializarComponentes();
        listarYllenarDatos();
        eventosClick();


    }

    @Override
    public void onBackPressed() {
        finish();
    }

    public void inicializarComponentes(){

         recyclerViewProductosVenta = (RecyclerView) findViewById(R.id.recyclerViewProductosVenta);
         btnbtnNuevaVenta = (Button) findViewById(R.id.btnNuevaVenta);
         btnImprimirComprob = (Button) findViewById(R.id.btnImprimirComprob);
         btnEnviarComprobCorreo = (Button) findViewById(R.id.btnEnviarComprobCorreo);

         textViewNombEmpresaVentaReal = (TextView) findViewById(R.id.textViewNombEmpresaVentaReal);
        textViewRucEmisorVentaReal = (TextView) findViewById(R.id.textViewRucEmisorVentaReal);
        textViewTipoDocVentaReal = (TextView) findViewById(R.id.textViewTipoDocVentaReal);
        textViewSerieCorrelVentaReal = (TextView) findViewById(R.id.textViewSerieCorrelVentaReal);
        textViewNombReceptVentaReal = (TextView) findViewById(R.id.textViewNombReceptVentaReal);
        textViewTipoDocReceptorVentaReal = (TextView) findViewById(R.id.textViewTipoDocReceptorVentaReal);
        textViewNumDocReceptVentaReal = (TextView) findViewById(R.id.textViewNumDocReceptVentaReal);
        textViewEmailReceptVentaReal = (TextView) findViewById(R.id.textViewEmailReceptVentaReal);
        textViewMntNeto = (TextView) findViewById(R.id.textViewMntNeto);
        textViewTotalIGV = (TextView) findViewById(R.id.textViewTotalIGV);
        textViewMntTotal = (TextView) findViewById(R.id.textViewMntTotal);



    }

    public void listarYllenarDatos(){

        textViewNombEmpresaVentaReal.setText(FacturactivaFreePOS.getDocElectronico().getNombreEmisor());
        textViewRucEmisorVentaReal.setText(FacturactivaFreePOS.getDocElectronico().getNumDocEmisor());
        String tipoDocumento = FacturactivaFreePOS.getDocElectronico().getTipoDocumento();
        if(tipoDocumento.equals("01")){
            textViewTipoDocVentaReal.setText(R.string.cadena_factura);
        } else if(tipoDocumento.equals("03")) {
            textViewTipoDocVentaReal.setText(R.string.cadena_boleta);
        }
        textViewSerieCorrelVentaReal.setText(FacturactivaFreePOS.getDocElectronico().getSerie()+"-"+String.valueOf(FacturactivaFreePOS.getDocElectronico().getCorrelativo()));

        if(FacturactivaFreePOS.getDocElectronico().getNombreReceptor().isEmpty()){
            textViewNombReceptVentaReal.setText("--");
        } else {
            textViewNombReceptVentaReal.setText(FacturactivaFreePOS.getDocElectronico().getNombreReceptor());
        }

        String tipoDocReceptor = FacturactivaFreePOS.getDocElectronico().getTipoDocReceptor();
        if(tipoDocReceptor.equals("01")){
            textViewTipoDocReceptorVentaReal.setText(R.string.cadena_dni);
        } else if(tipoDocReceptor.equals("06")) {
            textViewTipoDocReceptorVentaReal.setText(R.string.cadena_ruc);
        }

        if(FacturactivaFreePOS.getDocElectronico().getNumDocReceptor().isEmpty()){
            textViewNumDocReceptVentaReal.setText("--");
        } else {
            textViewNumDocReceptVentaReal.setText(FacturactivaFreePOS.getDocElectronico().getNumDocReceptor());
        }

        if(FacturactivaFreePOS.getCliente().getEmailContacto().isEmpty()){
            textViewEmailReceptVentaReal.setText("--");
        } else {
            textViewEmailReceptVentaReal.setText(FacturactivaFreePOS.getCliente().getEmailContacto());
        }

        //LayoutManager
        layoutManagerProductosVentaReal = new LinearLayoutManager(this);
        recyclerViewProductosVenta.setLayoutManager(layoutManagerProductosVentaReal);

        //Adapter
        listaProductosVentaRealAdapter = new ListaProductosVentaRealAdapter(FacturactivaFreePOS.getArrayListDocElectDetalle(), VentaRealizadaActivity.this);
        recyclerViewProductosVenta.setAdapter(listaProductosVentaRealAdapter);

        textViewMntNeto.setText("S/. "+String.format(java.util.Locale.US,"%.2f", FacturactivaFreePOS.getDocElectronico().getMntNeto()));
        textViewTotalIGV.setText("S/. "+String.format(java.util.Locale.US,"%.2f", FacturactivaFreePOS.getDocElectronico().getMntTotalIgv()));
        textViewMntTotal.setText("S/. "+String.format(java.util.Locale.US,"%.2f", FacturactivaFreePOS.getDocElectronico().getMntTotal()));

    }

    protected void sendEmail(String emailTO, String emailCC, String subject, String message) {
        Log.i("TAG----EMAIL", "Send email");

        String[] TO = {emailTO};
        String[] CC = {emailCC};
        Intent emailIntent = new Intent(Intent.ACTION_SEND);
        emailIntent.setData(Uri.parse("mailto:"));
        emailIntent.setType("text/plain");

        emailIntent.putExtra(Intent.EXTRA_EMAIL, TO);
        //emailIntent.putExtra(Intent.EXTRA_CC, CC);
        emailIntent.putExtra(Intent.EXTRA_SUBJECT, subject);
        emailIntent.putExtra(Intent.EXTRA_TEXT, message);

        try {
            startActivity(Intent.createChooser(emailIntent, "Send mail..."));
            finish();
            Log.i("TAG-----EMAIL", "Finished sending email.");
        } catch (android.content.ActivityNotFoundException ex) {
            Toast.makeText(VentaRealizadaActivity.this,"There is no email client installed.", Toast.LENGTH_SHORT).show();
        }
    }

    public void eventosClick(){

        btnbtnNuevaVenta.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });


        btnImprimirComprob.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {








            }
        });


        btnEnviarComprobCorreo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                AlertDialog.Builder builder = new AlertDialog.Builder(VentaRealizadaActivity.this);
                LayoutInflater inflater = getLayoutInflater();
                View dialogView = inflater.inflate(R.layout.popup_envio_comp_email_venta_real,null);
                builder.setView(dialogView);

                //inicializarComponentesPopUp();
                editTextCorreoPopup = (EditText) dialogView.findViewById(R.id.editTextPopupCorreoVentaReal);
                btnAceptPopup = (Button) dialogView.findViewById(R.id.btnAceptarCorreoPopupVentaReal);
                btnCancelPopup = (Button) dialogView.findViewById(R.id.btnCancelarCorreoPopupVentaReal);

                final AlertDialog dialog = builder.create();

                //Eventos Clic
                btnAceptPopup.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if(VentaRealizadaActivityValidations.validatePopupCorreoVentaReal(editTextCorreoPopup)){

                            String correoTo = editTextCorreoPopup.getText().toString();
                            String subject = "Compra realizada en "+FacturactivaFreePOS.getDocElectronico().getNombreEmisor();

                            String tipoComprobante = "";
                            if(FacturactivaFreePOS.getDocElectronico().getTipoDocumento().equals("01")){
                                tipoComprobante = "Factura";
                            } else if(FacturactivaFreePOS.getDocElectronico().getTipoDocumento().equals("03")) {
                                tipoComprobante = "Boleta";
                            }
                            String message = "Estimado cliente,\n"+
                                                              "\n"+
                                             "Te saluda el equipo de "+FacturactivaFreePOS.getDocElectronico().getNombreEmisor()+", queremos agradecerte la confianza depositada en nosotros. Asímismo enviarte el comprobante de la compra realizada en nuestras instalaciones.\n"+
                                                              "\n"+

                                             tipoComprobante+"    "+FacturactivaFreePOS.getDocElectronico().getSerie()+"-"+String.valueOf(FacturactivaFreePOS.getDocElectronico().getCorrelativo())+"\n"+
                                                                "\n"+
                                            "Monto Neto:  "+String.valueOf(FacturactivaFreePOS.getDocElectronico().getMntNeto())+"\n"+
                                            "Monto Total Igv:  "+String.valueOf(FacturactivaFreePOS.getDocElectronico().getMntTotalIgv())+"\n"+
                                            "Monto Total:  "+String.valueOf(FacturactivaFreePOS.getDocElectronico().getMntTotal());

                            sendEmail(correoTo,"kevin21051993@hotmail.com",subject,message);
                            dialog.cancel();
                        }
                    }
                });

                btnCancelPopup.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.cancel();
                    }
                });

                dialog.show();



            }
        });


    }









}
