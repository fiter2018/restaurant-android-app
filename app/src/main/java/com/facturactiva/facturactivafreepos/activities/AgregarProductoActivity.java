package com.facturactiva.facturactivafreepos.activities;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.database.sqlite.SQLiteStatement;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.media.MediaScannerConnection;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.facturactiva.facturactivafreepos.R;
import com.facturactiva.facturactivafreepos.constants.Constants;
import com.facturactiva.facturactivafreepos.models.Categoria;
import com.facturactiva.facturactivafreepos.sqlite.ConexionSQLiteHelper;
import com.facturactiva.facturactivafreepos.utilities.Utilidades;
import com.frosquivel.magicalcamera.MagicalCamera;
import com.frosquivel.magicalcamera.MagicalPermissions;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Map;

import id.zelory.compressor.Compressor;

import static android.Manifest.permission.CAMERA;
import static android.Manifest.permission.WRITE_EXTERNAL_STORAGE;

public class AgregarProductoActivity extends AppCompatActivity {


    EditText editTextNroPlatoCarta, editTextDescProd, editTextValorUnitProd;
    String codProd, descProd, codCateg, tipoMoneda;
    int nroPlatoCarta;
    int cantProdConEseNroPlato;
    float valorUnitProd;
    Spinner spinnerCateg, spinnerMoneda;
    ImageView imageViewProduct;
    Button btnGuardarProd, btnCancelProd, btnSelectImagen, btnTomarFoto;
    ArrayList<Categoria> arrayListValoresCateg;
    ArrayList<String> arrayListMostrarCateg;
    ArrayList<String> arrayListMostrarMonedas;
    SQLiteOpenHelper conn;
    Bitmap thumbnail;

    public int numProductosDeCategoria = 0;

    //Botones de Imagenes ProductosActivity
    private final String MY_IMAGES_PATH = Environment.getExternalStorageDirectory() + "/" + Environment.DIRECTORY_PICTURES + "/"+ProductosActivity.nombreEmpresa+"/";
    private MagicalPermissions magicalPermissions;
    private MagicalCamera magicalCamera;
    String imagePath="";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_agregar_producto);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar_agregar_producto);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);


        conn = new ConexionSQLiteHelper(getApplicationContext(), "facturactiva-service", null, 1);
        inicializarControles();
        llenarCombos();
        eventosClick();

        check_directory();
        check_permissions();

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        Map<String, Boolean> map = magicalPermissions.permissionResult(requestCode, permissions, grantResults);
        for (String permission : map.keySet()) {
            Log.d("PERMISSIONS", permission + " was: " + map.get(permission));
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        try {
            magicalCamera.resultPhoto(requestCode, resultCode, data);
        } catch (Exception e){
            e.printStackTrace();
            alert("Error en magicalCamera.resultPhoto(requestCode, resultCode, data)");
        }

        //CORRIGIENDO LA ORIENTACION DE LA FOTO
        if (magicalCamera.getPhoto() != null) {
            if (magicalCamera.initImageInformation()) {
                Log.e("MagicalCamera", "La foto contiene informacion privada");

                String orientation = magicalCamera.getPrivateInformation().getOrientation();
                Log.e("MagicalCamera", "Orientacion: " + orientation);

                int orientation_int = -1;

                try {
                    orientation_int = Integer.parseInt(orientation);
                } catch (Exception e) {
                    e.printStackTrace();
                    orientation_int = -1;
                }

                if (orientation_int != -1) {
                    switch (orientation_int) {
                        case 1:
                            //TOP
                            //magicalCamera.resultPhoto(requestCode, resultCode, data, MagicalCamera.ORIENTATION_ROTATE_180);
                            break;
                        case 3:
                            //BOTTOM
                            magicalCamera.resultPhoto(requestCode, resultCode, data, 180);
                            break;
                        case 6:
                            //RIGHT
                            magicalCamera.resultPhoto(requestCode, resultCode, data, 90);
                            break;
                        case 8:
                            //LEFT
                            magicalCamera.resultPhoto(requestCode, resultCode, data, 270);
                            break;
                    }
                }
            } else {
                Log.e("MagicalCamera", "La foto no contiene informacion privada");
            }
        }
        try {
            MagicalCameraGuardarFotoEnMemoria(this, magicalCamera.getPhoto(), "photo", MY_IMAGES_PATH, magicalCamera.PNG, true);
        } catch(Exception e){
            e.printStackTrace();
            alert("Error en MagicalCameraGuardarFotoEnMemoria(this, magicalCamera.getPhoto(), \"photo\", MY_IMAGES_PATH, magicalCamera.PNG, true)");
        }
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        /*Toast.makeText(this, "onConfigurationChanged()", Toast.LENGTH_SHORT).show();
        sharedpreferences = getSharedPreferences(LoginActivity.MyPREFERENCES, Context.MODE_PRIVATE);

        login_way = sharedpreferences.getString("enter_mode","EnterModeEnCasoDeNull");

        if (login_way.equals("login")) {
            Menu menu = navigationView.getMenu();
            MenuItem itemVentas = menu.findItem(R.id.nav_ordenar);
            MenuItem itemComprobantes = menu.findItem(R.id.nav_comprobantes);
            MenuItem itemProductos = menu.findItem(R.id.nav_productos);
            MenuItem itemComentarios = menu.findItem(R.id.nav_comentarios);
            MenuItem itemAdministrar = menu.findItem(R.id.nav_administrar);
            MenuItem itemConfiguracion = menu.findItem(R.id.nav_configuracion);
            MenuItem itemSoporte = menu.findItem(R.id.nav_soporte);

            if (itemEmisiones.isChecked()) {
                FragmentManager fragmentManager = getSupportFragmentManager();
                fragmentManager.beginTransaction().replace(R.id.contenedor_nav_drawer, new EmisionesFragment()).commit();
            } else if (itemConsultas.isChecked()) {
                FragmentManager fragmentManager = getSupportFragmentManager();
                fragmentManager.beginTransaction().replace(R.id.contenedor_nav_drawer, new ConsultasFragment()).commit();
            } else if(itemAdministrar.isChecked()) {
                FragmentManager fragmentManager = getSupportFragmentManager();
                fragmentManager.beginTransaction().replace(R.id.contenedor_nav_drawer, new AdministrarFragment()).commit();
            }
        }

*/
        alert("onConfigurationChanged");
    }

    private void check_directory() {
        File validator = new File(MY_IMAGES_PATH);
        if (!validator.exists()) {
            validator.mkdir();
        }
    }

    private void check_permissions() {
        this.magicalPermissions = new MagicalPermissions(this, new String[]{Manifest.permission.CAMERA, Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE});
        Runnable runable = new Runnable() {
            @Override
            public void run() {
                buildMagicalCamera();
            }
        };

        this.magicalPermissions.askPermissions(runable);
    }

    private void buildMagicalCamera() {
        if (magicalCamera == null) {
            int PORCANTAJE_TAMANIO = 100;
            magicalCamera = new MagicalCamera(this, PORCANTAJE_TAMANIO, magicalPermissions);
        }
    }

    private void MagicalCameraGuardarFotoEnMemoria(final Context context, final Bitmap photo, final String prefijo_nom_foto, final String directorio_destino, final Bitmap.CompressFormat format, final boolean increment) {
        if (photo == null)
            return;

        new AsyncTask<Object, Object, File>() {
            private String final_path = "";
            private ProgressDialog pDialog;

            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                pDialog = new ProgressDialog(context);
                pDialog.setTitle(R.string.app_name);
                pDialog.setMessage(getResources().getString(R.string.guardando_imagen));
                pDialog.setIndeterminate(true);
                pDialog.setCancelable(false);
                pDialog.show();
            }

            @Override
            protected File doInBackground(Object... params) {
                final_path = magicalCamera.savePhotoInMemoryDevice(photo, prefijo_nom_foto, directorio_destino, format, increment);

                File f = null;
                if (final_path != null) {
                    f = new File(final_path);
                }

                return f;
            }

            @Override
            protected void onPostExecute(File mFile) {
                super.onPostExecute(mFile);
                pDialog.dismiss();
                if (mFile.exists()) {
                    getImagenComprimida(context, mFile, MY_IMAGES_PATH, false);
                }
            }
        }.execute();
    }

    private void getImagenComprimida(final Context context, final File imagen_actual, final String destino_path, final boolean eliminar_original) {
        if (!imagen_actual.exists()){
            Log.i("IMAGEN NO EXISTE===", "IMAGEN NO EXISTE");
            return;
        }

        final File[] imagen_comprimida = new File[1];

        new AsyncTask<Void, Void, Void>() {

            private ProgressDialog pDialog;
            private File imagen_comprimida_temp;

            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                pDialog = new ProgressDialog(context);
                pDialog.setTitle(R.string.app_name);
                pDialog.setMessage(getResources().getString(R.string.redimensionando_imagen));
                pDialog.setIndeterminate(true);
                pDialog.setCancelable(false);
                pDialog.show();
            }

            @Override
            protected Void doInBackground(Void... params) {
                try {
                    int ancho_actual = BitmapFactory.decodeFile(imagen_actual.getAbsolutePath()).getWidth();
                    int alto_actual = BitmapFactory.decodeFile(imagen_actual.getAbsolutePath()).getHeight();

                    if (ancho_actual > alto_actual) {
                        imagen_comprimida_temp = new Compressor(context)
                                .setMaxWidth(800)
                                .setQuality(85)
                                .setCompressFormat(Bitmap.CompressFormat.JPEG)
                                .setDestinationDirectoryPath(destino_path)
                                .compressToFile(imagen_actual, "" + Calendar.getInstance().getTimeInMillis() + ".jpg");
                    } else {
                        imagen_comprimida_temp = new Compressor(context)
                                .setMaxHeight(800)
                                .setQuality(85)
                                .setCompressFormat(Bitmap.CompressFormat.JPEG)
                                .setDestinationDirectoryPath(destino_path)
                                .compressToFile(imagen_actual, "" + Calendar.getInstance().getTimeInMillis() + ".jpg");
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

                if (eliminar_original) {
                    imagen_actual.delete();
                }

                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                super.onPostExecute(aVoid);
                pDialog.dismiss();
                if (imagen_comprimida_temp != null) {
                    imagen_comprimida[0] = imagen_comprimida_temp;
                }
                //MOSTRAR IMAGEN
                imagePath = "";
                imagePath = imagen_comprimida[0].getAbsolutePath();

                Glide.with(context)
                        .load(imagePath)
                        .centerCrop()
                        .into(imageViewProduct);
            }
        }.execute();
    }

    private void llenarCombos() {

        //Combo Categorias ----------------------------------------------------------------------------------------------------------------------------------
        SQLiteDatabase db = conn.getReadableDatabase();

        Categoria categoria = null;
        arrayListValoresCateg = new ArrayList<Categoria>();
        //Select * from
        Cursor cursor = db.rawQuery("SELECT * FROM "+ Utilidades.TABLA_CATEGORIA + " WHERE " + Utilidades.CAMPO_ID_EMISOR_CATEG + "=" + String.valueOf(ProductosActivity.idEmisor), null);
        while(cursor.moveToNext()){
            categoria = new Categoria();
            categoria.setIdEmisor(cursor.getInt(0));
            categoria.setCodCategoria(cursor.getString(1));
            categoria.setDescripcion(cursor.getString(2));
            categoria.setAbreviatura(cursor.getString(3));
            categoria.setActivo(cursor.getInt(4));
            categoria.setUsuarioCreacion(cursor.getString(5));
            categoria.setFechaCreacion(cursor.getString(6));
            categoria.setUsuarioEdicion(cursor.getString(7));
            categoria.setFechaEdicion(cursor.getString(8));

            arrayListValoresCateg.add(categoria);
        }

        arrayListMostrarCateg = new ArrayList<String>();
        arrayListMostrarCateg.add("Seleccione");

        for (int i=0; i<arrayListValoresCateg.size(); i++){
            arrayListMostrarCateg.add(arrayListValoresCateg.get(i).getDescripcion());
        }

        ArrayAdapter<CharSequence> adapter = new ArrayAdapter(AgregarProductoActivity.this, android.R.layout.simple_spinner_item, arrayListMostrarCateg);
        spinnerCateg.setAdapter(adapter);

        //Combo Monedas----------------------------------------------------------------------------------------------------------------------------------
        arrayListMostrarMonedas = new ArrayList<String>();
        arrayListMostrarMonedas.add("Seleccione");
        arrayListMostrarMonedas.add("SOLES");
        arrayListMostrarMonedas.add("DOLAR AMERICANO");
        ArrayAdapter<CharSequence> adapter2 = new ArrayAdapter(AgregarProductoActivity.this, android.R.layout.simple_spinner_item, arrayListMostrarMonedas);
        spinnerMoneda.setAdapter(adapter2);

    }

    private void launchPermissionsSettings() {
        Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS,Uri.parse("package:" + AgregarProductoActivity.this.getPackageName()));
        intent.addCategory(Intent.CATEGORY_DEFAULT);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
    }

    private void eventosClick() {

        btnSelectImagen.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try{
                    magicalCamera.selectedPicture("Seleccione");
                } catch (Exception e){
                    e.printStackTrace();
                    alert("Error al seleccionar imagen");
                }
            }
        });

        btnTomarFoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
            /*
                try{
                    magicalCamera.takePhoto();
                } catch (Exception e){
                    e.printStackTrace();
                    alert("Error al tomar la foto");
                }
            */
            }
        });

        btnGuardarProd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                nroPlatoCarta = Integer.parseInt(editTextNroPlatoCarta.getText().toString());
                SQLiteDatabase db100 = conn.getReadableDatabase();
                Cursor cursor100 = db100.rawQuery("SELECT * FROM " + Utilidades.TABLA_PRODUCTO + " WHERE " + Utilidades.CAMPO_NRO_PLATO_CARTA_PROD + "=" + String.valueOf(nroPlatoCarta) + " AND " + Utilidades.CAMPO_ID_EMISOR_PROD + "=" + String.valueOf(ProductosActivity.idEmisor) + " AND " + Utilidades.CAMPO_ACTIVO_PROD + "=1",null);
                cantProdConEseNroPlato = cursor100.getCount();   //Cantida de productos que tienen ese nroPlatoCarta - debe devolver 1 o 0
                cursor100.close();
                if(cantProdConEseNroPlato == 1){
                    alert("Nro de Plato en uso, utilice un Nro Plato diferente!");
                } else {

                    descProd = editTextDescProd.getText().toString();
                    valorUnitProd = Float.parseFloat(editTextValorUnitProd.getText().toString());
                    int positionCateg = spinnerCateg.getSelectedItemPosition();
                    codCateg = arrayListValoresCateg.get(positionCateg-1).getCodCategoria();
                    codProd = arrayListValoresCateg.get(positionCateg-1).getDescripcion().toLowerCase()+String.valueOf(nroPlatoCarta);
                    int positionMoneda = spinnerMoneda.getSelectedItemPosition();
                    if(positionMoneda == 1){
                        tipoMoneda="PEN";
                    } else if (positionMoneda == 2){
                        tipoMoneda="USD";
                    }

                    //Obtenemos datetime actual
                    Date date = new Date();
                    //DateFormat hourdateFormat0 = new SimpleDateFormat("HH:mm:ss dd/MM/yyyy");
                    DateFormat hourdateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
                    //DateFormat hourdateFormat2 = DateFormat.getDateTimeInstance();
                    String fechaCreac = hourdateFormat.format(date);

                    try {
                        //Registro de producto utilizando INSERT INTO
                        SQLiteDatabase db = conn.getWritableDatabase();
                        String insert = "INSERT INTO "+Utilidades.TABLA_PRODUCTO+" VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
                        SQLiteStatement statement = db.compileStatement(insert);
                        statement.clearBindings();

                        statement.bindLong(1, ProductosActivity.idEmisor);
                        statement.bindString(2,codProd);
                        statement.bindString(3,codCateg);
                        statement.bindLong(4, nroPlatoCarta);
                        statement.bindString(5,descProd);
                        statement.bindString(6,tipoMoneda);
                        statement.bindDouble(7,valorUnitProd);
                        statement.bindLong(8,1);
                        statement.bindString(9,imagePath);
                        statement.bindString(10,ProductosActivity.login_email_usuario);
                        statement.bindString(11,fechaCreac);
                        statement.bindString(12,"");
                        statement.bindString(13,"");
                        statement.bindLong(14,0);

                        statement.executeInsert();
                        alert("Producto creado exitosamente!");

                    } catch (SQLException e) {
                        e.printStackTrace();
                        alert("Error al intentar agregar producto!");
                    }

                    //Limpiamos campos
                    editTextNroPlatoCarta.setText("");
                    editTextDescProd.setText("");
                    editTextValorUnitProd.setText("");
                    spinnerCateg.setSelection(0);
                    spinnerMoneda.setSelection(0);
                    imageViewProduct.setImageResource(R.drawable.producto);
                    finish();

                }

            }
        });

    }

    private void inicializarControles() {
        editTextNroPlatoCarta = (EditText) findViewById(R.id.editTextNroPlatoCarta);
        editTextDescProd = (EditText) findViewById(R.id.editTextDescProd);
        editTextValorUnitProd = (EditText) findViewById(R.id.editTextValorUnitProd);
        spinnerCateg = (Spinner) findViewById(R.id.spinnerCategoria);
        spinnerMoneda = (Spinner) findViewById(R.id.spinnerMoneda);
        imageViewProduct = (ImageView) findViewById(R.id.imageViewImagenProduct);
        btnSelectImagen = (Button) findViewById(R.id.btnSelecImagen);
        btnTomarFoto = (Button) findViewById(R.id.btnTomarFoto);
        btnGuardarProd = (Button) findViewById(R.id.btnGuardarProd);

    }

    private void alert(String s) {
        Toast.makeText(AgregarProductoActivity.this,s,Toast.LENGTH_SHORT).show();
    }

    private boolean validaPermisos() {

        if(Build.VERSION.SDK_INT<Build.VERSION_CODES.M){
            return true;
        }

        if((checkSelfPermission(CAMERA)==PackageManager.PERMISSION_GRANTED)&&(checkSelfPermission(WRITE_EXTERNAL_STORAGE)==PackageManager.PERMISSION_GRANTED)){
            return true;
        }

        if((shouldShowRequestPermissionRationale(CAMERA)) || (shouldShowRequestPermissionRationale(WRITE_EXTERNAL_STORAGE))){
            //cargarDialogoRecomendacion();
        }else{
            requestPermissions(new String[]{WRITE_EXTERNAL_STORAGE,CAMERA},100);
        }

        return false;
    }


}
