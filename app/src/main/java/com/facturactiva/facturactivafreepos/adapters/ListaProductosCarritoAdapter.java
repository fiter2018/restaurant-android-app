package com.facturactiva.facturactivafreepos.adapters;

import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.facturactiva.facturactivafreepos.R;
import com.facturactiva.facturactivafreepos.activities.NavDrawerActivity;
import com.facturactiva.facturactivafreepos.activities.TicketActualActivity;
import com.facturactiva.facturactivafreepos.constants.Constants;
import com.facturactiva.facturactivafreepos.models.ProductoCarrito;
import com.facturactiva.facturactivafreepos.sqlite.ConexionSQLiteHelper;
import com.facturactiva.facturactivafreepos.utilities.Utilidades;

import org.w3c.dom.Text;

import java.util.ArrayList;

public class ListaProductosCarritoAdapter extends RecyclerView.Adapter<ListaProductosCarritoAdapter.ViewHolder>{

    private ArrayList<ProductoCarrito> arrayListProductosCarr;
    private ConexionSQLiteHelper conn;
    private Context context;
    Button btnCobrar;
    float montoCobrar;
    int idEmisor;

    public ListaProductosCarritoAdapter() {
    }

    public ListaProductosCarritoAdapter(ArrayList<ProductoCarrito> arrayListProductosCarr, ConexionSQLiteHelper conn, Context context, Button btnCobrar, float montoCobrar, int idEmisor) {
        this.arrayListProductosCarr = arrayListProductosCarr;
        this.conn = conn;
        this.context = context;
        this.btnCobrar = btnCobrar;
        this.montoCobrar = montoCobrar;
        this.idEmisor = idEmisor;
    }

    @Override
    public ListaProductosCarritoAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolder(
                LayoutInflater
                        .from(parent.getContext())
                        .inflate(R.layout.item_list_prod_carrito, parent, false));
    }

    @Override
    public void onBindViewHolder(ListaProductosCarritoAdapter.ViewHolder holder, int position) {

        holder.textViewNombProdCarr.setText("Nro "+String.valueOf(arrayListProductosCarr.get(position).getNroPlatoCarta())+". "+arrayListProductosCarr.get(position).getDescripcion());
        holder.textViewCantProdCarr.setText(String.valueOf(arrayListProductosCarr.get(position).getCantidad())+" items");
        holder.textViewPrecio.setText("S/. "+String.valueOf(arrayListProductosCarr.get(position).getValorUnitario()));
        holder.textViewSubtotal.setText("S/. "+String.valueOf(arrayListProductosCarr.get(position).getValorUnitario()*arrayListProductosCarr.get(position).getCantidad()));

        final String codProductoCarr = arrayListProductosCarr.get(position).getCodProducto();
        final ProductoCarrito productoCarrito = new ProductoCarrito();
        productoCarrito.setIdEmisor(arrayListProductosCarr.get(position).getIdEmisor());
        productoCarrito.setCodProducto(arrayListProductosCarr.get(position).getCodProducto());
        productoCarrito.setCodCategoria(arrayListProductosCarr.get(position).getCodCategoria());
        productoCarrito.setNroPlatoCarta(arrayListProductosCarr.get(position).getNroPlatoCarta());
        productoCarrito.setDescripcion(arrayListProductosCarr.get(position).getDescripcion());
        productoCarrito.setTipoMoneda(arrayListProductosCarr.get(position).getTipoMoneda());
        productoCarrito.setValorUnitario(arrayListProductosCarr.get(position).getValorUnitario());
        productoCarrito.setActivo(arrayListProductosCarr.get(position).getActivo());
        productoCarrito.setImagePath(arrayListProductosCarr.get(position).getImagePath());
        productoCarrito.setCantidad(arrayListProductosCarr.get(position).getCantidad());

        final int adapterPosition = holder.getAdapterPosition();

        holder.imageViewBorrarProdCarr.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                SQLiteDatabase db = conn.getWritableDatabase();
                int numRowsAffected = db.delete(Utilidades.TABLA_CARRITO,Utilidades.CAMPO_ID_EMISOR_CARR+"="+ String.valueOf(idEmisor) +" AND "+Utilidades.CAMPO_COD_PROD_CARR+"="+"'"+codProductoCarr+"'",null);
                Toast.makeText(context, "rowsAffected: "+String.valueOf(numRowsAffected), Toast.LENGTH_SHORT).show();
                db.close();

                arrayListProductosCarr.remove(adapterPosition);
                notifyItemRemoved(adapterPosition);
                notifyItemRangeChanged(adapterPosition,getItemCount());

                montoCobrar -= (productoCarrito.getValorUnitario()*productoCarrito.getCantidad());
                btnCobrar.setText("Cobrar  S/."+String.valueOf(montoCobrar));
                TicketActualActivity.montoCobrar = montoCobrar;

            }
        });

        holder.imageViewAumentar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SQLiteDatabase db = conn.getWritableDatabase();
                ContentValues cv = new ContentValues();
                cv.put(Utilidades.CAMPO_ID_EMISOR_CARR,productoCarrito.getIdEmisor());
                cv.put(Utilidades.CAMPO_COD_PROD_CARR,productoCarrito.getCodProducto());
                cv.put(Utilidades.CAMPO_COD_CATEG_CARR,productoCarrito.getCodCategoria());
                cv.put(Utilidades.CAMPO_NRO_PLATO_CARTA_CARR,productoCarrito.getNroPlatoCarta());
                cv.put(Utilidades.CAMPO_DESCRIPCION_CARR,productoCarrito.getDescripcion());
                cv.put(Utilidades.CAMPO_TIPO_MONEDA_CARR,productoCarrito.getTipoMoneda());
                cv.put(Utilidades.CAMPO_VALOR_UNIT_CARR,productoCarrito.getValorUnitario());
                cv.put(Utilidades.CAMPO_ACTIVO_CARR,productoCarrito.getActivo());
                cv.put(Utilidades.CAMPO_IMAGEN_CARR,productoCarrito.getImagePath());
                cv.put(Utilidades.CAMPO_CANTIDAD_CARR,productoCarrito.getCantidad()+1);

                int rowsAffected = db.update(Utilidades.TABLA_CARRITO, cv, Utilidades.CAMPO_ID_EMISOR_CARR+"="+ String.valueOf(idEmisor) +" AND "+Utilidades.CAMPO_COD_PROD_CARR+"="+"'"+codProductoCarr+"'", null);
                Toast.makeText(context, "rowsAffected = "+rowsAffected, Toast.LENGTH_SHORT).show();
                db.close();

                arrayListProductosCarr.get(adapterPosition).setCantidad(productoCarrito.getCantidad()+1);
                notifyItemChanged(adapterPosition);
                notifyItemRangeChanged(adapterPosition,1);

                montoCobrar += productoCarrito.getValorUnitario();
                btnCobrar.setText("Cobrar  S/."+String.valueOf(montoCobrar));
                TicketActualActivity.montoCobrar = montoCobrar;

            }
        });

        holder.imageViewDisminuir.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(productoCarrito.getCantidad() > 0){
                    SQLiteDatabase db = conn.getWritableDatabase();
                    ContentValues cv = new ContentValues();
                    cv.put(Utilidades.CAMPO_ID_EMISOR_CARR, productoCarrito.getIdEmisor());
                    cv.put(Utilidades.CAMPO_COD_PROD_CARR, productoCarrito.getCodProducto());
                    cv.put(Utilidades.CAMPO_COD_CATEG_CARR, productoCarrito.getCodCategoria());
                    cv.put(Utilidades.CAMPO_NRO_PLATO_CARTA_CARR,productoCarrito.getNroPlatoCarta());
                    cv.put(Utilidades.CAMPO_DESCRIPCION_CARR, productoCarrito.getDescripcion());
                    cv.put(Utilidades.CAMPO_TIPO_MONEDA_CARR, productoCarrito.getTipoMoneda());
                    cv.put(Utilidades.CAMPO_VALOR_UNIT_CARR, productoCarrito.getValorUnitario());
                    cv.put(Utilidades.CAMPO_ACTIVO_CARR, productoCarrito.getActivo());
                    cv.put(Utilidades.CAMPO_IMAGEN_CARR, productoCarrito.getImagePath());
                    cv.put(Utilidades.CAMPO_CANTIDAD_CARR, productoCarrito.getCantidad() - 1);

                    int rowsAffected = db.update(Utilidades.TABLA_CARRITO, cv, Utilidades.CAMPO_ID_EMISOR_CARR+"="+ String.valueOf(idEmisor) +" AND "+Utilidades.CAMPO_COD_PROD_CARR+"="+"'"+codProductoCarr+"'", null);
                    Toast.makeText(context, "rowsAffected = "+rowsAffected, Toast.LENGTH_SHORT).show();
                    db.close();

                    arrayListProductosCarr.get(adapterPosition).setCantidad(productoCarrito.getCantidad()-1);
                    notifyItemChanged(adapterPosition);
                    notifyItemRangeChanged(adapterPosition,1);

                    montoCobrar -= productoCarrito.getValorUnitario();
                    btnCobrar.setText("Cobrar  S/."+String.valueOf(montoCobrar));
                    TicketActualActivity.montoCobrar = montoCobrar;

                } else {
                    //No se hace nada

                }
            }
        });


    }

    @Override
    public int getItemCount() {
        return arrayListProductosCarr.size();
    }

    //Getters and Setters
    public ArrayList<ProductoCarrito> getArrayListProductosCarr() {
        return arrayListProductosCarr;
    }

    public void setArrayListProductosCarr(ArrayList<ProductoCarrito> arrayListProductosCarr) {
        this.arrayListProductosCarr = arrayListProductosCarr;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        CardView itemListProductoCardView;
        TextView textViewNombProdCarr, textViewCantProdCarr,textViewPrecio, textViewSubtotal;
        ImageView imageViewBorrarProdCarr,imageViewAumentar, imageViewDisminuir;

        public ViewHolder(View itemView) {
            super(itemView);
            itemListProductoCardView = (CardView) itemView.findViewById(R.id.prodCarritoCardView);
            textViewNombProdCarr = (TextView) itemView.findViewById(R.id.textViewNombCarritoProduc);
            textViewCantProdCarr = (TextView) itemView.findViewById(R.id.textViewCantidadCarritoProduct);
            textViewPrecio = (TextView) itemView.findViewById(R.id.textViewPrecioCarritoProduc);
            imageViewBorrarProdCarr = (ImageView) itemView.findViewById(R.id.imageViewEliminarProdCarr);
            textViewSubtotal = (TextView) itemView.findViewById(R.id.textViewSubtotalCarritoProduct);
            imageViewAumentar = (ImageView) itemView.findViewById(R.id.imageViewAumentar);
            imageViewDisminuir = (ImageView) itemView.findViewById(R.id.imageViewDisminuir);
        }

    }

}
