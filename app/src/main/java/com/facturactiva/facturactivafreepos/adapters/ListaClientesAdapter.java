package com.facturactiva.facturactivafreepos.adapters;

import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.facturactiva.facturactivafreepos.R;
import com.facturactiva.facturactivafreepos.models.Cliente;

import java.util.ArrayList;

public class ListaClientesAdapter extends RecyclerView.Adapter<ListaClientesAdapter.ViewHolder>{

    private ArrayList<Cliente> arrayListClientes;

    public ListaClientesAdapter() {
    }

    public ListaClientesAdapter(ArrayList<Cliente> arrayListClientes) {
        this.arrayListClientes = arrayListClientes;
    }

    @Override
    public ListaClientesAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolder(
                LayoutInflater
                        .from(parent.getContext())
                        .inflate(R.layout.item_list_cliente, parent, false));
    }

    @Override
    public void onBindViewHolder(ListaClientesAdapter.ViewHolder holder, int position) {

        if((arrayListClientes.get(position).getTipoPersona()).equals("01")){
            //PERSONA JURÍDICA
            holder.textViewNombCli.setText(arrayListClientes.get(position).getNombre());
            holder.textViewTipoDoc.setText("RUC: ");
            holder.textViewNumDoc.setText(arrayListClientes.get(position).getNumDocPrincipal());
        }

        if((arrayListClientes.get(position).getTipoPersona()).equals("02")){
            //PERSONA NATURAL
            holder.textViewNombCli.setText(arrayListClientes.get(position).getNombre());
            holder.textViewTipoDoc.setText("DNI: ");
            holder.textViewNumDoc.setText(arrayListClientes.get(position).getNumDocPrincipal());

        }

    }

    @Override
    public int getItemCount() {
        return arrayListClientes.size();
    }

    //ACCESORES ------------------------------------------------------------------------------------
    public ArrayList<Cliente> getArrayListClientes() {
        return arrayListClientes;
    }

    public void setArrayListClientes(ArrayList<Cliente> arrayListClientes) {
        this.arrayListClientes = arrayListClientes;
    }
    // ---------------------------------------------------------------------------------------------


    public class ViewHolder extends RecyclerView.ViewHolder {
        CardView cardViewCliente;
        ImageView imageViewCli;
        TextView textViewNombCli, textViewTipoDoc, textViewNumDoc;

        public ViewHolder(View itemView) {
            super(itemView);
            cardViewCliente = (CardView) itemView.findViewById(R.id.clienteCardView);
            imageViewCli = (ImageView) itemView.findViewById(R.id.imageViewClient);
            textViewNombCli = (TextView) itemView.findViewById(R.id.textViewNombreClient);
            textViewTipoDoc = (TextView) itemView.findViewById(R.id.textViewTipoDocCli);
            textViewNumDoc = (TextView) itemView.findViewById(R.id.textViewNroDocCli);
        }
    }
}
