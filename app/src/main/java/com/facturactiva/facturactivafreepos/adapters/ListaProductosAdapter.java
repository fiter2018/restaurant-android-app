package com.facturactiva.facturactivafreepos.adapters;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.ColorDrawable;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.facturactiva.facturactivafreepos.R;
import com.facturactiva.facturactivafreepos.activities.ProductosActivity;
import com.facturactiva.facturactivafreepos.models.Producto;

import java.util.ArrayList;

public class ListaProductosAdapter extends RecyclerView.Adapter<ListaProductosAdapter.ViewHolder>{

    private ArrayList<Producto> arrayListProductos;
    Context context;
    ProductosActivity productosActivity;
    public ArrayList<Producto> selectedProducts = new ArrayList<Producto>();

    public ListaProductosAdapter() {
    }

    public ListaProductosAdapter(ArrayList<Producto> arrayListProductos, Context context) {
        this.arrayListProductos = arrayListProductos;
        this.context = context;
        this.productosActivity = (ProductosActivity) context;
    }

    @Override
    public ListaProductosAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolder(
                LayoutInflater
                        .from(parent.getContext())
                        .inflate(R.layout.item_list_producto, parent, false), productosActivity);
    }

    @Override
    public void onBindViewHolder(ListaProductosAdapter.ViewHolder holder, int position) {

        if(arrayListProductos.get(position).getImagePath().length() == 0) {
            String codProducto = arrayListProductos.get(position).getCodProducto();
            Resources res = context.getResources();
            int resID = res.getIdentifier(codProducto, "drawable", context.getPackageName());
            holder.imageViewProd.setImageResource(resID);
        } else {
            try{
                Glide.with(context)
                        .load(arrayListProductos.get(position).getImagePath())
                        .into(holder.imageViewProd);
            } catch (Exception e){
                e.printStackTrace();
            }
        }

        String nombProd = arrayListProductos.get(position).getDescripcion();
        if(nombProd.length()>25){
            String parteNombProd = nombProd.substring(0,24);
            holder.textViewNombProd.setText(String.valueOf(arrayListProductos.get(position).getNroPlatoCarta())+". "+parteNombProd+" ...");
        } else {
            holder.textViewNombProd.setText(String.valueOf(arrayListProductos.get(position).getNroPlatoCarta())+". "+nombProd);
        }

        holder.textViewPrecioProd.setText("S/. "+String.valueOf(arrayListProductos.get(position).getValorUnitario()));

        String idEmisorCodProductString = String.valueOf(arrayListProductos.get(position).getIdEmisor())+arrayListProductos.get(position).getCodProducto();

        if(selectedProducts.size()>0){
            if (selectedProducts.contains(arrayListProductos.get(position))){
                //if item is selected then,set foreground color of FrameLayout.
                holder.itemListProductoCardView.setBackground(new ColorDrawable(ContextCompat.getColor(context,R.color.red_color)));
                holder.linearInsideProductCardView.setBackground(new ColorDrawable(ContextCompat.getColor(context,R.color.red_color)));
                holder.imageViewCheck.setVisibility(View.VISIBLE);
            } else {
                //else remove selected item color.
                holder.itemListProductoCardView.setBackground(new ColorDrawable(ContextCompat.getColor(context,android.R.color.transparent)));
                holder.linearInsideProductCardView.setBackground(new ColorDrawable(ContextCompat.getColor(context,android.R.color.transparent)));
                holder.imageViewCheck.setVisibility(View.INVISIBLE);
            }
        } else {

            /*
            holder.itemListProductoCardView.setBackgroundResource(R.drawable.custom_ripple_border);
            holder.linearInsideProductCardView.setBackgroundResource(R.drawable.custom_ripple_border);
            */
            holder.itemListProductoCardView.setBackground(new ColorDrawable(ContextCompat.getColor(context,android.R.color.transparent)));
            holder.linearInsideProductCardView.setBackground(new ColorDrawable(ContextCompat.getColor(context,android.R.color.transparent)));
            holder.imageViewCheck.setVisibility(View.INVISIBLE);
        }


        /*
        if(selectedIdEmisorCodProductStrings.size()>0){
            if (selectedIdEmisorCodProductStrings.contains(idEmisorCodProductString)){
                //if item is selected then,set foreground color of FrameLayout.
                holder.itemListProductoCardView.setBackground(new ColorDrawable(ContextCompat.getColor(context,R.color.red_color)));
                holder.linearInsideProductCardView.setBackground(new ColorDrawable(ContextCompat.getColor(context,R.color.red_color)));

            } else {
                //else remove selected item color.
                holder.itemListProductoCardView.setBackground(new ColorDrawable(ContextCompat.getColor(context,android.R.color.transparent)));
                holder.linearInsideProductCardView.setBackground(new ColorDrawable(ContextCompat.getColor(context,android.R.color.transparent)));
            }
        } else {
            holder.itemListProductoCardView.setBackgroundResource(R.drawable.custom_ripple_border);
            holder.linearInsideProductCardView.setBackgroundResource(R.drawable.custom_ripple_border);
        }
        */




    }

    @Override
    public int getItemCount() {
        return arrayListProductos.size();
    }

    //ACCESORES-------------------------------------------------------------------------------------
    public ArrayList<Producto> getArrayListProductos() {
        return arrayListProductos;
    }

    public ListaProductosAdapter setArrayListProductos(ArrayList<Producto> arrayListProductos) {
        this.arrayListProductos = arrayListProductos;
        return this;
    }

    public Producto getItem(int position){
        return arrayListProductos.get(position);
    }

    public void setSelectedProducts(ArrayList<Producto> selectedProducts, int modificationType, int position) {
        this.selectedProducts = selectedProducts;
        if((modificationType == 1) || (modificationType == 2)){ //Select or Unselect one item(increase/decrease)
            notifyItemChanged(position);
            notifyItemRangeChanged(position,1);
        } else if(modificationType == 3) { //Delete all selected items(delete)
            notifyDataSetChanged();
        }
    }

    //----------------------------------------------------------------------------------------------

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener, View.OnLongClickListener {
        CardView itemListProductoCardView;
        LinearLayout linearInsideProductCardView;
        ImageView imageViewProd;
        ImageView imageViewCheck;
        TextView textViewNombProd;
        TextView textViewPrecioProd;
        ProductosActivity productosActivityHolder;

        public ViewHolder(View itemView, ProductosActivity productosActivity) {
            super(itemView);
            itemListProductoCardView = (CardView) itemView.findViewById(R.id.productoCardView);
            linearInsideProductCardView = (LinearLayout) itemView.findViewById(R.id.linearInsideProductCardView);
            imageViewProd = (ImageView) itemView.findViewById(R.id.imageViewProduct);
            imageViewCheck = (ImageView) itemView.findViewById(R.id.imageViewCheckMultiSelect);
            textViewNombProd = (TextView) itemView.findViewById(R.id.textViewNombreProduc);
            textViewPrecioProd = (TextView) itemView.findViewById(R.id.textViewPrecioProduc);
            this.productosActivityHolder = productosActivity;
            linearInsideProductCardView.setOnLongClickListener(this);
            linearInsideProductCardView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            productosActivityHolder.prepareSelection(view, getAdapterPosition());
        }

        @Override
        public boolean onLongClick(View view) {
            return productosActivityHolder.activeContextualActionMode(view, getAdapterPosition());
        }
    }

}







