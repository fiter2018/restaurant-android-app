package com.facturactiva.facturactivafreepos.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.facturactiva.facturactivafreepos.R;
import com.facturactiva.facturactivafreepos.models.DocElectronicoDetalle;

import java.util.ArrayList;

public class ListaProductosVentaRealAdapter extends RecyclerView.Adapter<ListaProductosVentaRealAdapter.ViewHolder>{

    private ArrayList<DocElectronicoDetalle> arrayListDocElectDetalle;
    Context context;

    public ListaProductosVentaRealAdapter() {
    }

    public ListaProductosVentaRealAdapter(ArrayList<DocElectronicoDetalle> arrayListDocElectDetalle, Context context) {
        this.arrayListDocElectDetalle = arrayListDocElectDetalle;
        this.context = context;
    }

    @Override
    public ListaProductosVentaRealAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolder(
                LayoutInflater
                        .from(parent.getContext())
                        .inflate(R.layout.item_list_prod_venta_real, parent, false));
    }

    @Override
    public void onBindViewHolder(ListaProductosVentaRealAdapter.ViewHolder holder, int position) {

        holder.textViewCodProdVentaReal.setText(arrayListDocElectDetalle.get(position).getCodItem());

        String nombreItem = arrayListDocElectDetalle.get(position).getNombreItem();
        if(nombreItem.length()>25){
            String parteNombItem = nombreItem.substring(0,24);
            holder.textViewNombVentaReal.setText(parteNombItem+" ...");
        } else {
            holder.textViewNombVentaReal.setText(nombreItem);
        }

        holder.textViewPrecUnitVentaReal.setText(String.valueOf(arrayListDocElectDetalle.get(position).getPrecioItem()));
        holder.textViewCantVentaReal.setText(String.valueOf(arrayListDocElectDetalle.get(position).getCantidadItem()));
        holder.textViewSubtotalVentaReal.setText(String.valueOf(arrayListDocElectDetalle.get(position).getPrecioItem()*arrayListDocElectDetalle.get(position).getCantidadItem()));

    }

    @Override
    public int getItemCount() {
        return arrayListDocElectDetalle.size();
    }

    public ArrayList<DocElectronicoDetalle> getArrayListDocElectDetalle() {
        return arrayListDocElectDetalle;
    }

    public void setArrayListDocElectDetalle(ArrayList<DocElectronicoDetalle> arrayListDocElectDetalle) {
        this.arrayListDocElectDetalle = arrayListDocElectDetalle;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView textViewCodProdVentaReal,textViewNombVentaReal,textViewPrecUnitVentaReal,textViewCantVentaReal,textViewSubtotalVentaReal,textViewEspacoVentaReal;

        public ViewHolder(View itemView) {
            super(itemView);
            textViewCodProdVentaReal = (TextView) itemView.findViewById(R.id.textViewCodProdVentaReal);
            textViewEspacoVentaReal = (TextView) itemView.findViewById(R.id.textViewEspacoVentaReal);
            textViewNombVentaReal = (TextView) itemView.findViewById(R.id.textViewNombVentaReal);
            textViewPrecUnitVentaReal = (TextView) itemView.findViewById(R.id.textViewPrecUnitVentaReal);
            textViewCantVentaReal = (TextView) itemView.findViewById(R.id.textViewCantVentaReal);
            textViewSubtotalVentaReal = (TextView) itemView.findViewById(R.id.textViewSubtotalVentaReal);

        }
    }


}
