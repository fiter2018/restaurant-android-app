package com.facturactiva.facturactivafreepos.adapters;

import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.content.ContentValues;
import android.content.Context;
import android.content.res.Resources;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteStatement;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.facturactiva.facturactivafreepos.R;
import com.facturactiva.facturactivafreepos.activities.AgregarProductoActivity;
import com.facturactiva.facturactivafreepos.activities.NavDrawerActivity;
import com.facturactiva.facturactivafreepos.activities.ProductosActivity;
import com.facturactiva.facturactivafreepos.models.Producto;
import com.facturactiva.facturactivafreepos.models.ProductoCarrito;
import com.facturactiva.facturactivafreepos.sqlite.ConexionSQLiteHelper;
import com.facturactiva.facturactivafreepos.utilities.Utilidades;

import java.util.ArrayList;

import okhttp3.internal.Util;

public class ListaProductosNavDrawerAdapter extends RecyclerView.Adapter<ListaProductosNavDrawerAdapter.ViewHolder>{

    private ArrayList<Producto> arrayListProductos;
    private TextView textViewCantProd;
    private ConexionSQLiteHelper connection;
    private long animationDuration = 1000; //miliseconds
    private Context context;


    public ListaProductosNavDrawerAdapter() {
    }

    public ListaProductosNavDrawerAdapter(ArrayList<Producto> arrayListProductos, TextView textViewCantProd, Context context, ConexionSQLiteHelper conn) {
        this.arrayListProductos = arrayListProductos;
        this.textViewCantProd = textViewCantProd;
        this.context = context;
        this.connection = conn;
    }


    @Override
    public ListaProductosNavDrawerAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            return new ViewHolder(
            LayoutInflater
            .from(parent.getContext())
            .inflate(R.layout.item_list_nav_drawer, parent, false));
    }

    @Override
    public void onBindViewHolder(ListaProductosNavDrawerAdapter.ViewHolder holder, int position) {

        if(arrayListProductos.get(position).getImagePath().length() == 0) {
            String codProducto = arrayListProductos.get(position).getCodProducto();
            Resources res = context.getResources();
            int resID = res.getIdentifier(codProducto, "drawable", context.getPackageName());
            holder.imageViewProd.setImageResource(resID);
        } else {
            try{
                Glide.with(context)
                        .load(arrayListProductos.get(position).getImagePath())
                        .into(holder.imageViewProd);
            } catch (Exception e){
                e.printStackTrace();
            }
        }

        String nombProd = arrayListProductos.get(position).getDescripcion();
        if(nombProd.length()>25){
            String parteNombProd = nombProd.substring(0,24);
            holder.textViewNombProd.setText(String.valueOf(arrayListProductos.get(position).getNroPlatoCarta())+". "+parteNombProd+" ...");
        } else {
            holder.textViewNombProd.setText(String.valueOf(arrayListProductos.get(position).getNroPlatoCarta())+". "+nombProd);
        }

        holder.textViewPrecioProd.setText("S/. "+String.valueOf(arrayListProductos.get(position).getValorUnitario()));
        final ImageView imageViewVolador2 = holder.imageViewProd;

        final Producto productCarr = new Producto();
        productCarr.setIdEmisor(arrayListProductos.get(position).getIdEmisor());
        productCarr.setCodProducto(arrayListProductos.get(position).getCodProducto());
        productCarr.setCodCategoria(arrayListProductos.get(position).getCodCategoria());
        productCarr.setNroPlatoCarta(arrayListProductos.get(position).getNroPlatoCarta());
        productCarr.setDescripcion(arrayListProductos.get(position).getDescripcion());
        productCarr.setTipoMoneda(arrayListProductos.get(position).getTipoMoneda());
        productCarr.setValorUnitario(arrayListProductos.get(position).getValorUnitario());
        productCarr.setActivo(arrayListProductos.get(position).getActivo());
        productCarr.setImagePath(arrayListProductos.get(position).getImagePath());
        productCarr.setUsuarioCreacion(arrayListProductos.get(position).getUsuarioCreacion());
        productCarr.setFechaCreacion(arrayListProductos.get(position).getFechaCreacion());
        productCarr.setUsuarioEdicion(arrayListProductos.get(position).getUsuarioEdicion());
        productCarr.setFechaEdicion(arrayListProductos.get(position).getFechaEdicion());

        holder.itemListProductoCardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                /*
                imageViewVolador2.setVisibility(View.VISIBLE);
                NavDrawerActivity.moveItem(imageViewVolador2);

                ObjectAnimator animatorX = ObjectAnimator.ofFloat(imageViewVolador2, "x", 200f);
                ObjectAnimator animatorY = ObjectAnimator.ofFloat(imageViewVolador2, "y", 250f);
                ObjectAnimator alphaAnimation = ObjectAnimator.ofFloat(imageViewVolador2, View.ALPHA, 1.0f, 0.0f);
                ObjectAnimator rotateAnimation = ObjectAnimator.ofFloat(imageViewVolador2,"rotation",0f, 250f);
                animatorX.setDuration(animationDuration);  animatorY.setDuration(animationDuration);  alphaAnimation.setDuration(animationDuration);  rotateAnimation.setDuration(animationDuration);
                AnimatorSet animatorSet = new AnimatorSet();
                animatorSet.playTogether(animatorX,animatorY,rotateAnimation,alphaAnimation);
                animatorSet.start();
                */

                int cantidadProd = Integer.parseInt(textViewCantProd.getText().toString());
                cantidadProd++;
                textViewCantProd.setText(String.valueOf(cantidadProd));

                //Revisa si el producto ya se encuentra en carrito
                SQLiteDatabase db1 = connection.getReadableDatabase();
                Cursor cursor = db1.rawQuery("SELECT * FROM "+ Utilidades.TABLA_CARRITO + " WHERE " + Utilidades.CAMPO_ID_EMISOR_CARR + "=" + productCarr.getIdEmisor() + " AND " + Utilidades.CAMPO_COD_PROD_CARR + "=" + "'" +productCarr.getCodProducto()+"'", null);
                boolean encontrado = false;
                if(cursor.getCount()==1){
                    //PRODUCTO SE ENCUENTRA EN CARRITO
                    encontrado = true;
                    ProductoCarrito productoActual = new ProductoCarrito();
                    while(cursor.moveToNext()){
                        productoActual.setIdEmisor(cursor.getInt(0));
                        productoActual.setCodProducto(cursor.getString(1));
                        productoActual.setCodCategoria(cursor.getString(2));
                        productoActual.setNroPlatoCarta(cursor.getInt(3));
                        productoActual.setDescripcion(cursor.getString(4));
                        productoActual.setTipoMoneda(cursor.getString(5));
                        productoActual.setValorUnitario(cursor.getFloat(6));
                        productoActual.setActivo(cursor.getInt(7));
                        productoActual.setImagePath(cursor.getString(8));
                        productoActual.setCantidad(cursor.getInt(9));
                    }

                    //Incremento cantidad en 1(Update) en tabla carrito
                    SQLiteDatabase db2 = connection.getWritableDatabase();
                    ContentValues cv = new ContentValues();
                    cv.put(Utilidades.CAMPO_ID_EMISOR_CARR,productoActual.getIdEmisor());
                    cv.put(Utilidades.CAMPO_COD_PROD_CARR,productoActual.getCodProducto());
                    cv.put(Utilidades.CAMPO_COD_CATEG_CARR,productoActual.getCodCategoria());
                    cv.put(Utilidades.CAMPO_NRO_PLATO_CARTA_CARR,productoActual.getNroPlatoCarta());
                    cv.put(Utilidades.CAMPO_DESCRIPCION_CARR,productoActual.getDescripcion());
                    cv.put(Utilidades.CAMPO_TIPO_MONEDA_CARR,productoActual.getTipoMoneda());
                    cv.put(Utilidades.CAMPO_VALOR_UNIT_CARR,productoActual.getValorUnitario());
                    cv.put(Utilidades.CAMPO_ACTIVO_CARR,productoActual.getActivo());
                    cv.put(Utilidades.CAMPO_IMAGEN_CARR,productoActual.getImagePath());
                    cv.put(Utilidades.CAMPO_CANTIDAD_CARR,productoActual.getCantidad()+1);

                    db2.update(Utilidades.TABLA_CARRITO, cv, Utilidades.CAMPO_ID_EMISOR_CARR+"="+ productoActual.getIdEmisor() +" AND "+Utilidades.CAMPO_COD_PROD_CARR+"="+"'"+productoActual.getCodProducto()+"'", null);
                    db2.close();
                    alert("Producto ya existente, nueva cantidad = "+String.valueOf(productoActual.getCantidad()+1));

                } else if (cursor.getCount()==0){
                    //PRODUCTO NO SE ENCUENTRA EN CARRITO
                    //Insertamos en carrito(Insert) el producto con cantidad 1

                    try {
                        //Registro de producto utilizando INSERT INTO
                        SQLiteDatabase db = connection.getWritableDatabase();
                        String insert = "INSERT INTO "+ Utilidades.TABLA_CARRITO+" VALUES(?,?,?,?,?,?,?,?,?,?)";
                        SQLiteStatement statement = db.compileStatement(insert);
                        statement.clearBindings();

                        statement.bindLong(1,productCarr.getIdEmisor());
                        statement.bindString(2,productCarr.getCodProducto());
                        statement.bindString(3,productCarr.getCodCategoria());
                        statement.bindLong(4,productCarr.getNroPlatoCarta());
                        statement.bindString(5,productCarr.getDescripcion());
                        statement.bindString(6,productCarr.getTipoMoneda());
                        statement.bindDouble(7,productCarr.getValorUnitario());
                        statement.bindLong(8,productCarr.getActivo());
                        statement.bindString(9,productCarr.getImagePath());
                        statement.bindLong(10,1);

                        statement.executeInsert();
                        db.close();

                        alert("Nuevo Producto agregado a carrito ");

                    } catch (SQLException e) {
                        e.printStackTrace();
                    }

                }
                cursor.close();

                db1.close();

            }

        });



    }

    @Override
    public int getItemCount() {
        return arrayListProductos.size();
    }

    private void alert(String s) {
        Toast.makeText(context,s,Toast.LENGTH_SHORT).show();
    }

    //Accesores
    public ArrayList<Producto> getArrayListProductos() {
        return arrayListProductos;
    }

    public ListaProductosNavDrawerAdapter setArrayListProductos(ArrayList<Producto> arrayListProductos) {
        this.arrayListProductos = arrayListProductos;
        return this;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        CardView itemListProductoCardView;
        ImageView imageViewProd,imageViewVolador;
        TextView textViewNombProd;
        TextView textViewPrecioProd;

        public ViewHolder(View itemView) {
            super(itemView);
            itemListProductoCardView = (CardView) itemView.findViewById(R.id.productoCardViewNavDrawer);
            imageViewProd = (ImageView) itemView.findViewById(R.id.imageViewProductNavDrawer);
            imageViewVolador = (ImageView) itemView.findViewById(R.id.imageViewVolador);
            textViewNombProd = (TextView) itemView.findViewById(R.id.textViewNomProdNavDrawer);
            textViewPrecioProd = (TextView) itemView.findViewById(R.id.textViewPrecProdNavDrawer);

        }
    }


}
