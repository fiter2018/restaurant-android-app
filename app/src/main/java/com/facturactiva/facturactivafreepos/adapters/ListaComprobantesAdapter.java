package com.facturactiva.facturactivafreepos.adapters;

import android.content.Context;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.facturactiva.facturactivafreepos.R;
import com.facturactiva.facturactivafreepos.models.DocElectronico;

import java.util.ArrayList;

public class ListaComprobantesAdapter extends RecyclerView.Adapter<ListaComprobantesAdapter.ViewHolder>{

    private ArrayList<DocElectronico> arrayListDocElectronicos;
    Context context;

    public ListaComprobantesAdapter() {
    }

    public ListaComprobantesAdapter(ArrayList<DocElectronico> arrayListDocElectronicos, Context context) {
        this.arrayListDocElectronicos = arrayListDocElectronicos;
        this.context = context;
    }

    @Override
    public ListaComprobantesAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolder(
                LayoutInflater
                        .from(parent.getContext())
                        .inflate(R.layout.item_list_comprobante, parent, false));
    }


    @Override
    public void onBindViewHolder(ListaComprobantesAdapter.ViewHolder holder, int position) {

        String nombreComprobante = arrayListDocElectronicos.get(position).getSerie()+"-"+String.valueOf(arrayListDocElectronicos.get(position).getCorrelativo());
        holder.textViewNombreComprob.setText(nombreComprobante);
        if(arrayListDocElectronicos.get(position).getTipoDocumento().equals("01")){
            holder.textViewTipoComprob.setText("Factura");
        } else if (arrayListDocElectronicos.get(position).getTipoDocumento().equals("03")){
            holder.textViewTipoComprob.setText("Boleta");
        }

        holder.textViewFechaEmis.setText(arrayListDocElectronicos.get(position).getFechaEmision());
        holder.textViewMontoComprob.setText(String.valueOf(arrayListDocElectronicos.get(position).getMntTotal()));

    }

    @Override
    public int getItemCount() {
        return arrayListDocElectronicos.size();
    }


    public ArrayList<DocElectronico> getArrayListDocElectronicos() {
        return arrayListDocElectronicos;
    }

    public void setArrayListDocElectronicos(ArrayList<DocElectronico> arrayListDocElectronicos) {
        this.arrayListDocElectronicos = arrayListDocElectronicos;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        CardView comprobanteCardView;
        ImageView imageViewComprobante,imageViewEstadoComprob;
        TextView textViewNombreComprob,textViewTipoComprob,textViewFechaEmis,textViewMontoComprob,textViewEstadoComprob;


        public ViewHolder(View itemView) {
            super(itemView);
            comprobanteCardView = (CardView) itemView.findViewById(R.id.comprobanteCardView);
            imageViewComprobante = (ImageView) itemView.findViewById(R.id.imageViewComprobante);
            imageViewEstadoComprob = (ImageView) itemView.findViewById(R.id.imageViewEstadoComprob);
            textViewNombreComprob = (TextView) itemView.findViewById(R.id.textViewNombreComprob);
            textViewTipoComprob = (TextView) itemView.findViewById(R.id.textViewTipoComprob);
            textViewFechaEmis = (TextView) itemView.findViewById(R.id.textViewFechaEmis);
            textViewMontoComprob = (TextView) itemView.findViewById(R.id.textViewMontoComprob);
            textViewEstadoComprob = (TextView) itemView.findViewById(R.id.textViewEstadoComprob);

        }

    }

}
