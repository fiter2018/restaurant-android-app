package com.facturactiva.facturactivafreepos.validations;

import android.graphics.Color;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

public class PagosValidations {

  public static boolean validateFormularioPago(EditText editTextMontoRecib, float montoAPagar, Spinner spinnerTipoComprob){

      boolean valid = true;
      String montoRecibidoCadena = editTextMontoRecib.getText().toString();
      float montoRecibido = Float.parseFloat(montoRecibidoCadena);
      if(montoRecibido < montoAPagar){
          editTextMontoRecib.setError("monto incorrecto");
          valid = false;
      }

      if(spinnerTipoComprob.getSelectedItemPosition() == 0){
          TextView errorText = (TextView) spinnerTipoComprob.getSelectedView();
          errorText.setError("");
          errorText.setTextColor(Color.RED);//just to highlight that this is an error
          errorText.setText("seleccione tipo de comprobante");//changes the selected item text to this
          valid = false;
      }

      return valid;
  }


}
