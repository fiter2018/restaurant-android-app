package com.facturactiva.facturactivafreepos.validations;

import android.widget.EditText;

public class ClientesValidations {


    public static boolean validateIngresoCli(EditText et_nombres, EditText et_dni, EditText et_correo, int tipoPers) {

        boolean valid = true;
        String nombres = et_nombres.getText().toString();
        String dni = et_dni.getText().toString();
        String email = et_correo.getText().toString();

        if(nombres.isEmpty()){
            if(tipoPers == 1){
                et_nombres.setError("ingrese nombres del cliente");
            }
            if(tipoPers == 2){
                et_nombres.setError("ingrese razón social del cliente");
            }
            valid = false;
        } else {
            et_nombres.setError(null);
        }

        int longitudNumDoc = 0;
        if(tipoPers == 1){
            longitudNumDoc = 8;
        }
        if(tipoPers == 2){
            longitudNumDoc = 11;
        }

        if(dni.isEmpty() || (dni.length() != longitudNumDoc)){
            if(tipoPers == 1){et_dni.setError("dni incorrecto");}
            if(tipoPers == 2){et_dni.setError("ruc incorrecto");}
            valid = false;
        } else {
            et_dni.setError(null);
        }

        if (email.isEmpty() || !android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
            et_correo.setError("ingrese un email válido");
            valid = false;
        } else {
            et_correo.setError(null);
        }

        return valid;
    }



































}
