package com.facturactiva.facturactivafreepos.validations;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.widget.EditText;
import android.widget.Toast;

import com.facturactiva.facturactivafreepos.sqlite.ConexionSQLiteHelper;
import com.facturactiva.facturactivafreepos.utilities.Utilidades;


public class LoginActivityValidations {

    public static boolean validateLogin(EditText et_email, EditText et_password, ConexionSQLiteHelper conn, Context context) {

        boolean valid = true;
        String email = et_email.getText().toString();
        String password = et_password.getText().toString();

        SQLiteDatabase db = conn.getReadableDatabase();

        if (email.isEmpty() || !android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
            et_email.setError("enter a valid email address");
            valid = false;
        } else {
            et_email.setError(null);
        }

        Cursor cursor = db.rawQuery("SELECT * FROM "+ Utilidades.TABLA_USUARIOS + " WHERE " + Utilidades.CAMPO_EMAIL_USER + "='" + email + "'", null);
        if (cursor.getCount() == 0) {
            Toast.makeText(context, "No existe una cuenta asociada a este email", Toast.LENGTH_SHORT).show();
            valid = false;
        } else {
            Cursor cursor1 = db.rawQuery("SELECT * FROM "+ Utilidades.TABLA_USUARIOS + " WHERE " + Utilidades.CAMPO_EMAIL_USER + "='" + email + "' AND " + Utilidades.CAMPO_PASS_USER + "='" + password + "'", null);
            if (cursor1.getCount() == 0) {
                Toast.makeText(context, "Password incorrecto", Toast.LENGTH_SHORT).show();
                valid = false;
            } else {
                et_email.setError(null);
            }
            cursor1.close();
        }
        cursor.close();
        db.close();

        if (password.isEmpty() || password.length() < 4 || password.length() > 15) {
            et_password.setError("between 4 and 15 alphanumeric characters");
            valid = false;
        } else {
            et_password.setError(null);
        }

        return valid;
    }



}
