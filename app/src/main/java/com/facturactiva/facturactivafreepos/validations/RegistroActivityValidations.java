package com.facturactiva.facturactivafreepos.validations;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;
import android.widget.EditText;
import android.widget.Toast;

import com.facturactiva.facturactivafreepos.sqlite.ConexionSQLiteHelper;
import com.facturactiva.facturactivafreepos.utilities.Utilidades;

public class RegistroActivityValidations {

    public static boolean validateRegistroUser(EditText et_correo, EditText et_pass, EditText et_razon_soc, EditText et_ruc, Context context, ConexionSQLiteHelper conn) {

        boolean validEmail = true;
        boolean validPass = true;
        boolean validRasonSoc = true;
        boolean validRuc = true;
        String razonSoc = et_razon_soc.getText().toString();
        String ruc = et_ruc.getText().toString();
        String email = et_correo.getText().toString();
        String pass = et_pass.getText().toString();

        SQLiteDatabase db = conn.getReadableDatabase();
        Cursor cursor1 = db.rawQuery("SELECT * FROM " + Utilidades.TABLA_USUARIOS + " WHERE " + Utilidades.CAMPO_EMAIL_USER + "='" + email + "'",null);

        if ((email.isEmpty() || !android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches()) && validEmail) {
            et_correo.setError("ingrese un email válido");
            Log.i("ERROR-EMAIL","ERROR EN EL FORMATO DEL EMAIL");
            validEmail = false;
        } else {
            et_correo.setError(null);
        }

        if ((cursor1.getCount() == 1) && validEmail) {
            et_correo.setError("usuario ya registrado anteriormente");
            Toast.makeText(context, "Usuario ya registrado anteriormente", Toast.LENGTH_SHORT).show();
            validEmail = false;
        } else {
            if(validEmail) {
                et_correo.setError(null);
            }
        }
        cursor1.close();

        if((pass.isEmpty() || (pass.length()<8)) && validPass){
            et_pass.setError("longitud mínima de 8 caracteres");
            validPass = false;
        } else {
            if(validPass) {
                et_pass.setError(null);
            }
        }

        Cursor cursor2 = db.rawQuery("SELECT * FROM " + Utilidades.TABLA_EMISOR + " WHERE " + Utilidades.CAMPO_NOMBRE_EMISOR + "='" + razonSoc + "'",null);

        if((razonSoc.isEmpty()) && validRasonSoc){
            et_razon_soc.setError("ingrese razón social de su empresa");
            validRasonSoc = false;
        } else {
            et_razon_soc.setError(null);
        }

        if((cursor2.getCount() == 1) && validRasonSoc){
            et_razon_soc.setError("empresa ya registrada anteriormente");
            Toast.makeText(context, "Empresa ya registrada anteriormente", Toast.LENGTH_SHORT).show();
            validRasonSoc = false;
        } else {
            if(validRasonSoc) {
                et_razon_soc.setError(null);
            }
        }
        cursor2.close();

        Cursor cursor3 = db.rawQuery("SELECT * FROM " + Utilidades.TABLA_EMISOR + " WHERE " + Utilidades.CAMPO_NUM_DOC_EMISOR + "='" + ruc + "'",null);

        int longitudNumDoc = 11;
        if((ruc.isEmpty() || (ruc.length() != longitudNumDoc)) && validRuc){
            et_ruc.setError("ruc incorrecto");
            validRuc = false;
        } else {
            et_ruc.setError(null);
        }

        if((cursor3.getCount() == 1) && validRuc){
            et_ruc.setError("ruc ya registrado anteriormente");
            Toast.makeText(context, "Ruc ya registrado anteriormente", Toast.LENGTH_SHORT).show();
            validRuc = false;
        } else {
            if(validRuc) {
                et_ruc.setError(null);
            }
        }
        cursor3.close();
        db.close();

        return (validEmail && validPass && validRasonSoc && validRuc);
    }

}
