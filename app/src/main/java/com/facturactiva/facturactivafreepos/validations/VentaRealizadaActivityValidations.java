package com.facturactiva.facturactivafreepos.validations;

import android.widget.EditText;

public class VentaRealizadaActivityValidations {

    public static boolean validatePopupCorreoVentaReal(EditText et_email) {

        boolean valid = true;
        String email = et_email.getText().toString();

        if (email.isEmpty() || !android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
            et_email.setError("enter a valid email address");
            valid = false;
        } else {
            et_email.setError(null);
        }

        return valid;
    }

}






